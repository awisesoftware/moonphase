/**
*** \file paneltooltipitem.cpp
*** \brief paneltooltipitem.h implementation.
*** \details Implementation file for paneltooltipitem.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief paneltooltipitem.cpp identifier.
*** \details Identifier for paneltooltipitem.cpp.
*** \endinternal
**/
#define   PANELTOOLTIPITEM_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "paneltooltipitem.h"
#if       defined(DEBUG_PANELTOOLTIPITEM_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_PANELTOOLTIPITEM_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "desktop.h"
#include  "information.h"

#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pInformationTypeKey="InformationType";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pUnitFormatKey="UnitFormat";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pBackgroundColor="BackgroundColor";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pCenterPadding="CenterPadding";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pDataAlignment="DataAlignment";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pFont="Font";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pLabelAlignment="LabelAlignment";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pLeftRightPadding="LeftRightPadding";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pTextColor="TextColor";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pTopBottomPadding="TopBottomPadding";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pUseSystemBackgroundColorFlag="UseSystemBackgroundColor";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pUseSystemFontFlag="UseSystemFont";

/**
*** \brief TODO
*** \details TODO
**/
static char const *fc_pUseSystemTextColorFlag="UseSystemTextColor";


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief TODO
*** \details TODO
*** \param Label TODO
*** \returns TODO
**/
static PANELITEM_C::ALIGNMENT_E Alignment_LabelToEnum(std::string const &Label)
{
  PANELITEM_C::ALIGNMENT_E Alignment;


  DEBUGLOG_Printf2("Alignment_LabelToEnum(%p(%s))",&Label,Label.c_str());
  DEBUGLOG_LogIn();

  if (Label=="Center")
    Alignment=PANELITEM_C::ALIGNMENT_CENTER;
  else if (Label=="Justify")
    Alignment=PANELITEM_C::ALIGNMENT_JUSTIFY;
  else if (Label=="Left")
    Alignment=PANELITEM_C::ALIGNMENT_LEFT;
  else if (Label=="Right")
    Alignment=PANELITEM_C::ALIGNMENT_RIGHT;
  else
    Alignment=PANELITEM_C::ALIGNMENT_LEFT;

  DEBUGLOG_LogOut();
  return(Alignment);
}

/**
*** \brief TODO
*** \details TODO
*** \param Alignment TODO
*** \returns TODO
**/
static std::string Alignment_EnumToLabel(PANELITEM_C::ALIGNMENT_E Alignment)
{
  std::string Label;


  DEBUGLOG_Printf1("Alignment_EnumToLabel(%d)",Alignment);
  DEBUGLOG_LogIn();

  switch(Alignment)
  {
    case PANELITEM_C::ALIGNMENT_CENTER:
      Label="Center";
      break;
    case PANELITEM_C::ALIGNMENT_JUSTIFY:
      Label="Justify";
      break;
    case PANELITEM_C::ALIGNMENT_LEFT:
    default:
      Label="Left";
      break;
    case PANELITEM_C::ALIGNMENT_RIGHT:
      Label="Right";
      break;
  }

  DEBUGLOG_LogOut();
  return(Label);
}

PANELTOOLTIPITEM_C::PANELTOOLTIPITEM_C(void) : DATETIMEOPTIONS_C()
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEM_C::PANELTOOLTIPITEM_C()");
  DEBUGLOG_LogIn();

  m_InformationIndex=-1;
  m_UnitFormatIndex=0;

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEM_C::PANELTOOLTIPITEM_C(PANELTOOLTIPITEM_C const &Item)
    : DATETIMEOPTIONS_C(Item)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::"
      "PANELTOOLTIPITEM_C(PANELTOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  m_InformationIndex=Item.m_InformationIndex;
  m_UnitFormatIndex=Item.m_UnitFormatIndex;

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEM_C::~PANELTOOLTIPITEM_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEM_C::~PANELTOOLTIPITEM_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEM_C const &
    PANELTOOLTIPITEM_C::operator=(PANELTOOLTIPITEM_C const &Item)
{
  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEM_C::operator=(PANELTOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  DATETIMEOPTIONS_C::operator=(Item);
  m_InformationIndex=Item.m_InformationIndex;
  m_UnitFormatIndex=Item.m_UnitFormatIndex;

  DEBUGLOG_LogOut();
  return(*this);
}

bool PANELTOOLTIPITEM_C::operator==(
    PANELTOOLTIPITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEM_C::operator==(PANELTOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=(DATETIMEOPTIONS_C::operator==(Item) &&
      (m_InformationIndex==Item.m_InformationIndex) &&
      (m_UnitFormatIndex==Item.m_UnitFormatIndex));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PANELTOOLTIPITEM_C::operator!=(PANELTOOLTIPITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEM_C::operator!=(PANELTOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=((*this)==Item);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

int PANELTOOLTIPITEM_C::GetInformationIndex(void) const
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEM_C::GetInformationIndex()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_InformationIndex);
}

int PANELTOOLTIPITEM_C::GetUnitFormatIndex(void) const
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEM_C::GetUnitFormatIndex()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UnitFormatIndex);
}

void PANELTOOLTIPITEM_C::Load(PREFERENCESMANAGER_C *pManager)
{
  std::string Label;


  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::Load(%p)",pManager);
  DEBUGLOG_LogIn();

  DATETIMEOPTIONS_C::Load(pManager);
  m_InformationIndex=Information_GetInformationIndex(
      pManager->GetPreference(fc_pInformationTypeKey,std::string("")));
  m_UnitFormatIndex=Information_GetUnitFormatIndex(m_InformationIndex,
      pManager->GetPreference(fc_pUnitFormatKey,std::string("")));

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEM_C::Save(PREFERENCESMANAGER_C *pManager) const
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::Save(%p)",pManager);
  DEBUGLOG_LogIn();

  DATETIMEOPTIONS_C::Save(pManager);
  pManager->SetPreference(fc_pInformationTypeKey,
      Information_GetInformationLabel(m_InformationIndex));
  pManager->SetPreference(fc_pUnitFormatKey,
      Information_GetUnitFormatLabel(m_InformationIndex,m_UnitFormatIndex));

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEM_C::SetInformationIndex(int Index)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::SetInformationIndex(%d)",Index);
  DEBUGLOG_LogIn();

  m_InformationIndex=Index;

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEM_C::SetUnitFormatIndex(int Index)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::SetUnitFormatIndex(%d)",Index);
  DEBUGLOG_LogIn();

  m_UnitFormatIndex=Index;

  DEBUGLOG_LogOut();
  return;
}

PANELITEM_C::PANELITEM_C(void)
{
  DEBUGLOG_Printf0("PANELITEM_C::PANELITEM_C()");
  DEBUGLOG_LogIn();

  m_BackgroundColor=GetDesktopWindowBackgroundColor();
  m_CenterPadding=0;
  m_DataAlignment=ALIGNMENT_LEFT;
  m_Font=GetDesktopWindowFont();
  m_LabelAlignment=ALIGNMENT_LEFT;
  m_LeftRightPadding=0;
  m_TextColor=GetDesktopWindowTextColor();
  m_TopBottomPadding=0;
  m_UseSystemBackgroundColorFlag=true;
  m_UseSystemFontFlag=true;
  m_UseSystemTextColorFlag=true;

  DEBUGLOG_LogOut();
  return;
}

PANELITEM_C::PANELITEM_C(PANELTOOLTIPITEM_C const &Item) : PANELITEM_C()
{
  DEBUGLOG_Printf1(
      "PANELITEM_C::PANELITEM_C(PANELTOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::operator=(Item);

  DEBUGLOG_LogOut();
  return;
}

PANELITEM_C::PANELITEM_C(PANELITEM_C const &Item) : PANELTOOLTIPITEM_C(Item)
{
  DEBUGLOG_Printf1("PANELITEM_C::PANELITEM_C(%p)",&Item);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::operator=(Item);
  m_BackgroundColor=Item.m_BackgroundColor;
  m_CenterPadding=Item.m_CenterPadding;
  m_DataAlignment=Item.m_DataAlignment;
  m_Font=Item.m_Font;
  m_LabelAlignment=Item.m_LabelAlignment;
  m_LeftRightPadding=Item.m_LeftRightPadding;
  m_TextColor=Item.m_TextColor;
  m_TopBottomPadding=Item.m_TopBottomPadding;
  m_UseSystemBackgroundColorFlag=Item.m_UseSystemBackgroundColorFlag;
  m_UseSystemFontFlag=Item.m_UseSystemFontFlag;
  m_UseSystemTextColorFlag=Item.m_UseSystemTextColorFlag;

  DEBUGLOG_LogOut();
  return;
}

PANELITEM_C::~PANELITEM_C(void)
{
  DEBUGLOG_Printf0("PANELITEM_C::~PANELITEM_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELITEM_C const & PANELITEM_C::operator=(PANELITEM_C const &Item)
{
  DEBUGLOG_Printf1("PANELITEM_C::operator=(PANELITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::operator=(Item);
  m_BackgroundColor=Item.m_BackgroundColor;
  m_CenterPadding=Item.m_CenterPadding;
  m_DataAlignment=Item.m_DataAlignment;
  m_Font=Item.m_Font;
  m_LabelAlignment=Item.m_LabelAlignment;
  m_LeftRightPadding=Item.m_LeftRightPadding;
  m_TextColor=Item.m_TextColor;
  m_TopBottomPadding=Item.m_TopBottomPadding;
  m_UseSystemBackgroundColorFlag=Item.m_UseSystemBackgroundColorFlag;
  m_UseSystemFontFlag=Item.m_UseSystemFontFlag;
  m_UseSystemTextColorFlag=Item.m_UseSystemTextColorFlag;

  DEBUGLOG_LogOut();
  return(*this);
}

bool PANELITEM_C::operator==(PANELITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PANELITEM_C::operator==(PANELITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=(PANELTOOLTIPITEM_C::operator==(Item) &&
      (m_CenterPadding==Item.m_CenterPadding) &&
      (m_DataAlignment==Item.m_DataAlignment) &&
      (m_LabelAlignment==Item.m_LabelAlignment) &&
      (m_LeftRightPadding==Item.m_LeftRightPadding) &&
      (m_TopBottomPadding==Item.m_TopBottomPadding) &&
      (m_UseSystemBackgroundColorFlag==Item.m_UseSystemBackgroundColorFlag) &&
      (m_UseSystemFontFlag==Item.m_UseSystemFontFlag) &&
      (m_UseSystemTextColorFlag==Item.m_UseSystemTextColorFlag));
  /* If both have m_UseSystemBackgroundColorFlag cleared,
      compare background colors. */
  if (!m_UseSystemBackgroundColorFlag && !Item.m_UseSystemBackgroundColorFlag)
    EqualFlag=EqualFlag&&(m_BackgroundColor==Item.m_BackgroundColor);
  /* If both have m_UseSystemFontFlag cleared, compare fonts. */
  if (!m_UseSystemFontFlag && !Item.m_UseSystemFontFlag)
    EqualFlag=EqualFlag&&(m_Font==Item.m_Font);
  /* If both have m_UseSystemBackgroundColorFlag cleared,
      compare text colors. */
  if (!m_UseSystemTextColorFlag && !Item.m_UseSystemTextColorFlag)
    EqualFlag=EqualFlag&&(m_TextColor==Item.m_TextColor);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PANELITEM_C::operator!=(PANELITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PANELITEM_C::operator!=(PANELITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=((*this)==Item);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

COLOR_C const PANELITEM_C::GetBackgroundColor(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetBackgroundColor()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_BackgroundColor);
}

int PANELITEM_C::GetCenterPadding(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetCenterPadding()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_CenterPadding);
}

PANELITEM_C::ALIGNMENT_E PANELITEM_C::GetDataAlignment(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetDataAlignment()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_DataAlignment);
}

FONT_C PANELITEM_C::GetFont(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetFont()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Font);
}

PANELITEM_C::ALIGNMENT_E PANELITEM_C::GetLabelAlignment(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetLabelAlignment()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_LabelAlignment);
}

int PANELITEM_C::GetLeftRightPadding(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetLeftRightPadding()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_LeftRightPadding);
}

COLOR_C PANELITEM_C::GetTextColor(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetTextColor()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_TextColor);
}

int PANELITEM_C::GetTopBottomPadding(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetTopBottomPadding()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_TopBottomPadding);
}

bool PANELITEM_C::GetUseSystemBackgroundColorFlag(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetUseSystemBackgroundColorFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UseSystemBackgroundColorFlag);
}

bool PANELITEM_C::GetUseSystemFontFlag(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetUseSystemFontFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UseSystemFontFlag);
}

bool PANELITEM_C::GetUseSystemTextColorFlag(void) const
{
  DEBUGLOG_Printf0("PANELITEM_C::GetUseSystemTextColorFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UseSystemTextColorFlag);
}

void PANELITEM_C::Load(PREFERENCESMANAGER_C *pManager)
{
  DEBUGLOG_Printf1("PANELITEM_C::Load(%p)",pManager);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::Load(pManager);
  m_BackgroundColor=pManager->GetPreference(
      fc_pBackgroundColor,m_BackgroundColor);
  m_CenterPadding=pManager->GetPreference(fc_pCenterPadding,m_CenterPadding);
  m_DataAlignment=Alignment_LabelToEnum(pManager->GetPreference(
      fc_pDataAlignment,std::string("")));
  m_Font=pManager->GetPreference(fc_pFont,m_Font);
  m_LabelAlignment=Alignment_LabelToEnum(pManager->GetPreference(
      fc_pLabelAlignment,std::string("")));
  m_LeftRightPadding=
      pManager->GetPreference(fc_pLeftRightPadding,m_LeftRightPadding);
  m_TextColor=pManager->GetPreference(fc_pTextColor,m_TextColor);
  m_TopBottomPadding=
      pManager->GetPreference(fc_pTopBottomPadding,m_TopBottomPadding);
  m_UseSystemBackgroundColorFlag=pManager->GetPreference(
      fc_pUseSystemBackgroundColorFlag,m_UseSystemBackgroundColorFlag);
  m_UseSystemFontFlag=pManager->GetPreference(
      fc_pUseSystemFontFlag,m_UseSystemFontFlag);
  m_UseSystemTextColorFlag=pManager->GetPreference(
      fc_pUseSystemTextColorFlag,m_UseSystemTextColorFlag);

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::Save(PREFERENCESMANAGER_C *pManager) const
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEM_C::Save(%p)",pManager);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::Save(pManager);
  pManager->SetPreference(fc_pBackgroundColor,m_BackgroundColor);
  pManager->SetPreference(fc_pCenterPadding,m_CenterPadding);
  pManager->SetPreference(
      fc_pDataAlignment,Alignment_EnumToLabel(m_DataAlignment));
  pManager->SetPreference(fc_pFont,m_Font);
  pManager->SetPreference(
      fc_pLabelAlignment,Alignment_EnumToLabel(m_LabelAlignment));
  pManager->SetPreference(fc_pLeftRightPadding,m_LeftRightPadding);
  pManager->SetPreference(fc_pTextColor,m_TextColor);
  pManager->SetPreference(fc_pTopBottomPadding,m_TopBottomPadding);
  pManager->SetPreference(
      fc_pUseSystemBackgroundColorFlag,m_UseSystemBackgroundColorFlag);
  pManager->SetPreference(fc_pUseSystemFontFlag,m_UseSystemFontFlag);
  pManager->SetPreference(fc_pUseSystemTextColorFlag,m_UseSystemTextColorFlag);

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetBackgroundColor(COLOR_C const &Color)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetBackgroundColor(%p)",&Color);
  DEBUGLOG_LogIn();

  m_BackgroundColor=Color;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetCenterPadding(int Padding)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetCenterPadding(%d)",Padding);
  DEBUGLOG_LogIn();

  m_CenterPadding=Padding;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetDataAlignment(ALIGNMENT_E Alignment)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetDataAlignment(%d)",Alignment);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (Alignment<_ALIGNMENT_FIRST) || (Alignment>_ALIGNMENT_LAST) )
  {
    MESSAGELOG_Error("Invalid parameter");
    Alignment=PANELITEM_C::ALIGNMENT_LEFT;
  }

  m_DataAlignment=Alignment;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetFont(FONT_C const &Font)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetFont(%p)",&Font);
  DEBUGLOG_LogIn();

  m_Font=Font;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetLabelAlignment(ALIGNMENT_E Alignment)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetLabelAlignment(%d)",Alignment);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (Alignment<_ALIGNMENT_FIRST) || (Alignment>_ALIGNMENT_LAST) )
  {
    MESSAGELOG_Error("Invalid parameter");
    Alignment=PANELITEM_C::ALIGNMENT_LEFT;
  }

  m_LabelAlignment=Alignment;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetLeftRightPadding(int Padding)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetLeftRightPadding(%d)",Padding);
  DEBUGLOG_LogIn();

  m_LeftRightPadding=Padding;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetTextColor(COLOR_C const &Color)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetTextColor(%p)",&Color);
  DEBUGLOG_LogIn();

  m_TextColor=Color;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetTopBottomPadding(int Padding)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetTopBottomPadding(%d)",Padding);
  DEBUGLOG_LogIn();

  m_TopBottomPadding=Padding;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetUseSystemBackgroundColorFlag(bool Flag)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetUseSystemBackgroundColorFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  m_UseSystemBackgroundColorFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetUseSystemFontFlag(bool Flag)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetUseSystemFontFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  m_UseSystemFontFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PANELITEM_C::SetUseSystemTextColorFlag(bool Flag)
{
  DEBUGLOG_Printf1("PANELITEM_C::SetUseSystemTextColorFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  m_UseSystemTextColorFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEM_C::TOOLTIPITEM_C(void)
{
  DEBUGLOG_Printf0("TOOLTIPITEM_C::TOOLTIPITEM_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEM_C::TOOLTIPITEM_C(PANELTOOLTIPITEM_C const &Item) : TOOLTIPITEM_C()
{
  DEBUGLOG_Printf1("PANELITEM_C::PANELITEM_C(%p)",&Item);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::operator=(Item);

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEM_C::TOOLTIPITEM_C(TOOLTIPITEM_C const &Item) : TOOLTIPITEM_C()
{
  PANELTOOLTIPITEM_C::operator=(Item);

  return;
}


TOOLTIPITEM_C::~TOOLTIPITEM_C(void)
{
  DEBUGLOG_Printf0("TOOLTIPITEM_C::~TOOLTIPITEM_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEM_C const & TOOLTIPITEM_C::operator=(TOOLTIPITEM_C const &Item)
{
  DEBUGLOG_Printf1("TOOLTIPITEM_C::operator=(TOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  PANELTOOLTIPITEM_C::operator=(Item);

  DEBUGLOG_LogOut();
  return(*this);
}

bool TOOLTIPITEM_C::operator==(TOOLTIPITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("TOOLTIPITEM_C::operator==(%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=(PANELTOOLTIPITEM_C::operator==(Item));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool TOOLTIPITEM_C::operator!=(TOOLTIPITEM_C const &Item) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("TOOLTIPITEM_C::operator!=(%p)",&Item);
  DEBUGLOG_LogIn();

  EqualFlag=((*this)==Item);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

PANELTOOLTIPITEMLIST_C::PANELTOOLTIPITEMLIST_C(void) : m_pList(new TYPE_C)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMLIST_C::PANELTOOLTIPITEMLIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::~PANELTOOLTIPITEMLIST_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMLIST_C::~PANELTOOLTIPITEMLIST_C()");
  DEBUGLOG_LogIn();

  Clear();
  if (m_pList->size()>0)
  {
    MESSAGELOG_Error("List size > 0.");
  }
  delete m_pList.release();

  DEBUGLOG_LogOut();
  return;
}

bool PANELTOOLTIPITEMLIST_C::
    operator==(PANELTOOLTIPITEMLIST_C const &List) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
    "PANELTOOLTIPITEMLIST_C::operator!=(PANELTOOLTIPITEMLIST_C const &:%p)",
    &List);
  DEBUGLOG_LogIn();

  EqualFlag=(Size()==List.Size());
  if (EqualFlag)
  {
    for(CONSTITERATOR_C ppIt0=Begin(),ppIt1=List.Begin();
        (ppIt0!=End()) && (ppIt1!=List.End());++ppIt0,++ppIt1)
      if (**ppIt0!=**ppIt1)
      {
        EqualFlag=false;
        break;
      }
  }

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

PANELTOOLTIPITEM_C * PANELTOOLTIPITEMLIST_C::operator[](SIZETYPE_T Index) const
{
  PANELTOOLTIPITEM_C *pReturn;


  DEBUGLOG_Printf1("PANELTOOLTIPITEMLIST_C::operator[](:%p)",Index);
  DEBUGLOG_LogIn();

  if (Index>=m_pList->size())
  {
    MESSAGELOG_Error("Index out of bounds.");
    pReturn=nullptr;
  }
  else
    pReturn=(*m_pList)[Index];

  DEBUGLOG_LogOut();
  return(pReturn);
}

void PANELTOOLTIPITEMLIST_C::Append(PANELTOOLTIPITEM_C const &Item)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMLIST_C::Append(%p)",&Item);
  DEBUGLOG_LogIn();

  m_pList->push_back(NewItem(Item));

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::CONSTITERATOR_C PANELTOOLTIPITEMLIST_C::Begin(void) const
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMLIST_C::Begin()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pList->begin());
}

void PANELTOOLTIPITEMLIST_C::Clear(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMLIST_C::Clear()");
  DEBUGLOG_LogIn();

  /* Clear the list. */
  while(m_pList->size()>0)
    Remove(0);

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::CONSTITERATOR_C PANELTOOLTIPITEMLIST_C::End(void) const
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMLIST_C::End()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pList->end());
}

void PANELTOOLTIPITEMLIST_C::Remove(SIZETYPE_T Index)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMLIST_C::Remove(%d)",Index);
  DEBUGLOG_LogIn();

  if (Index>=m_pList->size())
  {
    MESSAGELOG_Error("Index out of bounds.");
  }
  else
  {
    delete (*m_pList)[Index];
    m_pList->erase(m_pList->begin()+static_cast<long int>(Index));
  }

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::SIZETYPE_T PANELTOOLTIPITEMLIST_C::Size(void) const
{
  DEBUGLOG_Printf0("PANELITEMLIST_C::Size()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pList->size());
}

void PANELTOOLTIPITEMLIST_C::Swap(SIZETYPE_T Index0,SIZETYPE_T Index1)
{
  DEBUGLOG_Printf2("PANELTOOLTIPITEMLIST_C::Swap(%d,%d)",Index0,Index1);
  DEBUGLOG_LogIn();

  if ( (Index0>=m_pList->size()) || (Index1>=m_pList->size()) )
  {
    MESSAGELOG_Error("Index out of bounds.");
  }
  else
    std::swap((*m_pList)[Index0],(*m_pList)[Index1]);

  DEBUGLOG_LogOut();
  return;
}

PANELITEMLIST_C::PANELITEMLIST_C(void)
{
  DEBUGLOG_Printf0("PANELITEMLIST_C::PANELITEMLIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELITEMLIST_C::PANELITEMLIST_C(PANELITEMLIST_C const &List) : PANELITEMLIST_C()
{
  DEBUGLOG_Printf1(
      "PANELITEMLIST_C::PANELITEMLIST_C(PANELITEMLIST_C const:%p)",&List);
  DEBUGLOG_LogIn();

  for(PANELTOOLTIPITEMLIST_C::CONSTITERATOR_C ppIt=List.m_pList->begin();
      ppIt!=List.m_pList->end();++ppIt)
    Append(**ppIt);

  DEBUGLOG_LogOut();
  return;
}

bool PANELITEMLIST_C::operator==(PANELITEMLIST_C const &List) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PANELITEMLIST_C::operator==(PANELITEMLIST_C const:%p)",&List);
  DEBUGLOG_LogIn();

  EqualFlag=(Size()==List.Size());
  if (EqualFlag)
  {
    for(CONSTITERATOR_C ppIt0=Begin(),
        ppIt1=List.Begin();(ppIt0!=End()) && (ppIt1!=List.End());++ppIt0,++ppIt1)
    {
      PANELITEM_C const *pI0;
      PANELITEM_C const *pI1;


      pI0=dynamic_cast<PANELITEM_C const *>(*ppIt0);
      pI1=dynamic_cast<PANELITEM_C const *>(*ppIt1);
      if ( (pI0==nullptr) || (pI1==nullptr) )
      {
        MESSAGELOG_Error("(pI0!=nullptr) || (pI1!=nullptr)");
        EqualFlag=false;
        break;
      }
      else if (*pI0!=*pI1)
      {
        EqualFlag=false;
        break;
      }
    }
  }

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PANELITEMLIST_C::operator!=(PANELITEMLIST_C const &List) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PANELITEMLIST_C::operator!=(PANELITEMLIST_C const:%p)",&List);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==List);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

PANELITEM_C * PANELITEMLIST_C::operator[](SIZETYPE_T Index) const
{
  PANELITEM_C *pItem;


  DEBUGLOG_Printf1("PANELITEMLIST_C::operator[](%d)",Index);
  DEBUGLOG_LogIn();

  pItem=dynamic_cast<PANELITEM_C*>(PANELTOOLTIPITEMLIST_C::operator[](Index));

  DEBUGLOG_LogOut();
  return(pItem);
}

void PANELITEMLIST_C::Append(PANELITEM_C const &Item)
{
  DEBUGLOG_Printf1("PANELITEMLIST_C::Append(PANELITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  m_pList->push_back(new PANELITEM_C(Item));

  DEBUGLOG_LogOut();
  return;
}

void PANELITEMLIST_C::Load(void)
{
  int Count;
  PANELITEM_C Item;


  DEBUGLOG_Printf0("PANELITEMLIST_C::Load()");
  DEBUGLOG_LogIn();

  Count=BeginReadArray("PanelItemList");
  for(int Index=0;Index<Count;Index++)
  {
    SetArrayIndex(Index);
    Item.Load(this);
    Append(Item);
  }
  EndArray();

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::BASETYPE_C
    PANELITEMLIST_C::NewItem(PANELTOOLTIPITEM_C const &Item) const
{
  BASETYPE_C pItem;


  DEBUGLOG_Printf1("PANELITEMLIST_C::NewItem(%p)",&Item);
  DEBUGLOG_LogIn();

  pItem=new PANELITEM_C(Item);

  DEBUGLOG_LogOut();
  return(pItem);
}

void PANELITEMLIST_C::Save(void)
{
  int Index;
  PANELITEM_C const *pPanelItem;


  DEBUGLOG_Printf0("PANELITEMLIST_C::Save()");
  DEBUGLOG_LogIn();

  /* Clear the old list. */
  BeginGroup("PanelItemList");
  PREFERENCESMANAGER_C::Remove("");
  EndGroup();

  Index=0;
  BeginWriteArray("PanelItemList");
  for(PANELTOOLTIPITEM_C *pItem:*m_pList)
  {
    SetArrayIndex(Index);
    pPanelItem=dynamic_cast<PANELITEM_C const *>(pItem);
    if (pPanelItem==nullptr)
    {
      MESSAGELOG_Error("pPanelItem==nullptr");
    }
    else
      pPanelItem->Save(this);
    Index++;
  }
  EndArray();

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEMLIST_C::TOOLTIPITEMLIST_C(void)
{
  DEBUGLOG_Printf0("TOOLTIPITEMLIST_C::TOOLTIPITEMLIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

TOOLTIPITEMLIST_C::TOOLTIPITEMLIST_C(TOOLTIPITEMLIST_C const &List) : TOOLTIPITEMLIST_C()
{
  DEBUGLOG_Printf1("TOOLTIPITEMLIST_C::TOOLTIPITEMLIST_C(%p)",&List);
  DEBUGLOG_LogIn();

  for(PANELTOOLTIPITEMLIST_C::CONSTITERATOR_C ppIt=List.m_pList->begin();
      ppIt!=List.m_pList->end();++ppIt)
    Append(**ppIt);

  DEBUGLOG_LogOut();
  return;
}

bool TOOLTIPITEMLIST_C::operator==(TOOLTIPITEMLIST_C const &List) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "TOOLTIPITEMLIST_C::operator==(TOOLTIPITEMLIST_C const:%p)",&List);
  DEBUGLOG_LogIn();

  EqualFlag=PANELTOOLTIPITEMLIST_C::operator==(
      *dynamic_cast<PANELTOOLTIPITEMLIST_C const *>(&List));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool TOOLTIPITEMLIST_C::operator!=(TOOLTIPITEMLIST_C const &List) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "TOOLTIPITEMLIST_C::operator!=(TOOLTIPITEMLIST_C const:%p)",&List);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==List);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

void TOOLTIPITEMLIST_C::Append(TOOLTIPITEM_C const &Item)
{
  DEBUGLOG_Printf1("TOOLTIPITEMLIST_C::Append(TOOLTIPITEM_C const:%p)",&Item);
  DEBUGLOG_LogIn();

  m_pList->push_back(new TOOLTIPITEM_C(Item));

  DEBUGLOG_LogOut();
  return;
}

void TOOLTIPITEMLIST_C::Load(void)
{
  int Count;
  TOOLTIPITEM_C Item;


  DEBUGLOG_Printf0("TOOLTIPITEMLIST_C::Load()");
  DEBUGLOG_LogIn();

  Count=BeginReadArray("ToolTipItemList");
  for(int Index=0;Index<Count;Index++)
  {
    SetArrayIndex(Index);
    Item.Load(this);
    Append(Item);
  }
  EndArray();

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C::BASETYPE_C
    TOOLTIPITEMLIST_C::NewItem(PANELTOOLTIPITEM_C const &Item) const
{
  BASETYPE_C pItem;


  DEBUGLOG_Printf1("TOOLTIPITEMLIST_C::NewItem(%p)",&Item);
  DEBUGLOG_LogIn();

  pItem=new TOOLTIPITEM_C(Item);

  DEBUGLOG_LogOut();
  return(pItem);
}

void TOOLTIPITEMLIST_C::Save(void)
{
  int Index;
  TOOLTIPITEM_C const *pToolTipItem;


  DEBUGLOG_Printf0("TOOLTIPITEMLIST_C::Save()");
  DEBUGLOG_LogIn();

  /* Clear the old list. */
  BeginGroup("ToolTipItemList");
  PREFERENCESMANAGER_C::Remove("");
  EndGroup();

  Index=0;
  BeginWriteArray("ToolTipItemList");
  for(PANELTOOLTIPITEM_C *pItem:*m_pList)
  {
    SetArrayIndex(Index);
    pToolTipItem=dynamic_cast<TOOLTIPITEM_C const *>(pItem);
    if (pToolTipItem==nullptr)
    {
      MESSAGELOG_Error("pToolTipItem==nullptr");
    }
    else
      pToolTipItem->Save(this);
    Index++;
  }
  EndArray();

  DEBUGLOG_LogOut();
  return;
}


#undef    PANELTOOLTIPITEM_CPP


/**
*** paneltooltipitem.cpp
**/

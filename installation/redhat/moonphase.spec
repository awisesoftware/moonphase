%define MmVersion 0.4

Name:           moonphase
Version:        %{MmVersion}.2
Release:        1%{?dist}

Summary:        No top level summary
License:        GPLv3+
URL:            https://gitlab.com/awisesoftware/%{name}
Source:         https://gitlab.com/awisesoftware/binaries/blob/master/%{name}/files/%{MmVersion}/%{name}-%{version}.tar.gz
BuildRequires:  cmake,libcurl-devel,qt5-devel

Group:          Applications/Engineering

%description
No top level description.

%package qt
Summary:        Qt version of %{name}

%description qt
Displays the phase of the moon in the system tray. Additionally, a window
 can be displayed that shows user configurable data about the moon
 (rise/set times, percent illumination, etc).

%prep
%setup -q
cmake  -DPACKAGE_COMPILE_FLAGS="-g" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_INSTALL_PREFIX="/usr"

%build
make %{?_smp_mflags}

%install
%make_install

%preun qt
pkill -x %{name}-qt || true
exit 0

%files qt
%defattr(-,root,root)
%{_bindir}/%{name}-qt
%{_datadir}/icons/%{name}-qt.png
%{_datadir}/applications/%{name}-qt.desktop
%{_datadir}/%{name}-qt/moon_56frames.png
%license %{_docdir}/%{name}-qt/license.txt
%{_docdir}/%{name}-qt/changelog.txt
%{_mandir}/man1/%{name}-qt.1.gz

%changelog
* Thu Dec 29 2016 Alan Wise <awisesoftware@gmail.com> - 0.4.2-1
- New upstream version.

* Tue Dec 29 2015 Alan Wise <awisesoftware@gmail.com> - 0.4.1-1
- New upstream version.

* Mon Dec 14 2015 Alan Wise <awisesoftware@gmail.com> - 0.4.0-1
- New upstream version.

* Wed Aug 19 2015 Alan Wise <awisesoftware@gmail.com> - 0.3.2-1
- New upstream version.

* Sun Aug 16 2015 Alan Wise <awisesoftware@gmail.com> - 0.3.1-1
- New upstream version.

* Tue Oct 14 2014 Alan Wise <awisesoftware@gmail.com> - 0.2.3-1
- New upstream version.

* Thu Sep 4 2014 Alan Wise <awisesoftware@gmail.com> - 0.2.2-1
- New upstream version.
- Minor spec changes.
- Added desktop entry.

* Thu Jul 31 2014 Alan Wise <awisesoftware@gmail.com> - 0.2.1-1
- New upstream version.

* Thu Jul 31 2014 Alan Wise <awisesoftware@gmail.com> - 0.2.0-1
- Initial version of the package.

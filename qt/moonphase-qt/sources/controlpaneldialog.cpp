/**
*** \file controlpaneldialog.cpp
*** \brief controlpaneldialog.h implementation.
*** \details Implementation file for controlpaneldialog.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief controlpaneldialog.cpp identifier.
*** \details Identifier for controlpaneldialog.cpp.
*** \endinternal
**/
#define   CONTROLPANELDIALOG_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "controlpaneldialog.h"
#include  "ui_controlpaneldialog.h"
#if       defined(DEBUG_CONTROLPANELDIALOG_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_CONTROLPANELDIALOG_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "changelog.h"
#include  "config.h"
#include  "conversions.h"
#include  "drawframe.h"
#include  "licenseagreement.h"
#include  "moonanimation.h"
#include  "moondata.h"
#include  "paneldialog.h"
#include  "trayicon.h"
#include  "updatecheck.h"
#include  "versionstring.h"

#include  <QDate>
#include  <QDir>
#include  <QFileInfo>
#include  <QMessageBox>
#include  <QTimer>


/****
*****
***** DEFINES
*****
****/

/**
*** \brief Icon update rate.
*** \details Time between checks for any icon updates.
**/
#define   UPDATETIMER_RATE        (60*60*1000)  /* 1 hour, in milliseconds. */

/**
*** \brief Information panel update rate.
*** \details Time between checks for information panel updates.
**/
#define   INFORMATIONPANELTIMER_RATE  (1000)    /* 1 second, in milliseconds. */

/**
*** \brief Binary subdirectory.
*** \details Subdirectory containing the executable.
**/
#define   DIRECTORY_BINARY          "/bin"

/**
*** \brief Image subdirectory.
*** \details Subdirectory containing the moonphase images.
**/
#define   DIRECTORY_IMAGES          "/share/" MOONPHASEQT_EXECUTABLENAME

#define   DIRECTORY_IMAGERESOURCES  "/../Resources/Images"

/**
*** \brief Default animation.
*** \details Filename of the default animation.
**/
#define   FILENAME_IMAGE    "moonphase_56.png"


/****
*****
***** DATA TYPES
*****
****/

class CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    **/
    CONTROLPANELDIALOGPRIVATE_C(CONTROLPANELDIALOG_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    CONTROLPANELDIALOGPRIVATE_C(
        CONTROLPANELDIALOGPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~CONTROLPANELDIALOGPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    CONTROLPANELDIALOGPRIVATE_C const & operator=(
        CONTROLPANELDIALOGPRIVATE_C const &Dialog)=delete;

    /**
    *** \brief Checks for changed preferences, might prompt to save/discard.
    *** \details Checks if the preferences have changed, and if desired, prompts
    ***   the user to save/discard them.
    **/
    void CheckSavePreferences(bool QuitFlag);

    /**
    *** \brief Forces moon data recalculation and (possibly) icon update.
    *** \details Forces the application to recalculate the moon data and
    ***   (possibly) updates the icon.
    **/
    void ForceUpdate(void);

    /**
    *** \brief About tab initializer.
    *** \details Initializes the About tab.
    **/
    void InitializeAboutTab(void);

    /**
    *** \brief Recalculates moon data.
    *** \details Recalculates the moon data for a specific time.
    *** \param Time Time to use for calculations.
    **/
    void RecalculateMoonData(time_t Time);

    /**
    *** \brief Shows/hides the control panel.
    *** \details Shows/hides the control panel, may also start or stop the
    ***   animation timer.
    *** \param VisibleFlag 0=Hide control panel,\n!0=Show control panel.
    **/
    void SetVisible(bool VisibleFlag);

    /**
    *** \brief Updates tray icon.
    *** \details Updates the moon phase icon in the system tray.
    **/
    void UpdateTrayIcon(void);

    /**
    *** \brief Close warning issued.
    *** \details Used with the Still Running Reminder - Once Per Session
    ***   preference. Indicates if the reminder has been issued already.
    **/
    bool m_CloseReminderIssued;

    /**
    *** \brief First update flag.
    *** \details If true, the application has yet to download the current
    ***   version file from the internet.
    **/
    bool m_FirstUpdateFlag;

    /**
    *** \brief Information panel and tool tip timer.
    *** \details Timer used to update the information panel and tool tip display.
    **/
    QTimer m_InformationPanelToolTipTimer;

    /**
    *** \brief Moon data.
    *** \details All data pertaining to the moon (phase, etc.).
    **/
    MOONDATA_T m_MoonData;

    /**
    *** \brief Tray icon.
    *** \details The images displayed in the tray icon.
    **/
    MOONANIMATION_T m_MoonTrayImages;

    /**
    *** \brief Next update check.
    *** \details Date to next check for a program update.
    **/
    QDate m_NextUpdateCheck;

    /**
    *** \brief Information panel dialog box.
    *** \details Dialog box for the information panel.
    **/
    QScopedPointer<PANELDIALOG_C> m_pPanelDialog;

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    CONTROLPANELDIALOG_C *m_pPublic;

    /**
    *** \brief Preferences.
    *** \details Preferences read from the configuration file, modified by the
    ***   user, and written to the configuration file.
    **/
    PREFERENCES_C m_Preferences;

    /**
    *** \brief Tray icon.
    *** \details Icon display in the tray.
    **/
    TRAYICON_C *m_pTrayIcon;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_CONTROLPANELDIALOGUI_C> m_pUI;

    /**
    *** \brief Update interval counter.
    *** \details Counter used to determine when to update the moon data and
    ***   (possibly) the tray icon.
    **/
    unsigned int m_UpdateIntervalCounter;

    /**
    *** \brief Update timer.
    *** \details Timer used to update the moon data and (possibly) the tray
    ***   icon.
    **/
    QTimer m_UpdateTimer;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::
    CONTROLPANELDIALOGPRIVATE_C(CONTROLPANELDIALOG_C *pPublic)
    : m_pUI(new Ui::CONTROLPANELDIALOGUI_C)
{
  DEBUGLOG_Printf1("CONTROLPANELDIALOG_C::"
      "CONTROLPANELDIALOGPRIVATE_C::CONTROLPANELDIALOGPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  /* Initialize member variables. */
  m_CloseReminderIssued=false;
  m_FirstUpdateFlag=true;
  if (MoonData_Initialize(&m_MoonData)<0)
    throw(std::runtime_error("MoonData_Initialize()"));
  if (MoonAnimation_Initialize(&m_MoonTrayImages)<0)
    throw(std::runtime_error("MoonAnimation_Initialize()"));
  m_NextUpdateCheck=QDate::currentDate();
  m_pTrayIcon=nullptr;
  m_UpdateIntervalCounter=0;

  m_pPublic=pPublic;

  DEBUGLOG_LogOut();
  return;
}

CONTROLPANELDIALOG_C::
    CONTROLPANELDIALOGPRIVATE_C::~CONTROLPANELDIALOGPRIVATE_C(void)
{
  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::"
      "CONTROLPANELDIALOGPRIVATE_C::CONTROLPANELDIALOGPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::
    CONTROLPANELDIALOGPRIVATE_C::CheckSavePreferences(bool QuitFlag)
{
  int OriginalTab;
  QString Msg;
  bool ConfirmFlag;


  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::"
      "CONTROLPANELDIALOGPRIVATE_C::CheckSavePreferences()");
  DEBUGLOG_LogIn();


  /* Save the currently visible tab. */
  OriginalTab=m_pUI->m_pTabWidget->currentIndex();

  if (QuitFlag)
  {
    Msg=tr("They are about to be discarded. Do you want to save them now?");
    ConfirmFlag=m_Preferences.
        GetApplicationPreferencesPointer()->GetConfirmDiscardFlag();
  }
  else
  {
    Msg=tr("They will continue to be used, but do you want to save them now?");
    ConfirmFlag=m_Preferences.
        GetApplicationPreferencesPointer()->GetUnsavedChangesReminderFlag();
  }

  if (m_pUI->m_pApplicationPreferencesWidget->PreferencesChanged() &&
      ConfirmFlag==true)
  {
    m_pUI->m_pTabWidget->setCurrentIndex(0);
    if (QMessageBox::question(m_pPublic,MOONPHASEQT_DISPLAYNAME,
        tr("The <b>Application</b> preferences have changed. ")+Msg,
        QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes)
      m_pUI->m_pApplicationPreferencesWidget->SavePreferences();
  }

  if (m_pUI->m_pMoonDataWidget->PreferencesChanged() && (ConfirmFlag==true) )
  {
    m_pUI->m_pTabWidget->setCurrentIndex(1);
    if (QMessageBox::question(m_pPublic,MOONPHASEQT_DISPLAYNAME,
        tr("The <b>Moon Information</b> preferences have changed. ")+Msg,
        QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes)
      m_pUI->m_pMoonDataWidget->SavePreferences();
  }

  /* Restore the originally visible tab. */
  m_pUI->m_pTabWidget->setCurrentIndex(OriginalTab);

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::ForceUpdate(void)
{
  DEBUGLOG_Printf0(
      "CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::ForceUpdate()");
  DEBUGLOG_LogIn();

  m_UpdateIntervalCounter=UINT_MAX-1;
  m_pPublic->UpdateTimerTriggeredSlot();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::InitializeAboutTab(void)
{
  QString Information;


  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::"
      "CONTROLPANELDIALOGPRIVATE_C::InitializeAboutTab()");
  DEBUGLOG_LogIn();

  /* Set the data in the various widgets. */
  m_pUI->m_pAboutWidget->SetDisplayName(MOONPHASEQT_DISPLAYNAME);
  m_pUI->m_pAboutWidget->SetExecutableName(MOONPHASEQT_EXECUTABLENAME);
  m_pUI->m_pAboutWidget->SetVersion(MOONPHASEQT_VERSION);
  Information=MOONPHASEQT_DESCRIPTION;
  Information+="<br><br>" MOONPHASEQT_COPYRIGHTNOTICE;
#if   defined(_WIN32) || defined(__APPLE__) // TODO STATIC
  Information+="<br><br>"+
      QString(QObject::tr("Statically linked with Qt (Version "))+
      qVersion()+QString(QObject::tr(")\n"));
#endif    /* defined(_WIN32) || defined(__APPLE__) */

  /* Show moon phase web site. */
  Information+="<br><br><a href=\"" MOONPHASEQT_WEBSITEURL "\">"
      MOONPHASE_DISPLAYNAME " "+QObject::tr("Web Site")+QString("</a>\n");

  m_pUI->m_pAboutWidget->SetInformationText(Information);

#if       !defined(MOONPHASEQT_UPDATECHECKFLAG)
  m_pUI->m_pAboutWidget->SetCheckForUpdateWidgetVisible(false);
#endif    /* !defined(MOONPHASEQT_UPDATECHECKFLAG) */

  m_pUI->m_pAboutWidget->SetLicenseText(f_pLicenseAgreement);

  m_pUI->m_pAboutWidget->SetChangeLogText(f_pChangeLog);

  QObject::connect(
      m_pUI->m_pAboutWidget,SIGNAL(CheckForUpdateButtonClickedSignal()),
      m_pPublic,SLOT(CheckButtonClickedSlot()));

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::
    CONTROLPANELDIALOGPRIVATE_C::RecalculateMoonData(time_t Time)
{
  DEBUGLOG_Printf1("CONTROLPANELDIALOG_C::"
      "CONTROLPANELDIALOGPRIVATE_C::RecalculateMoonData(%u)",time);
  DEBUGLOG_LogIn();

  /* Recalculate the astronomical data. */
  MoonData_Recalculate(&m_MoonData,Time);

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::
    CONTROLPANELDIALOGPRIVATE_C::SetVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1(
      "CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::SetVisible(%u)",
      VisibleFlag);
  DEBUGLOG_LogIn();

  m_pPublic->setVisible(VisibleFlag);
  if (VisibleFlag==true)
  {
    /* Window already visible, bring to front. */
    m_pPublic->raise();
    m_pPublic->activateWindow();
  }

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::UpdateTrayIcon(void)
{
  QPixmap Pixmap;


  DEBUGLOG_Printf0(
      "CONTROLPANELDIALOG_C::CONTROLPANELDIALOGPRIVATE_C::UpdateTrayIcon()");
  DEBUGLOG_LogIn();

  /* Update the icon. */
  Pixmap=DrawFrame(&m_MoonTrayImages,
      (unsigned int)MoonData_GetMoonPhasePercent(&m_MoonData),
      m_Preferences.GetApplicationPreferencesPointer()->
      GetUseOpaqueBackgroundFlag(),
      ToQColor(m_Preferences.GetApplicationPreferencesPointer()->
      GetBackgroundColor()));
  m_pTrayIcon->setIcon(Pixmap);

  DEBUGLOG_LogOut();
  return;
}

CONTROLPANELDIALOG_C::CONTROLPANELDIALOG_C(QWidget *pParent)
    : QDialog(pParent), m_pPrivate(new CONTROLPANELDIALOGPRIVATE_C(this))
{
  DEBUGLOG_Printf1("CONTROLPANELDIALOG_C::CONTROLPANELDIALOG_C(%p)",pParent);
  DEBUGLOG_LogIn();

  /* Set up the user interface. */
  m_pPrivate->m_pUI->setupUi(this);

  /* Set window title. */
  setWindowTitle(windowTitle()+tr(" - ")+tr(MOONPHASEQT_DISPLAYNAME));

  /* Add minimize button. */
  setWindowFlags(Qt::Window);

  /* Read configuration. */
  m_pPrivate->m_Preferences.Load();

  /* Create and set up the tray icon. */
  m_pPrivate->m_pTrayIcon=new TRAYICON_C(this);
  connect(m_pPrivate->m_pTrayIcon,SIGNAL(ShowControlPanelSignal()),
      this,SLOT(ShowControlPanelSlot()));
  connect(m_pPrivate->m_pTrayIcon,SIGNAL(ShowInformationPanelSignal()),
      this,SLOT(ShowInformationPanelSlot()));
  connect(m_pPrivate->m_pTrayIcon,SIGNAL(QuitSignal()),this,SLOT(QuitSlot()));

  /* Set up, and start the update timer. */
  connect(&m_pPrivate->m_UpdateTimer,SIGNAL(timeout()),
      this,SLOT(UpdateTimerTriggeredSlot()));
  m_pPrivate->m_UpdateTimer.start(UPDATETIMER_RATE);

  /* Create the information panel dialog. */
  m_pPrivate->m_pPanelDialog.reset(new PANELDIALOG_C(this));
  InformationPanelToolTipTimerTriggeredSlot();

  /* Set up the information panel/tool tip timer. */
  connect(&m_pPrivate->m_InformationPanelToolTipTimer,SIGNAL(timeout()),
      this,SLOT(InformationPanelToolTipTimerTriggeredSlot()));
  m_pPrivate->m_InformationPanelToolTipTimer.start(INFORMATIONPANELTIMER_RATE);

  /* Initialize the about tab. */
  m_pPrivate->InitializeAboutTab();

  /* The animation pathname hasn't been set. Let's try to find the default. */
  if (m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetAnimationPathname()=="")
  {
    QString BaseDirectory;
    QFileInfo FileInfo;
    bool FoundFlag;


    /* Under Unix, the executable is in a base directory + "/bin".
        Under Windows, the executable is in the base directory.
        Under macOS, the executable is in a base directory + "/MacOS". */
    BaseDirectory=QCoreApplication::applicationDirPath();
    QMessageBox::warning(this,"Message",qPrintable(BaseDirectory));
    qDebug("%s",qPrintable(BaseDirectory));

    /* Strip off any "/bin". */
    if (BaseDirectory.endsWith(DIRECTORY_BINARY,Qt::CaseInsensitive)==true)
      BaseDirectory.chop(static_cast<int>(strlen(DIRECTORY_BINARY)));

    /* Check the base directory. Should work on Windows. */
    FoundFlag=0;
    FileInfo.setFile(BaseDirectory,FILENAME_IMAGE);
    FoundFlag=FileInfo.exists();
    if (FoundFlag==false)
    {
      /* Check the "images" directory. Should work on Unix. */
      FileInfo.setFile(BaseDirectory+QString(DIRECTORY_IMAGES),FILENAME_IMAGE);
      FoundFlag=FileInfo.exists();
    }
    if (FoundFlag==false)
    {
      /* Check the "Resources/Images" directory. Should work on macOS. */
      FileInfo.setFile(BaseDirectory+QString(DIRECTORY_IMAGERESOURCES),FILENAME_IMAGE);
      FoundFlag=FileInfo.exists();
    }

    /* Found something, set it. */
    if (FoundFlag==true)
    {
      m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
          SetAnimationPathname(FileInfo.canonicalFilePath().toStdString());
      TrayIconPreferencesChangedSlot();
    }

    /* Save the defaults to the config file. */
    m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->Save();
  }

  /* Initialize the application and moon data widgets. */
  m_pPrivate->m_pUI->m_pApplicationPreferencesWidget->SetPreferencesPointer(
      m_pPrivate->m_Preferences.GetApplicationPreferencesPointer());
  connect(m_pPrivate->m_pUI->m_pApplicationPreferencesWidget,
      SIGNAL(TrayIconPreferencesChangedSignal()),
      this,SLOT(TrayIconPreferencesChangedSlot()));
  m_pPrivate->m_pUI->m_pMoonDataWidget->SetPreferencesPointer(
      m_pPrivate->m_Preferences.GetMoonDataPreferencesPointer());

  /* Resize the control panel to minimum size. */
  resize(minimumSizeHint());

  /* Any error here is because of an invalid animation file. Show the control
      panel, and display a messsage. */
  if (MoonAnimation_ReadFile(&m_pPrivate->m_MoonTrayImages,
      m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetAnimationPathname().c_str())<0)
  {
    ShowControlPanelSlot();
    QMessageBox::warning(this,tr(MOONPHASEQT_DISPLAYNAME), \
        tr("Unable to read moon animation file. Please select a new file."));
  }
  m_pPrivate->ForceUpdate();

  /* Show the tray icon. */
  m_pPrivate->m_pTrayIcon->show();

  DEBUGLOG_LogOut();
  return;
}

CONTROLPANELDIALOG_C::~CONTROLPANELDIALOG_C(void)
{
  ERRORCODE_T ErrorCode;
#if       !defined(ENABLE_MESSAGELOG)
  Q_UNUSED(ErrorCode);
#endif    /* !defined(ENABLE_MESSAGELOG) */


  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::~CONTROLPANELDIALOG_C()");
  DEBUGLOG_LogIn();

  if (m_pPrivate->m_pTrayIcon==nullptr)
  {
    MESSAGELOG_Warning("m_pTrayIcon==nullptr");
  }
  delete m_pPrivate->m_pTrayIcon;
  ErrorCode=MoonAnimation_Uninitialize(&m_pPrivate->m_MoonTrayImages);
  MESSAGELOG_LogError(ErrorCode);

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::ButtonBoxButtonClickedSlot(QAbstractButton *pButton)
{
  DEBUGLOG_Printf1(
      "CONTROLPANELDIALOG_C::ButtonBoxButtonClickedSlot(%p)",pButton);
  DEBUGLOG_LogIn();

  switch(m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton))
  {
    case QDialogButtonBox::Close:
      close();
      break;

    default:
      MESSAGELOG_Error("Invalid button.");
      break;
  }

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::CheckButtonClickedSlot(void)
{
  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::CheckButtonClickedSlot()");
  DEBUGLOG_LogIn();

#if       defined(MOONPHASEQT_UPDATECHECKFLAG)
  m_pPrivate->m_pUI->m_pAboutWidget->SetCheckForUpdateButtonEnabled(false);
  m_pPrivate->m_pUI->m_pAboutWidget->
      SetUpdateText(tr("Checking for an update..."));

  UpdateCheck(this,SLOT(VersionSlot(QString)));
#endif    /* defined(MOONPHASEQT_UPDATECHECKFLAG) */

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::closeEvent(QCloseEvent *pEvent)
{
  DEBUGLOG_Printf1("CONTROLPANELDIALOG_C::closeEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  /* Check if preferences have been changed.
      If so, ask if user wants to save them, and if yes, save them. */
  m_pPrivate->CheckSavePreferences(false);

  /* DialogVisible && ReminderFlag && (!OnceFlag || (OnceFlag && !First)) */
  if ( (isVisible()==true) &&
      (m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetStillRunningReminderFlag()==true) &&
      ((m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetRemindOncePerSessionFlag()==false) ||
      ((m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetRemindOncePerSessionFlag()==true) &&
      (m_pPrivate->m_CloseReminderIssued==false))) )
  {
#if       !defined(__APPLE__)
    QString Click=tr("right ");
#else     /* !defined(__APPLE__) */
    QString Click=tr("");
#endif    /* !defined(__APPLE__) */


    QMessageBox::information(this,MOONPHASEQT_DISPLAYNAME,
        tr("This program will continue to run in the system tray. To stop it, ")+
        Click+tr("click the system tray icon and select <b>Quit</b>."));
    if (m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
          GetRemindOncePerSessionFlag()==true)
      m_pPrivate->m_CloseReminderIssued=true;
  }
  m_pPrivate->SetVisible(false);
  pEvent->ignore();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::InformationPanelToolTipTimerTriggeredSlot(void)
{
  DEBUGLOG_Printf0(
        "CONTROLPANELDIALOG_C::InformationPanelTimerTriggeredSlot()");
  DEBUGLOG_LogIn();

  /* Update the latitude and longitude. */
  m_pPrivate->m_MoonData.CTransData.Glat=
      m_pPrivate->m_Preferences.GetMoonDataPreferencesPointer()->
      GetOptionsPointer()->GetLatitude();
  m_pPrivate->m_MoonData.CTransData.Glon=
      -m_pPrivate->m_Preferences.GetMoonDataPreferencesPointer()->
      GetOptionsPointer()->GetLongitude();

  /* Update the moon data. */
  m_pPrivate->RecalculateMoonData(time(nullptr));

  /* Update the information panel, and get its visibility, position, and size. */
  *m_pPrivate->m_Preferences.GetMoonDataPreferencesPointer()->
      GetPanelPreferencesPointer()->GetDialogDataPointer()=
      m_pPrivate->m_pPanelDialog->Update(
      m_pPrivate->m_Preferences.GetMoonDataPreferencesPointer()->
      GetPanelPreferencesPointer(),m_pPrivate->m_MoonData);

  m_pPrivate->m_pTrayIcon->Update(m_pPrivate->m_Preferences.
      GetMoonDataPreferencesPointer()->GetToolTipPreferencesPointer(),
      m_pPrivate->m_MoonData);

  DEBUGLOG_LogOut();

  return;
}

void CONTROLPANELDIALOG_C::InstanceMessageSlot(QString const &Message)
{
  DEBUGLOG_Printf2("CONTROLPANELDIALOG_C::InstanceMessageSlot(%p(%s))",
      &Message,qPrintable(Message));
  DEBUGLOG_LogIn();

  if (QString::localeAwareCompare(Message,"Activate")==0)
    ShowControlPanelSlot();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::QuitSlot(void)
{
  bool QuitFlag;


  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::QuitSlot()");
  DEBUGLOG_LogIn();

  /* Need to confirm quit? */
  if (m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
      GetConfirmQuitFlag()==true)
  {
    /* Yes. */
    QuitFlag=false;
    if (QMessageBox::question(this,MOONPHASEQT_DISPLAYNAME,
        tr("Are you sure you want to quit?"),
        QMessageBox::Yes|QMessageBox::No,QMessageBox::No)==QMessageBox::Yes)
      QuitFlag=true;
  }
  else
  {
    /* No. */
    QuitFlag=true;
  }

  /* Check if preferences have been changed.
      If so, ask if user wants to save them, and if yes, save them. */
  m_pPrivate->CheckSavePreferences(true);

  if (QuitFlag==true)
    qApp->quit();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::ShowControlPanelSlot(void)
{
  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::ShowControlPanelSlot()");
  DEBUGLOG_LogIn();

  m_pPrivate->SetVisible(true);

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::ShowInformationPanelSlot(void)
{
  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::ShowInformationPanelSlot()");
  DEBUGLOG_LogIn();

  m_pPrivate->m_pPanelDialog->show();
  m_pPrivate->m_pPanelDialog->raise();
  m_pPrivate->m_pPanelDialog->activateWindow();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::TrayIconPreferencesChangedSlot(void)
{
  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::TrayIconPreferencesChangedSlot()");
  DEBUGLOG_LogIn();

  /* Try to load the file. Don't care if it fails. */
  MoonAnimation_ReadFile(&m_pPrivate->m_MoonTrayImages,
    m_pPrivate->m_Preferences.GetApplicationPreferencesPointer()->
    GetAnimationPathname().c_str());
  m_pPrivate->ForceUpdate();

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::UpdateTimerTriggeredSlot(void)
{
  int FrameCount;
  unsigned Interval;


  DEBUGLOG_Printf0("CONTROLPANELDIALOG_C::UpdateTimerTriggeredSlot()");
  DEBUGLOG_LogIn();

#if       defined(DEBUG)
    {
      time_t TimeSeconds1970;
      struct tm *pTimeInfo;
#define   BUFFER_SIZE   (1024)
      char pPtr[BUFFER_SIZE];


      time(&TimeSeconds1970);
      pTimeInfo=localtime(&TimeSeconds1970);
      strftime(pPtr,BUFFER_SIZE,"UpdateTimerTriggeredSlot(): %H:%M:%S",pTimeInfo);
#undef    BUFFER_SIZE
      MESSAGELOG_Info(pPtr);
    }
#endif    /* defined(DEBUG) */

  /* Check weekly for a program update. */
  if (QDate::currentDate()>=m_pPrivate->m_NextUpdateCheck)
  {
#if       defined(DEBUG)
    {
      time_t TimeSeconds1970;
      struct tm *pTimeInfo;
#define   BUFFER_SIZE   (1024)
      char pPtr[BUFFER_SIZE];


      time(&TimeSeconds1970);
      pTimeInfo=localtime(&TimeSeconds1970);
      strftime(pPtr,BUFFER_SIZE,"Check for update: %H:%M:%S",pTimeInfo);
#undef    BUFFER_SIZE
      MESSAGELOG_Info(pPtr);
    }
#endif    /* defined(DEBUG) */

#if       !defined(DEBUG)
    /* Force a check for an update. */
    CheckButtonClickedSlot();
#endif    /* !defined(DEBUG) */
    m_pPrivate->m_NextUpdateCheck=m_pPrivate->m_NextUpdateCheck.addDays(7);
  }

  /* Check if calculations need to be run, and (possibly) the icon updated. */
  m_pPrivate->m_UpdateIntervalCounter++;
  FrameCount=MoonAnimation_GetFrameCount(&m_pPrivate->m_MoonTrayImages);
  if (FrameCount<0) // Error.
    FrameCount=1;
  Interval=static_cast<unsigned>(29.5/FrameCount*24/2);
      // Days / frames * hours per day * twice as much as necessary
      // = hours per frame
  if (Interval>24)
    Interval=24;              // Update at least once a day.
  if (m_pPrivate->m_UpdateIntervalCounter>=Interval)
  {
#if       defined(DEBUG)
    {
      time_t TimeSeconds1970;
      struct tm *pTimeInfo;
#define   BUFFER_SIZE   (1024)
      char pPtr[BUFFER_SIZE];


      time(&TimeSeconds1970);
      pTimeInfo=localtime(&TimeSeconds1970);
      strftime(pPtr,BUFFER_SIZE,"Update tray: %H:%M:%S",pTimeInfo);
#undef    BUFFER_SIZE
      MESSAGELOG_Info(pPtr);
    }
#endif    /* defined(DEBUG) */

    /* Recalculate the astronomical data. */
    m_pPrivate->RecalculateMoonData(time(nullptr));

    /* Update the tray icon. */
    m_pPrivate->UpdateTrayIcon();

    /* Reset the counter. */
    m_pPrivate->m_UpdateIntervalCounter=0;
  }

  DEBUGLOG_LogOut();
  return;
}

void CONTROLPANELDIALOG_C::VersionSlot(QString const &Version)
{
  VERSIONSTRING_C const Current(MOONPHASEQT_VERSION);
  VERSIONSTRING_C Internet;


  DEBUGLOG_Printf2(
      "CONTROLPANELDIALOG_C::VersionSlot(%p(%s))",&Version,qPrintable(Version));
  DEBUGLOG_LogIn();

  m_pPrivate->m_pUI->m_pAboutWidget->SetCheckForUpdateButtonEnabled(true);

  if (Version.isEmpty())
    m_pPrivate->m_pUI->m_pAboutWidget->SetUpdateText(
        tr("Error accessing the internet."));
  else
  {
    Internet.Set(qPrintable(Version));
    if (Current>Internet)
      m_pPrivate->m_pUI->m_pAboutWidget->SetUpdateText(
          tr("You have a pre-release version."));
    else if (Current==Internet)
      m_pPrivate->m_pUI->m_pAboutWidget->SetUpdateText(
          tr("This program is up to date."));
    else
    {
      m_pPrivate->m_pUI->m_pAboutWidget->SetUpdateText(
          tr("An update is available."));

      if (m_pPrivate->m_FirstUpdateFlag==true)
      {
        QMessageBox UpdateMessageBox(m_pPrivate->m_pPublic);


        UpdateMessageBox.setWindowIcon(QIcon(":/MoonPhaseQtIcon"));
        UpdateMessageBox.setWindowTitle(MOONPHASEQT_DISPLAYNAME);
        UpdateMessageBox.setTextFormat(Qt::RichText); // Makes links clickable
        UpdateMessageBox.setText(tr("An update to this program is available!"
            "<br><br>Visit the <a href='" MOONPHASEQT_WEBSITEURL "'>"
            MOONPHASE_DISPLAYNAME " Web Site "
            "to download the latest version.</a><br>"));
        UpdateMessageBox.exec();
      }
    }
    m_pPrivate->m_FirstUpdateFlag=false;
  }

  DEBUGLOG_LogOut();
}


#undef    CONTROLPANELDIALOG_CPP


/**
*** controlpaneldialog.cpp
**/

/**
*** \file panelpreferencesdialog.cpp
*** \brief panelpreferencesdialog.h implementation.
*** \details Implementation file for panelpreferencesdialog.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief panelpreferencesdialog.cpp identifier.
*** \details Identifier for panelpreferencesdialog.cpp.
*** \endinternal
**/
#define   PANELPREFERENCESDIALOG_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "panelpreferencesdialog.h"
#include  "ui_panelpreferencesdialog.h"
#ifdef    DEBUG_PANELPREFERENCESDIALOG_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_PANELPREFERENCESDIALOG_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "commontypes.h"
#include  "preferences.h"

#include  <QPushButton>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pPublic Pointer to the interface class
    ***   (PANELPREFERENCESDIALOG_C).
    **/
    PANELPREFERENCESDIALOGPRIVATE_C(PANELPREFERENCESDIALOG_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELPREFERENCESDIALOGPRIVATE_C(
        PANELPREFERENCESDIALOGPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELPREFERENCESDIALOGPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELPREFERENCESDIALOGPRIVATE_C const & operator=(
        PANELPREFERENCESDIALOGPRIVATE_C const &Dialog)=delete;

    /**
    *** \brief Read dialog box controls.
    *** \details Reads the controls in the dialog box.
    *** \returns Panel display preferences.
    **/
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C GetPreferences(void) const;

    /**
    *** \brief Initialize/reinitialize the dialog box.
    *** \details Initializes/reinitializes the controls in the dialog box.
    **/
    void UpdateDialog(void);

    /**
    *** \brief Dialog data pointer.
    *** \details Pointer to dialog data.
    **/
    PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C const *m_pDialogData;

    /**
    *** \brief Panel display preferences pointer.
    *** \details Pointer to panel display preferences.
    **/
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C *m_pPreferences;

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    PANELPREFERENCESDIALOG_C *m_pPublic;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_PANELPREFERENCESDIALOGUI_C> m_pUI;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C::
    PANELPREFERENCESDIALOGPRIVATE_C(PANELPREFERENCESDIALOG_C *pPublic)
    : m_pPublic(pPublic), m_pUI(new Ui_PANELPREFERENCESDIALOGUI_C)
{
  DEBUGLOG_Printf1("PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C"
      "PANELPREFERENCESDIALOGPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_pDialogData=nullptr;
  m_pPreferences=nullptr;

  /* Set up the user interface. */
  m_pUI->setupUi(pPublic);

  DEBUGLOG_LogOut();
  return;
}

PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C::
    ~PANELPREFERENCESDIALOGPRIVATE_C(void)
{
  DEBUGLOG_Printf0("PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C::"
      "~PANELPREFERENCESDIALOGPRIVATE_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOG_C(QWidget *pParent)
    : QDialog(pParent), m_pPrivate(new PANELPREFERENCESDIALOGPRIVATE_C(this))
{
  DEBUGLOG_Printf1(
      "PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOG_C(%p)",pParent);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELPREFERENCESDIALOG_C::~PANELPREFERENCESDIALOG_C(void)
{
  DEBUGLOG_Printf0("PANELPREFERENCESDIALOG_C::~PANELPREFERENCESDIALOG_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void PANELPREFERENCESDIALOG_C::
    ButtonBoxButtonClickedSlot(QAbstractButton *pButton)
{
  DEBUGLOG_Printf1(
      "PANELPREFERENCESDIALOG_C::ButtonBoxButtonClickedSlot(%p)",pButton);
  DEBUGLOG_LogIn();

  switch(m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton))
  {
    case QDialogButtonBox::Apply:
    case QDialogButtonBox::Ok:
      /* Save preferences. */
      *m_pPrivate->m_pPreferences=m_pPrivate->GetPreferences();

      /* Close dialog if "Ok" button clicked. */
      if (m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton)==
          QDialogButtonBox::Ok)
        accept();

      /* Reinitialize the dialog box. */
      m_pPrivate->m_pUI->m_pPositionNoChangeRadioButton->setChecked(true);
      m_pPrivate->m_pUI->m_pSizeNoChangeRadioButton->setChecked(true);
      PreferenceChangedSlot();
      break;

    case QDialogButtonBox::Cancel:
      reject();
      break;

    default:
      MESSAGELOG_Error("Invalid dialog box button.");
      break;
  }

  DEBUGLOG_LogOut();
  return;
}

int PANELPREFERENCESDIALOG_C::Execute(
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C *pPreferences,
    PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C const *pDialogData)
{
  int Return;


  DEBUGLOG_Printf2(
      "PANELPREFERENCESDIALOG_C::Execute(%p,%p)",pPreferences,pDialogData);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (pPreferences==nullptr) || (pDialogData==nullptr) )
  {
    MESSAGELOG_Error("pPreferences==nullptr || pDialogData==nullptr.");
    Return=QDialog::Rejected;
  }
  else
  {
    m_pPrivate->m_pPreferences=pPreferences;
    m_pPrivate->m_pDialogData=pDialogData;

    m_pPrivate->UpdateDialog();

    Return=exec();
  }

  DEBUGLOG_LogOut();
  return(Return);
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C
    PANELPREFERENCESDIALOG_C::PANELPREFERENCESDIALOGPRIVATE_C::
    GetPreferences(void) const
{
  PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C Preferences;


  DEBUGLOG_Printf0("PANELDISPLAYPREFERENCESDIALOGPRIVATE_C::GetPreferences()");
  DEBUGLOG_LogIn();

  if (m_pUI->m_pPositionSaveRadioButton->isChecked()==true)
    Preferences.SetPosition(m_pDialogData->GetPosition());
  else if (m_pUI->m_pPositionClearRadioButton->isChecked()==true)
    Preferences.SetPosition(POSITION_C());
  if (m_pUI->m_pSizeSaveRadioButton->isChecked()==true)
    Preferences.SetSize(m_pDialogData->GetSize());
  else if (m_pUI->m_pSizeClearRadioButton->isChecked()==true)
    Preferences.SetSize(SIZE_C());
  Preferences.SetKeepOnScreenFlag(
      m_pUI->m_pKeepOnScreenCheckBox->isChecked());
  Preferences.SetHideCloseButtonFlag(
      m_pUI->m_pHideCloseButtonCheckBox->isChecked());
  Preferences.SetDisplayOnStartUpFlag(
      m_pUI->m_pDisplayOnStartUpCheckBox->isChecked());
  Preferences.SetAlwaysOnTopFlag(
      m_pUI->m_pAlwaysOnTopCheckBox->isChecked());

  DEBUGLOG_LogOut();
  return(Preferences);
}

void PANELPREFERENCESDIALOG_C::PreferenceChangedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELPREFERENCESDIALOG_C::PreferenceChangedSlot()");
  DEBUGLOG_LogIn();

  m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Apply)->setEnabled(
      *m_pPrivate->m_pPreferences!=m_pPrivate->GetPreferences());

  DEBUGLOG_LogOut();
  return;
}

void PANELPREFERENCESDIALOG_C::
    PANELPREFERENCESDIALOGPRIVATE_C::UpdateDialog(void)
{
  DEBUGLOG_Printf0("PANELDISPLAYPREFERENCESDIALOG_C::"
      "PANELPREFERENCESDIALOGPRIVATE_C::UpdateDialog()");
  DEBUGLOG_LogIn();

  m_pUI->m_pPositionGroupBox->setEnabled(m_pDialogData->GetVisibleFlag());
  m_pUI->m_pSizeGroupBox->setEnabled(m_pDialogData->GetVisibleFlag());
  m_pUI->m_pKeepOnScreenCheckBox->setChecked(
      m_pPreferences->GetKeepOnScreenFlag());
  m_pUI->m_pHideCloseButtonCheckBox->setChecked(
      m_pPreferences->GetHideCloseButtonFlag());
  m_pUI->m_pDisplayOnStartUpCheckBox->setChecked(
      m_pPreferences->GetDisplayOnStartUpFlag());
  m_pUI->m_pAlwaysOnTopCheckBox->setChecked(
      m_pPreferences->GetAlwaysOnTopFlag());

  m_pUI->m_pPositionNoChangeRadioButton->setChecked(true);
  m_pUI->m_pSizeNoChangeRadioButton->setChecked(true);

  m_pPublic->PreferenceChangedSlot();

  DEBUGLOG_LogOut();
  return;
}


#undef    PANELPREFERENCESDIALOG_CPP


/**
*** panelpreferencesdialog.cpp
**/

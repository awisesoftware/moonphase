/**
*** \file moondatawidget.h
*** \brief Information panel/tool tip preferences widget class.
*** \details A widget class which allows the user to change preferences for the
***   information panel/tool tip.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MOONDATAWIDGET_H)
/**
*** \brief moondatawidget.h identifier.
*** \details Identifier for moondatawidget.h.
*** \internal
**/
#define   MOONDATAWIDGET_H


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"

#include  <QWidget>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief QAbstract forward declaration.
*** \details Forward declaration of QAbstractButton.
**/
class QAbstractButton;

/**
*** \brief Moon data preferences widget class.
*** \details A widget class which allows the user to change preferences for the
***   information panel/tool tip.
**/
class MOONDATAWIDGET_C : public QWidget
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    MOONDATAWIDGET_C(QWidget *pParent=nullptr);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    MOONDATAWIDGET_C(MOONDATAWIDGET_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~MOONDATAWIDGET_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    MOONDATAWIDGET_C const & operator=(MOONDATAWIDGET_C const &Widget)=delete;

    bool PreferencesChanged(void);

    void SavePreferences(void);

    /**
    *** \brief Set information panel/tool tip preferences.
    *** \details Sets the preferences of the information panel/tool tip.
    *** \param pPreferences Information panel/tool tip preferences pointer.
    **/
    void SetPreferencesPointer(PREFERENCES_C::MOONDATA_C *pPreferences);

  private slots:
    /**
    *** \brief Button box button clicked.
    *** \details A button in the button box was clicked.
    *** \param pButton Button data.
    **/
    void ButtonBoxButtonClickedSlot(QAbstractButton *pButton);

    /**
    *** \brief Preferences changed.
    *** \details The preferences have changed.
    **/
    void PreferencesChangedSlot(void);

  private:
    /**
    *** \brief Moon data widget implementation class.
    *** \details The implementation class for the moon data widget.
    **/
    class MOONDATAWIDGETPRIVATE_C;
    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend class MOONDATAWIDGETPRIVATE_C;
    /**
    *** \brief Implementation pointer (PIMPL).
    *** \details Pointer to implementation object (PIMPL).
    **/
    QScopedPointer<MOONDATAWIDGETPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(MOONDATAWIDGET_H) */


/**
*** moondatawidget.h
**/

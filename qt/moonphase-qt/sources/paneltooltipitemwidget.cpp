/**
*** \file paneltooltipitemwidget.cpp
*** \brief paneltooltipitemwidget.h implementation.
*** \details Implementation file for paneltooltipitemwidget.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief paneltooltipitemwidget.cpp identifier.
*** \details Identifier for paneltooltipitemwidget.cpp.
**/
#define   PANELTOOLTIPITEMWIDGET_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "ui_paneltooltipitemwidget.h"
#include  "paneltooltipitemwidget.h"
#ifdef    DEBUG_PANELTOOLTIPITEMWIDGET_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_PANELTOOLTIPITEMWIDGET_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "information.h"
#include  "panelpreferencesdialog.h"
#include  "paneltooltipitem.h"
#include  "paneltooltipitempreferencesdialog.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PANELTOOLTIPITEMWIDGET_C::PANELTOOLTIPITEMWIDGETPRIVATE_C
{
  public:
    PANELTOOLTIPITEMWIDGETPRIVATE_C(
        PANELTOOLTIPITEMWIDGET_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELTOOLTIPITEMWIDGETPRIVATE_C(
        PANELTOOLTIPITEMWIDGETPRIVATE_C const &Widget)=delete;

    ~PANELTOOLTIPITEMWIDGETPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELTOOLTIPITEMWIDGETPRIVATE_C const & operator=(
        PANELTOOLTIPITEMWIDGETPRIVATE_C const &Thread)=delete;

    /**
    *** \brief Move a display item.
    *** \details Moves a display item up or down in the display list.
    *** \param Direction 1 to move the item up,\n
    ***   -1 to move the item down.
    **/
    void MoveItem(int Direction);

    void PreferencesChanged(void);

    /**
    *** \brief Update information panel tab controls.
    *** \details Enables/disables the add/remove/up/down/preferences buttons in
    ***   the information panel tab.
    **/
    void UpdateControls(void);

    void UpdateDisplayListWidget(int SelectedIndex=-1);

    bool m_DefaultToGMTFlag;

    bool m_DefaultToMetricUnitsFlag;

    PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C *m_pPreferences;

    PANELTOOLTIPITEMWIDGET_C *m_pPublic;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_PANELTOOLTIPITEMWIDGETUI_C> m_pUI;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

static QString BuildLabel(int Index)
{
  QString Label;


  DEBUGLOG_Printf1("BuildLabel(%d)",Index);
  DEBUGLOG_LogIn();

  Label=QString::fromStdString(Information_GetInformationLabel(Index));
  if (Information_GetNeedsCoordinatesFlag(Index)==true)
    Label+=" *";

  DEBUGLOG_LogOut();
  return(Label);
}

PANELTOOLTIPITEMWIDGET_C::PANELTOOLTIPITEMWIDGETPRIVATE_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C(PANELTOOLTIPITEMWIDGET_C *pPublic)
    : m_pUI(new Ui_PANELTOOLTIPITEMWIDGETUI_C)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMWIDGET_C::PANELTOOLTIPITEMWIDGETPRIVATE_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_DefaultToGMTFlag=false;
  m_DefaultToMetricUnitsFlag=true;
  m_pPreferences=nullptr;
  m_pPublic=pPublic;

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMWIDGET_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C::~PANELTOOLTIPITEMWIDGETPRIVATE_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C::~PANELTOOLTIPITEMWIDGETPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C::MoveItem(int Direction)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMWIDGET_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C::MoveItem(%d)",Direction);
  DEBUGLOG_LogIn();

  /* Check the pointer. */
  if (m_pPreferences==nullptr)
  {
    MESSAGELOG_Error("m_pPreferences==nullptr");
  }
  /* Check the direction. */
  else if ( (Direction!=1) && (Direction!=-1) )
    MESSAGELOG_Error("Invalid direction");
  /* Check if more than 1 item selected. */
  else if (m_pUI->m_pDisplayListWidget->selectedItems().count()!=1)
    MESSAGELOG_Error("Multiple items selected");
  /* Check if 0 items selected. */
  else if (m_pUI->m_pDisplayListWidget->selectedItems().count()==0)
    MESSAGELOG_Error("No item selected");
  else
  {
    int Row;


    /* Get the row of the selected item. */
    Row=m_pUI->m_pDisplayListWidget->row(
        m_pUI->m_pDisplayListWidget->selectedItems()[0]);

    /* Don't go below lower bound. */
    if ( (Row==0) && (Direction==-1) )
    {
      MESSAGELOG_Error("Selection < 0");
    }
    /* Don't go beyond upper bound. */
    else if ( (Row==(m_pUI->m_pDisplayListWidget->count()-1)) && (Direction==1) )
    {
      MESSAGELOG_Error("Selection >= list count");
    }
    else if (m_pPreferences->GetItemListPointer()!=nullptr)
    {
      /* Swap the items. */
      m_pPreferences->GetItemListPointer()->Swap(
          static_cast<unsigned>(Row),static_cast<unsigned>(Row+Direction));

      /* Update the display. */
      UpdateDisplayListWidget(Row+Direction);
      UpdateControls();
      PreferencesChanged();
    }
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C::PreferencesChanged(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C::PreferencesChanged()");
  DEBUGLOG_LogIn();

  emit m_pPublic->PreferencesChangedSignal();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C::UpdateControls(void)
{
  bool EnableAddFlag;
  bool EnableRemoveFlag;
  bool EnableUpFlag;
  bool EnableDownFlag;
  bool EnableOptionsFlag;
  QListWidgetItem *pLWItem;


  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C::UpdateControls()");
  DEBUGLOG_LogIn();

  EnableAddFlag=false;
  EnableRemoveFlag=false;
  EnableUpFlag=false;
  EnableDownFlag=false;
  EnableOptionsFlag=false;

  if (m_pUI->m_pDataListWidget->selectedItems().count()!=0)
    EnableAddFlag=true;

  if (m_pUI->m_pDisplayListWidget->selectedItems().count()!=0)
  {
    EnableRemoveFlag=true;

    pLWItem=m_pUI->m_pDisplayListWidget->selectedItems()[0];
    if (pLWItem==nullptr)
      MESSAGELOG_Error("No selected item");
    else
    {
      if (m_pUI->m_pDisplayListWidget->row(pLWItem)!=0)
        EnableUpFlag=true;

      if ((m_pUI->m_pDisplayListWidget->row(pLWItem)+1)!=
            m_pUI->m_pDisplayListWidget->count())
        EnableDownFlag=true;

      if (m_pUI->m_pDisplayListWidget->selectedItems().count()==1)
        EnableOptionsFlag=true;
    }
  }

  m_pUI->m_pAddDisplayItemToolButton->setEnabled(EnableAddFlag);
  m_pUI->m_pRemoveDisplayItemToolButton->setEnabled(EnableRemoveFlag);
  m_pUI->m_pMoveUpDisplayItemToolButton->setEnabled(EnableUpFlag);
  m_pUI->m_pMoveDownDisplayItemToolButton->setEnabled(EnableDownFlag);
  m_pUI->m_pPreferencesDisplayItemToolButton->setEnabled(EnableOptionsFlag);

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::
    PANELTOOLTIPITEMWIDGETPRIVATE_C::UpdateDisplayListWidget(int SelectedIndex)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::"
      "PANELTOOLTIPITEMWIDGETPRIVATE_C::UpdateDisplayListWidget()");
  DEBUGLOG_LogIn();

  if (m_pPreferences==nullptr)
  {
    MESSAGELOG_Error("m_pPreferences==nullptr");
  }
  else
  {
    PANELTOOLTIPITEMLIST_C::CONSTITERATOR_C pItem;


    /* Clear the widget. */
    m_pUI->m_pDisplayListWidget->clear();

    /* Add the current items. */
    for(pItem=m_pPreferences->GetItemListPointer()->Begin();
        pItem!=m_pPreferences->GetItemListPointer()->End();++pItem)
      m_pUI->m_pDisplayListWidget->addItem(
          BuildLabel((*pItem)->GetInformationIndex()));

    /* Set the selected index (if set). */
    if (SelectedIndex>=0)
    {
      QListWidgetItem *pWidgetItem;


      if (SelectedIndex>=m_pUI->m_pDisplayListWidget->count())
        SelectedIndex=m_pUI->m_pDisplayListWidget->count()-1;

      pWidgetItem=m_pUI->m_pDisplayListWidget->item(SelectedIndex);
      if (pWidgetItem==nullptr)
        MESSAGELOG_Error("Invalid selected index");
      else
        m_pUI->m_pDisplayListWidget->setCurrentItem(pWidgetItem);
    }
  }

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMWIDGET_C::PANELTOOLTIPITEMWIDGET_C(QWidget *pParent)
    : QWidget(pParent), m_pPrivate(new PANELTOOLTIPITEMWIDGETPRIVATE_C(this))
{
  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEMWIDGET_C::PANELTOOLTIPITEMWIDGET_C(%p)",pParent);
  DEBUGLOG_LogIn();

  /* Set up the user interface. */
  m_pPrivate->m_pUI->setupUi(this);

  /* Populate the information list widget. */
  for(unsigned Index=0;Index<Information_GetInformationLabels().size();Index++)
    m_pPrivate->m_pUI->m_pDataListWidget->addItem(
        BuildLabel(static_cast<signed>(Index)));

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMWIDGET_C::~PANELTOOLTIPITEMWIDGET_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::~PANELTOOLTIPITEMWIDGET_C()");
  DEBUGLOG_LogIn();

  delete m_pPrivate.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::AddDataItemButtonClickedSlot(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::AddDataItemButtonClickedSlot()");
  DEBUGLOG_LogIn();

  /* Check the pointer. */
  if (m_pPrivate->m_pPreferences==nullptr)
  {
    MESSAGELOG_Error("m_pPrivate->m_pPreferences==nullptr");
  }
  else
  {
    QListWidgetItem *pSelectedItem;


    foreach(pSelectedItem,m_pPrivate->m_pUI->m_pDataListWidget->selectedItems())
    {
      PANELTOOLTIPITEM_C Item;
      int InfoItemIndex;


      InfoItemIndex=m_pPrivate->m_pUI->m_pDataListWidget->row(pSelectedItem);

      /* The information item index is the same as the row index. */
      Item.SetInformationIndex(InfoItemIndex);

      /* Get and set the default unit/format index. */
      Item.SetUnitFormatIndex(Information_GetDefaultUnitFormatIndex(
        InfoItemIndex,m_pPrivate->m_DefaultToMetricUnitsFlag));
      Item.SetUseGMTFlag(m_pPrivate->m_DefaultToGMTFlag);

      /* Add it to the list. */
      m_pPrivate->m_pPreferences->GetItemListPointer()->Append(Item);
    }

    /* Update the display. */
    m_pPrivate->UpdateDisplayListWidget();
    m_pPrivate->UpdateControls();
    m_pPrivate->PreferencesChanged();
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::DataItemSelectionChangedSlot(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMWIDGET_C::DataItemSelectionChangedSlot()");
  DEBUGLOG_LogIn();

  /* Allow only one list widget to have a selected item. */
  if (m_pPrivate->m_pUI->m_pDataListWidget->selectedItems().count()!=0)
    m_pPrivate->m_pUI->m_pDisplayListWidget->clearSelection();

  m_pPrivate->UpdateControls();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::DisplayItemSelectionChangedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::DisplayItemSelectionChangedSlot()");
  DEBUGLOG_LogIn();

  /* Allow only one list widget to have a selected item. */
  if (m_pPrivate->m_pUI->m_pDisplayListWidget->selectedItems().count()!=0)
    m_pPrivate->m_pUI->m_pDataListWidget->clearSelection();

  m_pPrivate->UpdateControls();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::DisplayPreferencesButtonClickedSlot(void)
{
  PANELPREFERENCESDIALOG_C Dialog(this);


  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::DisplayPreferencesButtonClickedSlot()");
  DEBUGLOG_LogIn();

  /* dynamic_cast is okay because there are no tool tip display preferences. */
  Dialog.Execute(dynamic_cast<PREFERENCES_C::MOONDATA_C::PANEL_C*>(
      m_pPrivate->m_pPreferences)->GetDisplayPreferencesPointer(),
      dynamic_cast<PREFERENCES_C::MOONDATA_C::PANEL_C*>(
      m_pPrivate->m_pPreferences)->GetDialogDataPointer());
  m_pPrivate->PreferencesChanged();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::ItemPreferencesButtonClickedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::ItemPreferencesButtonClickedSlot()");
  DEBUGLOG_LogIn();

  if (m_pPrivate->m_pPreferences==nullptr)
  {
    MESSAGELOG_Error("m_pPrivate->m_pPreferences==nullptr.");
  }
  else
  {
    QListWidgetItem *pLWItem;


    pLWItem=m_pPrivate->m_pUI->m_pDisplayListWidget->selectedItems()[0];
    if (pLWItem==nullptr)
    {
      MESSAGELOG_Error("pLWItem==nullptr.");
    }
    else
    {
      int Row;
      PANELTOOLTIPITEMPREFERENCESDIALOG_C Dialog(this);


      Row=m_pPrivate->m_pUI->m_pDisplayListWidget->row(pLWItem);
      Dialog.Execute((*m_pPrivate->m_pPreferences->GetItemListPointer())[
          static_cast<PANELTOOLTIPITEMLIST_C::SIZETYPE_T>(Row)]);
    }
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::MoveDownDisplayItemButtonClickedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::MoveDownDisplayItemButtonClickedSlot()");
  DEBUGLOG_LogIn();

  m_pPrivate->MoveItem(1);

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::MoveUpDisplayItemButtonClickedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::MoveUpDisplayItemButtonClickedSlot()");
  DEBUGLOG_LogIn();

  m_pPrivate->MoveItem(-1);

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::RemoveDisplayItemButtonClickedSlot(void)
{
  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMWIDGET_C::RemoveDisplayItemButtonClickedSlot()");
  DEBUGLOG_LogIn();

  /* Check the pointer. */
  if (m_pPrivate->m_pPreferences==nullptr)
  {
    MESSAGELOG_Error("m_pPrivate->m_pPreferences==nullptr");
  }
  else
  {
    int CurrentIndex;
    QList<QListWidgetItem *> RemoveList;
    QList<QListWidgetItem *>::reverse_iterator ppSelectedItem;


    /* Remove from end to beginning because higher up items (index++) get
        reordered on each removal. */
    RemoveList=m_pPrivate->m_pUI->m_pDisplayListWidget->selectedItems();
    CurrentIndex=m_pPrivate->m_pUI->m_pDisplayListWidget->row(RemoveList[0]);
    for(ppSelectedItem=RemoveList.rbegin();ppSelectedItem!=RemoveList.rend();
        ++ppSelectedItem)
      m_pPrivate->m_pPreferences->GetItemListPointer()->Remove(
          static_cast<PANELTOOLTIPITEMLIST_C::SIZETYPE_T>(
          m_pPrivate->m_pUI->m_pDisplayListWidget->row(*ppSelectedItem)));

    /* Update the display. */
    m_pPrivate->UpdateDisplayListWidget(CurrentIndex);
    m_pPrivate->UpdateControls();
    m_pPrivate->PreferencesChanged();
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::SetDefaultToGMTFlag(bool GMTFlag)
{
  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEMWIDGET_C::SetDefaultToGMTFlag(%u)",GMTFlag);
  DEBUGLOG_LogIn();

  m_pPrivate->m_DefaultToGMTFlag=GMTFlag;

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::SetDefaultToMetricUnitsFlag(bool MetricFlag)
{
  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEMWIDGET_C::SetDefaultToMetricUnitsFlag(%u)",MetricFlag);
  DEBUGLOG_LogIn();

  m_pPrivate->m_DefaultToMetricUnitsFlag=MetricFlag;

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMWIDGET_C::SetPreferencesPointer(
    PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C *pPreferences)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMWIDGET_C::"
      "SetPreferencesPointer(SETTINGS_C::INFORMATION_C::PANELTOOLTIP_C:%p)",
      pPreferences);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pPreferences==nullptr)
  {
    MESSAGELOG_Error("pPreferences==nullptr.");
  }
  else
  {
    /* Save the pointer. */
    m_pPrivate->m_pPreferences=pPreferences;

    /* Update the display. */
    m_pPrivate->UpdateDisplayListWidget();
    m_pPrivate->UpdateControls();
    m_pPrivate->PreferencesChanged();

    /* If tool tip preferences, hide preferences button. */
    if (dynamic_cast<PREFERENCES_C::MOONDATA_C::TOOLTIP_C *>(pPreferences))
    {
      m_pPrivate->m_pUI->m_pDisplayPreferencesButton->setVisible(false);
    }
  }

  DEBUGLOG_LogOut();
  return;
}


#undef    PANELTOOLTIPITEMWIDGET_CPP


/**
*** paneltooltipitemwidget.cpp
**/

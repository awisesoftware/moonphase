/**
*** \file drawframe.h
*** \brief Draw animation frame.
*** \details Draws a frame of the moon animation.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(DRAWFRAME_H)
/**
*** \brief drawframe.h identifier.
*** \details Identifier for drawframe.h.
*** \internal
**/
#define   DRAWFRAME_H


/****
*****
***** INCLUDES
*****
****/

#include  "moonanimation.h"

#include  <QPixmap>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */

/**
*** \brief Draws a frame of an animation.
*** \details Given a phase percentage of the moon, draws a frame of an
***   animation. The background is transparent, but may be filled with a
***   color, if desired.
*** \param pMoonAnimation Animation image data.
*** \param Percent Phase percentage of moon [0-100].
*** \param UseBackgroundColorFlag true - Draw background using a color,\n
***   false - Draw transparent background.
*** \param BackgroundColor Background color (only used if
***   UseBackgroundColorFlag==true).
*** \return Animation frame.
**/
QPixmap DrawFrame(MOONANIMATION_T const *pMoonAnimation,unsigned int Percent,
    bool UseBackgroundColorFlag,QColor BackgroundColor);


#endif    /* !defined(DRAWFRAME_H) */


/**
*** drawframe.h
**/

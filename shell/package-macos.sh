#!/bin/bash

# $1 - Project directory (required).
# $2 - Project (required).
# $3 - Version (required).
# $4 - Distribution (unused).
# $5 - Architecture (required).

if [ "$#" -ne "5" ]; then
  if [ "$#" -lt "5" ]; then
    echo "Error: missing parameters."
  else
    echo "Error: extra parameters."
  fi

  echo "Usage: $0 [projectdir] [projectname] [projectversion] [distribution] [x86_64]"
  exit 1
fi

# Make script easier to read.
ProjDir="$1"
Project="$2"
Version="$3"
Dist="$4"
Arch="$5"

if [ "$Arch" != "x86_64" ]; then
  echo "Error: invalid architecture (use 'x86_64')."
  exit 1
fi

# Create macOS package.
shell/build-macos.sh &&
cd .build &&
cpack .. &&
mv ${Project}-qt-${Version}-Darwin.dmg ../${Project}-qt-${Version}-darwin.dmg &&
cd "${ProjDir}"

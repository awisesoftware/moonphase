/**
*** \file test_module_moondata.cpp
*** \brief 'moondata.cpp' unit tests.
*** \details Unit tests for the functions in 'moondata.cpp'.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/

#include  <boost/test/unit_test.hpp>

#include  "moondata.c"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/


/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Module__MoonData)
  BOOST_AUTO_TEST_SUITE(Function__MoonData_InitializeMembers)
    BOOST_AUTO_TEST_CASE(Test_NoChange)
    {
      MOONDATA_T MoonData;
      MOONDATA_T ResultMoonData;


      memset(&MoonData,0x80,sizeof(MoonData));
      memset(&ResultMoonData,0x80,sizeof(ResultMoonData));

      BOOST_TEST(MoonData_InitializeMembers(&MoonData)==ERRORCODE_SUCCESS);
      BOOST_TEST(memcmp(&MoonData,&ResultMoonData,sizeof(MoonData))==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__MoonData_UninitializeMembers)
    BOOST_AUTO_TEST_CASE(Test_NoChange)
    {
      MOONDATA_T MoonData;
      MOONDATA_T ResultMoonData;


      memset(&MoonData,0x80,sizeof(MoonData));
      memset(&ResultMoonData,0x80,sizeof(ResultMoonData));

      BOOST_TEST(MoonData_UninitializeMembers(&MoonData)==ERRORCODE_SUCCESS);
      BOOST_TEST(memcmp(&MoonData,&ResultMoonData,sizeof(MoonData))==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__MoonData_Initialize)
    BOOST_AUTO_TEST_CASE(Test_Cleared)
    {
      MOONDATA_T MoonData;
      MOONDATA_T ResultMoonData;


      memset(&MoonData,0x80,sizeof(MoonData));
      memset(&ResultMoonData,0,sizeof(ResultMoonData));

      BOOST_TEST(MoonData_Initialize(&MoonData)==ERRORCODE_SUCCESS);
      BOOST_TEST(memcmp(&MoonData,&ResultMoonData,sizeof(MoonData))==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__MoonData_Uninitialize)
    BOOST_AUTO_TEST_CASE(Test_Cleared)
    {
      MOONDATA_T MoonData;
      MOONDATA_T ResultMoonData;


      memset(&MoonData,0x80,sizeof(MoonData));
      memset(&ResultMoonData,0,sizeof(ResultMoonData));

      BOOST_TEST(MoonData_Uninitialize(&MoonData)==ERRORCODE_SUCCESS);
      BOOST_TEST(memcmp(&MoonData,&ResultMoonData,sizeof(MoonData))==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__FindPhase)
    BOOST_AUTO_TEST_CASE(Test_NextNewMoon)
    {
      MOONDATA_T MoonData;


      BOOST_TEST(FindPhase(20180101,0,0)==1516155467);
          /* Jan. 16, 2018 2:17:47 PM GMT */
    }
    BOOST_AUTO_TEST_CASE(Test_NextFirstQuarterMoon)
    {
      MOONDATA_T MoonData;


      BOOST_TEST(FindPhase(20180101,0,.25)==1516832443);
          /* Jan. 24, 2018 2:20:43 PM GMT */
    }
    BOOST_AUTO_TEST_CASE(Test_NextFullMoon)
    {
      MOONDATA_T MoonData;


      BOOST_TEST(FindPhase(20180101,0,.5)==1514859867);
          /* Jan. 2, 2018 2:24:27 AM GMT */
    }
    BOOST_AUTO_TEST_CASE(Test_NextLastQuarterMoon)
    {
      MOONDATA_T MoonData;


      BOOST_TEST(FindPhase(20180101,0,.75)==1515450359);
          /* Jan. 8, 2018 22:25:59 PM GMT */
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__MoonData_GetMoonPhasePercent)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      MOONDATA_T MoonData;


      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(MoonData_GetMoonPhasePercent(&MoonData)==0);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      MOONDATA_T MoonData;


      MoonData.CTransData.MoonPhase=.25;
      BOOST_TEST(MoonData_GetMoonPhasePercent(&MoonData)==25);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      MOONDATA_T MoonData;


      MoonData.CTransData.MoonPhase=.5;
      BOOST_TEST(MoonData_GetMoonPhasePercent(&MoonData)==50);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      MOONDATA_T MoonData;


      MoonData.CTransData.MoonPhase=.75;
      BOOST_TEST(MoonData_GetMoonPhasePercent(&MoonData)==75);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__MoonData_Recalculate)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      MOONDATA_T MoonData;


      MoonData_Initialize(&MoonData);

      /* Calculate initial dates/times. */
      MoonData_Recalculate(&MoonData,1546300800);
          // Jan. 1, 2019 12:00:00 AM GMT
      BOOST_TEST(MoonData.NextNewMoonGMt==1546738121);            /* Updated. */
          // Jan. 6, 2019 01:28:41 AM GMT
      BOOST_TEST(MoonData.NextFirstQuarterMoonGMt==1547448344);   /* Updated. */
          // Jan. 13, 2019 06:45:44 AM GMT
      BOOST_TEST(MoonData.NextFullMoonGMt==1548047785);           /* Updated. */
          // Jan. 21, 2019 05:16:25 AM GMT
      BOOST_TEST(MoonData.NextLastQuarterMoonGMt==1548623456);    /* Updated. */
          // Jan. 27, 2019 09:11:56 PM GMT

      /* 1 second past new moon. */
      MoonData_Recalculate(&MoonData,1546738122);
          // Jan. 6, 2019 01:28:42 AM GMT
      BOOST_TEST(MoonData.NextNewMoonGMt==1549314234);            /* Updated. */
          // Feb. 4, 2019 09:04:54 PM GMT
      BOOST_TEST(MoonData.NextFirstQuarterMoonGMt==1547448344);
          // Jan. 13, 2019 06:45:44 AM GMT
      BOOST_TEST(MoonData.NextFullMoonGMt==1548047785);
          // Jan. 21, 2019 05:16:25 AM GMT
      BOOST_TEST(MoonData.NextLastQuarterMoonGMt==1548623456);
          // Jan. 27, 2019 09:11:56 PM GMT

      /* 1 second past first quarter moon. */
      MoonData_Recalculate(&MoonData,1547448345);
          // Jan. 13, 2019 06:45:45 AM GMT
      BOOST_TEST(MoonData.NextNewMoonGMt==1549314234);
          // Feb. 4, 2019 09:04:54 PM GMT
      BOOST_TEST(MoonData.NextFirstQuarterMoonGMt==1550010379);   /* Updated. */
          // Feb. 12, 2019 10:26:19 PM GMT
      BOOST_TEST(MoonData.NextFullMoonGMt==1548047785);
          // Jan. 21, 2019 05:16:25 AM GMT
      BOOST_TEST(MoonData.NextLastQuarterMoonGMt==1548623456);
          // Jan. 27, 2019 09:11:56 PM GMT

      /* 1 second past full moon. */
      MoonData_Recalculate(&MoonData,1548047786);
          // Jan. 13, 2019 06:45:45 AM GMT
      BOOST_TEST(MoonData.NextNewMoonGMt==1549314234);
          // Feb. 4, 2019 09:04:54 PM GMT
      BOOST_TEST(MoonData.NextFirstQuarterMoonGMt==1550010379);
          // Feb. 12, 2019 10:26:19 PM GMT
      BOOST_TEST(MoonData.NextFullMoonGMt==1550591627);           /* Updated. */
          // Feb. 19, 2019 03:53:47 PM GMT
      BOOST_TEST(MoonData.NextLastQuarterMoonGMt==1548623456);
          // Jan. 27, 2019 09:11:56 PM GMT

      /* 1 second past last quarter moon. */
      MoonData_Recalculate(&MoonData,1548623457);
          // Jan. 13, 2019 06:45:45 AM GMT
      BOOST_TEST(MoonData.NextNewMoonGMt==1549314234);
          // Feb. 4, 2019 09:04:54 PM GMT
      BOOST_TEST(MoonData.NextFirstQuarterMoonGMt==1550010379);
          // Feb. 12, 2019 10:26:19 PM GMT
      BOOST_TEST(MoonData.NextFullMoonGMt==1550591627);
          // Feb. 19, 2019 03:53:47 PM GMT
      BOOST_TEST(MoonData.NextLastQuarterMoonGMt==1551180497);    /* Updated. */
          // Feb. 26, 2019 11:28:17 AM GMT
    }
  BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()


/**
*** test_module_moondata.cpp
**/

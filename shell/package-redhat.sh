#!/bin/bash

# $1 - Project directory (required).
# $2 - Project (required).
# $3 - Version (required).
# $4 - Distribution (required).
# $5 - Architecture (required).

if [ "$#" -ne "5" ]; then
  if [ "$#" -lt "5" ]; then
    echo "Error: missing parameters."
  else
    echo "Error: extra parameters."
  fi

  echo "Usage: $0 [projectdir] [projectname] [projectversion] [distribution] [x86 | x86_64]"
  exit 1
fi

# Make script easier to read.
ProjDir="$1"
Project="$2"
Version="$3"
Dist="$4"
Arch="$5"

if [ "$Arch" == "x86" ]; then
  BArch="i386"
  TArch="i686"
elif [ "$Arch" == "x86_64" ]; then
  BArch="x86_64"
  TArch="x86_64"
else
  echo "Error: invalid architecture (use 'x86' or 'x86_64')."
  exit 1
fi

# Create Fedora package.
cd "${ProjDir}" &&
tar xzf "${ProjDir}/${Project}-${Version}.tar.gz" &&
cd "${Project}-${Version}" &&
cp -rf installation/redhat . &&
mock --buildsrpm -r fedora-${Dist}-${BArch} --spec "redhat/${Project}.spec" --sources "${ProjDir}/${Project}-${Version}.tar.gz" &&
cp -f "/var/lib/mock/fedora-${Dist}-${BArch}/result/${Project}-${Version}"-?.fc${Dist}.src.rpm ${ProjDir} &&
mock --rebuild -r fedora-${Dist}-${BArch} "${ProjDir}/${Project}-${Version}"-?.fc${Dist}.src.rpm &&
cp -f "/var/lib/mock/fedora-${Dist}-${BArch}/root/builddir/build/SRPMS/${Project}-${Version}"-?.fc${Dist}.src.rpm "${ProjDir}" &&
cp -f "/var/lib/mock/fedora-${Dist}-${BArch}/root/builddir/build/RPMS/${Project}-qt-${Version}"-?.fc${Dist}.${TArch}.rpm "${ProjDir}" &&
rpmlint -i ${ProjDir}/${Project}-qt-${Version}-?.fc${Dist}.src.rpm &&
rpmlint -i ${ProjDir}/${Project}-qt-${Version}-?.fc${Dist}.${TArch}.rpm &&
cd "${ProjDir}"

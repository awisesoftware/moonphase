/**
*** \file preferences.h
*** \brief Load/change/save preferences classes.
*** \details Classes to load, change, and save preferences.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PREFERENCES_H)
/**
*** \internal
*** \brief preferences.h identifier.
*** \details Identifier for preferences.h.
*** \endinternal
**/
#define   PREFERENCES_H


/****
*****
***** INCLUDES
*****
****/

#include  "commontypes.h"
#include  "preferencesmanager.h"

#include  <memory>
#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PANELTOOLTIPITEMLIST_C;
class PANELITEMLIST_C;
class TOOLTIPITEMLIST_C;

class PREFERENCES_C
{
  public:
    /**
    *** \brief Application preferences.
    *** \details Preferences related to running the application.
    **/
    class APPLICATION_C : private PREFERENCESMANAGER_C
    {
      public:
        /**
        *** \brief Default constructor.
        *** \details Default constructor.
        **/
        APPLICATION_C(void);

        /**
        *** \brief Copy constructor.
        *** \details Copy constructor.
        *** \param Preferences Preferences to copy.
        **/
        APPLICATION_C(APPLICATION_C const &Preferences);

        /**
        *** \brief Destructor.
        *** \details Destructor.
        **/
        ~APPLICATION_C(void);

        /**
        *** \brief = operator.
        *** \details Copy operator.
        *** \param Preferences Preferences to copy.
        **/
        APPLICATION_C const & operator=(APPLICATION_C const &Preferences);

        /**
        *** \brief == operator.
        *** \details Equality operator.
        *** \param Preferences Object on right hand side of operator.
        *** \retval true Objects are equivalent.
        *** \retval false Objects are not equivalent.
        **/
        bool operator==(APPLICATION_C const &Preferences) const;

        /**
        *** \brief != operator.
        *** \details Inequality operator.
        *** \param Preferences Object on right hand side of operator.
        *** \retval true Objects are not equivalent.
        *** \retval false Objects are equivalent.
        **/
        bool operator!=(APPLICATION_C const &Preferences) const;

        /**
        *** \brief Returns the animation pathname.
        *** \details Returns the pathname of the animation image.
        *** \returns Pathname of the animation image.
        **/
        std::string GetAnimationPathname(void) const;

        /**
        *** \brief Returns the background color.
        *** \details Returns the color of the background.
        *** \returns Color of the background.
        **/
        COLOR_C GetBackgroundColor(void) const;

        /**
        *** \brief Returns the confirm discard flag.
        *** \details Returns the confirm discarding of changes flag.
        *** \retval true Prompt for confirmation.
        *** \retval false Discard changes without confirming.
        **/
        bool GetConfirmDiscardFlag(void) const;

        /**
        *** \brief Returns the confirm quit flag.
        *** \details Returns the confirm quit flag.
        *** \retval true Prompt for confirmation.
        *** \retval false Quit without confirming.
        **/
        bool GetConfirmQuitFlag(void) const;

        /**
        *** \brief Returns the remind once per session flag.
        *** \details Returns the still running reminder - once per session flag.
        *** \retval true Remind only once per session.
        *** \retval false Remind every time.
        **/
        bool GetRemindOncePerSessionFlag(void) const;

        /**
        *** \brief Returns the still running reminder flag.
        *** \details Returns the still running reminder flag.
        *** \retval true Remind on every close.
        *** \retval false No reminder on close.
        **/
        bool GetStillRunningReminderFlag(void) const;

        bool GetUnsavedChangesReminderFlag(void) const;

        /**
        *** \brief Returns the use opaque background flag.
        *** \details Returns the use opaque background flag.
        *** \retval true Draw background using a color.
        *** \retval false Draw transparent background.
        **/
        bool GetUseOpaqueBackgroundFlag(void) const;

        void Load(void);
        void Save(void);

        /**
        *** \brief Sets the animation pathname.
        *** \details Sets the pathname of the animation image.
        *** \param Pathname Pathname of the animation image.
        **/
        void SetAnimationPathname(std::string const &Pathname);

        /**
        *** \brief Sets the background color.
        *** \details Sets the color of the background.
        *** \param Color Color of the background.
        **/
        void SetBackgroundColor(COLOR_C const &Color);

        /**
        *** \brief Sets the confirm discard flag.
        *** \details Sets the confirm discarding of changes flag.
        *** \param ConfirmDiscardFlag true - Prompt for confirmation,\n
        ***   false - Discard changes without confirming.
        **/
        void SetConfirmDiscardFlag(bool ConfirmDiscardFlag);

        /**
        *** \brief Sets the confirm quit flag.
        *** \details Sets the confirm quit flag.
        *** \param ConfirmQuitFlag true - Prompt for confirmation,\n
        ***   false - Quit without confirming.
        **/
        void SetConfirmQuitFlag(bool ConfirmQuitFlag);

        /**
        *** \brief Sets the remind once per session flag.
        *** \details Sets the still running reminder once per session flag.
        *** \param OnceFlag true - Remind only once per session,\n
        ***   false - Remind every time.
        **/
        void SetRemindOncePerSessionFlag(bool OnceFlag);

        /**
        *** \brief Sets the still running reminder flag.
        *** \details Sets the still running reminder flag.
        *** \param ReminderFlag true - Remind on every close,\n
        ***   false - No reminder on close.
        **/
        void SetStillRunningReminderFlag(bool ReminderFlag);

        /**
        *** \brief Set the unsaved changes reminder flag.
        *** \details Sets the unsaved changes reminder flag.
        *** \param Interval Interval (in hours).
        **/
        void SetUnsavedChangesReminderFlag(bool ReminderFlag);

        /**
        *** \brief Sets the use opaque background flag.
        *** \details Sets the use opaque background flag.
        *** \param UseFlag true - Draw background using a color,\n
        ***   false - Draw transparent background.
        **/
        void SetUseOpaqueBackgroundFlag(bool UseFlag);

      protected:
        /**
        *** \brief Animation pathname.
        *** \details Pathname of the animation image.
        **/
        std::string m_AnimationPathname;

        /**
        *** \brief Background color.
        *** \details Color of the background.
        **/
        COLOR_C m_BackgroundColor;

        /**
        *** \brief Confirm discard flag.
        *** \details Confirm discarding of changes flag.
        **/
        bool m_ConfirmDiscardFlag;

        /**
        *** \brief Confirm quit flag.
        *** \details Confirm quit flag.
        **/
        bool m_ConfirmQuitFlag;

        /**
        *** \brief Remind once per session flag.
        *** \details Still running reminder once per session flag.
        **/
        bool m_RemindOncePerSessionFlag;

        /**
        *** \brief Still running reminder flag.
        *** \details Application still running reminder flag.
        **/
        bool m_StillRunningReminderFlag;

        bool m_UnsavedChangesReminderFlag;

        /**
        *** \brief Use opaque background flag.
        *** \details Use opaque background flag.
        **/
        bool m_UseOpaqueBackgroundFlag;
    };

    class MOONDATA_C : private PREFERENCESMANAGER_C
    {
      public:
        class OPTIONS_C : private PREFERENCESMANAGER_C
        {
          public:
            /**
            *** \brief Default constructor.
            *** \details Default constructor.
            **/
            OPTIONS_C(void);

            OPTIONS_C(OPTIONS_C const &Options);

            /**
            *** \brief Destructor.
            *** \details Destructor.
            **/
            ~OPTIONS_C(void);

            OPTIONS_C const & operator=(OPTIONS_C const &Options);

            bool operator==(OPTIONS_C const &Options) const;

            bool operator!=(OPTIONS_C const &Options) const;

            /**
            *** \brief Returns the default to local time zone flag.
            *** \details Returns the default to local time zone flag.
            *** \retval true - Default to local time zone.
            *** \retval false - Default to GMT.
            **/
            bool GetDefaultToGMTFlag(void) const;

            /**
            *** \brief Returns the default to metric units flag.
            *** \details Returns the default to metric units flag.
            *** \retval true - Default to metric units.
            *** \retval false - Default to imperial units.
            **/
            bool GetDefaultToMetricUnitsFlag(void) const;

            /**
            *** \brief Return latitude.
            *** \details Returns the latitude.
            *** \returns Latitude.
            **/
            double GetLatitude(void) const;

            /**
            *** \brief Return longitude.
            *** \details Returns the longitude.
            *** \returns Longitude.
            **/
            double GetLongitude(void) const;

            /**
            *** \brief Load preferences.
            *** \details Reads the preferences from the configuration file.
            **/
            void Load(void);

            /**
            *** \brief Save preferences.
            *** \details Writes the preferences to the configuration file.
            **/
            void Save(void);

            /**
            *** \brief Sets the default to local time zone flag.
            *** \details Sets the default to local time zone flag.
            *** \param DefaultToGMTFlag true - Default to local time zone,\n
            ***   false - Default to GMT.
            **/
            void SetDefaultToGMTFlag(bool DefaultToGMTFlag);

            /**
            *** \brief Sets the default to metric units flag.
            *** \details Sets the default to metric units flag.
            *** \param DefaultToMetricUnitsFlag
            ***   true - Default to metric units,\n
            ***   false - Default to imperial units.
            **/
            void SetDefaultToMetricUnitsFlag(bool DefaultToMetricUnitsFlag);

            /**
            *** \brief Set latitude.
            *** \details Set the latitude.
            *** \returns Latitude.
            **/
            void SetLatitude(double Latitude);

            /**
            *** \brief Set longitude.
            *** \details Set the longitude.
            *** \returns Longitude.
            **/
            void SetLongitude(double Longitude);

          private:
            /**
            *** \brief Default to local time zone flag.
            *** \details Default to local time zone flag.
            **/
            bool m_DefaultToGMTFlag;

            /**
            *** \brief Default to metric units flag.
            *** \details Default to metric units flag.
            **/
            bool m_DefaultToMetricUnitsFlag;

            /**
            *** \brief Latitude.
            *** \details Laitude.
            **/
            double m_Latitude;

            /**
            *** \brief Longitude.
            *** \details Longitude.
            **/
            double m_Longitude;
        };

    public:
        class PANELTOOLTIP_C
        {
          protected:
            PANELTOOLTIP_C(void);

            /**
            *** \brief Copy constructor.
            *** \details Copy constructor.
            *** \param pWidget Pointer to widget to copy.
            *** \note Function explicitly defined as deleted.
            **/
            PANELTOOLTIP_C(PANELTOOLTIP_C const &PanelTooltip)=delete;

            /**
            *** \brief = operator.
            *** \details Assignment operator.
            *** \param Widget Object to copy.
            *** \returns Copy of original widget.
            **/
            PANELTOOLTIP_C const & operator=(
                PANELTOOLTIP_C const &Widget)=delete;

          public:
            virtual ~PANELTOOLTIP_C(void);

            PANELTOOLTIPITEMLIST_C * GetItemListPointer(void);
            PANELTOOLTIPITEMLIST_C const * GetItemListPointer(void) const;

          protected:
            std::unique_ptr<PANELTOOLTIPITEMLIST_C> m_pItemList;
        };

        class PANEL_C : public PANELTOOLTIP_C, private PREFERENCESMANAGER_C
        {
          public:
            class DISPLAY_C : private PREFERENCESMANAGER_C
            {
              public:
                static const POSITION_C mc_InvalidPosition;
                static const SIZE_C mc_InvalidSize;

              public:
                /**
                *** \brief Default constructor.
                *** \details Default constructor.
                **/
                DISPLAY_C(void);

                DISPLAY_C(DISPLAY_C const &Preferences);

                /**
                *** \brief Destructor.
                *** \details Destructor.
                **/
                ~DISPLAY_C(void);

                DISPLAY_C const & operator=(DISPLAY_C const &Preferences);

                bool operator==(DISPLAY_C const &Preferences) const;
                bool operator!=(DISPLAY_C const &Preferences) const;

                bool GetAlwaysOnTopFlag(void) const;
                bool GetDisplayOnStartUpFlag(void) const;
                bool GetKeepOnScreenFlag(void) const;
                bool GetHideCloseButtonFlag(void) const;
                POSITION_C GetPosition(void) const;
                SIZE_C GetSize(void) const;

                void Load(void);
                void Save(void);

                void SetAlwaysOnTopFlag(bool AlwaysOnTopFlag);
                void SetDisplayOnStartUpFlag(bool DisplayOnStartUpFlag);
                void SetHideCloseButtonFlag(bool HideCloseButtonFlag);
                void SetKeepOnScreenFlag(bool KeepOnScreenFlag);
                void SetPosition(POSITION_C const &Position);
                void SetSize(SIZE_C const &Size);

              protected:
                bool m_AlwaysOnTopFlag;
                bool m_DisplayOnStartUpFlag;
                bool m_KeepOnScreenFlag;
                bool m_HideCloseButtonFlag;
                POSITION_C m_Position;
                SIZE_C m_Size;
            };

            class DIALOGDATA_C
            {
              public:
                DIALOGDATA_C(void);
                DIALOGDATA_C(bool VisibleFlag,
                    POSITION_C const &Position,SIZE_C const &Size);
                ~DIALOGDATA_C(void);

                bool GetVisibleFlag(void) const;
                POSITION_C GetPosition(void) const;
                SIZE_C GetSize(void) const;

              private:
                POSITION_C m_Position;
                SIZE_C m_Size;
                bool m_VisibleFlag;
            };

            PANEL_C(void);

            /**
            *** \brief Copy constructor.
            *** \details Copy constructor.
            *** \param pWidget Pointer to widget to copy.
            *** \note Function explicitly defined as deleted.
            **/
            PANEL_C(PANEL_C const &Panel)=delete;

            ~PANEL_C(void);

            PANEL_C const & operator=(PANEL_C const &Preferences);

            bool operator==(PANEL_C const &Preferences) const;
            bool operator!=(PANEL_C const &Preferences) const;

            PANELITEMLIST_C * GetItemListPointer(void);
            PANELITEMLIST_C const * GetItemListPointer(void) const;

            void Load(void);
            void Save(void);

            DISPLAY_C * GetDisplayPreferencesPointer(void);
            DISPLAY_C const * GetDisplayPreferencesPointer(void) const;
            DIALOGDATA_C * GetDialogDataPointer(void);

          private:
            DIALOGDATA_C m_DialogData;
            DISPLAY_C m_DisplayPreferences;
        };

        class TOOLTIP_C : public PANELTOOLTIP_C
        {
          public:
            TOOLTIP_C(void);

            /**
            *** \brief Copy constructor.
            *** \details Copy constructor.
            *** \param pWidget Pointer to widget to copy.
            *** \note Function explicitly defined as deleted.
            **/
            TOOLTIP_C(TOOLTIP_C const &Tooltip)=delete;

            ~TOOLTIP_C(void);

            TOOLTIP_C const & operator=(TOOLTIP_C const &Preferences);

            bool operator==(TOOLTIP_C const &Preferences) const;
            bool operator!=(TOOLTIP_C const &Preferences) const;

            TOOLTIPITEMLIST_C * GetItemListPointer(void);
            TOOLTIPITEMLIST_C const * GetItemListPointer(void) const;

            void Load(void);
            void Save(void);
        };

        MOONDATA_C(void);

        MOONDATA_C(MOONDATA_C const &Preferences);

        ~MOONDATA_C(void);

        MOONDATA_C const & operator=(MOONDATA_C const &Preferences);

        bool operator==(MOONDATA_C const &Preferences) const;

        bool operator!=(MOONDATA_C const &Preferences) const;

        OPTIONS_C * GetOptionsPointer(void);
        OPTIONS_C const * GetOptionsPointer(void) const;

        PANEL_C * GetPanelPreferencesPointer(void);
        PANEL_C const * GetPanelPreferencesPointer(void) const;

        TOOLTIP_C * GetToolTipPreferencesPointer(void);
        TOOLTIP_C const * GetToolTipPreferencesPointer(void) const;

        void Load(void);
        void Save(void);

      private:
        OPTIONS_C m_Options;
        PANEL_C m_PanelPreferences;
        TOOLTIP_C m_ToolTipPreferences;
    };

  public://TODOprivate:
    PREFERENCES_C(void);

  public:
    ~PREFERENCES_C(void);

    APPLICATION_C * GetApplicationPreferencesPointer(void);
    PREFERENCES_C::MOONDATA_C * GetMoonDataPreferencesPointer(void);

    void Load(void);
    void Save(void);

  private:
    APPLICATION_C m_ApplicationPreferences;
    MOONDATA_C m_MoonDataPreferences;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PREFERENCES_H) */


/**
*** preferences.h
**/

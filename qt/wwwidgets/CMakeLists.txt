#
# CMakeLists.txt
# CMake makefile for the Moon Phase wwWidgets library.
#

#
# This file is part of moonphase.
# Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


#
# Setup
#

# Component information.
SET(COMPONENTNAME "wwWidgets")
SET(DISPLAYNAME "wwWidgets")
SET(FILENAME "wwwidgets4")
STRING(TOUPPER "${COMPONENTNAME}" COMPONENTNAMEUC)
STRING(TOLOWER "${COMPONENTNAME}" COMPONENTNAMELC)


#
# Configuration
#

INCLUDE(ExternalProject)

# Need Qt5.
FIND_PACKAGE(Qt5 COMPONENTS Core REQUIRED)
SET(QMAKE_EXECUTABLE Qt5::qmake)

# This is a bit of a hack.
# These directories are expected to exist at configuration time, but are
# not created until the external project (wwwidgets) is built. Temporarily
# fake the directories so CMake doesn't exit with a configuration error.
# Note: Any additions to this list must be duplicated in the
# SET_PROPERTY(... PROPERTY INTERFACE_INCLUDE_DIRECTORIES ...) line towards
# the end of this file.
FILE(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets")
FILE(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwbuttonlineedit")
FILE(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwcolorbutton")
FILE(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwfilechooser")


#
# Targets
#

# Calculate where the name and relative location of the library.
IF(UNIX)
  SET(WWWIDGETS_LIBRARYPATHNAME ".")
ELSEIF(MSVC)
  SET(WWWIDGETS_LIBRARYPATHNAME "${BUILDTYPELC}")
ENDIF()
SET(WWWIDGETS_LIBRARYPATHNAME
    "${WWWIDGETS_LIBRARYPATHNAME}/${CMAKE_STATIC_LIBRARY_PREFIX}${FILENAME}")
IF(MSVC)
  IF("${BUILDTYPELC}" STREQUAL "debug")
    SET(WWWIDGETS_LIBRARYPATHNAME "${WWWIDGETS_LIBRARYPATHNAME}d")
  ENDIF()
ENDIF()
SET(WWWIDGETS_LIBRARYPATHNAME
    "${WWWIDGETS_LIBRARYPATHNAME}${CMAKE_STATIC_LIBRARY_SUFFIX}")

# Build the library.
EXTERNALPROJECT_ADD(
    lib${FILENAME}
    URL "http://www.wysota.eu.org/wwwidgets/wwWidgets-1.0-qt5.tar.gz"
    URL_HASH MD5=e9f71030ae7846f39ec89bb86bb61b97
    DOWNLOAD_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../${COMPONENTNAMELC}/"
    SOURCE_DIR "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/"
    BINARY_DIR "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/"
    PATCH_COMMAND COMMAND patch --binary -p1 -i
        "${CMAKE_CURRENT_SOURCE_DIR}/01-dont_build_plugins.patch"
    PATCH_COMMAND COMMAND patch --binary -p1 -i
        "${CMAKE_CURRENT_SOURCE_DIR}/02-dont_build_all_widgets.patch"
    PATCH_COMMAND COMMAND patch --binary -p1 -i
        "${CMAKE_CURRENT_SOURCE_DIR}/03-g++_-fvisibility=hidden.patch"
    PATCH_COMMAND COMMAND patch --binary -p1 -i
        "${CMAKE_CURRENT_SOURCE_DIR}/04-build-release-only.patch"
    PATCH_COMMAND COMMAND patch --binary -p1 -i
        "${CMAKE_CURRENT_SOURCE_DIR}/05-remove-export-macro-definitions.patch"
      # --binary switches are needed by Windows GNU diff.
    CONFIGURE_COMMAND ${QMAKE_EXECUTABLE} "CONFIG+=staticlib ${BUILDTYPELC}"
    BUILD_COMMAND ${BUILD_COMMAND}
    INSTALL_COMMAND ""
    BUILD_BYPRODUCTS "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/${WWWIDGETS_LIBRARYPATHNAME}")

# Import the library, and set up dependencies and properties.
ADD_LIBRARY(${COMPONENTNAME}::${COMPONENTNAME} STATIC IMPORTED GLOBAL)
ADD_DEPENDENCIES(${COMPONENTNAME}::${COMPONENTNAME} lib${FILENAME})
SET_PROPERTY(TARGET lib${FILENAME} PROPERTY
    MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
SET_PROPERTY(
    TARGET ${COMPONENTNAME}::${COMPONENTNAME}
    PROPERTY IMPORTED_LOCATION
    "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/${WWWIDGETS_LIBRARYPATHNAME}")
SET_PROPERTY(
    TARGET ${COMPONENTNAME}::${COMPONENTNAME}
    PROPERTY INTERFACE_INCLUDE_DIRECTORIES
    "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets"
    "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwbuttonlineedit"
    "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwcolorbutton"
    "${CMAKE_BINARY_DIR}/${COMPONENTNAMELC}/widgets/qwwfilechooser")


#
# Subdirectories
#


#
# Installation
#


#
# CMakeLists.txt
#

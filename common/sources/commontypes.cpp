/**
*** \file commontypes.cpp
*** \brief commontypes.cpp implementation.
*** \details Implementation file for commontypes.cpp.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief commontypes.cpp identifier.
*** \details Identifier for commontypes.cpp.
**/
#define   COMMONTYPES_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "commontypes.h"
#ifdef    DEBUG_COMMONTYPES_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_COMMONTYPES_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  <limits>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

/**
*** \brief Invalid color component.
*** \details Indicates that a color component is invalid.
**/
float const COLOR_C::mc_Invalid=-1.0;

float const COLOR_C::mc_Minimum=0.0;

float const COLOR_C::mc_Maximum=1.0;

int const FONT_C::mc_Invalid=-1;

/**
*** \brief Invalid position value.
*** \details Indicates that a position value is invalid.
**/
int const POSITION_C::mc_Invalid=std::numeric_limits<int>::min();

int const POSITION_C::mc_Minimum=std::numeric_limits<int>::min()/2;

int const POSITION_C::mc_Maximum=std::numeric_limits<int>::max()/2;

/**
*** \brief Invalid size value.
*** \details Indicates that a size value is invalid.
**/
int const SIZE_C::mc_Invalid=std::numeric_limits<int>::min();

int const SIZE_C::mc_Minimum=0;

int const SIZE_C::mc_Maximum=std::numeric_limits<int>::max();


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

COLOR_C::COLOR_C(void)
{
  DEBUGLOG_Printf0("COLOR_C::COLOR_C()");
  DEBUGLOG_LogIn();

  m_Alpha=mc_Invalid;
  m_Blue=mc_Invalid;
  m_Green=mc_Invalid;
  m_Red=mc_Invalid;

  DEBUGLOG_LogOut();
  return;
}

COLOR_C::COLOR_C(float Red,float Green,float Blue,float Alpha)
{
  DEBUGLOG_Printf4("COLOR_C::COLOR_C(%f,%f,%f,%f)",Red,Green,Blue,Alpha);
  DEBUGLOG_LogIn();

  SetRGBA(Red,Green,Blue,Alpha);

  DEBUGLOG_LogOut();
  return;
}

COLOR_C::~COLOR_C(void)
{
  DEBUGLOG_Printf0("COLOR_C::~COLOR_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool COLOR_C::operator==(COLOR_C const &Color) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("COLOR_C::operator==(COLOR_C:%p)",&Color);
  DEBUGLOG_LogIn();

  if ( (IsValid()==false) && (Color.IsValid()==false) )
    EqualFlag=true;
  else if ( (IsValid()==false) || (Color.IsValid()==false) )
    EqualFlag=false;
  else
    EqualFlag=((GetAlpha()==Color.GetAlpha()) && (GetBlue()==Color.GetBlue()) &&
        (GetGreen()==Color.GetGreen()) && (GetRed()==Color.GetRed()));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool COLOR_C::operator!=(COLOR_C const &Color) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("COLOR_C::operator!=(COLOR_C:%p)",Color);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Color);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

float COLOR_C::GetAlpha(void) const
{
  DEBUGLOG_Printf0("COLOR_C::GetAlpha()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Alpha);
}

float COLOR_C::GetBlue(void) const
{
  DEBUGLOG_Printf0("COLOR_C::GetBlue()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Blue);
}

float COLOR_C::GetGreen(void) const
{
  DEBUGLOG_Printf0("COLOR_C::GetGreen()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Green);
}

float COLOR_C::GetRed(void) const
{
  DEBUGLOG_Printf0("COLOR_C::GetRed()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Red);
}

void COLOR_C::Invalidate(void)
{
  DEBUGLOG_Printf0("COLOR_C::Invalidate()");
  DEBUGLOG_LogIn();

  *this=COLOR_C();

  DEBUGLOG_LogOut();
  return;
}

bool COLOR_C::IsValid(void) const
{
  DEBUGLOG_Printf0("COLOR_C::IsValid()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return( (m_Alpha!=mc_Invalid) && (m_Blue!=mc_Invalid) &&
      (m_Green!=mc_Invalid) && (m_Red!=mc_Invalid) );
}

void COLOR_C::SetAlpha(float Alpha)
{
  DEBUGLOG_Printf1("COLOR_C::SetAlpha(%f)",Alpha);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if ( (Alpha<mc_Minimum) || (Alpha>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    Alpha=mc_Invalid;
  }

  m_Alpha=Alpha;

  DEBUGLOG_LogOut();
  return;
}

void COLOR_C::SetBlue(float Blue)
{
  DEBUGLOG_Printf1("COLOR_C::SetBlue(%f)",Blue);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if ( (Blue<mc_Minimum) || (Blue>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    Blue=mc_Invalid;
  }

  m_Blue=Blue;

  DEBUGLOG_LogOut();
  return;
}

void COLOR_C::SetGreen(float Green)
{
  DEBUGLOG_Printf1("COLOR_C::SetGreen(%f)",Green);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if ( (Green<mc_Minimum) || (Green>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    Green=mc_Invalid;
  }

  m_Green=Green;

  DEBUGLOG_LogOut();
  return;
}

void COLOR_C::SetRed(float Red)
{
  DEBUGLOG_Printf1("COLOR_C::SetRed(%f)",Red);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if ( (Red<mc_Minimum) || (Red>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    Red=mc_Invalid;
  }

  m_Red=Red;

  DEBUGLOG_LogOut();
  return;
}

void COLOR_C::SetRGBA(float Red,float Green,float Blue,float Alpha)
{
  DEBUGLOG_Printf4("COLOR_C::SetRGBA(%f,%f,%f,%f)",Red,Green,Blue,Alpha);
  DEBUGLOG_LogIn();

  SetRed(Red);
  SetGreen(Green);
  SetBlue(Blue);
  SetAlpha(Alpha);

  DEBUGLOG_LogOut();
  return;
}

FONT_C::FONT_C(void)
{
  DEBUGLOG_Printf0("FONT_C::FONT_C()");
  DEBUGLOG_LogIn();

  m_ItalicFlag=false;
  m_PointSize=mc_Invalid;
  m_Weight=mc_Invalid;

  DEBUGLOG_LogOut();
  return;
}

FONT_C::FONT_C(
    std::string const &Family,int PointSize,int Weight,bool ItalicFlag)
{
  DEBUGLOG_Printf5("FONT_C::FONT_C(%p(%s),%d,%d,%u)",
      &Family,Family.c_str(),PointSize,Weight,ItalicFlag);
  DEBUGLOG_LogIn();

  SetFamily(Family);
  SetItalicFlag(ItalicFlag);
  SetPointSize(PointSize);
  SetWeight(Weight);

  DEBUGLOG_LogOut();
  return;
}

FONT_C::~FONT_C(void)
{
  DEBUGLOG_Printf0("FONT_C::~FONT_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool FONT_C::operator==(FONT_C const &Font) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("FONT_C::operator==(FONT_C const:%p",&Font);
  DEBUGLOG_LogIn();

  if ( (IsValid()==false) && (Font.IsValid()==false) )
    EqualFlag=true;
  else if ( (IsValid()==false) || (Font.IsValid()==false) )
    EqualFlag=false;
  else
    EqualFlag=( (m_Family==Font.m_Family) && (m_PointSize==Font.m_PointSize) &&
    (m_Weight==Font.m_Weight) && (m_ItalicFlag==Font.m_ItalicFlag));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool FONT_C::operator!=(FONT_C const &Font) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("FONT_C::operator!=(FONT_C const:%p",&Font);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Font);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

std::string FONT_C::GetFamily(void) const
{
  DEBUGLOG_Printf0("FONT_C::GetFamily()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Family);
}

bool FONT_C::GetItalicFlag(void) const
{
  DEBUGLOG_Printf0("FONT_C::GetItalicFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_ItalicFlag);
}

int FONT_C::GetPointSize(void) const
{
  DEBUGLOG_Printf0("FONT_C::GetPointSize()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_PointSize);
}

int FONT_C::GetWeight(void) const
{
  DEBUGLOG_Printf0("FONT_C::GetWeight()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Weight);
}

void FONT_C::Invalidate(void)
{
  DEBUGLOG_Printf0("FONT_C::Invalidate()");
  DEBUGLOG_LogIn();

  *this=FONT_C();

  DEBUGLOG_LogOut();
  return;
}

bool FONT_C::IsValid(void) const
{
  DEBUGLOG_Printf0("FONT_C::IsValid()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return( (m_Family!="")&&(m_PointSize!=mc_Invalid)&&(m_Weight!=mc_Invalid) );
}

void FONT_C::SetFamily(std::string const &Family)
{
  DEBUGLOG_Printf2("FONT_C::SetFamily(%p(%s))",&Family,Family.c_str());
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (Family.empty()==true)
  {
    MESSAGELOG_Error("Empty string.");
  }

  m_Family=Family;

  DEBUGLOG_LogOut();
  return;
}

void FONT_C::SetItalicFlag(bool ItalicFlag)
{
  DEBUGLOG_Printf1("FONT_C::SetItalicFlag(%u)",ItalicFlag);
  DEBUGLOG_LogIn();

  m_ItalicFlag=ItalicFlag;

  DEBUGLOG_LogOut();
  return;
}

void FONT_C::SetPointSize(int PointSize)
{
  DEBUGLOG_Printf1("FONT_C::SetPointSize(%d)",PointSize);
  DEBUGLOG_LogIn();

  if (PointSize<0)
  {
    MESSAGELOG_Error("Negative point size.");
    PointSize=mc_Invalid;
  }
  m_PointSize=PointSize;

  DEBUGLOG_LogOut();
  return;
}

void FONT_C::SetWeight(int Weight)
{
  DEBUGLOG_Printf1("FONT_C::SetWeight(%d)",Weight);
  DEBUGLOG_LogIn();

  if (Weight<0)
  {
    MESSAGELOG_Error("Negative weight.");
    Weight=mc_Invalid;
  }
  m_Weight=Weight;

  DEBUGLOG_LogOut();
  return;
}

POSITION_C::POSITION_C(void)
{
  DEBUGLOG_Printf0("POSITION_C::POSITION_C()");
  DEBUGLOG_LogIn();

  m_X=mc_Invalid;
  m_Y=mc_Invalid;

  DEBUGLOG_LogOut();
  return;
}

POSITION_C::POSITION_C(int X,int Y)
{
  DEBUGLOG_Printf2("POSITION_C::POSITION_C(%d,%d)",X,Y);
  DEBUGLOG_LogIn();

  SetXY(X,Y);

  DEBUGLOG_LogOut();
  return;
}

POSITION_C::~POSITION_C(void)
{
  DEBUGLOG_Printf0("POSITION_C::~POSITION_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool POSITION_C::operator==(POSITION_C const &Position) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("POSITION_C::operator==(POSITION_C:%p)",Position);
  DEBUGLOG_LogIn();

  if ( (IsValid()==false) && (Position.IsValid()==false) )
    EqualFlag=true;
  else if ( (IsValid()==false) || (Position.IsValid()==false) )
    EqualFlag=false;
  else
    EqualFlag=((GetX()==Position.GetX()) && (GetY()==Position.GetY()));

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool POSITION_C::operator!=(POSITION_C const &Position) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("POSITION_C::operator!=(POSITION_C:%p)",Position);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Position);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

int POSITION_C::GetX(void) const
{
  DEBUGLOG_Printf0("POSITION_C::GetX()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_X);
}

int POSITION_C::GetY(void) const
{
  DEBUGLOG_Printf0("POSITION_C::GetY()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Y);
}

void POSITION_C::Invalidate(void)
{
  DEBUGLOG_Printf0("POSITION_C::Invalidate()");
  DEBUGLOG_LogIn();

  *this=POSITION_C();

  DEBUGLOG_LogOut();
  return;
}

bool POSITION_C::IsValid(void) const
{
  DEBUGLOG_Printf0("POSITION_C::IsValid()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return( (m_X!=mc_Invalid) && (m_Y!=mc_Invalid) );
}

void POSITION_C::SetX(int X)
{
  DEBUGLOG_Printf1("POSITION_C::SetX(%d)",X);
  DEBUGLOG_LogIn();

  if ( (X<mc_Minimum) || (X>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    X=mc_Invalid;
  }
  m_X=X;

  DEBUGLOG_LogOut();
  return;
}

void POSITION_C::SetXY(int X,int Y)
{
  DEBUGLOG_Printf2("POSITION_C::SetXY(%d,%d)",X,Y);
  DEBUGLOG_LogIn();

  SetX(X);
  SetY(Y);

  DEBUGLOG_LogOut();
  return;
}

void POSITION_C::SetY(int Y)
{
  DEBUGLOG_Printf1("POSITION_C::SetY(%d)",Y);
  DEBUGLOG_LogIn();

  if ( (Y<mc_Minimum) || (Y>mc_Maximum) )
  {
    MESSAGELOG_Error("Value out of range");
    Y=mc_Invalid;
  }
  m_Y=Y;

  DEBUGLOG_LogOut();
  return;
}

SIZE_C::SIZE_C(void)
{
  DEBUGLOG_Printf0("SIZE_C::SIZE_C()");
  DEBUGLOG_LogIn();

  m_Height=mc_Invalid;
  m_Width=mc_Invalid;

  DEBUGLOG_LogOut();
  return;
}

SIZE_C::SIZE_C(int Width,int Height)
{
  DEBUGLOG_Printf2("SIZE_C::SIZE_C(%d,%d)",Width,Height);
  DEBUGLOG_LogIn();

  SetWidth(Width);
  SetHeight(Height);

  DEBUGLOG_LogOut();
  return;
}

SIZE_C::~SIZE_C(void)
{
  DEBUGLOG_Printf0("SIZE_C::~SIZE_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool SIZE_C::operator==(SIZE_C const &Size) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("SIZE_C::operator==(SIZE_C:%p)",Size);
  DEBUGLOG_LogIn();

  if ( (IsValid()==false) && (Size.IsValid()==false) )
    EqualFlag=true;
  else if ( (IsValid()==false) || (Size.IsValid()==false) )
    EqualFlag=false;
  else
    EqualFlag=(GetHeight()==Size.GetHeight()) && (GetWidth()==Size.GetWidth());

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool SIZE_C::operator!=(SIZE_C const &Size) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("SIZE_C::operator!=(SIZE_C:%p)",Size);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Size);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

int SIZE_C::GetHeight(void) const
{
  DEBUGLOG_Printf0("SIZE_C::GetHeight()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Height);
}

int SIZE_C::GetWidth(void) const
{
  DEBUGLOG_Printf0("SIZE_C::GetWidth()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Width);
}

void SIZE_C::Invalidate(void)
{
  DEBUGLOG_Printf0("SIZE_C::Invalidate()");
  DEBUGLOG_LogIn();

  *this=SIZE_C();

  DEBUGLOG_LogOut();
  return;
}

bool SIZE_C::IsValid(void) const
{
  DEBUGLOG_Printf0("SIZE_C::IsValid()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_Width>=0)&&(m_Height>=0));
}

void SIZE_C::SetHeight(int Height)
{
  DEBUGLOG_Printf1("SIZE_C::SetHeight(%d)",Height);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if (Height<0)
  {
    MESSAGELOG_Error("Value out of range");
    Height=mc_Invalid;
  }

  m_Height=Height;

  DEBUGLOG_LogOut();
  return;
}

void SIZE_C::SetWidth(int Width)
{
  DEBUGLOG_Printf1("SIZE_C::SetWidth(%d)",Width);
  DEBUGLOG_LogIn();

  /* Warning if out of range. */
  if (Width<0)
  {
    MESSAGELOG_Error("Value out of range");
    Width=mc_Invalid;
  }

  m_Width=Width;

  DEBUGLOG_LogOut();
  return;
}

void SIZE_C::SetWidthHeight(int Width,int Height)
{
  DEBUGLOG_Printf2("SIZE_C::SetWidthHeight(%d,%d)",Width,Height);
  DEBUGLOG_LogIn();

  SetWidth(Width);
  SetHeight(Height);

  DEBUGLOG_LogOut();
  return;
}


#undef    COMMONTYPES_CPP


/**
*** commontypes.cpp
**/

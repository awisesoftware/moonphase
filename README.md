![Moon Phase Logo](logo.png)
# Moon Phase
Moonphase is an application that displays the phase of the moon in the system tray. Additionally, a window and/or tool tip can be displayed that shows user configurable data about the moon (rise/set times, percent illumination, etc).

## Versions
This version is 0.5.0.

The latest version can be found at [GitLab](https://gitlab.com/awisesoftware/moonphase/).

See [documentation/changelog.txt](documentation/changelog.txt) for details.

## Installing

### Binaries
Operating system specific installation packages are available at [GitLab: Binaries](https://gitlab.com/alanwise/binaries/) and [Source Forge: Moon Phase Binaries](https://sourceforge.net/projects/moonphase/files/)[^1]. Packages exist for macOS, Debian based Linux distributions, Fedora based Linux distributions, and Windows. All systems except macOS have both 32 bit and 64 bit versions available.

Please note that under Windows, if the installer is run using "Run as...", uncheck the "Start program" checkbox and start the program manually using the Start menu. Failure to do so will result in the program will be running under the control of the "Run as..." user and any configuration changes will be saved to that account, not to the current user.

### Source
Installation from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Upgrading

### Binaries

#### Versions prior to 0.4.0.
Follow the recommendations for uninstalling, and then install as normal.

#### Versions 0.4.0 and after.
The installer will uninstall the older version.

### Source
Upgrading from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Uninstalling

### Binaries

#### Versions prior to 0.4.0.
Stop the program before uninstalling. Under Windows, if the program is still running during the uninstall process, the executable and installation directory will not be removed. Under Linux, the program will remain in the system tray until the next logout or reboot.

#### Versions 0.4.0 and after.
Under Windows, the uninstaller should be run as "Administrator". Failure to do so will prevent the program from properly uninstalling.

### Source
Uninstallation from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Problems
Solutions to common problems can be found in [documentation/problems.txt](documentation/problems.txt).

If you do not find the solution to your problem in the documentation, it may be a bug. Refer to the **Contributing** section on how to view recent issues and/or submit bug reports.

## Contributing
This project is hosted at [GitLab](https://gitlab.com/awisesoftware/moonphase).

Ideas, bugs, etc., can be added to the issue tracker at [GitLab](https://gitlab.com/awisesoftware/moonphase/issues) (registration required) or sent to me via [email](mailto:awisesoftware@gmail.com).

## Authors

* **Alan Wise** <awisesoftware@gmail.com> - Initial work.

## License
This file is part of moonphase.
Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

See [documentation/license.txt](documentation/license.txt) for details.

[^1]: Hosting at [Source Forge](https://www.sourceforge.net) is being phased out. The source code is no longer maintained there, and binaries *might not* be hosted there beyond version 0.5.0.

/**
*** \file test_module_desktop.cpp
*** \brief 'desktop.cpp' unit tests.
*** \details Unit tests for the functions in 'desktop.cpp'.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/

#include  <boost/test/unit_test.hpp>

#include  "desktop.cpp"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/


/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Module__desktop)
  BOOST_AUTO_TEST_SUITE(Function__GetDesktopWindowBackgroundColor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      BOOST_TEST((GetDesktopWindowBackgroundColor()==
          COLOR_C(0x10/255.0,0x20/255.0,0x40/255.0,0x80/255.0)));
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetDesktopWindowFont)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      BOOST_TEST((GetDesktopWindowFont()==FONT_C("TestFont",10,100)));
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetDesktopWindowTextColor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      BOOST_TEST((GetDesktopWindowTextColor()==
          COLOR_C(0x08/255.0,0x04/255.0,0x02/255.0,0x01/255.0)));
    }
  BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()


/**
*** test_module_desktop.cpp
**/

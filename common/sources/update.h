/**
*** \file update.h
*** \brief Download and extract a version string from a URL.
*** \details Downloads a file from a given URL, reads the file, and extracts a
***   version number from the text.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(UPDATE_H)
/**
*** \internal
*** \brief update.h identifier.
*** \details Identifier for update.h.
*** \endinternal
**/
#define   UPDATE_H


/****
*****
***** INCLUDES
*****
****/

#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Check for program update.
*** \details Checks for an update to the program by downloading the file at
***   the given URL, and extracting the text version number in the file.
*** \param URL Location of file to download/read.
*** \param Version Storage for version string read from file or empty string on
***   error.
**/
void CheckForUpdate(std::string const &URL,std::string &Version);


#endif    /* !defined(UPDATE_H) */


/**
*** update.h
**/

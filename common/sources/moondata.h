/**
*** \file moondata.h
*** \brief Moon data computations and information.
*** \details Recompuates the moon data as neeeded, and provides access to the
***   data.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MOONDATA_H)
/**
*** \brief moondata.h identifier.
*** \details Identifier for moondata.h.
*** \internal
**/
#define   MOONDATA_H


/****
*****
***** INCLUDES
*****
****/

#include  "calcephem.h"
#include  "structure.h"

#include  <time.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Moon data calculation results.
*** \details The results of the moon data calculations.
**/
typedef struct structMOONDATA
{
  /**
*** \brief 'calcephem' moon data calculation results.
*** \details The results of the moon data calculations from module 'calcephem'.
  **/
  CTrans CTransData;

  /**
  *** \brief Next new moon date/time.
  *** \details Date and time of next new moon.
  **/
  time_t NextNewMoonGMt;

  /**
  *** \brief Next first quarter moon date/time.
  *** \details Date and time of next first quarter moon.
  **/
  time_t NextFirstQuarterMoonGMt;

  /**
  *** \brief Next full moon date/time.
  *** \details Date and time of next full moon.
  **/
  time_t NextFullMoonGMt;

  /**
  *** \brief Next last quarter moon date/time.
  *** \details Date and time of next last quarter moon.
  **/
  time_t NextLastQuarterMoonGMt;

  /**
  *** \brief Todays moon rise time.
  *** \details The time the moon rose/will rise today.
  **/
  double TodaysRiseLT;

  /**
  *** \brief Todays moon set time.
  *** \details The time the moon set/will set today.
  **/
  double TodaysSetLT;

  /**
  *** \brief Tomorrows moon rise time.
  *** \details The time the moon will rise tomorrow.
  **/
  double TomorrowsRiseLT;

  /**
  *** \brief Tomorrows moon set time.
  *** \details The time the moon set/will set tomorrow.
  **/
  double TomorrowsSetLT;

  /**
  *** \brief Yesterdays moon rise time.
  *** \details The time the moon rise yesterday.
  **/
  double YesterdaysRiseLT;

  /**
  *** \brief Yesterdays moon set time.
  *** \details The time the moon set yesterday.
  **/
  double YesterdaysSetLT;
} MOONDATA_T;


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if       defined(__cplusplus)
extern "C" {
#endif    /* defined(__cplusplus) */

STRUCTURE_PROTOTYPE_INITIALIZE(MoonData,MOONDATA_T);
STRUCTURE_PROTOTYPE_UNINITIALIZE(MoonData,MOONDATA_T);
/**
*** \brief Returns the moon phase percent.
*** \details Returns the moon phase as a percent [0,100).
*** \param pMoonData Calculated moon data.
*** \returns The moon phase as a percent [0,100).
**/
float MoonData_GetMoonPhasePercent(MOONDATA_T const *pMoonData);

/**
*** \brief Recalculate moon data.
*** \details Recalculates the moon data as input parameters change.
*** \param pMoonData Pointer to the moon data.
*** \param GMTt Current date/time in GMT.
**/
void MoonData_Recalculate(MOONDATA_T *pMoonData,time_t GMTt);

#if       defined(__cplusplus)
}
#endif    /* defined(__cplusplus) */


#endif    /* !defined(MOONDATA_H) */


/**
*** moondata.h
**/

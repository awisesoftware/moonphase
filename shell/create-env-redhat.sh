#!/bin/bash

# Create the x86 environment.
mock -r fedora-31-i386 --init
mock -r fedora-31-i386 --install cmake git make libcurl-devel patch qt5-devel

# Create the x86_64 environment.
mock -r fedora-31-x86_64 --init
mock -r fedora-31-x86_64 --install cmake git make libcurl-devel patch qt5-devel

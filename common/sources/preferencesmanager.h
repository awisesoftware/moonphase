/**
*** \file preferencesmanager.h
*** \brief Load/change/save preferences.
*** \details Loading, changing, and saving of preferences.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef   PREFERENCESMANAGER_H
/**
*** \internal
*** \brief preferencesmanager.h identifier.
*** \details Identifier for preferencesmanager.h.
*** \endinternal
**/
#define   PREFERENCESMANAGER_H


/****
*****
***** INCLUDES
*****
****/

#include  "commontypes.h"

#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PREFERENCESMANAGERPRIVATE_C;

class PREFERENCESMANAGER_C
{
  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    PREFERENCESMANAGER_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PREFERENCESMANAGER_C(PREFERENCESMANAGER_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PREFERENCESMANAGER_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PREFERENCESMANAGER_C const &
      operator=(PREFERENCESMANAGER_C const &Widget)=delete;

    void BeginGroup(std::string const &Name);
    int BeginReadArray(std::string const &Prefix);
    void BeginWriteArray(std::string const &Prefix,int Size=-1);
    void EndArray(void);
    void EndGroup(void);
    void Remove(std::string const &Key);
    void SetArrayIndex(int Index);

    bool GetPreference(std::string const &Key,bool Default) const;
    COLOR_C GetPreference(std::string const &Key,COLOR_C const &Default) const;
    double GetPreference(std::string const &Key,double Default) const;
    FONT_C GetPreference(std::string const &Key,FONT_C const &Default) const;
    int GetPreference(std::string const &Key,int Default) const;
    POSITION_C GetPreference(
        std::string const &Key,POSITION_C const &Default) const;
    SIZE_C GetPreference(std::string const &Key,SIZE_C const &Default) const;
    std::string GetPreference(
        std::string const &Key,std::string const &Default) const;

    void SetPreference(std::string const &Key,bool Value);
    void SetPreference(std::string const &Key,COLOR_C const &Value);
    void SetPreference(std::string const &Key,double Value);
    void SetPreference(std::string const &Key,FONT_C const &Value);
    void SetPreference(std::string const &Key,int const &Value);
    void SetPreference(std::string const &Key,POSITION_C const &Value);
    void SetPreference(std::string const &Key,SIZE_C const &Value);
    void SetPreference(std::string const &Key,std::string const &Value);

  private:
    PREFERENCESMANAGERPRIVATE_C *m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* PREFERENCESMANAGER_H */


/**
*** preferencesmanager.h
**/

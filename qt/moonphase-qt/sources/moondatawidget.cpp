/**
*** \file moondatawidget.cpp
*** \brief moondatawidget.h implementation.
*** \details Implementation file for moondatawidget.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief moondatawidget.cpp identifier.
*** \details Identifier for moondatawidget.cpp.
*** \internal
**/
#define   MOONDATAWIDGET_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "moondatawidget.h"
#include  "ui_moondatawidget.h"
#if       defined(DEBUG_MOONDATAWIDGET_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MOONDATAWIDGET_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  <QPushButton>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Moon data widget implementation class.
*** \details The implementation class for the moon data widget.
**/
class MOONDATAWIDGET_C::MOONDATAWIDGETPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pPublic Pointer to the interface class (MOONDATAWIDGET_C).
    **/
    MOONDATAWIDGETPRIVATE_C(MOONDATAWIDGET_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    MOONDATAWIDGETPRIVATE_C(MOONDATAWIDGETPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~MOONDATAWIDGETPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    MOONDATAWIDGETPRIVATE_C const & operator=(
        MOONDATAWIDGETPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Set preferences.
    *** \details Sets the preferences of the moon preferences widget.
    *** \param Preferences Preferences data.
    **/
    void SetMoonPreferences(PREFERENCES_C::MOONDATA_C const &Preferences);

    bool m_NoUpdateFlag;

    /**
    *** \brief Application preferences pointer.
    *** \details Pointer to the application preferences.
    **/
    PREFERENCES_C::MOONDATA_C *m_pPreferences;

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    MOONDATAWIDGET_C *m_pPublic;

    /**
    *** \brief Saved moon data preferences.
    *** \details Preferences in configuration file.
    **/
    PREFERENCES_C::MOONDATA_C m_SavedMoonPreferences;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_PANELTOOLTIPWIDGETUI_C> m_pUI;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

MOONDATAWIDGET_C::
    MOONDATAWIDGETPRIVATE_C::MOONDATAWIDGETPRIVATE_C(MOONDATAWIDGET_C *pPublic)
    : m_pUI(new Ui_PANELTOOLTIPWIDGETUI_C)
{
  DEBUGLOG_Printf1("MOONDATAWIDGET_C::"
      "MOONDATAWIDGETPRIVATE_C::MOONDATAWIDGETPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_NoUpdateFlag=false;
  m_pPreferences=nullptr;
  m_pPublic=pPublic;

  /* Set up the user interface. */
  m_pUI->setupUi(pPublic);

  DEBUGLOG_LogOut();
  return;
}

MOONDATAWIDGET_C::MOONDATAWIDGETPRIVATE_C::~MOONDATAWIDGETPRIVATE_C(void)
{
  DEBUGLOG_Printf0(
      "MOONDATAWIDGET_C::MOONDATAWIDGETPRIVATE_C::~MOONDATAWIDGETPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

void MOONDATAWIDGET_C::MOONDATAWIDGETPRIVATE_C::
    SetMoonPreferences(PREFERENCES_C::MOONDATA_C const &Preferences)
{
  DEBUGLOG_Printf1(
      "MOONDATAWIDGET_C::MOONDATAWIDGETPRIVATE_C::SetMoonPreferences(%p)",
      &Preferences);
  DEBUGLOG_LogIn();

  m_SavedMoonPreferences=Preferences;

  /* Set the widget data. */
  m_NoUpdateFlag=true;
  m_pUI->m_pLatitudeSpinBox->setValue(
      Preferences.GetOptionsPointer()->GetLatitude());
  m_pUI->m_pLongitudeSpinBox->setValue(
      Preferences.GetOptionsPointer()->GetLongitude());
  m_pUI->m_pMetricUnitsRadioButton->setChecked(
      Preferences.GetOptionsPointer()->GetDefaultToMetricUnitsFlag());
  m_pUI->m_pStandardUnitsRadioButton->setChecked(
      !Preferences.GetOptionsPointer()->GetDefaultToMetricUnitsFlag());
  m_pUI->m_pGMTRadioButton->setChecked(
      Preferences.GetOptionsPointer()->GetDefaultToGMTFlag());
  m_pUI->m_pLocalTimeZoneRadioButton->setChecked(
      !Preferences.GetOptionsPointer()->GetDefaultToGMTFlag());
  m_NoUpdateFlag=false;
  m_pPublic->PreferencesChangedSlot();

  DEBUGLOG_LogOut();
  return;
}

MOONDATAWIDGET_C::MOONDATAWIDGET_C(QWidget *pParent) : QWidget(pParent),
    m_pPrivate(new MOONDATAWIDGETPRIVATE_C(this))
{
  DEBUGLOG_Printf1("MOONDATAWIDGET_C::MOONDATAWIDGET_C(%p)",pParent);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

MOONDATAWIDGET_C::~MOONDATAWIDGET_C(void)
{
  DEBUGLOG_Printf0("MOONDATAWIDGET_C::~MOONDATAWIDGET_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void MOONDATAWIDGET_C::ButtonBoxButtonClickedSlot(QAbstractButton *pButton)
{
  DEBUGLOG_Printf1(
      "PREFERENCESWIDGET_C::ButtonBoxButtonClickedSlot(%p)",pButton);
  DEBUGLOG_LogIn();

  switch(m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton))
  {
    case QDialogButtonBox::Save:
      m_pPrivate->m_SavedMoonPreferences=*m_pPrivate->m_pPreferences;
      m_pPrivate->m_pPreferences->Save();
      break;

    case QDialogButtonBox::Reset:
      m_pPrivate->SetMoonPreferences(m_pPrivate->m_SavedMoonPreferences);
      break;

    default:
      MESSAGELOG_Error("Invalid button.");
      break;
  }

  PreferencesChangedSlot();

  DEBUGLOG_LogOut();
  return;
}

bool MOONDATAWIDGET_C::PreferencesChanged(void)
{
  DEBUGLOG_Printf0("MOONDATAWIDGET_C::PreferencesChanged()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_pUI->m_pButtonBox->
      button(QDialogButtonBox::Save)->isEnabled());
}

void MOONDATAWIDGET_C::PreferencesChangedSlot(void)
{
  bool EnableFlag;


  DEBUGLOG_Printf0("MOONDATAWIDGET_C::PreferencesChangedSlot()");
  DEBUGLOG_LogIn();

  if (m_pPrivate->m_NoUpdateFlag==false)
  {
    /* Get the moon data widget data. */
    m_pPrivate->m_NoUpdateFlag=true;
    m_pPrivate->m_pPreferences->GetOptionsPointer()->SetLatitude(
        m_pPrivate->m_pUI->m_pLatitudeSpinBox->value());
    m_pPrivate->m_pPreferences->GetOptionsPointer()->SetLongitude(
        m_pPrivate->m_pUI->m_pLongitudeSpinBox->value());
    m_pPrivate->m_NoUpdateFlag=false;

    m_pPrivate->m_pPreferences->GetOptionsPointer()->SetDefaultToMetricUnitsFlag(
        m_pPrivate->m_pUI->m_pMetricUnitsRadioButton->isChecked());
    m_pPrivate->m_pPreferences->GetOptionsPointer()->SetDefaultToGMTFlag(
        m_pPrivate->m_pUI->m_pGMTRadioButton->isChecked());

    m_pPrivate->m_pUI->m_pPanelItemWidget->SetDefaultToMetricUnitsFlag(
      m_pPrivate->m_pUI->m_pMetricUnitsRadioButton->isChecked());
    m_pPrivate->m_pUI->m_pToolTipItemWidget->SetDefaultToMetricUnitsFlag(
      m_pPrivate->m_pUI->m_pMetricUnitsRadioButton->isChecked());
    m_pPrivate->m_pUI->m_pPanelItemWidget->SetDefaultToGMTFlag(
      m_pPrivate->m_pUI->m_pGMTRadioButton->isChecked());
    m_pPrivate->m_pUI->m_pToolTipItemWidget->SetDefaultToGMTFlag(
      m_pPrivate->m_pUI->m_pGMTRadioButton->isChecked());

    /* Enable/disable the "Reset" and "Save" buttons. */
    EnableFlag=(m_pPrivate->m_SavedMoonPreferences!=*m_pPrivate->m_pPreferences);

    m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Reset)->
        setEnabled(EnableFlag);
    m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Save)->
        setEnabled(EnableFlag);
  }

  DEBUGLOG_LogOut();
  return;
}

void MOONDATAWIDGET_C::SavePreferences(void)
{
  DEBUGLOG_Printf0("MOONDATAWIDGET_C::SavePreferences()");
  DEBUGLOG_LogIn();

  ButtonBoxButtonClickedSlot(m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Save));

  DEBUGLOG_LogOut();
  return;
}

void MOONDATAWIDGET_C::SetPreferencesPointer(
    PREFERENCES_C::MOONDATA_C *pPreferences)
{
  DEBUGLOG_Printf1("MOONDATAWIDGET_C::SetPreferences(%p)",pPreferences);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pPreferences==nullptr)
  {
    MESSAGELOG_Error("pPreferences==nullptr");
    throw(std::invalid_argument("pPreferences==nullptr"));
  }

  m_pPrivate->m_pPreferences=pPreferences;
  m_pPrivate->m_pUI->m_pPanelItemWidget->SetPreferencesPointer(
      pPreferences->GetPanelPreferencesPointer());
  connect(
      m_pPrivate->m_pUI->m_pPanelItemWidget,SIGNAL(PreferencesChangedSignal()),
      this,SLOT(PreferencesChangedSlot()));
  m_pPrivate->m_pUI->m_pToolTipItemWidget->SetPreferencesPointer(
      pPreferences->GetToolTipPreferencesPointer());
  connect(
      m_pPrivate->m_pUI->m_pToolTipItemWidget,SIGNAL(PreferencesChangedSignal()),
      this,SLOT(PreferencesChangedSlot()));

  /* Set the widget data. */
  m_pPrivate->SetMoonPreferences(*m_pPrivate->m_pPreferences);

  DEBUGLOG_LogOut();
  return;
}


#undef    MOONDATAWIDGET_CPP


/**
*** moondatawidget.cpp
**/

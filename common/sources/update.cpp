/**
*** \file update.cpp
*** \brief update.h implementation file.
*** \details Implementation file for update.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief update.cpp identifier.
*** \details Identifier for update.cpp.
*** \endinternal
**/
#define   UPDATE_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "update.h"
#if       defined(DEBUG_UPDATE_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_UPDATE_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"
#include  "errorcode.h"

#include  <curl/curl.h>
#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief cURL callback function.
*** \details Function called by cURL to receive and store the downloaded data.
*** \param pData Pointer to downloaded data.
*** \param Size Size of data.
*** \param SizeCount Number of "Size" elements.
*** \param pUserData Pointer to user data.
*** \returns Number of bytes actually processed. If the value is not
***   "Size"*"SizeCount", this signals cURL that an error has occured, and the
***   transfer will abort.
**/
static size_t cURLCallback(
    char const *pData,size_t Size,size_t SizeCount,void *pUserData)
{
  size_t ByteCount;
  std::string *pVersion;


  DEBUGLOG_Printf4("cURLCallback(%p,%u,%u,*p)",pData,Size,SizeCount,pUserData);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pUserData==NULL)
  {
    MESSAGELOG_LogError(ERRORCODE_NULLPARAMETER);
    ByteCount=0;
  }
  else
  {
    try
    {
      /* Copy and convert the user pointer. */
      pVersion=static_cast<std::string*>(pUserData);

      /* Copy the data. */
      ByteCount=Size*SizeCount;
      pVersion->append(pData,ByteCount);

      /* Erase any trailing whitespace. */
      pVersion->erase(pVersion->find_last_not_of("\f\n\r\t\v ")+1);
    }
    catch(...)
    {
      ByteCount=0;
    }
  }

  DEBUGLOG_LogOut();
  return(ByteCount);
}

void CheckForUpdate(std::string const &URL,std::string &Version)
{
  CURL *pHandle;
  CURLcode Result;


  DEBUGLOG_Printf2("CheckForUpdate(%p(%s))",&URL,URL.c_str());
  DEBUGLOG_LogIn();

  pHandle=curl_easy_init();
  curl_easy_setopt(pHandle,CURLOPT_FAILONERROR,true);
  curl_easy_setopt(pHandle,CURLOPT_FOLLOWLOCATION,1L);
  curl_easy_setopt(pHandle,CURLOPT_MAXREDIRS,10L);
  curl_easy_setopt(pHandle,CURLOPT_URL,URL.c_str());
  curl_easy_setopt(pHandle,CURLOPT_WRITEDATA,&Version);
#if       defined(_WIN32)
#pragma   warning(push)
/*
Disable warning C5039: "pointer or reference to potentially throwing function
passed to 'extern "C"' function under -EHc. Undefined behavior may occur if
this function throws an exception." Any exceptions should be handled.
*/
#pragma   warning(disable:5039)
#endif    /* defined(_WIN32) */
  curl_easy_setopt(pHandle,CURLOPT_WRITEFUNCTION,cURLCallback);
#if       defined(_WIN32)
#pragma   warning(pop)
#endif    /* defined(_WIN32) */

  Result=curl_easy_perform(pHandle);
  if (Result!=CURLE_OK)
  {
    MESSAGELOG_Error(curl_easy_strerror(Result));
  }
  curl_easy_cleanup(pHandle);

  DEBUGLOG_LogOut();
  return;
}


#undef    UPDATE_CPP


/**
*** update.cpp
**/

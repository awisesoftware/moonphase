/**
*** \file desktop.h
*** \brief User desktop settings.
*** \details Provides access to user desktop settings.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(DESKTOP_H)
/**
*** \internal
*** \brief desktop.h identifier.
*** \details Identifier for desktop.h.
*** \endinternal
**/
#define   DESKTOP_H


/****
*****
***** INCLUDES
*****
****/

#include  "commontypes.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Return desktop window background color.
*** \details Returns the desktop window background color.
*** \returns Window background color.
**/
COLOR_C GetDesktopWindowBackgroundColor(void);

/**
*** \brief Return desktop window font.
*** \details Returns the desktop window font.
*** \returns Window desktop font.
**/
FONT_C GetDesktopWindowFont(void);

/**
*** \brief Return desktop window text color.
*** \details Returns the desktop window text color.
*** \returns Window text color.
**/
COLOR_C GetDesktopWindowTextColor(void);


#endif    /* !defined(DESKTOP_H) */


/**
*** desktop.h
**/

/**
*** \file conversions.cpp
*** \brief conversions.h implementation.
*** \details Implementation file for conversions.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief conversions.cpp identifier.
*** \details Identifier for conversions.cpp.
**/
#define   CONVERSIONS_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "conversions.h"
#ifdef    DEBUG_CONVERSIONS_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_CONVERSIONS_CPP */
#include  "debuglog.h"
#include  "messagelog.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

QColor ToQColor(COLOR_C const &Color)
{
  QColor Return;


  DEBUGLOG_Printf1("ToQColor(COLOR_C:%p",&Color);
  DEBUGLOG_LogIn();

  if (Color.IsValid())
    Return.setRgb((int)(255*Color.GetRed()),(int)(255*Color.GetGreen()),
        (int)(255*Color.GetBlue()),(int)(255*Color.GetAlpha()));

  DEBUGLOG_LogOut();
  return(Return);
}

COLOR_C ToCOLOR_C(QColor const &Color)
{
  COLOR_C Return;


  DEBUGLOG_Printf1("ToCOLOR_C(QColor:%p",&Color);
  DEBUGLOG_LogIn();

  if (Color.isValid())
    Return.SetRGBA(((float)Color.red())/255,((float)Color.green())/255,
        ((float)Color.blue())/255,((float)Color.alpha())/255);

  DEBUGLOG_LogOut();
  return(Return);
}

QFont ToQFont(FONT_C const &Font)
{
  QFont Return;


  DEBUGLOG_Printf1("ToQFont(FONT_C:%p",&Font);
  DEBUGLOG_LogIn();

  Return.setFamily(QString::fromStdString(Font.GetFamily()));
  Return.setPointSize(Font.GetPointSize());
  Return.setWeight(Font.GetWeight());
  Return.setItalic(Font.GetItalicFlag());

  DEBUGLOG_LogOut();
  return(Return);
}

FONT_C ToFONT_C(QFont const &Font)
{
  FONT_C Return;


  DEBUGLOG_Printf1("ToFONT_C(QFont:%p",&Font);
  DEBUGLOG_LogIn();

  Return.SetFamily(Font.family().toStdString());
  Return.SetItalicFlag(Font.italic());
  Return.SetPointSize(Font.pointSize());
  Return.SetWeight(Font.weight());

  DEBUGLOG_LogOut();
  return(Return);
}

QPoint ToQPoint(POSITION_C const &Point)
{
  QPoint Return;


  DEBUGLOG_Printf1("ToQPoint(POSITION_C:%p",&Point);
  DEBUGLOG_LogIn();

  Return.setX(Point.GetX());
  Return.setY(Point.GetY());

  DEBUGLOG_LogOut();
  return(Return);
}

POSITION_C ToPOSITION_C(QPoint const &Point)
{
  POSITION_C Return;


  DEBUGLOG_Printf1("ToPOSITION_C(QColor:%p",&Point);
  DEBUGLOG_LogIn();

  Return.SetXY(Point.x(),Point.y());

  DEBUGLOG_LogOut();
  return(Return);
}

QSize ToQSize(SIZE_C const &Size)
{
  QSize Return;


  DEBUGLOG_Printf1("ToQSize(POSITION_C:%p",&Size);
  DEBUGLOG_LogIn();

  if (Size.IsValid())
  {
    Return.setWidth(Size.GetWidth());
    Return.setHeight(Size.GetHeight());
  }

  DEBUGLOG_LogOut();
  return(Return);
}

SIZE_C ToSIZE_C(QSize const &Size)
{
  SIZE_C Return;


  DEBUGLOG_Printf1("ToSIZE_C(QSize:%p",&Size);
  DEBUGLOG_LogIn();

  Return.SetWidthHeight(Size.width(),Size.height());

  DEBUGLOG_LogOut();
  return(Return);
}


#undef    CONVERSIONS_CPP


/**
*** conversions.cpp
**/

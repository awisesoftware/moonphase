/**
*** \file trayicon.cpp
*** \brief trayicon.h implementation.
*** \details Implementation file for trayicon.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief trayicon.cpp identifier.
*** \details Identifier for trayicon.cpp.
**/
#define   TRAYICON_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "trayicon.h"
#ifdef    DEBUG_TRAYICON_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_TRAYICON_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "config.h"
#include  "information.h"
#include  "paneltooltipitem.h"

#include  <QApplication>
#include  <QMenu>
#include  <QTimer>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

TRAYICON_C::TRAYICON_C(QWidget *pParent) : QSystemTrayIcon(pParent)
{
  DEBUGLOG_Printf1("TRAYICON_C::TRAYICON_C(%p)",pParent);
  DEBUGLOG_LogIn();

  /* Create and set up the tray icon context menu. */
  m_pTrayIconMenu=new QMenu(pParent);
  m_pTrayIconMenu->addAction(QIcon(":/ActionIcons/ControlPanelBWIcon"),
      "Control Panel",this,SIGNAL(ShowControlPanelSignal()));
  m_pTrayIconMenu->addSeparator();
  m_pTrayIconMenu->addAction(QIcon(":/ActionIcons/InformationPanelBWIcon"),
      "Information Panel",this,SIGNAL(ShowInformationPanelSignal()));
  m_pTrayIconMenu->addSeparator();
  m_pTrayIconMenu->addAction(QIcon(":/ActionIcons/QuitBWIcon"),
      "Quit",this,SIGNAL(QuitSignal()));
  setContextMenu(m_pTrayIconMenu);

  /* Handle tray icon activation messages. */
  connect(this,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
      this,SLOT(ControlPanelActivatedSlot(QSystemTrayIcon::ActivationReason)));

  /* Set up a timer used to deferentiate one or two clicks from double clicks. */
#if       !defined(__APPLE__)
  m_pDoubleClickTimeoutTimer=new QTimer(this);
  m_pDoubleClickTimeoutTimer->setSingleShot(true);
  connect(m_pDoubleClickTimeoutTimer,SIGNAL(timeout()),
      this,SIGNAL(ShowInformationPanelSignal()));
#endif    /* !defined(__APPLE__) */

  DEBUGLOG_LogOut();
  return;
}

TRAYICON_C::~TRAYICON_C(void)
{
  DEBUGLOG_Printf0("TRAYICON_C::~TRAYICON_C()");
  DEBUGLOG_LogIn();

  if (m_pDoubleClickTimeoutTimer==nullptr)
  {
    MESSAGELOG_Warning("m_pDoubleClickTimeoutTimer==nullptr");
  }
  delete m_pDoubleClickTimeoutTimer;
  if (m_pTrayIconMenu==nullptr)
  {
    MESSAGELOG_Warning("m_pTrayIconMenu==nullptr");
  }
  delete m_pTrayIconMenu;

  DEBUGLOG_LogOut();
  return;
}

void TRAYICON_C::
    ControlPanelActivatedSlot(QSystemTrayIcon::ActivationReason Reason)
{
  DEBUGLOG_Printf1(
      "TRAYICON_C::ControlPanelActivatedSlot(%d)",Reason);
  DEBUGLOG_LogIn();

  switch(Reason)
  {
    case QSystemTrayIcon::DoubleClick:
#if       !defined(__APPLE__)
      m_pDoubleClickTimeoutTimer->stop();
#endif    /* !defined(__APPLE__) */
      emit ShowControlPanelSignal();
      break;

#if       !defined(__APPLE__)
    case QSystemTrayIcon::Trigger:
      if (m_pDoubleClickTimeoutTimer->isActive()==true)
        return;
      m_pDoubleClickTimeoutTimer->setInterval(
          QApplication::doubleClickInterval());
      m_pDoubleClickTimeoutTimer->start();
      break;
#endif    /* !defined(__APPLE__) */

    default:
      break;
  }

  DEBUGLOG_LogOut();
  return;
}

void TRAYICON_C::Update(
    PREFERENCES_C::MOONDATA_C::TOOLTIP_C const *pPreferences,
    MOONDATA_T const &MoonData)
{
  QString Text;


  DEBUGLOG_Printf2(
      "INFORMATIONPANELDIALOG_C::Update(%p,%p)",pPreferences,&MoonData);
  DEBUGLOG_LogIn();

  if (pPreferences->GetItemListPointer()->Size()==0)
  {
    Text=tr(MOONPHASEQT_DISPLAYNAME);
  }
  else
  {
    TOOLTIPITEMLIST_C::CONSTITERATOR_C ppItem;


    for(ppItem=pPreferences->GetItemListPointer()->Begin();
        ppItem!=pPreferences->GetItemListPointer()->End();++ppItem)
    {

      /* Calculate the data and display both the label and data. */
      Text+=QString::fromStdString(
          Information_GetInformationLabel((*ppItem)->GetInformationIndex()));
      Text+=": ";
      Text+=QString::fromStdString(
          Information_Print(MoonData,(*ppItem)->GetInformationIndex(),
          (*ppItem)->GetUnitFormatIndex(),**ppItem));
      Text+="\n";
    }

    /* Remove last '\n'. */
    Text.truncate(Text.size()-1);
  }

  /* Update the tool tip */
  setToolTip(Text);

  DEBUGLOG_LogOut();
  return;
}


#undef    TRAYICON_CPP


/**
*** trayicon.cpp
**/

/**
*** \file test_class_POSITION.cpp
*** \brief POSITION_C unit tests.
*** \details Unit tests for POSITION_C.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Class__POSITION)

  static int const fc_Range=POSITION_C::mc_Maximum-POSITION_C::mc_Minimum;
  static int const fc_Normal=(POSITION_C::mc_Maximum+POSITION_C::mc_Minimum)/2;
  static int const fc_OverMinimum=POSITION_C::mc_Minimum+1;
  static int const fc_OverMaximum=POSITION_C::mc_Maximum+1;
  static int const fc_UnderMaximum=POSITION_C::mc_Maximum-1;
  static int const fc_UnderMinimum=POSITION_C::mc_Minimum-1;

  BOOST_AUTO_TEST_SUITE(Function__DefaultConstructor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position;


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Constructor_int_int)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(fc_UnderMinimum,0);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(POSITION_C::mc_Minimum,0);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(fc_OverMinimum,0);


      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(fc_Normal,0);


      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(fc_UnderMaximum,0);


      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(POSITION_C::mc_Maximum,0);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      POSITION_C Position(fc_OverMaximum,0);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      POSITION_C Position(0,fc_UnderMinimum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      POSITION_C Position(0,POSITION_C::mc_Minimum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      POSITION_C Position(0,fc_OverMinimum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      POSITION_C Position(0,fc_Normal);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==fc_Normal);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      POSITION_C Position(0,fc_UnderMaximum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_0C)
    {
      POSITION_C Position(0,POSITION_C::mc_Maximum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_0D)
    {
      POSITION_C Position(0,fc_OverMaximum);


      BOOST_TEST(Position.GetX()==0);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__EqualityOperator_InequalityOperator)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_15)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_16)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_17)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_18)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_19)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_20)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_21)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_22)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_23)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_24)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_25)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_26)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_27)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_28)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_29)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_2A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_2B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_30)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_31)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_32)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_33)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_34)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_35)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_36)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_37)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_38)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_39)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_3A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_3B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_40)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_41)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_42)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_43)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_44)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_45)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_46)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_47)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_48)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_49)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_4A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_4B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_50)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_51)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_52)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_53)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_54)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_55)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_56)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_57)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_58)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_59)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_5A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_5B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_60)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_61)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_62)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_63)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_64)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_65)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_66)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_67)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_68)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_69)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_6A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_6B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_70)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_71)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_72)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_73)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_74)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_75)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_76)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_77)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_78)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_79)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_7A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_7B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_80)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_81)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_82)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_83)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_84)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_85)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_86)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_87)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_88)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_89)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_8A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_8B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_90)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_91)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_92)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_93)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_94)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_95)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_96)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_97)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_98)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_99)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_9A)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_9B)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A0)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A1)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A2)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A3)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A4)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A5)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A6)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A7)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A8)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A9)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_AA)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_AB)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B0)
    {
      POSITION_C Position0(fc_UnderMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B1)
    {
      POSITION_C Position0(POSITION_C::mc_Minimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B2)
    {
      POSITION_C Position0(fc_OverMinimum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B3)
    {
      POSITION_C Position0(fc_UnderMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B4)
    {
      POSITION_C Position0(POSITION_C::mc_Maximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B5)
    {
      POSITION_C Position0(fc_OverMaximum,fc_Normal);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B6)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMinimum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B7)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Minimum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B8)
    {
      POSITION_C Position0(fc_Normal,fc_OverMinimum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B9)
    {
      POSITION_C Position0(fc_Normal,fc_UnderMaximum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_BA)
    {
      POSITION_C Position0(fc_Normal,POSITION_C::mc_Maximum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==false);
      BOOST_TEST((Position0!=Position1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_BB)
    {
      POSITION_C Position0(fc_Normal,fc_OverMaximum);
      POSITION_C Position1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Position0==Position1)==true);
      BOOST_TEST((Position0!=Position1)==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetX)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(fc_UnderMinimum,fc_Normal);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(POSITION_C::mc_Minimum,fc_Normal);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(fc_OverMinimum,fc_Normal);


      BOOST_TEST(Position.GetX()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(fc_UnderMaximum,fc_Normal);


      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(POSITION_C::mc_Maximum,fc_Normal);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(fc_OverMaximum,fc_Normal);


      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetY)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(fc_Normal,fc_UnderMinimum);


      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(fc_Normal,POSITION_C::mc_Minimum);


      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(fc_Normal,fc_OverMinimum);


      BOOST_TEST(Position.GetY()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(fc_Normal,fc_UnderMaximum);


      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(fc_Normal,POSITION_C::mc_Maximum);


      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(fc_Normal,fc_OverMaximum);


      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Invalidate)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(0,0);


      BOOST_TEST(Position.IsValid()==true);

      Position.Invalidate();

      BOOST_TEST(Position.IsValid()==false);
      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__IsValid)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position;


      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position;


      Position.SetX(fc_Normal);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position;


      Position.SetY(fc_Normal);
      BOOST_TEST(Position.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position;
      Position.SetX(fc_Normal);
      Position.SetY(fc_Normal);
      BOOST_TEST(Position.IsValid()==true);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetX)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(fc_OverMinimum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetX(fc_OverMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_Normal);
      BOOST_TEST(Position.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetXY)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_15)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_20)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_21)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_22)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_23)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_24)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_25)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,fc_OverMinimum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_30)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_31)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_32)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_33)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_34)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_35)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_40)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_41)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_42)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_43)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_44)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_45)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_50)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMinimum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_51)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Minimum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_52)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMinimum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==fc_OverMinimum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_53)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_UnderMaximum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==fc_UnderMaximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_54)
    {
      POSITION_C Position(0,0);


      Position.SetXY(POSITION_C::mc_Maximum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_55)
    {
      POSITION_C Position(0,0);


      Position.SetXY(fc_OverMaximum,fc_OverMaximum);

      BOOST_TEST(Position.GetX()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetY)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(fc_UnderMinimum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(POSITION_C::mc_Minimum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Minimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(fc_OverMinimum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==fc_OverMinimum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(fc_UnderMaximum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==fc_UnderMaximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(POSITION_C::mc_Maximum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Maximum);
      BOOST_TEST(Position.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      POSITION_C Position(fc_Normal,fc_Normal);


      Position.SetY(fc_OverMaximum);

      BOOST_TEST(Position.GetX()==fc_Normal);
      BOOST_TEST(Position.GetY()==POSITION_C::mc_Invalid);
      BOOST_TEST(Position.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()


/**
*** test_class_POSITION.cpp
**/

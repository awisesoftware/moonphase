/**
*** \file information.cpp
*** \brief information.cpp implementation.
*** \details Implementation file for information.cpp.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief information.cpp identifier.
*** \details Identifier for information.cpp.
*** \internal
**/
#define   INFORMATION_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "information.h"
#if       defined(DEBUG_INFORMATION_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_INFORMATION_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "stdio_msvc.h"

#include  <codecvt>
#include  <iomanip>
#include  <limits>
#include  <math.h>
#include  <sstream>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/

/**
*** \brief Earth radius.
*** \details The radius of the earth (in miles).
**/
#define   EARTH_RADIUS    (3959.0)

/**
*** \brief Number of array elements.
*** \details Returns the number of elements in an array.
*** \note Array should never get large enough to overflow a signed integer.
*** \warning The array must be declared as an array (with the [] syntax) for
***   this to work.
**/
#define   ARRAY_ELEMENTCOUNT(a) \
    (int)(sizeof(a)/sizeof(*a))

/**
*** \brief Converts a double to a tm structure.
*** \details Converts a time (as a double; in hours) to a tm structure.
*** \param db Time (as a double; in hours).
*** \param tm tm structure.
*** \hideinitializer
**/
#define   DOUBLE2STRUCTTM(db,tm) \
              { \
                time_t D2STimeT; \
                D2STimeT=time(NULL); \
                (tm)=*localtime(&D2STimeT); \
                (tm).tm_hour=(int)(db); \
                (tm).tm_min=(int)(60*((db)-(tm).tm_hour)); \
                (tm).tm_sec=(int)(3600*((db)-((tm).tm_hour+(tm).tm_min/60.0))); \
                D2STimeT=mktime(&tm); \
                (tm)=*gmtime(&D2STimeT); \
              }

/**
*** \brief Create a unit type pointer from a unit type index.
*** \details Convert a unit type index unto a unit type pointer.
*** \param ut Unit type index.
*** \param up Storage for unit type pointer.
*** \param us Storage for unit type array element count.
*** \retval up Unit type pointer.
*** \retval us Count of elements in unit type array.
*** \hideinitializer
**/
#define   UNITTYPE2UNITPOINTERANDSIZE(ut,up,us) \
              (up)=nullptr; \
              (us)=0; \
              switch(ut) \
              { \
                case UNITTYPE_NONE: \
                case UNITTYPE_DATETIME: \
                case UNITTYPE_TIME: \
                default: \
                  break; \
                case UNITTYPE_AGE: \
                  (up)=f_pAgeUnits; \
                  (us)=ARRAY_ELEMENTCOUNT(f_pAgeUnits); \
                  break; \
                case UNITTYPE_ANGLE: \
                  (up)=f_pAngleUnits; \
                  (us)=ARRAY_ELEMENTCOUNT(f_pAngleUnits); \
                  break; \
                case UNITTYPE_ARC: \
                  (up)=f_pArcUnits; \
                  (us)=ARRAY_ELEMENTCOUNT(f_pArcUnits); \
                  break; \
                case UNITTYPE_DISTANCE: \
                  (up)=f_pDistanceUnits; \
                  (us)=ARRAY_ELEMENTCOUNT(f_pDistanceUnits); \
                  break; \
                case UNITTYPE_PERCENT: \
                  (up)=f_pPercentUnits; \
                  (us)=ARRAY_ELEMENTCOUNT(f_pPercentUnits); \
                  break; \
              }

/**
*** \brief Create a unit format pointer from a unit type index.
*** \details Convert a unit type index unto a unit format pointer.
*** \param ut Unit type index.
*** \param fp Storage for unit format pointer.
*** \param fs Storage for unit format array element count.
*** \retval fp Unit format pointer.
*** \retval fs Count of elements in unit format array.
*** \hideinitializer
**/
#define   UNITTYPE2FORMATPOINTERANDSIZE(ut,fp,fs) \
              (fp)=nullptr; \
              (fs)=0; \
              switch(ut) \
              { \
                case UNITTYPE_NONE: \
                case UNITTYPE_AGE: \
                case UNITTYPE_ANGLE: \
                case UNITTYPE_ARC: \
                case UNITTYPE_DISTANCE: \
                case UNITTYPE_PERCENT: \
                case UNITTYPE_TIME: \
                default: \
                  break; \
                case UNITTYPE_DATETIME: \
                  (fp)=f_pDateTimeFormatList; \
                  (fs)=ARRAY_ELEMENTCOUNT(f_pDateTimeFormatList); \
                  break; \
              }

/**
*** \brief Age information index.
*** \details Information index for age data.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_AGE                (0)

/**
*** \brief Altitude information index.
*** \details Information index for altitude.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_ALTITUDE           (1)

/**
*** \brief Azimuth information index.
*** \details Information index for azimuth.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_AZIMUTH            (2)

/**
*** \brief Universal time information index.
*** \details Information index for universal time.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_DATETIME           (3)

/**
*** \brief Declination information index.
*** \details Information index for declination.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_DECLINATION        (4)

/**
*** \brief Distance from earth information index.
*** \details Information index for distance from earth.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_DISTANCEFROMEARTH  (5)

/**
*** \brief Illumination information index.
*** \details Information index for illumination.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_ILLUMINATION       (6)

/**
*** \brief Next new moon information index.
*** \details Information index for the next new moon.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_NEXTNEWMOON        (7)

/**
*** \brief Next first quarter information index.
*** \details Information index for the next first quarter.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_NEXTFIRSTQUARTER   (8)

/**
*** \brief Next full moon information index.
*** \details Information index for the next full moon.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_NEXTFULLMOON       (9)

/**
*** \brief Next last quarter moon information index.
*** \details Information index for the next last quarter moon.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_NEXTLASTQUARTER    (10)

/**
*** \brief Phase percent information index.
*** \details Information index for phase percent.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_PHASEPERCENT       (11)

/**
*** \brief Phase text information index.
*** \details Information index for phase text.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_PHASETEXT          (12)

/**
*** \brief Right ascention information index.
*** \details Information index for right ascention.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_RIGHTASCENTION     (13)

/**
*** \brief Todays rise information index.
*** \details Information index for todays rise.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_TODAYSRISE         (14)

/**
*** \brief Todays set information index.
*** \details Information index for todays set.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_TODAYSSET          (15)

/**
*** \brief Tomorrows rise information index.
*** \details Information index for tomorrows rise.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_TOMORROWSRISE      (16)

/**
*** \brief Tomorrows set information index.
*** \details Information index for tomorrows set.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_TOMORROWSSET       (17)

/**
*** \brief Visibility information index.
*** \details Information index for visibility.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_VISIBLE            (18)

/**
*** \brief Yesterdays rise information index.
*** \details Information index for yesterdays rise.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_YESTERDAYSRISE     (19)

/**
*** \brief Yesterdays set information index.
*** \details Information index for yesterdays set.
*** \note The value of this define MUST match the data in the corresponding
***   index in the f_pInformation table.
**/
#define   INFOTYPE_YESTERDAYSSET      (20)


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Unit flags.
*** \details Modifies the display of units.
**/
typedef enum enumUNITFLAGS
{
  UNITFLAGS_NONE,               /**< No unit modifications. **/
  UNITFLAGS_DEFAULTMETRIC=1,    /**< Default metric unit. **/
  UNITFLAGS_DEFAULTSTANDARD=2,  /**< Default standard unit. **/
  UNITFLAGS_DONTSHOW=4,         /**< Hide display of unit. **/
  UNITFLAGS_HIDEFIRSTSPACE=8    /**< Remove space between data and unit. **/
} UNITFLAGS_F;

/**
*** \brief Unit data.
*** \details Defines various data about a measurement unit.
**/
typedef struct structUNITDATA
{
  wchar_t const *pShortName;
      /**< Short name (abbreviation) for the unit. **/
  char const *pLongName;
      /**< Long name for the unit. **/
  double Scale;
      /**< Scale factor used to convert between internal units and user units. **/
  int Precision;
      /**< How many digits to print after the decimal point. **/
  UNITFLAGS_F UnitFlags;
      /**< Default unit flags (metric, standard, none). **/
} UNITDATA_T;

/**
*** \brief Unit types.
*** \details Types of units used by information items.
**/
typedef enum enumUNITTYPE
{
  UNITTYPE_NONE,      /**< No unit. **/
  UNITTYPE_AGE,       /**< Age unit (days, etc.). **/
  UNITTYPE_ANGLE,     /**< Angle unit (degrees, radians, etc.). **/
  UNITTYPE_ARC,       /**< Arc unit (arc hours/minutes/seconds, etc.). **/
  UNITTYPE_DATETIME,  /**< Date and time unit (year, month, hour, etc.). **/
  UNITTYPE_DISTANCE,  /**< Distance unit (miles, kilometers, etc.). **/
  UNITTYPE_PERCENT,   /**< Percent unit. **/
  UNITTYPE_TIME       /**< Time unit (hour, minute, seconds, etc.). **/
} UNITTYPE_E;

/**
*** \brief Moon data item.
*** \details Defines a label, possible unit list, the dialog edit mode, and a
***   needs coordinates flag for each item in the data list.
**/
typedef struct structINFORMATIONDATA
{
  char const *pLabel;
      /**< Label for the displayed data. **/
  UNITTYPE_E UnitType;
      /**< Type of unit that this item can use to display information. **/
  EDITMODE_T EditMode;
      /**< Determine what data is editable (modifiable) and how to edit it. **/
  bool NeedsCoordinatesFlag;
      /**< Needs coordinates for accurate results. **/
} INFORMATIONDATA_T;

/**
*** \brief Time/date description and strftime() equivalent format.
*** \details Converts a time/date description into a format that strftime() can
***   use to create the equivalent format.
**/
typedef struct structDATETIMEDATA
{
  char const *pName;
      /**< Descriptive name for the display format. **/
  char const *pFormat;
      /**< strftime() format specification for the display format. **/
} DATETIMEDATA_T;

/**
*** \brief Moon phase percentage names and ranges.
*** \details Names and ranges for moon phase percentages.
**/
typedef struct structPHASENAMES
{
  short Min;
      /**< Minimum value for this label. **/
  short Max;
      /**< Maximum (up to but not including) value for this label. **/
  char const *pName;
      /**< Label. **/
} PHASENAMES_T;


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Convert degrees to arc hours, minutes, and seconds.
*** \details Converts degrees to arc hours, minutes, and seconds.
*** \param DTime Degrees.
*** \param Precision Number of decimal places.
*** \returns A string containing the arc hours, minutes, and seconds conversion
***   of the degrees.
**/
static std::string PrintArcHMS(double DTime,int Precision);

/**
*** \brief Convert std::wstring to std::string.
*** \details Converts a wide character string to a "normal" character string.
*** \param WString Wide string.
*** \return String equivalent of wide string.
**/
static std::string WideStringToString(const std::wstring& WString);


/****
*****
***** DATA
*****
****/

/**
*** \brief Age units.
*** \details Units used by data items that are ages.
**/
static const UNITDATA_T f_pAgeUnits[]=
{
  { L"days","Days",1.0,1,
      (UNITFLAGS_F)(UNITFLAGS_DEFAULTMETRIC|UNITFLAGS_DEFAULTSTANDARD) }
};

/**
*** \brief Angle units.
*** \details Units used by data items that are angles.
**/
static const UNITDATA_T f_pAngleUnits[]=
{
  { L"\u00B0","Degrees",1.0,2,
      (UNITFLAGS_F)(UNITFLAGS_DEFAULTMETRIC|
      UNITFLAGS_DEFAULTSTANDARD|UNITFLAGS_HIDEFIRSTSPACE) },
  { L"\u33AD","Radians",M_PI/180.0,5,
      (UNITFLAGS_F)(UNITFLAGS_NONE|UNITFLAGS_HIDEFIRSTSPACE) }
};

/**
*** \brief Arc units.
*** \details Units used by data item that are arc hours.
**/
static const UNITDATA_T f_pArcUnits[]=
{
  { L"hms","Hours, minutes, seconds",1.0,2,
      (UNITFLAGS_F)(UNITFLAGS_DEFAULTMETRIC|UNITFLAGS_DEFAULTSTANDARD|
      UNITFLAGS_DONTSHOW) }
};

/**
*** \brief Distance units.
*** \details Units used by data items that are distances.
**/
static const UNITDATA_T f_pDistanceUnits[]=
{
  { L"ft","Feet",5280.0,0,UNITFLAGS_NONE },
  { L"m","Meters",1609.344,0,UNITFLAGS_NONE },
  { L"km","Kilometers",1.609344,0,UNITFLAGS_DEFAULTMETRIC },
  { L"mi","Miles",1.0,0,UNITFLAGS_DEFAULTSTANDARD },
  { L"au","Astronomical units",1.0758e-8,5,UNITFLAGS_NONE }
};

/**
*** \brief Percent units.
*** \details Units used by data items that are percents.
**/
static const UNITDATA_T f_pPercentUnits[]=
{
  { L"%","Percent",1.0,1,(UNITFLAGS_F)(UNITFLAGS_DEFAULTMETRIC|
      UNITFLAGS_DEFAULTSTANDARD|UNITFLAGS_HIDEFIRSTSPACE) }
};

/**
*** \brief Information item data.
*** \details Data used to display and/or edit information items.
*** \note The data in this table MUST match the indices in the corresponding
***   INFOTYPE_ defines.
*** \showinitializer
**/
static const INFORMATIONDATA_T f_pInformation[]=
{
  { "Age",UNITTYPE_AGE,EDITMODE_UNITS,false },
  { "Altitude",UNITTYPE_ANGLE,EDITMODE_UNITS,true },
  { "Azimuth",UNITTYPE_ANGLE,EDITMODE_UNITS,true },
  { "Date/time",UNITTYPE_DATETIME,EDITMODE_DATETIME,false },
  { "Declination",UNITTYPE_ARC,EDITMODE_UNITS,true },
  { "Distance from earth",UNITTYPE_DISTANCE,EDITMODE_UNITS,false },
  { "Illumination (%)",UNITTYPE_PERCENT,EDITMODE_UNITS,false },
  { "Next new moon",UNITTYPE_DATETIME,EDITMODE_DATETIME,false },
  { "Next first quarter",UNITTYPE_DATETIME,EDITMODE_DATETIME,false },
  { "Next full moon",UNITTYPE_DATETIME,EDITMODE_DATETIME,false },
  { "Next last quarter",UNITTYPE_DATETIME,EDITMODE_DATETIME,false },
  { "Phase (%)",UNITTYPE_PERCENT,EDITMODE_UNITS,false },
  { "Phase (text)",UNITTYPE_NONE,EDITMODE_DISPLAY,false },
  { "Right ascention",UNITTYPE_ARC,EDITMODE_UNITS,true },
  { "Todays rise",UNITTYPE_TIME,EDITMODE_TIME,true },
  { "Todays set",UNITTYPE_TIME,EDITMODE_TIME,true },
  { "Tomorrows rise",UNITTYPE_TIME,EDITMODE_TIME,true },
  { "Tomorrows set",UNITTYPE_TIME,EDITMODE_TIME,true },
  { "Visible",UNITTYPE_NONE,EDITMODE_DISPLAY,true },
  { "Yesterdays rise",UNITTYPE_TIME,EDITMODE_TIME,true },
  { "Yesterdays set",UNITTYPE_TIME,EDITMODE_TIME,true }
};

/**
*** \brief Time/date description and strftime() equivalent format.
*** \details Converts a time/date description into a format that strftime() can
***   use to create the equivalent format.
*** \note If this structure is edited, the tests in 'test_datetime.cpp' should
***   be updated.
**/
static const DATETIMEDATA_T f_pDateTimeFormatList[]=
{
  { "M. d, y","%b. %d, %y" },   // KEEP THIS FIRST!
                                // "paneltooltipitempreferencesdialog.cpp"
                                //  depends on this format at index 0.
  { "m/d/y","%m/%d/%y" },
  { "d/m/y","%d/%m/%y" },
};

/**
*** \brief Moon phase percentage names and ranges.
*** \details Names and ranges for moon phase percentages.
**/
static const PHASENAMES_T f_pPhaseNameData[]=
{
  {  0,  2,"New moon" },
  {  2, 24,"Waxing crescent" },
  { 24, 27,"First quarter" },
  { 27, 49,"Waxing gibbous" },
  { 49, 52,"Full moon" },
  { 52, 74,"Waning gibbous" },
  { 74, 77,"Last quarter" },
  { 77, 99,"Waning crescent" },
  { 99,100,"New moon" },
};


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

static std::string PrintArcHMS(double DTime,int Precision)
{
  int H,M;
  double S;
  std::stringstream Stream;


  DEBUGLOG_Printf2("PrintArcHMS(%d,%d)",DTime,Precision);
  DEBUGLOG_LogIn();

  if ( (DTime<0) || (DTime>=360) )
  {
    DTime=(DTime-(((int)DTime/360)*360));
    if (DTime<0)
      DTime+=360;
  }

  /* Break the time into H, M, S. */
  DTime/=15.0;
  H=(int)DTime;
  M=(int)(60*(DTime-H)+.0000000005);  // Compensate for floating point errors.
  S=60*(60*DTime-60*H-M);

  Stream << abs(H) << "h ";
  Stream << abs(M) << "m ";
  Stream << std::fixed << std::setprecision(Precision) << S << "s";

  DEBUGLOG_LogOut();
  return(Stream.str());
}

static std::string WideStringToString(const std::wstring& WString)
{
  std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> Converter;


  DEBUGLOG_Printf2(
      "WideStringToString(%p(%s))",&WString,WString.c_str());
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(Converter.to_bytes(WString));
}

int Information_GetDefaultUnitFormatIndex(int InfoItemIndex,bool MetricFlag)
{
  int Return;
  UNITDATA_T const *pUnitList;
  int UnitListSize;
  unsigned int DefaultUnitFlag;


  DEBUGLOG_Printf2(
      "GetDefaultUnitFormatIndex(%d,%u)",InfoItemIndex,MetricFlag);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds");
    Return=-1;
  }
  else
  {
    UNITTYPE2UNITPOINTERANDSIZE(
        f_pInformation[InfoItemIndex].UnitType,pUnitList,UnitListSize);

    /* Unit list. */
    if (pUnitList!=nullptr)
    {
      /* Set up the default mask. */
      if (MetricFlag==true)
        DefaultUnitFlag=UNITFLAGS_DEFAULTMETRIC;
      else
        DefaultUnitFlag=UNITFLAGS_DEFAULTSTANDARD;

      /* Loop through the list, looking for a matching default. */
      Return=-1;
      for(int Index=0;Index<UnitListSize;Index++)
      {
        if ( (pUnitList[Index].UnitFlags&DefaultUnitFlag)!=0 )
        {
          Return=Index;
          break;
        }
      }
      if (Return==-1)
      {
        MESSAGELOG_Error("Invalid data");
      }
    }
    /* Format list. */
    else
      Return=0;
  }

  DEBUGLOG_LogOut();
  return(Return);
}

EDITMODE_T Information_GetEditMode(int InfoItemIndex)
{
  EDITMODE_T EditMode;


  DEBUGLOG_Printf1("Information_GetEditMode(%d,%p)",InfoItemIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds.");
    EditMode=EDITMODE_INVALID;
  }
  else
    EditMode=f_pInformation[InfoItemIndex].EditMode;

  DEBUGLOG_LogOut();
  return(EditMode);
}

int Information_GetInformationIndex(std::string const &Label)
{
  int Index;


  for(Index=0;Index<ARRAY_ELEMENTCOUNT(f_pInformation);Index++)
    if (Label==f_pInformation[Index].pLabel)
      break;
  if (Index==ARRAY_ELEMENTCOUNT(f_pInformation))
    Index=-1;

  return(Index);
}

std::string Information_GetInformationLabel(int InfoItemIndex)
{
  std::string Label;


  DEBUGLOG_Printf1("Information_GetInformationLabel(%d)",InfoItemIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds.");
  }
  else
    Label=f_pInformation[InfoItemIndex].pLabel;

  DEBUGLOG_LogOut();
  return(Label);
}

std::vector<std::string> Information_GetInformationLabels(void)
{
  std::vector<std::string> List;


  DEBUGLOG_Printf0("Information_GetLabels()");
  DEBUGLOG_LogIn();

  for(int Index=0;Index<ARRAY_ELEMENTCOUNT(f_pInformation);Index++)
    List.push_back(f_pInformation[Index].pLabel);

  DEBUGLOG_LogOut();
  return(List);
}

bool Information_GetNeedsCoordinatesFlag(int InfoItemIndex)
{
  bool Return;


  DEBUGLOG_Printf1("Information_GetNeedsCoordinatesFlag(%d)",InfoItemIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds.");
    Return=false;
  }
  else
    Return=f_pInformation[InfoItemIndex].NeedsCoordinatesFlag;

  DEBUGLOG_LogOut();
  return(Return);
}

std::vector<std::string>
    Information_GetUnitFormatDescriptionList(int InfoItemIndex)
{
  std::vector<std::string> List;
  UNITDATA_T const *pUnitList;
  int UnitListSize;
  DATETIMEDATA_T const *pFormatList;
  int FormatListSize;


  DEBUGLOG_Printf1(
      "Information_GetUnitFormatDescriptionList(%d)",InfoItemIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds.");
  }
  else
  {
    UNITTYPE2UNITPOINTERANDSIZE(
        f_pInformation[InfoItemIndex].UnitType,pUnitList,UnitListSize);
    UNITTYPE2FORMATPOINTERANDSIZE(f_pInformation[InfoItemIndex].UnitType,
        pFormatList,FormatListSize);
    if (pUnitList!=nullptr)
    {
      for(int Index=0;Index<UnitListSize;Index++)
        List.push_back(WideStringToString(pUnitList[Index].pShortName)+
            " ("+pUnitList[Index].pLongName+")");
    }
    else if (pFormatList!=nullptr)
    {
      for(int Index=0;Index<FormatListSize;Index++)
        List.push_back(pFormatList[Index].pName);
    }
  }

  DEBUGLOG_LogOut();
  return(List);
}

int Information_GetUnitFormatIndex(int InfoItemIndex,std::string const &Label)
{
  int Return;
  int Index;
  UNITDATA_T const *pUnitList;
  int UnitListSize;
  DATETIMEDATA_T const *pFormatList;
  int FormatListSize;


  Return=-1;
  if (InfoItemIndex>=0)
  {
    UNITTYPE2UNITPOINTERANDSIZE(
        f_pInformation[InfoItemIndex].UnitType,pUnitList,UnitListSize);
    UNITTYPE2FORMATPOINTERANDSIZE(f_pInformation[InfoItemIndex].UnitType,
        pFormatList,FormatListSize);
    if (pUnitList!=nullptr)
    {
      for(Index=0;Index<UnitListSize;Index++)
        if (Label==pUnitList[Index].pLongName)
        {
          Return=Index;
          break;
        }
    }
    else if (pFormatList!=nullptr)
    {
      for(Index=0;Index<FormatListSize;Index++)
        if (Label==pFormatList[Index].pName)
        {
          Return=Index;
          break;
        }
    }
    else
      Return=0;
  }

  return(Return);
}

std::string Information_GetUnitFormatLabel(
    int InfoItemIndex,int UnitFormatIndex)
{
  std::string UnitFormatName;
  UNITDATA_T const *pUnitList;
  int UnitListSize;
  DATETIMEDATA_T const *pFormatList;
  int FormatListSize;


  DEBUGLOG_Printf2(
      "Information_GetUnitFormatLabel(%d,%d)",InfoItemIndex,UnitFormatIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (InfoItemIndex<0) ||
      (InfoItemIndex>=ARRAY_ELEMENTCOUNT(f_pInformation)) )
  {
    MESSAGELOG_Error("Index out of bounds.");
  }
  else
  {
    UNITTYPE2UNITPOINTERANDSIZE(
        f_pInformation[InfoItemIndex].UnitType,pUnitList,UnitListSize);
    UNITTYPE2FORMATPOINTERANDSIZE(
        f_pInformation[InfoItemIndex].UnitType,pFormatList,FormatListSize);
    if (pUnitList!=nullptr)
    {
      if ( (UnitFormatIndex<0) || (UnitFormatIndex>=UnitListSize) )
      {
        MESSAGELOG_Error("Index out of bounds.");
      }
      else
        UnitFormatName=pUnitList[UnitFormatIndex].pLongName;
    }
    else if (pFormatList!=nullptr)
    {
      if ( (UnitFormatIndex<0) || (UnitFormatIndex>=FormatListSize) )
      {
        MESSAGELOG_Error("Index out of bounds.");
      }
      else
        UnitFormatName=pFormatList[UnitFormatIndex].pName;
    }
  }

  DEBUGLOG_LogOut();
  return(UnitFormatName);
}

std::string Information_Print(MOONDATA_T const &MoonData,int InfoItemIndex,
    int UnitFormatIndex,DATETIMEOPTIONS_C const &Options)
{
  UNITDATA_T const *pUnitList;
  int UnitListSize;
  DATETIMEDATA_T const *pFormatList;
  int FormatListSize;
  std::stringstream Stream;
  std::wstring UnitFormat;
  UNITFLAGS_F UnitFlags;
  time_t TimeT;
  short Percent;
  struct tm tmTime;
  std::string Return;


  DEBUGLOG_Printf4("Information_Print(%p,%d,%d,%p)",
      &MoonData,InfoItemIndex,UnitFormatIndex,&Options);
  DEBUGLOG_LogIn();

  /* Get the unit/format table pointers and sizes. */
  UNITTYPE2UNITPOINTERANDSIZE(
      f_pInformation[InfoItemIndex].UnitType,pUnitList,UnitListSize);
  UNITTYPE2FORMATPOINTERANDSIZE(
      f_pInformation[InfoItemIndex].UnitType,pFormatList,FormatListSize);
  UnitFlags=UNITFLAGS_NONE;

  /* Bounds check the unit/format index. */
  if ( ((pUnitList!=nullptr) &&
      ((UnitFormatIndex<0) || (UnitFormatIndex>=UnitListSize))) ||
      ((pFormatList!=nullptr) &&
      ((UnitFormatIndex<0) || (UnitFormatIndex>=FormatListSize))) )
    MESSAGELOG_Error("Index out of bounds.");
  else
  {
    switch(InfoItemIndex)
    {
      case INFOTYPE_AGE:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed <<
              std::setprecision(pUnitList[UnitFormatIndex].Precision) <<
              MoonData.CTransData.MoonAge;
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_ALTITUDE:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed <<
              std::setprecision(pUnitList[UnitFormatIndex].Precision) <<
              MoonData.CTransData.h_moon*pUnitList[UnitFormatIndex].Scale;
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_AZIMUTH:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed <<
              std::setprecision(pUnitList[UnitFormatIndex].Precision) <<
              MoonData.CTransData.A_moon*pUnitList[UnitFormatIndex].Scale;
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_DECLINATION:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << PrintArcHMS(MoonData.CTransData.DEC_moon,
              pUnitList[UnitFormatIndex].Precision).c_str();
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_DISTANCEFROMEARTH:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed <<
              std::setprecision(pUnitList[UnitFormatIndex].Precision) <<
              MoonData.CTransData.EarthMoonDistance*EARTH_RADIUS*
              pUnitList[UnitFormatIndex].Scale;
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_ILLUMINATION:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed <<
              std::setprecision(pUnitList[UnitFormatIndex].Precision) <<
              100*0.5*(1.0-cos(MoonData.CTransData.MoonPhase*2*M_PI));
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_NEXTNEWMOON:
        if (pFormatList==nullptr)
        {
          MESSAGELOG_Error("pFormatList==nullptr");
        }
        else
        {
          tmTime=*gmtime(&MoonData.NextNewMoonGMt);
          Stream << DateTime_Print(!0,&tmTime,
              pFormatList[UnitFormatIndex].pFormat,Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_NEXTFIRSTQUARTER:
        if (pFormatList==nullptr)
        {
          MESSAGELOG_Error("pFormatList==nullptr");
        }
        else
        {
          tmTime=*gmtime(&MoonData.NextFirstQuarterMoonGMt);
          Stream << DateTime_Print(!0,&tmTime,
              pFormatList[UnitFormatIndex].pFormat,Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_NEXTFULLMOON:
        if (pFormatList==nullptr)
        {
          MESSAGELOG_Error("pFormatList==nullptr");
        }
        else
        {
          tmTime=*gmtime(&MoonData.NextFullMoonGMt);
          Stream << DateTime_Print(!0,&tmTime,
              pFormatList[UnitFormatIndex].pFormat,Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_NEXTLASTQUARTER:
        if (pFormatList==nullptr)
        {
          MESSAGELOG_Error("pFormatList==nullptr");
        }
        else
        {
          tmTime=*gmtime(&MoonData.NextLastQuarterMoonGMt);
          Stream << DateTime_Print(!0,&tmTime,
              pFormatList[UnitFormatIndex].pFormat,Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_PHASEPERCENT:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << std::fixed
              << std::setprecision(pUnitList[UnitFormatIndex].Precision)
              << MoonData_GetMoonPhasePercent(&MoonData);
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_PHASETEXT:
        if (UnitFormatIndex==0)
        {
          Percent=(short)MoonData_GetMoonPhasePercent(&MoonData);
          for(int Index=0;Index<ARRAY_ELEMENTCOUNT(f_pPhaseNameData);Index++)
            if ( (Percent>=f_pPhaseNameData[Index].Min)
                && (Percent<f_pPhaseNameData[Index].Max) )
            {
              Stream << f_pPhaseNameData[Index].pName;
              break;
            }
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_RIGHTASCENTION:
        if (pUnitList==nullptr)
        {
          MESSAGELOG_Error("pUnitList==nullptr");
        }
        else
        {
          Stream << PrintArcHMS(MoonData.CTransData.RA_moon,
              pUnitList[UnitFormatIndex].Precision);
          UnitFormat=pUnitList[UnitFormatIndex].pShortName;
          UnitFlags=pUnitList[UnitFormatIndex].UnitFlags;
        }
        break;
      case INFOTYPE_DATETIME:
        if (pFormatList==nullptr)
        {
          MESSAGELOG_Error("pFormatList==nullptr");
        }
        else
        {
          time(&TimeT);
          tmTime=*gmtime(&TimeT);
          Stream << DateTime_Print(!0,&tmTime,
              pFormatList[UnitFormatIndex].pFormat,Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_TODAYSRISE:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.TodaysRiseLT,tmTime);
          if (MoonData.TodaysRiseLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_TODAYSSET:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.TodaysSetLT,tmTime);
          if (MoonData.TodaysSetLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_TOMORROWSRISE:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.TomorrowsRiseLT,tmTime);
          if (MoonData.TomorrowsRiseLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_TOMORROWSSET:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.TomorrowsSetLT,tmTime);
          if (MoonData.TomorrowsSetLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_VISIBLE:
        if (UnitFormatIndex==0)
        {
          if (MoonData.CTransData.Visible==0)
            Stream << "No";
          else
            Stream << "Yes";
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_YESTERDAYSRISE:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.YesterdaysRiseLT,tmTime);
          if (MoonData.YesterdaysRiseLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      case INFOTYPE_YESTERDAYSSET:
        if (UnitFormatIndex==0)
        {
          DOUBLE2STRUCTTM(MoonData.YesterdaysSetLT,tmTime);
          if (MoonData.YesterdaysSetLT==-999)
            Stream << "None";
          else
            Stream << DateTime_Print(0,&tmTime,std::string(),Options);
          UnitFlags=UNITFLAGS_DONTSHOW;
        }
        break;
      default:
        MESSAGELOG_Error("Invalid data");
        break;
    }

    if (!Stream.str().empty())
    {
      std::string Separator;


      if ( (UnitFlags&UNITFLAGS_DONTSHOW)!=0 )
        UnitFormat=L"";

      if ( (UnitFlags&(UNITFLAGS_DONTSHOW|UNITFLAGS_HIDEFIRSTSPACE))!=0 )
        Separator="";
      else
        Separator=" ";
      Return=Stream.str()+Separator+WideStringToString(UnitFormat).c_str();
    }
  }

  DEBUGLOG_LogOut();
  return(Return);
}


#undef    INFORMATION_CPP


/**
*** information.cpp
**/

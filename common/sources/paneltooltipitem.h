/**
*** \file paneltooltipitem.h
*** \brief TODO
*** \details TODO
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELTOOLTIPITEM_H)
/**
*** \internal
*** \brief paneltooltipitem.h identifier.
*** \details Identifier for paneltooltipitem.h.
*** \endinternal
**/
#define   PANELTOOLTIPITEM_H


/****
*****
***** INCLUDES
*****
****/

#include  "datetime.h"
#include  "commontypes.h"
#include  "preferencesmanager.h"

#include  <memory>
#include  <vector>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief TODO Information item base class.
*** \details TODO Base class for information items.
**/
class PANELTOOLTIPITEM_C : public DATETIMEOPTIONS_C
{
  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    PANELTOOLTIPITEM_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param Item TODO
    **/
    PANELTOOLTIPITEM_C(PANELTOOLTIPITEM_C const &Item);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    virtual ~PANELTOOLTIPITEM_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Item Object on right hand side of operator.
    *** \return Copy of data of object on right hand side.
    **/
    PANELTOOLTIPITEM_C const & operator=(PANELTOOLTIPITEM_C const &Item);

    /**
    *** \brief == operator.
    *** \details Equality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are equivalent.
    *** \retval false Objects are not equivalent.
    **/
    bool operator==(PANELTOOLTIPITEM_C const &Item) const;

    /**
    *** \brief != operator.
    *** \details Inequality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are not equivalent.
    *** \retval false Objects are equivalent.
    **/
    bool operator!=(PANELTOOLTIPITEM_C const &Item) const;

    /**
    *** \brief Return information index.
    *** \details Returns the information index. This value is an index into the
    ***   information table, which contains type of data to be edited and/or
    ***   displayed.
    *** \return Information index.
    **/
    int GetInformationIndex(void) const;

    /**
    *** \brief Return unit or format index.
    *** \details Returns the index of the display unit or the date/time format
    ***   index.
    *** \return Unit or format index.
    **/
    int GetUnitFormatIndex(void) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param pManager TODO
    **/
    virtual void Load(PREFERENCESMANAGER_C *pManager);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param pManager TODO
    **/
    virtual void Save(PREFERENCESMANAGER_C *pManager) const;

    /**
    *** \brief Set information index.
    *** \details Sets the information index. This value is an index into the
    ***   information table, which contains type of data to be edited and/or
    ***   displayed.
    *** \param Index Information index.
    **/
    void SetInformationIndex(int Index);

    /**
    *** \brief Set unit or format index.
    *** \details Sets the index of the display unit or the index of the
    ***   date/time format index.
    *** \param Index Unit or format index.
    **/
    void SetUnitFormatIndex(int Index);

  private:
    /**
    *** \brief Information index.
    *** \details The index into the information table.
    **/
    int m_InformationIndex;

    /**
    *** \brief Unit or format index.
    *** \details Index of the display unit or index of the date/time format.
    **/
    int m_UnitFormatIndex;
};

/**
*** \brief TODO
*** \details TODO
**/
class PANELITEM_C : public PANELTOOLTIPITEM_C
{
  public:
    /**
    *** \brief Text alignment.
    *** \details Alignment for the text in the label and data display items.
    **/
    typedef enum enumALIGNMENT
    {
      _ALIGNMENT_FIRST=0,   /**< First alignment enumeration. **/
      ALIGNMENT_CENTER=_ALIGNMENT_FIRST,
                            /**< Centers the text horizontally. **/
      ALIGNMENT_JUSTIFY=1,  /**< Text is aligned with the left edge, and
                                adjusted to fill the space. **/
      ALIGNMENT_LEFT=2,     /**< Aligns the text with the left edge. **/
      ALIGNMENT_RIGHT=3,    /**< Aligns the text with the right edge. **/
      _ALIGNMENT_LAST=ALIGNMENT_RIGHT,
                            /**< Last alignment enumeration. **/
      _ALIGNMENT_COUNT=_ALIGNMENT_LAST+1
                            /**< Number of alignment enumerations. **/
    } ALIGNMENT_E;

    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    PANELITEM_C(void);

    /**
    *** \brief Copy constructor. TODO
    *** \details Copy constructor. TODO
    *** \param Item TODO
    **/
    PANELITEM_C(PANELITEM_C const &Item);

    /**
    *** \brief Copy constructor. TODO
    *** \details Copy constructor. TODO
    *** \param Item TODO
    **/
    PANELITEM_C(PANELTOOLTIPITEM_C const &Item);

    ~PANELITEM_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Item Object on right hand side of operator.
    *** \return Copy of data of object on right hand side.
    **/
    PANELITEM_C const & operator=(PANELITEM_C const &Item);

    /**
    *** \brief == operator.
    *** \details Equality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are equivalent.
    *** \retval false Objects are not equivalent.
    **/
    bool operator==(PANELITEM_C const &Item) const;

    /**
    *** \brief != operator.
    *** \details Inequality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are not equivalent.
    *** \retval false Objects are equivalent.
    **/
    bool operator!=(PANELITEM_C const &Item) const;

    /**
    *** \brief Return background color.
    *** \details Returns the color of the background.
    *** \return Background color.
    **/
    COLOR_C const GetBackgroundColor(void) const;

    /**
    *** \brief Return center padding.
    *** \details Returns the padding used between the label and data parts of
    ***   the item.
    *** \return Center padding.
    **/
    int GetCenterPadding(void) const;

    /**
    *** \brief Return data alignment.
    *** \details Returns the alignment of the data part of the item.
    *** \return Data alignment.
    **/
    ALIGNMENT_E GetDataAlignment(void) const;

    /**
    *** \brief Return font.
    *** \details Returns the font.
    *** \return Font.
    **/
    FONT_C GetFont(void) const;

    /**
    *** \brief Return label alignment.
    *** \details Returns the alignment of the label part of the item.
    *** \return Label alignment.
    **/
    ALIGNMENT_E GetLabelAlignment(void) const;

    /**
    *** \brief Return left/right padding.
    *** \details Returns the padding used for the left edge of the label part
    ***   and the right edge of the data part of the item.
    *** \return Left/right padding.
    **/
    int GetLeftRightPadding(void) const;

    /**
    *** \brief Return text color.
    *** \details Returns the color of the text.
    *** \return Text color.
    **/
    COLOR_C GetTextColor(void) const;

    /**
    *** \brief Return top/bottom padding.
    *** \details Returns the top/bottom padding used for both the label and data
    ***   parts of the item.
    *** \return Top/bottom padding.
    **/
    int GetTopBottomPadding(void) const;

    /**
    *** \brief Return use system background color flag.
    *** \details Returns the use system background color flag.
    *** \retval true Use system background color.
    *** \retval false Use user defined color.
    **/
    bool GetUseSystemBackgroundColorFlag(void) const;

    /**
    *** \brief Return use system font flag
    *** \details Returns the use system font flag.
    *** \retval true Use system font.
    *** \retval false Use user defined font.
    **/
    bool GetUseSystemFontFlag(void) const;

    /**
    *** \brief Return use system text color flag.
    *** \details Returns the use system text color flag.
    *** \retval true Use system text color.
    *** \retval false Use user defined color.
    **/
    bool GetUseSystemTextColorFlag(void) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param pManager TODO
    **/
    void Load(PREFERENCESMANAGER_C *pManager);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param pManager TODO
    **/
    void Save(PREFERENCESMANAGER_C *pManager) const;

    /**
    *** \brief Set background color.
    *** \details Sets the color of the background.
    *** \param Color Background color.
    **/
    void SetBackgroundColor(COLOR_C const &Color);

    /**
    *** \brief Set center padding.
    *** \details Sets the padding used between the label and data parts of the
    ***   the item.
    *** \param Padding Center padding.
    **/
    void SetCenterPadding(int Padding);

    /**
    *** \brief Set data alignment.
    *** \details Sets the alignment of the data part of the item.
    *** \param Alignment Data alignment.
    **/
    void SetDataAlignment(ALIGNMENT_E Alignment);

    /**
    *** \brief Set font.
    *** \details Sets the font.
    *** \param Font Font.
    **/
    void SetFont(FONT_C const &Font);

    /**
    *** \brief Set label alignment.
    *** \details Sets the alignment of the label part of the item.
    *** \param Alignment Label alignment.
    **/
    void SetLabelAlignment(ALIGNMENT_E Alignment);

    /**
    *** \brief Set left/right padding.
    *** \details Sets the padding used for the left edge of the label part
    ***   and the right edge of the data part of the item.
    *** \param Padding Left/right padding.
    **/
    void SetLeftRightPadding(int Padding);

    /**
    *** \brief Set text color.
    *** \details Sets the color of the text.
    *** \param Color Text color.
    **/
    void SetTextColor(COLOR_C const &Color);

    /**
    *** \brief Set top/bottom padding.
    *** \details Sets the top/bottom padding used for both the label and data
    ***   parts of the item.
    *** \param Padding Top/bottom padding.
    **/
    void SetTopBottomPadding(int Padding);

    /**
    *** \brief Set use system background color flag.
    *** \details Sets the use system background color flag.
    *** \param Flag true - Use system background color.\n
    ***   false - Use user defined color.
    **/
    void SetUseSystemBackgroundColorFlag(bool Flag);

    /**
    *** \brief Sets the use system font flag.
    *** \details Sets the use system font flag.
    *** \param Flag true - Use system background font.\n
    ***   false - Use user defined font.
    **/
    void SetUseSystemFontFlag(bool Flag);

    /**
    *** \brief Sets the use system text color flag.
    *** \details Sets the use system text color flag.
    *** \param Flag true - Use system text color.\n
    ***   false - Use user defined color.
    **/
    void SetUseSystemTextColorFlag(bool Flag);

  private:
    /**
    *** \brief Background color.
    *** \details The color of the background.
    **/
    COLOR_C m_BackgroundColor;

    /**
    *** \brief Center padding.
    *** \details The padding used between the label and data parts of the item.
    **/
    int m_CenterPadding;

    /**
    *** \brief Data alignment.
    *** \details Alignment of the data part of the item.
    **/
    ALIGNMENT_E m_DataAlignment;

    /**
    *** \brief Font.
    *** \details Text font.
    **/
    FONT_C m_Font;

    /**
    *** \brief Label alignment.
    *** \details Alignment of the label part of the item.
    **/
    ALIGNMENT_E m_LabelAlignment;

    /**
    *** \brief Left/right padding.
    *** \details The padding used for the left edge of the label part and the
    ***   right edge of the data part of the item.
    **/
    int m_LeftRightPadding;

    /**
    *** \brief Text color.
    *** \details The color of the text.
    **/
    COLOR_C m_TextColor;

    /**
    *** \brief Top/bottom padding.
    *** \details Returns the top/bottom padding used for both the label and data
    ***   parts of the item.
    **/
    int m_TopBottomPadding;

    /**
    *** \brief Use system background color flag.
    *** \details Use the system background color flag.
    **/
    bool m_UseSystemBackgroundColorFlag;

    /**
    *** \brief Use system font flag.
    *** \details Use the system font flag.
    **/
    bool m_UseSystemFontFlag;

    /**
    *** \brief Use system text color flag.
    *** \details Use the system text color flag.
    **/
    bool m_UseSystemTextColorFlag;
};

/**
*** \brief TODO
*** \details TODO
**/
class TOOLTIPITEM_C : public PANELTOOLTIPITEM_C
{
  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    TOOLTIPITEM_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param Item TODO
    **/
    TOOLTIPITEM_C(PANELTOOLTIPITEM_C const &Item);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param Item TODO
    **/
    TOOLTIPITEM_C(TOOLTIPITEM_C const &Item);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~TOOLTIPITEM_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Item Object on right hand side of operator.
    *** \return Copy of data of object on right hand side.
    **/
    TOOLTIPITEM_C const & operator=(TOOLTIPITEM_C const &Item);

    /**
    *** \brief == operator.
    *** \details Equality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are equivalent.
    *** \retval false Objects are not equivalent.
    **/
    bool operator==(TOOLTIPITEM_C const &Item) const;

    /**
    *** \brief != operator.
    *** \details Inequality operator.
    *** \param Item Object on right hand side of operator.
    *** \retval true Objects are not equivalent.
    *** \retval false Objects are equivalent.
    **/
    bool operator!=(TOOLTIPITEM_C const &Item) const;
};

/**
*** \brief TODO
*** \details TODO
**/
class PANELTOOLTIPITEMLIST_C : public PREFERENCESMANAGER_C
{
  public:
    /**
    *** \brief TODO
    *** \details TODO
    **/
    typedef PANELTOOLTIPITEM_C * BASETYPE_C;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    typedef std::vector<BASETYPE_C> TYPE_C;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    typedef TYPE_C::size_type SIZETYPE_T;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    typedef TYPE_C::iterator ITERATOR_C;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    typedef TYPE_C::const_iterator CONSTITERATOR_C;

    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    PANELTOOLTIPITEMLIST_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELTOOLTIPITEMLIST_C(PANELTOOLTIPITEMLIST_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    virtual ~PANELTOOLTIPITEMLIST_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELTOOLTIPITEMLIST_C const & operator=(
        PANELTOOLTIPITEMLIST_C const &Widget)=delete;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    *** \retval true TODO
    *** \retval false TODO
    **/
    bool operator==(PANELTOOLTIPITEMLIST_C const &List) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Index TODO
    *** \returns TODO
    **/
    PANELTOOLTIPITEM_C * operator[](SIZETYPE_T Index) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    **/
    void Append(PANELTOOLTIPITEM_C const &Item);

    /**
    *** \brief TODO
    *** \details TODO
    *** \returns TODO
    **/
    CONSTITERATOR_C Begin(void) const;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    void Clear(void);

    /**
    *** \brief TODO
    *** \details TODO
    *** \returns TODO
    **/
    CONSTITERATOR_C End(void) const;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    virtual void Load(void)=0;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Index TODO
    **/
    void Remove(SIZETYPE_T Index);

    /**
    *** \brief TODO
    *** \details TODO
    **/
    virtual void Save(void)=0;

    /**
    *** \brief TODO
    *** \details TODO
    *** \returns TODO
    **/
    SIZETYPE_T Size(void) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Index0 TODO
    *** \param Index1 TODO
    **/
    void Swap(SIZETYPE_T Index0,SIZETYPE_T Index1);

  private:
    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    *** \returns TODO
    **/
    virtual BASETYPE_C NewItem(PANELTOOLTIPITEM_C const &Item) const=0;

  protected:
    /**
    *** \brief TODO
    *** \details TODO
    **/
    std::unique_ptr<TYPE_C> m_pList;
};

/**
*** \brief TODO
*** \details TODO
**/
class PANELITEMLIST_C : public PANELTOOLTIPITEMLIST_C
{
  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    PANELITEMLIST_C(void);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    **/
    PANELITEMLIST_C(PANELITEMLIST_C const &List);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
//    ~PANELITEMLIST_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELITEMLIST_C const & operator=(PANELITEMLIST_C const &Widget)=delete;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    *** \retval true TODO
    *** \retval false TODO
    **/
    bool operator==(PANELITEMLIST_C const &List) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    *** \retval true TODO
    *** \retval false TODO
    **/
    bool operator!=(PANELITEMLIST_C const &List) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Index TODO
    *** \returns TODO
    **/
    PANELITEM_C * operator[](SIZETYPE_T Index) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    **/
    void Append(PANELITEM_C const &Item);

    /**
    *** \brief TODO
    *** \details TODO
    **/
    void Load(void);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    *** \returns TODO
    **/
    BASETYPE_C NewItem(PANELTOOLTIPITEM_C const &Item) const;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    void Save(void);
};

/**
*** \brief TODO
*** \details TODO
**/
class TOOLTIPITEMLIST_C : public PANELTOOLTIPITEMLIST_C
{
  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    TOOLTIPITEMLIST_C(void);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    **/
    TOOLTIPITEMLIST_C(TOOLTIPITEMLIST_C const &List);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    TOOLTIPITEMLIST_C const & operator=(TOOLTIPITEMLIST_C const &Widget)=delete;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    *** \retval true TODO
    *** \retval false TODO
    **/
    bool operator==(TOOLTIPITEMLIST_C const &List) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param List TODO
    *** \retval true TODO
    *** \retval false TODO
    **/
    bool operator!=(TOOLTIPITEMLIST_C const &List) const;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    **/
    void Append(TOOLTIPITEM_C const &Item);

    /**
    *** \brief TODO
    *** \details TODO
    **/
    void Load(void);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param Item TODO
    *** \returns TODO
    **/
    BASETYPE_C NewItem(PANELTOOLTIPITEM_C const &Item) const;

    /**
    *** \brief TODO
    *** \details TODO
    **/
    void Save(void);
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELTOOLTIPITEM_H) */


/**
*** paneltooltipitem.h
**/

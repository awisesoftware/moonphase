/**
*** \file test_class_COLOR.cpp
*** \brief COLOR_C unit tests.
*** \details Unit tests for COLOR_C.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Class__COLOR)

  static float const fc_Range=COLOR_C::mc_Maximum-COLOR_C::mc_Minimum;
  static float const fc_Step=fc_Range/256.0;
  static float const fc_Tolerance=fc_Range/1000;
  static float const fc_Normal=(COLOR_C::mc_Maximum+COLOR_C::mc_Minimum)/2;
  static float const fc_OverMinimum=COLOR_C::mc_Minimum+fc_Step;
  static float const fc_OverMaximum=COLOR_C::mc_Maximum+fc_Step;
  static float const fc_UnderMaximum=COLOR_C::mc_Maximum-fc_Step;
  static float const fc_UnderMinimum=COLOR_C::mc_Minimum-fc_Step;

  BOOST_AUTO_TEST_SUITE(Function__DefaultConstructor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color;


      BOOST_TEST(Color.GetAlpha()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetBlue()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetGreen()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetRed()==COLOR_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Constructor_float_float_float_float)
    /* One parameter under minimum. */
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
    /* Default parameter for alpha channel. */
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    /* All parameters in range. */
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    /* One parameter over minimum. */
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      COLOR_C Color(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_OverMinimum,fc_Tolerance);
    }
    /* One parameter under maximum. */
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      COLOR_C Color(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      COLOR_C Color(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_0C)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_0D)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_UnderMaximum,fc_Tolerance);
    }
    /* One parameter at maximum. */
    BOOST_AUTO_TEST_CASE(Test_0E)
    {
      COLOR_C Color(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_0F)
    {
      COLOR_C Color(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    /* One parameter over maximum. */
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      COLOR_C Color(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      COLOR_C Color(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_15)
    {
      COLOR_C Color(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__EqualityOperator_InequalityOperator)
    BOOST_AUTO_TEST_CASE(Test_0000)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0001)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0002)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0003)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0004)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0005)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0006)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0007)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0008)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_009)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_000F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0010)
    {
      COLOR_C Color0(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0011)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0012)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0013)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0014)
    {
      COLOR_C Color0(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0015)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0016)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0017)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0100)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0101)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0102)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0103)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0104)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0105)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0106)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0107)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0108)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0109)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_010F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0110)
    {
      COLOR_C Color0(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0111)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0112)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0113)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0114)
    {
      COLOR_C Color0(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0115)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0116)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0117)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0200)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0201)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0202)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0203)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0204)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0205)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0206)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0207)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0208)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0209)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_020F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0210)
    {
      COLOR_C Color0(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0211)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0212)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0213)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0214)
    {
      COLOR_C Color0(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0215)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0216)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0217)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0300)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0301)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0302)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0303)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0304)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0305)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0306)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0307)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0308)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0309)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_030F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0310)
    {
      COLOR_C Color0(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0311)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0312)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0313)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0314)
    {
      COLOR_C Color0(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0315)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0316)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0317)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0400)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0401)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0402)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0403)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0404)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0405)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0406)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0407)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0408)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0409)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
        COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_040F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0410)
    {
      COLOR_C Color0(
          COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0411)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0412)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0413)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0414)
    {
      COLOR_C Color0(
          fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0415)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0416)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0417)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0500)
    {
      COLOR_C Color0(
          fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0501)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0502)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0503)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0504)
    {
      COLOR_C Color0(
          COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0505)
    {
      COLOR_C Color0(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0506)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0507)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0508)
    {
      COLOR_C Color0(
          fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0509)
    {
      COLOR_C Color0(
          fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050A)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050B)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050C)
    {
      COLOR_C Color0(
          fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050D)
    {
      COLOR_C Color0(
          fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050E)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_050F)
    {
      COLOR_C Color0(
          fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(
          fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0510)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0511)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0512)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0513)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0514)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0515)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0516)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0517)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0600)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0601)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0602)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0603)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0604)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0605)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0606)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0607)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0608)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0609)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_060F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0610)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0611)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0612)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0613)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0614)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0615)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0616)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0617)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0700)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0701)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0702)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0703)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0704)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0705)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0706)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0707)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0708)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0709)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_070F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0710)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0711)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0712)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0713)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0714)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0715)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0716)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0717)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0800)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0801)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0802)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0803)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0804)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0805)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0806)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0807)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0808)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0809)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_080F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0810)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0811)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0812)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0813)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0814)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0815)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0816)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0817)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0900)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0901)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0902)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0903)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0904)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0905)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0906)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0907)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0908)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0909)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_090A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_090B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_090C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_090D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_090E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_090F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0910)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0911)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0912)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0913)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0914)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0915)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0916)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0917)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0E0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0E17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F00)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F01)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F02)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F03)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F04)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F05)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F06)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F07)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F08)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F09)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F0F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0F10)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F11)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F12)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F13)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F14)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F15)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F16)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F17)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1000)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1001)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1002)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1003)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1004)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1005)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1006)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1007)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1008)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1009)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_100F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1010)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1011)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1012)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1013)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1014)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1015)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1016)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1017)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1100)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1101)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1102)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1103)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1104)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1105)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1106)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1107)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1108)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1109)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_110F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1110)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1111)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1112)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1113)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1114)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1115)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1116)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1117)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1200)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1201)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1202)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1203)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1204)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1205)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1206)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1207)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1208)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1209)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_120F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1210)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1211)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1212)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1213)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1214)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1215)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1216)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1217)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1300)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1301)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1302)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1303)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1304)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1305)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1306)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1307)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1308)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1309)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_130F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1310)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1311)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1312)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1313)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1314)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1315)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1316)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1317)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1400)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1401)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1402)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1403)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1404)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1405)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1406)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1407)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1408)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1409)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_140F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1410)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1411)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1412)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1413)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1414)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1415)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1416)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1417)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1500)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1501)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1502)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1503)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1504)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1505)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1506)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1507)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1508)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1509)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_150F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1510)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1511)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1512)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1513)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1514)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1515)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1516)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1517)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1600)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1601)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1602)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1603)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1604)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1605)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1606)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1607)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1608)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1609)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_160F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1610)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1611)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1612)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1613)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1614)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1615)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1616)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1617)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }

    BOOST_AUTO_TEST_CASE(Test_1700)
    {
      COLOR_C Color0(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1701)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1702)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1703)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1704)
    {
      COLOR_C Color0(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1705)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Minimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1706)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Minimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1707)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1708)
    {
      COLOR_C Color0(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1709)
    {
      COLOR_C Color0(fc_Normal,fc_OverMinimum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170A)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMinimum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170B)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170C)
    {
      COLOR_C Color0(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170D)
    {
      COLOR_C Color0(fc_Normal,fc_UnderMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170E)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_UnderMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_170F)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1710)
    {
      COLOR_C Color0(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1711)
    {
      COLOR_C Color0(fc_Normal,COLOR_C::mc_Maximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1712)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,COLOR_C::mc_Maximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1713)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==false);
      BOOST_TEST((Color0!=Color1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1714)
    {
      COLOR_C Color0(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1715)
    {
      COLOR_C Color0(fc_Normal,fc_OverMaximum,fc_Normal,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1716)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_OverMaximum,fc_Normal);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_1717)
    {
      COLOR_C Color0(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      COLOR_C Color1(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);


      BOOST_TEST((Color0==Color1)==true);
      BOOST_TEST((Color0!=Color1)==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetAlpha)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_UnderMinimum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Minimum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_OverMinimum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_OverMinimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_UnderMaximum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_UnderMaximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,COLOR_C::mc_Maximum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_OverMaximum);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetBlue)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(0,0,fc_UnderMinimum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(0,0,COLOR_C::mc_Minimum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(0,0,fc_OverMinimum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_OverMinimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(0,0,fc_Normal,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(0,0,fc_UnderMaximum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_UnderMaximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(0,0,COLOR_C::mc_Maximum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(0,0,fc_OverMaximum,0);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetGreen)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(0,fc_UnderMinimum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(0,COLOR_C::mc_Minimum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(0,fc_OverMinimum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_OverMinimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(0,fc_Normal,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(0,fc_UnderMaximum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_UnderMaximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(0,COLOR_C::mc_Maximum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(0,fc_OverMaximum,0,0);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetRed)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(fc_UnderMinimum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(fc_OverMinimum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),fc_OverMinimum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(fc_UnderMaximum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),fc_UnderMaximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(COLOR_C::mc_Maximum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Maximum,fc_Tolerance);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(fc_OverMaximum,fc_Normal,fc_Normal,fc_Normal);
      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Invalidate)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST(Color.IsValid()==true);

      Color.Invalidate();

      BOOST_TEST(Color.IsValid()==false);
      BOOST_TEST(Color.GetAlpha()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetBlue()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetGreen()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetRed()==COLOR_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(fc_Normal,fc_Normal,fc_Normal,fc_Normal);


      BOOST_TEST(Color.IsValid()==true);

      Color.Invalidate();

      BOOST_TEST(Color.IsValid()==false);
      BOOST_TEST(Color.GetAlpha()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetBlue()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetGreen()==COLOR_C::mc_Invalid);
      BOOST_TEST(Color.GetRed()==COLOR_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__IsValid)
    BOOST_AUTO_TEST_CASE(IsValid_00)
    {
      COLOR_C Color;


      Color.SetRed(fc_Normal);
      Color.SetGreen(fc_Normal);
      Color.SetBlue(fc_Normal);
      Color.SetAlpha(fc_Normal);

      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(IsValid_01)
    {
      COLOR_C Color;


      Color.SetGreen(fc_Normal);
      Color.SetBlue(fc_Normal);
      Color.SetAlpha(fc_Normal);

      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(IsValid_02)
    {
      COLOR_C Color;


      Color.SetRed(fc_Normal);
      Color.SetBlue(fc_Normal);
      Color.SetAlpha(fc_Normal);

      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(IsValid_03)
    {
      COLOR_C Color;


      Color.SetRed(fc_Normal);
      Color.SetGreen(fc_Normal);
      Color.SetAlpha(fc_Normal);

      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(IsValid_04)
    {
      COLOR_C Color;


      Color.SetRed(fc_Normal);
      Color.SetGreen(fc_Normal);
      Color.SetBlue(fc_Normal);

      BOOST_TEST(Color.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetRed)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(fc_UnderMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(fc_OverMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(fc_UnderMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(COLOR_C::mc_Maximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetRed(fc_OverMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetGreen)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(fc_UnderMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(fc_OverMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(fc_UnderMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(COLOR_C::mc_Maximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetGreen(fc_OverMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetBlue)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(fc_UnderMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(fc_OverMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_OverMinimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(fc_UnderMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_UnderMaximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(COLOR_C::mc_Maximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetBlue(fc_OverMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()



  BOOST_AUTO_TEST_SUITE(Function__SetAlpha)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(fc_UnderMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(fc_OverMinimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_OverMinimum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(fc_UnderMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_UnderMaximum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }

    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(COLOR_C::mc_Maximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      COLOR_C Color(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);


      Color.SetAlpha(fc_OverMaximum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Invalid,fc_Tolerance);
      BOOST_TEST(Color.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()




















  BOOST_AUTO_TEST_SUITE(Function__SetRGBA)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      COLOR_C Color;


      Color.SetRGBA(COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Maximum,fc_Tolerance);
    }

    BOOST_AUTO_TEST_CASE(Test_01)
    {
      COLOR_C Color;


      Color.SetRGBA(
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }

    BOOST_AUTO_TEST_CASE(Test_02)
    {
      COLOR_C Color;


      Color.SetRGBA(
          fc_Normal,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      COLOR_C Color;


      Color.SetRGBA(
          COLOR_C::mc_Minimum,fc_Normal,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
    }

    BOOST_AUTO_TEST_CASE(Test_04)
    {
      COLOR_C Color;


      Color.SetRGBA(
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,fc_Normal,COLOR_C::mc_Minimum);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),fc_Normal,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),COLOR_C::mc_Minimum,fc_Tolerance);
    }

    BOOST_AUTO_TEST_CASE(Test_05)
    {
      COLOR_C Color;


      Color.SetRGBA(
          COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,COLOR_C::mc_Minimum,fc_Normal);

      BOOST_CHECK_CLOSE(Color.GetRed(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetGreen(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetBlue(),COLOR_C::mc_Minimum,fc_Tolerance);
      BOOST_CHECK_CLOSE(Color.GetAlpha(),fc_Normal,fc_Tolerance);
    }
  BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()


/**
*** test_class_COLOR.cpp
**/

/**
*** \file datetime.cpp
*** \brief datetime.h implementation.
*** \details Implementation file for datetime.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief datetime.cpp identifier.
*** \details Identifier for datetime.cpp.
*** \endinternal
**/
#define   DATETIME_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "datetime.h"
#if       defined(DEBUG_DATETIME_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_DATETIME_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "preferencesmanager.h"
#include  "time_msvc.h"


/****
*****
***** DEFINES
*****
****/

/**
*** \brief Print buffer size.
*** \details Size (in chars) of the print buffer.
**/
#define   BUFFER_SIZEOF   (2048)


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

static char const *fc_24HourFormatKey="24HourFormat";
static char const *fc_4DigitYearKey="4DigitYear";
static char const *fc_LongDayOfWeekFormatKey="LongDayOfWeekFormat";
static char const *fc_LongMonthFormatKey="LongMonthFormat";
static char const *fc_ShowDayOfWeekKey="ShowDayOfWeek";
static char const *fc_ShowSecondsKey="ShowSeconds";
static char const *fc_ShowTimeZoneKey="ShowTimeZone";


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

DATETIMEOPTIONS_C::DATETIMEOPTIONS_C(void)
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::DATETIMEOPTIONS_C()");
  DEBUGLOG_LogIn();

  m_DateTimeOptions=DATETIMEOPTION_CLEARALL;
  m_UseGMTFlag=false;

  DEBUGLOG_LogOut();
  return;
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_C(
    DATETIMEOPTIONS_F const &Options,bool UseGMTFlag)
{
  DEBUGLOG_Printf2(
      "DATETIMEOPTIONS_C::DATETIMEOPTIONS_C(%p,%u)",&Options,UseGMTFlag);
  DEBUGLOG_LogIn();

  m_DateTimeOptions=Options;
  m_UseGMTFlag=UseGMTFlag;

  DEBUGLOG_LogOut();
  return;
}

DATETIMEOPTIONS_C::~DATETIMEOPTIONS_C(void)
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::~DATETIMEOPTIONS_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool DATETIMEOPTIONS_C::operator==(DATETIMEOPTIONS_C const &Options) const
{
  unsigned ThisMask;
  unsigned OptionsMask;


  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::operator==(%p)",&Options);
  DEBUGLOG_LogIn();

  /* If the ShowDayOfWeek option is not set,
      ignore the LongDayOfWeekFormat option. */
  ThisMask=static_cast<unsigned>(~0);
  if (!(m_DateTimeOptions&DATETIMEOPTION_SHOWDAYOFWEEK))
    ThisMask=~DATETIMEOPTION_LONGDAYOFWEEKFORMAT;
  OptionsMask=static_cast<unsigned>(~0);
  if (!(Options.m_DateTimeOptions&DATETIMEOPTION_SHOWDAYOFWEEK))
    OptionsMask=~DATETIMEOPTION_LONGDAYOFWEEKFORMAT;

  DEBUGLOG_LogOut();
  return(
      ((m_DateTimeOptions&ThisMask)==(Options.m_DateTimeOptions&OptionsMask)) &&
      (m_UseGMTFlag==Options.m_UseGMTFlag));
}

bool DATETIMEOPTIONS_C::operator!=(DATETIMEOPTIONS_C const &Options) const
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::operator!=(%p)",&Options);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(!(*this==Options));
}

bool DATETIMEOPTIONS_C::Get24HourFormatFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::Get24HourFormatFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_24HOURFORMAT)!=0);
}

bool DATETIMEOPTIONS_C::Get4DigitYearFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::Get4DigitYearFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_4DIGITYEAR)!=0);
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F
    DATETIMEOPTIONS_C::GetDateTimeOptions(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetDateTimeOptions()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_DateTimeOptions);
}

bool DATETIMEOPTIONS_C::GetLongDayOfWeekFormatFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetLongDayOfWeekFormatFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_LONGDAYOFWEEKFORMAT)!=0);
}

bool DATETIMEOPTIONS_C::GetLongMonthFormatFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetLongMonthFormatFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_LONGMONTHFORMAT)!=0);
}

bool DATETIMEOPTIONS_C::GetShowDayOfWeekFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetShowDayOfWeekFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_SHOWDAYOFWEEK)!=0);
}

bool DATETIMEOPTIONS_C::GetShowSecondsFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetShowSecondsFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_SHOWSECONDS)!=0);
}

bool DATETIMEOPTIONS_C::GetShowTimeZoneFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetShowTimeZoneFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((m_DateTimeOptions&DATETIMEOPTION_SHOWTIMEZONE)!=0);
}

bool DATETIMEOPTIONS_C::GetUseGMTFlag(void) const
{
  DEBUGLOG_Printf0("DATETIMEOPTIONS_C::GetUseGMTFlag()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UseGMTFlag);
}

void DATETIMEOPTIONS_C::Load(PREFERENCESMANAGER_C *pManager)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::Load(%p)",pManager);
  DEBUGLOG_LogIn();

  Set24HourFormatFlag(
      pManager->GetPreference(fc_24HourFormatKey,Get24HourFormatFlag()));
  Set4DigitYearFlag(
      pManager->GetPreference(fc_4DigitYearKey,Get4DigitYearFlag()));
  SetLongDayOfWeekFormatFlag(pManager->GetPreference(
      fc_LongDayOfWeekFormatKey,GetLongDayOfWeekFormatFlag()));
  SetLongMonthFormatFlag(
      pManager->GetPreference(fc_LongMonthFormatKey,GetLongMonthFormatFlag()));
  SetShowDayOfWeekFlag(
      pManager->GetPreference(fc_ShowDayOfWeekKey,GetShowDayOfWeekFlag()));
  SetShowSecondsFlag(
      pManager->GetPreference(fc_ShowSecondsKey,GetShowSecondsFlag()));
  SetShowTimeZoneFlag(
      pManager->GetPreference(fc_ShowTimeZoneKey,GetShowTimeZoneFlag()));

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::Save(PREFERENCESMANAGER_C *pManager) const
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::Save(%p)",pManager);
  DEBUGLOG_LogIn();

  pManager->SetPreference(fc_24HourFormatKey,Get24HourFormatFlag());
  pManager->SetPreference(fc_4DigitYearKey,Get4DigitYearFlag());
  pManager->SetPreference(
      fc_LongDayOfWeekFormatKey,GetLongDayOfWeekFormatFlag());
  pManager->SetPreference(fc_LongMonthFormatKey,GetLongMonthFormatFlag());
  pManager->SetPreference(fc_ShowDayOfWeekKey,GetShowDayOfWeekFlag());
  pManager->SetPreference(fc_ShowSecondsKey,GetShowSecondsFlag());
  pManager->SetPreference(fc_ShowTimeZoneKey,GetShowTimeZoneFlag());

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::Set24HourFormatFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::Set24HourFormatFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_24HOURFORMAT;
  else
    m_DateTimeOptions|=DATETIMEOPTION_24HOURFORMAT;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::Set4DigitYearFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::Set4DigitYearFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_4DIGITYEAR;
  else
    m_DateTimeOptions|=DATETIMEOPTION_4DIGITYEAR;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetDateTimeOptions(DATETIMEOPTIONS_F const &Options)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetDateTimeOptions(0x%x)",Options);
  DEBUGLOG_LogIn();

  m_DateTimeOptions=Options;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetLongDayOfWeekFormatFlag(bool Flag)
{
  DEBUGLOG_Printf1(
      "DATETIMEOPTIONS_C::SetLongDayOfWeekFormatFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_LONGDAYOFWEEKFORMAT;
  else
    m_DateTimeOptions|=DATETIMEOPTION_LONGDAYOFWEEKFORMAT;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetLongMonthFormatFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetLongMonthFormatFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_LONGMONTHFORMAT;
  else
    m_DateTimeOptions|=DATETIMEOPTION_LONGMONTHFORMAT;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetShowDayOfWeekFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetShowDayOfWeekFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_SHOWDAYOFWEEK;
  else
    m_DateTimeOptions|=DATETIMEOPTION_SHOWDAYOFWEEK;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetShowSecondsFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetShowSecondsFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_SHOWSECONDS;
  else
    m_DateTimeOptions|=DATETIMEOPTION_SHOWSECONDS;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetShowTimeZoneFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetShowTimeZoneFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  if (Flag==0)
    m_DateTimeOptions&=~DATETIMEOPTION_SHOWTIMEZONE;
  else
    m_DateTimeOptions|=DATETIMEOPTION_SHOWTIMEZONE;

  DEBUGLOG_LogOut();
  return;
}

void DATETIMEOPTIONS_C::SetUseGMTFlag(bool Flag)
{
  DEBUGLOG_Printf1("DATETIMEOPTIONS_C::SetUseGMTFlag(%u)",Flag);
  DEBUGLOG_LogIn();

  m_UseGMTFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

std::string DateTime_Print(bool DateTimeModeFlag,struct tm const *pTime,
    std::string const &DTFormat,DATETIMEOPTIONS_C const &Options)
{
  std::string Format;
  std::string::size_type Position;
  char pTimeString[BUFFER_SIZEOF];
  std::string DateTime;
  struct tm AdjustedTime;


  DEBUGLOG_Printf5("DateTime_Print(%d,%p,%p(%s),%p)",
      DateTimeModeFlag,pTime,&DTFormat,DTFormat.c_str(),&Options);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pTime==nullptr)
  {
    MESSAGELOG_Error("nullptr parameter");
  }
  else if ( (DateTimeModeFlag) && (DTFormat.empty()) )
  {
    MESSAGELOG_Error("invalid parameter");
  }
  else if ( (DateTimeModeFlag) && (!DTFormat.empty()) &&
      (DTFormat.size()>=BUFFER_SIZEOF) )
  {
    MESSAGELOG_Error("invalid data");
  }
  else
  {
    if (DateTimeModeFlag)
    {
      /* Day of week? */
      if (Options.GetShowDayOfWeekFlag())
      {
        if (Options.GetLongDayOfWeekFormatFlag())
          Format+="%A";                               /* Long DOW. */
        else
          Format+="%a.";                              /* Short DOW. */
        Format+=", ";                                 /* Comma+Space. */
      }

      Format+=DTFormat;                               /* Day, month, year. */

      /* Long month? */
      if (Options.GetLongMonthFormatFlag())
      {
        Position=Format.find("%b.");                  /* From short month. */
        if (Position!=std::string::npos)
        {
          Format[Position+1]='B';                     /* to long month. */
          Format.erase(Position+2,1);
        }
      }

      /* 4 digit year? */
      if (Options.Get4DigitYearFlag())
      {
        Position=Format.find("%y");                   /* From 2 digits. */
        if (Position!=std::string::npos)
          Format[Position+1]='Y';                     /* to 4 digits. */
      }
      Format+=" ";                                    /* Space. */
    }

    Format+="%I:%M";                                  /* Hours:Minutes. */

    /* 24 hour format? */
    if (Options.Get24HourFormatFlag())
    {
      Position=Format.find("%I");                     /* From am/pm format. */
      if (Position!=std::string::npos)
        Format[Position+1]='H';                       /* to 24 hour format. */
    }

    /* Show seconds? */
    if (Options.GetShowSecondsFlag())
      Format+=":%S";                                  /* Colon+Seconds. */

    /* Show AM/PM? */
    if (!Options.Get24HourFormatFlag())
      Format+=" %p";                                  /* AM/PM. */

    if (Options.GetShowTimeZoneFlag())
      Format+=" %Z";                                  /* Time zone. */

    if (Options.GetUseGMTFlag()==0)
    {
      struct tm TimeTM;
      time_t TimeT;


      TimeTM=*pTime;
      TimeT=timegm(&TimeTM);  // Won't use pTime because of const.
      AdjustedTime=*localtime(&TimeT);
    }
    else
      AdjustedTime=*pTime;

#if       defined(__GNUC__) && !defined(__APPLE__)
#pragma   GCC diagnostic push
#pragma   GCC diagnostic ignored "-Wformat-nonliteral"
#endif    /* defined(__GNUC__) && !defined(__APPLE__) */
    if (strftime(pTimeString,BUFFER_SIZEOF,Format.c_str(),&AdjustedTime)==0)
      DateTime="";
    else
      DateTime=pTimeString;
#if       defined(__GNUC__) && !defined(__APPLE__)
#pragma   GCC diagnostic pop
#endif    /* defined(__GNUC__) && !defined(__APPLE__) */
  }

  DEBUGLOG_LogOut();
  return(DateTime);
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F
    operator~(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F RHS)
{
  DEBUGLOG_Printf1(
      "operator~(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x))",RHS);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(static_cast<DATETIMEOPTIONS_C::DATETIMEOPTIONS_F>(
      ~static_cast<unsigned>(RHS)));
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator|(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS)
{
  DEBUGLOG_Printf2("operator|(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x),"
      "DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x))",LHS,RHS);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(static_cast<DATETIMEOPTIONS_C::DATETIMEOPTIONS_F>(
      static_cast<unsigned>(LHS)|static_cast<unsigned>(RHS)));
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator|=(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS)
{
  DEBUGLOG_Printf2("operator|=(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x),"
      "DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x))",LHS,RHS);
  DEBUGLOG_LogIn();

  LHS=LHS|RHS;

  DEBUGLOG_LogOut();
  return(LHS);
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator&(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS)
{
  DEBUGLOG_Printf2("operator&(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x),"
      "DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x))",LHS,RHS);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(static_cast<DATETIMEOPTIONS_C::DATETIMEOPTIONS_F>(
    static_cast<unsigned>(LHS)&static_cast<unsigned>(RHS)));
}

DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator&=(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS)
{
  DEBUGLOG_Printf2("operator&=(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x),"
      "DATETIMEOPTIONS_C::DATETIMEOPTIONS_F:(0x%08x))",LHS,RHS);
  DEBUGLOG_LogIn();

  LHS=LHS&RHS;

  DEBUGLOG_LogOut();
  return(LHS);
}


#undef    DATETIME_CPP


/**
*** datetime.cpp
**/

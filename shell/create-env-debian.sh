#!/bin/bash

# Command line options for "pbuilder".
PBUILDER_OPT='--distribution ${DIST} --architecture ${ARCH} --basetgz /root/moonphase-${DIST}-${ARCH}.tgz'

# Create .pbuilderrc file.
PBUILDERRCTEMP=$(tempfile)
echo 'DEBOOTSTRAPOPTS="--keyring=/usr/share/keyrings/debian-archive-keyring.gpg"'  >>${PBUILDERRCTEMP}
echo 'MIRRORSITE=https://mirrors.kernel.org/debian/' >>${PBUILDERRCTEMP}
# Cache dependencies so they are not downloaded EVERY time.
echo 'EXTRAPACKAGES="fakeroot debhelper cmake git libcurl4-openssl-dev qtbase5-dev"' >>${PBUILDERRCTEMP}
    # fakeroot needed by pbuilder, git needed by .gitlab-ci.yml.
    # All others by moonphase.
sudo mv ${PBUILDERRCTEMP} /root/.pbuilderrc

# Create the x86 environment.
DIST="buster"
ARCH="i386"
sudo pbuilder create $(eval echo $PBUILDER_OPT)

# Create the x86_64 environment.
DIST="buster"
ARCH="amd64"
sudo pbuilder create $(eval echo $PBUILDER_OPT)

# Clean up.
sudo pbuilder clean

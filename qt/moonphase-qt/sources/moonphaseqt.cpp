/**
*** \file moonphaseqt.cpp
*** \brief moonphase-qt operating system entry point.
*** \details Operating system entry point for moonphase-qt. Initializes the
***   application, creates the main dialog, and begins event processing.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief moonphaseqt.cpp identifier.
*** \details Identifier for moonphaseqt.cpp.
*** \internal
**/
#define   MOONPHASEQT_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "config.h"
#if       defined(DEBUG_MOONPHASEQT_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MOONPHASEQT_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "errorcode.h"
#include  "controlpaneldialog.h"
#include  "QtSingleApplication"

#include  <QSystemTrayIcon>
#include  <QMessageBox>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

#if       defined(STATICLINK)
#include  <QtPlugin>
#if       defined(_WIN32)
Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#elif     defined(__APPLE__)
//Q_IMPORT_PLUGIN(QCocoaIntegrationPlugin)
#else
#error Unknown/unsupported operating system for static linking.
#endif    /* defined(_WIN32), defined(__APPLE__) */
#endif    /* defined(STATICLINK) */


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief Application starting point.
*** \details Operating system entry function for the application. Initializes
***   the application, creates the main window, and begins event processing on
***   the window.
*** \param ArgC Number of command line arguments.
*** \param ppArgV Pointer to array of command line argument strings.
*** \retval 0 Success.
*** \retval !0 Error.
**/
int main(int ArgC, char *ppArgV[])
{
  int Return;


  /* Initialize the debug and message logs (possibly, depends on defines). */
  DEBUGLOG_Initialize(!0);
  MESSAGELOG_Initialize();

  DEBUGLOG_Printf2("main(%d,%p)",ArgC,ppArgV);
  DEBUGLOG_LogIn();

  /* Initialize resources specified by the .qrc file. */
  Q_INIT_RESOURCE(moonphaseqt);

  /* Set up preferences keys. */
  QCoreApplication::setApplicationName(MOONPHASEQT_EXECUTABLENAME);
  QCoreApplication::setOrganizationName(MOONPHASEQT_ORGANIZATIONNAME);
  QCoreApplication::setOrganizationDomain(MOONPHASEQT_WEBSITEURL);

  try
  {
    QtSingleApplication Application(ArgC,ppArgV);   // Create the application.
    CONTROLPANELDIALOG_C Dialog;                    // Create the main window.


    Return=EXIT_SUCCESS;
    if (QSystemTrayIcon::isSystemTrayAvailable()==false)
    {
      QMessageBox::critical(&Dialog,MOONPHASEQT_DISPLAYNAME,
          "The system tray was not detected.\nThis program will quit now.");
    }
    else
    {
      QApplication::setQuitOnLastWindowClosed(false);

      /* Check for an already running instance and
          for allow multiple instances flag cleared. */
      if (Application.isRunning()==true)
      {
        /* Not allowed by configuration. */
        QMessageBox::warning(&Dialog,QObject::tr(MOONPHASEQT_DISPLAYNAME),
            QObject::tr("Another instance of this program is already running. "
            "This instance will be stopped and the other instance will be activated."));
        Application.sendMessage("Activate");
      }
      else
      {
        QObject::connect(&Application,SIGNAL(messageReceived(const QString &)),
            &Dialog,SLOT(InstanceMessageSlot(const QString&)));

        /* Begin processing events. */
        Return=Application.exec();
      }
    }
  }
  catch(ERRORCODE_T const &EC)
  {
#if       defined(DEBUG)
    MESSAGELOG_LogError(EC);
#else     /* defined(DEBUG) */
    Q_UNUSED(EC);
#endif    /* defined(DEBUG) */
    Return=EXIT_FAILURE;
  }
  catch(std::exception const &Exception)
  {
#if       defined(DEBUG)
    MESSAGELOG_Error(Exception.what());
#else     /* defined(DEBUG) */
    Q_UNUSED(Exception);
#endif    /* defined(DEBUG) */
    Return=EXIT_FAILURE;
  }

  DEBUGLOG_LogOut();
  return(Return);
}


#undef    MOONPHASEQT_CPP


/**
*** moonphaseqt.cpp
**/

/**
*** \file paneltooltipitempreferencesdialog.h
*** \brief Panel/tooltip item preferences dialog.
*** \details A dialog that allows editing of various preferences of an item in
***   the information panel or tooltip.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELTOOLTIPITEMPREFERENCESDIALOG_H)
/**
*** \internal
*** \brief paneltooltipitempreferencesdialog.h identifier.
*** \details Identifier for paneltooltipitempreferencesdialog.h.
*** \endinternal
**/
#define   PANELTOOLTIPITEMPREFERENCESDIALOG_H


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"

#include  <QDialog>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PANELTOOLTIPITEM_C;
class QAbstractButton;

/**
*** \brief Panel/tooltip item preferences dialog.
*** \details A dialog that allows editing of various preferences of an item in
***   the information panel or tooltip.
**/
class PANELTOOLTIPITEMPREFERENCESDIALOG_C : protected QDialog
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOG_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOG_C(
        PANELTOOLTIPITEMPREFERENCESDIALOG_C const &Dialog)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELTOOLTIPITEMPREFERENCESDIALOG_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOG_C const & operator=(
        PANELTOOLTIPITEMPREFERENCESDIALOG_C const &Widget)=delete;

    /**
    *** \brief Display dialog box.
    *** \details Displays the dialog box using the passed item.
    *** \param pItem Pointer to item to display/edit.
    *** \retval 0 Edits canceled (rejected).
    *** \retval 1 Edits accepted.
    **/
    int Execute(PANELTOOLTIPITEM_C *pItem);

  private slots:
    /**
    *** \brief Button box button clicked.
    *** \details A button in the button box was clicked.
    *** \param pButton Button data.
    **/
    void ButtonBoxButtonClickedSlot(QAbstractButton *pButton);

    /**
    *** \brief Font picked.
    *** \details A font was selected.
    **/
    void ChangeFontButtonClickedSlot(void);

    /**
    *** \brief An option changed.
    *** \details One of the option widgets in the dialog was changed.
    **/
    void OptionChangedSlot(void);

  private:
    /**
    *** \brief Panel/tooltip item preferences dialog implementation class.
    *** \details The implementation class for the panel/tooltip item preferences
    ***   dialog.
    **/
    class PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C;
    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend class PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C;
    /**
    *** \brief Implementation pointer.
    *** \details Pointer to implementation.
    **/
    QScopedPointer<PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELTOOLTIPITEMPREFERENCESDIALOG_H) */


/**
*** paneltooltipitempreferencesdialog.h
**/

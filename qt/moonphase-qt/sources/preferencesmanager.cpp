/**
*** \file preferencesmanager.cpp
*** \brief preferencesmanager.cpp implementation.
*** \details Implementation file for preferencesmanager.cpp.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief preferencesmanager.cpp identifier.
*** \details Identifier for preferencesmanager.cpp.
**/
#define   PREFERENCESMANAGER_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "preferencesmanager.h"
#ifdef    DEBUG_PREFERENCESMANAGER_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_PREFERENCESMANAGER_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "conversions.h"

#include  <QColor>
#include  <QFont>
#include  <QPoint>
#include  <QSettings>
#include  <QSize>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PREFERENCESMANAGERPRIVATE_C
{
  public:
    PREFERENCESMANAGERPRIVATE_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PREFERENCESMANAGERPRIVATE_C(
        PREFERENCESMANAGERPRIVATE_C const &Widget)=delete;

    ~PREFERENCESMANAGERPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PREFERENCESMANAGERPRIVATE_C const & operator=(
        PREFERENCESMANAGERPRIVATE_C const &Thread)=delete;

    QSettings m_Settings;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

PREFERENCESMANAGERPRIVATE_C::PREFERENCESMANAGERPRIVATE_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCESMANAGERPRIVATE_C::PREFERENCESMANAGERPRIVATE_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCESMANAGERPRIVATE_C::~PREFERENCESMANAGERPRIVATE_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCESMANAGERPRIVATE_C::~PREFERENCESMANAGERPRIVATE_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::BeginGroup(std::string const &Name)
{
  DEBUGLOG_Printf2(
      "PREFERENCESMANAGER_C::BeginGroup(%p(%s))",&Name,Name.c_str());
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.beginGroup(QString::fromStdString(Name));

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::EndGroup(void)
{
  DEBUGLOG_Printf0("PREFERENCESMANAGER_C::EndGroup()");
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.endGroup();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::Remove(std::string const &Key)
{
  DEBUGLOG_Printf2("PREFERENCESMANAGER_C::Remove(%p(%s))",&Key,Key.c_str());
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.remove(QString::fromStdString(Key));

  DEBUGLOG_LogOut();
  return;
}

PREFERENCESMANAGER_C::PREFERENCESMANAGER_C(void)
    : m_pPrivate(new PREFERENCESMANAGERPRIVATE_C)
{
  DEBUGLOG_Printf0("PREFERENCESMANAGER_C::PREFERENCESMANAGER_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCESMANAGER_C::~PREFERENCESMANAGER_C(void)
{
  DEBUGLOG_Printf0("PREFERENCESMANAGER_C::~PREFERENCESMANAGER_C()");
  DEBUGLOG_LogIn();

  delete m_pPrivate;

  DEBUGLOG_LogOut();
  return;
}

bool PREFERENCESMANAGER_C::
    GetPreference(std::string const &Key,bool Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),bool:%u)",
      &Key,Key.c_str(),Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_Settings.value(Key.c_str(),Default).toBool());
}

COLOR_C PREFERENCESMANAGER_C::GetPreference(
    std::string const &Key,COLOR_C const &Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),COLOR_C:%p)",
      &Key,Key.c_str(),&Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(ToCOLOR_C(m_pPrivate->m_Settings.value(
      Key.c_str(),ToQColor(Default)).value<QColor>()));
}

double PREFERENCESMANAGER_C::GetPreference(
    std::string const &Key,double Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),double:%f)",
      &Key,Key.c_str(),&Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_Settings.value(Key.c_str(),Default).toDouble());
}

POSITION_C PREFERENCESMANAGER_C::GetPreference(
    std::string const &Key,POSITION_C const &Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),POSITION_C:%p)",
      &Key,Key.c_str(),&Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(ToPOSITION_C(
      m_pPrivate->m_Settings.value(Key.c_str(),ToQPoint(Default)).toPoint()));
}

SIZE_C PREFERENCESMANAGER_C::GetPreference(
    std::string const &Key,SIZE_C const &Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),SIZE_C:%p)",
      &Key,Key.c_str(),&Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(ToSIZE_C(
      m_pPrivate->m_Settings.value(Key.c_str(),ToQSize(Default)).toSize()));
}

std::string PREFERENCESMANAGER_C::GetPreference(
    std::string const &Key,std::string const &Default) const
{
  DEBUGLOG_Printf4(
      "PREFERENCESMANAGER_C::GetPreference(%p(%s),std::string:%p(%s))",
      &Key,Key.c_str(),&Default,Default.c_str());
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_Settings.value(
      Key.c_str(),Default.c_str()).toString().toStdString());
}

void PREFERENCESMANAGER_C::SetPreference(std::string const &Key,bool Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%u)",
      &Key,Key.c_str(),Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),Value);

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::SetPreference(
    std::string const &Key,COLOR_C const &Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%p)",
      &Key,Key.c_str(),&Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),ToQColor(Value));

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::SetPreference(std::string const &Key,double Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%f)",
      &Key,Key.c_str(),Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),Value);

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::
    SetPreference(std::string const &Key,std::string const &Value)
{
  DEBUGLOG_Printf4("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%p(%s))",
      &Key,Key.c_str(),&Value,Value.c_str());
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(
      QString::fromStdString(Key),QString::fromStdString(Value));

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::SetPreference(
    std::string const &Key,POSITION_C const &Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%p)",
      &Key,Key.c_str(),&Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),ToQPoint(Value));

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::SetPreference(
    std::string const &Key,SIZE_C const &Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),bool:%p)",
      &Key,Key.c_str(),&Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),ToQSize(Value));

  DEBUGLOG_LogOut();
  return;
}

int PREFERENCESMANAGER_C::BeginReadArray(std::string const &Prefix)
{
  DEBUGLOG_Printf2(
      "PREFERENCESMANAGER_C::BeginReadArray(%p(%s))",&Prefix,Prefix.c_str());
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_Settings.beginReadArray(QString::fromStdString(Prefix)));
}

void PREFERENCESMANAGER_C::BeginWriteArray(std::string const &Prefix,int Size)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::BeginWriteArray(%p(%s),%d)",
      &Prefix,Prefix.c_str(),Size);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.beginWriteArray(QString::fromStdString(Prefix),Size);

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::EndArray(void)
{
  DEBUGLOG_Printf0("PREFERENCESMANAGER_C::EndArray()");
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.endArray();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::SetArrayIndex(int Index)
{
  DEBUGLOG_Printf1("PREFERENCESMANAGER_C::SetArrayIndex(%d)",Index);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setArrayIndex(Index);

  DEBUGLOG_LogOut();
  return;
}

int PREFERENCESMANAGER_C::
    GetPreference(std::string const &Key,int Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),int:%d)",
      &Key,Key.c_str(),Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_Settings.value(Key.c_str(),Default).toInt());
}

FONT_C PREFERENCESMANAGER_C::
    GetPreference(std::string const &Key,FONT_C const &Default) const
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::GetPreference(%p(%s),FONT_C:%u)",
      &Key,Key.c_str(),&Default);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(ToFONT_C(m_pPrivate->m_Settings.value(Key.c_str(),
      ToQFont(Default)).value<QFont>()));
}

void
    PREFERENCESMANAGER_C::SetPreference(const std::string& Key,int const &Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),FONT_C:%f)",
      &Key,Key.c_str(),Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(QString::fromStdString(Key),Value);

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCESMANAGER_C::
    SetPreference(const std::string& Key,FONT_C const &Value)
{
  DEBUGLOG_Printf3("PREFERENCESMANAGER_C::SetPreference(%p(%s),FONT_C:%f)",
      &Key,Key.c_str(),Value);
  DEBUGLOG_LogIn();

  m_pPrivate->m_Settings.setValue(
      QString::fromStdString(Key),qVariantFromValue(ToQFont(Value)));

  DEBUGLOG_LogOut();
  return;
}


#undef    PREFERENCESMANAGER_CPP


/**
*** preferencesmanager.cpp
**/

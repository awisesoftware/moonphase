#
# CMakeLists.txt
# CMake makefile for the Moon Phase common library.
#

#
# This file is part of moonphase.
# Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


#
# Setup
#

# Component information.
SET(COMPONENTNAME "Common")
SET(DISPLAYNAME "Common")
SET(FILENAME "common")
STRING(TOUPPER "${COMPONENTNAME}" COMPONENTNAMEUC)
STRING(TOLOWER "${COMPONENTNAME}" COMPONENTNAMELC)
STRING(CONCAT DESCRIPTION
    "A collection of code and data which is common for the Moon Phase project.")


#
# Configuration
#

IF(${MOONPHASE_APPLICATIONS})
  IF(MOONPHASE_OPTION_ENABLEUPDATECHECK)
    FIND_PACKAGE(CURL REQUIRED)
    IF(WIN32)
      INCLUDE_DIRECTORIES("$ENV{VCPKG_ROOT_DIR}/installed/x64-windows-static/include")
        # Windows is having problems finding the include directory for curl, so hardcode it here for now.
    ENDIF()
  ENDIF()
ENDIF()


#
# Targets
#

# Create the component.
IF(${MOONPHASE_APPLICATIONS})
  IF(MOONPHASE_OPTION_ENABLEUPDATECHECK)
    SET(UPDATE_SOURCE "${CMAKE_CURRENT_LIST_DIR}/sources/update.cpp")
  ENDIF()
  ADD_LIBRARY(
      ${FILENAME}
      STATIC
      "${CMAKE_CURRENT_LIST_DIR}/sources/calcephem.c"
      "${CMAKE_CURRENT_LIST_DIR}/sources/datetime.cpp"
      "${CMAKE_CURRENT_LIST_DIR}/sources/information.cpp"
      "${CMAKE_CURRENT_LIST_DIR}/sources/commontypes.cpp"
      "${CMAKE_CURRENT_LIST_DIR}/sources/moondata.c"
      "${CMAKE_CURRENT_LIST_DIR}/sources/paneltooltipitem.cpp"
      "${CMAKE_CURRENT_LIST_DIR}/sources/preferences.cpp"
      ${UPDATE_SOURCE})
  SET_PROPERTY(TARGET ${FILENAME} PROPERTY
      MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
  ADD_LIBRARY(MoonPhase::Common ALIAS ${FILENAME})
  TARGET_COMPILE_DEFINITIONS(
      ${FILENAME}
      PRIVATE
      ${PROJECT_TARGET_COMPILE_DEFINITIONS})
  TARGET_COMPILE_FEATURES(
      ${FILENAME}
      PRIVATE
      ${PROJECT_TARGET_COMPILE_FEATURES})
  TARGET_COMPILE_OPTIONS(
      ${FILENAME}
      PRIVATE
      ${PROJECT_TARGET_COMPILE_OPTIONS})
  TARGET_INCLUDE_DIRECTORIES(
      ${FILENAME}
      PUBLIC
      "${CMAKE_CURRENT_LIST_DIR}/sources")
  TARGET_LINK_LIBRARIES(
      ${FILENAME}
      PRIVATE
      AWiseToolbox::Generic
      ${CURL_LIBRARIES})
ENDIF()


#
# Subdirectories
#


#
# Installation
#


#
# CMakeLists.txt
#

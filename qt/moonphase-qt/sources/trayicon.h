/**
*** \file trayicon.h
*** \brief Display tray icon and handle user input.
*** \details Displays the application icon in the system tray and converts
***   user input into application messages.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef   TRAYICON_H
/**
*** \brief trayicon.h identifier.
*** \details Identifier for trayicon.h.
**/
#define   TRAYICON_H


/****
*****
***** INCLUDES
*****
****/

#include  "moondata.h"
#include  "preferences.h"

#include  <QSystemTrayIcon>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class QMenu;
class QTimer;

class TRAYICON_C : public QSystemTrayIcon
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    TRAYICON_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    TRAYICON_C(TRAYICON_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~TRAYICON_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    TRAYICON_C const & operator=(TRAYICON_C const &Icon)=delete;

    void Update(PREFERENCES_C::MOONDATA_C::TOOLTIP_C const *pPreferences,
        MOONDATA_T const &MoonData);

  public slots:
    /**
    *** \brief Control panel activated.
    *** \details The control panel was activated.
    *** \param Reason The reason the system tray was activated.
    **/
    void ControlPanelActivatedSlot(QSystemTrayIcon::ActivationReason Reason);

  signals:
    /**
    *** \brief Show control panel.
    *** \details The control panel needs to be shown.
    **/
    void ShowControlPanelSignal(void);

    /**
    *** \brief Show the information panel.
    *** \details User request that the information panel be shown.
    **/
    void ShowInformationPanelSignal(void);

    /**
    *** \brief Quit requested.
    *** \details User request that the application quit.
    **/
    void QuitSignal(void);

  private:
    /**
    *** \brief Double click timout timer.
    *** \details Timer used to differentiate between one or two clicks and a
    ***   double click.
    **/
    QTimer *m_pDoubleClickTimeoutTimer;

    /**
    *** \brief Tray icon menu.
    *** \details Menu displayed when the tray icon is right clicked.
    **/
    QMenu *m_pTrayIconMenu;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* TRAYICON_H */


/**
*** trayicon.h
**/

/**
*** \file paneldialog.h
*** \brief Information panel dialog box.
*** \details Displays the information panel dialog box.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELDIALOG_H)
/**
*** \internal
*** \brief paneldialog.h identifier.
*** \details Identifier for paneldialog.h.
*** \endinternal
**/
#define   PANELDIALOG_H


/****
*****
***** INCLUDES
*****
****/

#include  "moondata.h"
#include  "preferences.h"

#include  <QDialog>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Information panel dialog box class.
*** \details Class to display the information panel dialog box.
**/
class PANELDIALOG_C : public QDialog
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    PANELDIALOG_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELDIALOG_C(PANELDIALOG_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELDIALOG_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELDIALOG_C const & operator=(PANELDIALOG_C const &Dialog)=delete;

    /**
    *** \brief Update the dialog box information.
    *** \details Updates the information the dialog box.
    *** \param pPreferences Pointer to preferences.
    *** \param MoonData Moon data.
    *** \returns Data about the dialog (position, size, etc.).
    **/
    PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C
        Update(PREFERENCES_C::MOONDATA_C::PANEL_C const *pPreferences,
        MOONDATA_T const &MoonData);

  protected:
    /**
    *** \brief Close event handler.
    *** \details Handler called when a window close event occurs.
    *** \param pEvent Pointer to close event data.
    **/
    void closeEvent(QCloseEvent *pEvent) override;

    /**
    *** \brief Move event handler.
    *** \details Handler called when a window move event occurs.
    *** \param pEvent Pointer to move event data.
    **/
    void moveEvent(QMoveEvent *pEvent) override;

    /**
    *** \brief Resize event handler.
    *** \details Handler called when a window resize event occurs.
    *** \param pEvent Pointer to resize event data.
    **/
    void resizeEvent(QResizeEvent *pEvent) override;

  private slots:
    /**
    *** \brief Close button clicked.
    *** \details The Close button in the dialog box was clicked.
    **/
    void CloseButtonClickedSlot(void);

  private:
    /**
    *** \brief Panel dialog implementation class.
    *** \details The implementation class for the panel dialog.
    **/
    class PANELDIALOGPRIVATE_C;

    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend class PANELDIALOGPRIVATE_C;

    /**
    *** \brief Implementation pointer.
    *** \details Pointer to implementation.
    **/
    QScopedPointer<PANELDIALOGPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELDIALOG_H) */


/**
*** paneldialog.h
**/

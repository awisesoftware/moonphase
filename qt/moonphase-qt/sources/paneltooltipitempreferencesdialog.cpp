/**
*** \file paneltooltipitempreferencesdialog.cpp
*** \brief paneltooltipitempreferencesdialog.h implementation.
*** \details Implementation file for paneltooltipitempreferencesdialog.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief paneltooltipitempreferencesdialog.cpp identifier.
*** \details Identifier for paneltooltipitempreferencesdialog.cpp.
*** \endinternal
**/
#define   PANELTOOLTIPITEMPREFERENCESDIALOG_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "paneltooltipitempreferencesdialog.h"
#include  "ui_paneltooltipitempreferencesdialog.h"
#ifdef    DEBUG_PANELTOOLTIPITEMPREFERENCESDIALOG_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_PANELTOOLTIPITEMPREFERENCESDIALOG_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "conversions.h"
#include  "desktop.h"
#include  "information.h"
#include  "paneltooltipitem.h"

#include  <QFontDialog>
#include  <QRadioButton>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/

#if       defined(_WIN32) || defined(_WIN64)
#define   tzname    _tzname
#endif    /* defined(_WIN32) || defined(_WIN64) */


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Panel/tooltip item preferences dialog implementation class.
*** \details The implementation class for the panel/tooltip item preferences
***   dialog.
**/
class PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pPublic Pointer to the interface class (MOONDATAWIDGET_C).
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(
        PANELTOOLTIPITEMPREFERENCESDIALOG_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(
        PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C const & operator=(
        PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Initialize widgets.
    *** \details Initializes the controls in the dialog box.
    *** \param pItem Pointer to the item data to initialize the controls.
    **/
    void InitializeWidgets(PANELTOOLTIPITEM_C const *pItem);

    /**
    *** \brief Read widgets.
    *** \details Gets the states of the widgets in the dialog box.
    *** \param Item A reference to an information item that is to be
    ***   initialized according to the states of the widgets.
    **/
    void ReadWidgets(PANELTOOLTIPITEM_C &Item);

    /**
    *** \brief Current font.
    *** \details Font used to display this item.
    **/
    QFont m_Font;

    /**
    *** \brief Pointer to item to edit.
    *** \details Pointer to item to be edited.
    **/
    PANELTOOLTIPITEM_C *m_pItem;

    /**
    *** \brief Public interface pointer.
    *** \details Pointer to the public interface.
    **/
    PANELTOOLTIPITEMPREFERENCESDIALOG_C *m_pPublic;

    /**
    *** \brief Qt UI pointer.
    *** \details Pointer to the Qt user interface.
    **/
    QScopedPointer<Ui::PANELTOOLTIPITEMPREFERENCESDIALOGUI_C> m_pUI;

    /**
    *** \brief Pointer to currently edited item.
    *** \details Pointer to the item being edited.
    **/
    QScopedPointer<PANELTOOLTIPITEM_C> m_pWorkItem;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

/**
*** \brief Combobox text to enum mapping.
*** \details Maps the text displayed in the combobox to an enumeration.
**/
struct
{
  QString Text;                 /**< GUI text. **/
  PANELITEM_C::ALIGNMENT_E Id;  /**< Internal value. **/
} m_pAlignmentTable[]=
{
  { QObject::tr("Center"),  PANELITEM_C::ALIGNMENT_E::ALIGNMENT_CENTER },
  { QObject::tr("Justify"), PANELITEM_C::ALIGNMENT_E::ALIGNMENT_JUSTIFY },
  { QObject::tr("Left"),    PANELITEM_C::ALIGNMENT_E::ALIGNMENT_LEFT },
  { QObject::tr("Right"),   PANELITEM_C::ALIGNMENT_E::ALIGNMENT_RIGHT },
  { nullptr,                PANELITEM_C::ALIGNMENT_E::ALIGNMENT_CENTER }
      // Terminator.
};


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(
    PANELTOOLTIPITEMPREFERENCESDIALOG_C *pPublic)
    : m_pPublic(pPublic), m_pUI(new Ui::PANELTOOLTIPITEMPREFERENCESDIALOGUI_C)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C()");
  DEBUGLOG_LogIn();

  m_pItem=nullptr;

  /* Set up the user interface. */
  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::
    ~PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::"
      "~PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C()");
  DEBUGLOG_LogIn();

  /* Remove and delete the unit options. */
  while(m_pUI->m_pUnitsVLayout->count()>0)
    delete m_pUI->m_pUnitsVLayout->takeAt(0)->widget();


  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::
    InitializeWidgets(PANELTOOLTIPITEM_C const *pItem)
{
  EDITMODE_T EditMode;
  std::vector<std::string> List;
  PANELITEM_C const *pPanelItem;
  TOOLTIPITEM_C const *pToolTipItem;


  DEBUGLOG_Printf1(
      "PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::InitializeWidgets(%p)",
      pItem);
  DEBUGLOG_LogIn();

  /*
  ** Initialize the moon data widgets.
  */

  /* Get the edit mode for this item. */
  EditMode=Information_GetEditMode(m_pItem->GetInformationIndex());

  /* Show/hide various widgets depending on edit mode. */
  switch(EditMode)
  {
    case EDITMODE_DISPLAY:
      m_pUI->m_pUnitsGroupBox->setVisible(false);
      m_pUI->m_pFormatGroupBox->setVisible(false);
      break;
    case EDITMODE_UNITS:
      m_pUI->m_pFormatGroupBox->setVisible(false);
      break;
    case EDITMODE_TIME:
      m_pUI->m_pUnitsGroupBox->setVisible(false);
      m_pUI->m_pDateGroupBox->setVisible(false);
      break;
    case EDITMODE_DATETIME:
      m_pUI->m_pUnitsGroupBox->setVisible(false);
      break;
    default:
      MESSAGELOG_Error("Unknown edit mode.");
      throw(std::runtime_error("Unknown edit mode."));
      break;
  }

  /* Populate the units widget or the date format combobox. */
  List=Information_GetUnitFormatDescriptionList(
      m_pItem->GetInformationIndex());
  if ( (EditMode!=EDITMODE_TIME) && (EditMode!=EDITMODE_DATETIME) )
  {
    for(unsigned Index=0;Index<List.size();Index++)
    {
      QRadioButton *pRadioButton=new QRadioButton(m_pUI->m_pUnitsGroupBox);
      pRadioButton->setText(QString::fromStdString(List[Index]));
      m_pPublic->connect(pRadioButton,SIGNAL(clicked()),
          m_pPublic,SLOT(OptionChangedSlot()));
      m_pUI->m_pUnitsVLayout->insertWidget(-1,pRadioButton);
          // -1 == end of list.
    }
  }
  else if (EditMode==EDITMODE_DATETIME)
    for(unsigned Index=0;Index<List.size();Index++)
      m_pUI->m_pStyleComboBox->addItem(
          QString::fromStdString(List[Index]));
  if ( (EditMode==EDITMODE_TIME) || (EditMode==EDITMODE_DATETIME) )
  {
    QString LocalName;


    /* Time zone format. */
    m_pUI->m_pTimeZoneComboBox->addItem("Greenwich mean time (GMT)");
    LocalName=tr("Local time (")+tzname[0];
    if (tzname[1]!=nullptr)
      LocalName+=tr(" or ")+tzname[1]+tr(")");
    m_pUI->m_pTimeZoneComboBox->addItem(LocalName);
    if (pItem->GetUseGMTFlag()==true)
      m_pUI->m_pTimeZoneComboBox->setCurrentIndex(0);
    else
      m_pUI->m_pTimeZoneComboBox->setCurrentIndex(1);

    /* Time format. */
    m_pUI->m_p24HourFormatCheckBox->setChecked(pItem->Get24HourFormatFlag());
    m_pUI->m_pShowSecondsCheckBox->setChecked(pItem->GetShowSecondsFlag());
    m_pUI->m_pShowTimeZoneCheckBox->setChecked(pItem->GetShowTimeZoneFlag());

    /* Date format. */
    m_pUI->m_pStyleComboBox->setCurrentIndex(pItem->GetUnitFormatIndex());
    m_pUI->m_pLongMonthFormatCheckBox->setChecked(
        pItem->GetLongMonthFormatFlag());
    m_pUI->m_p4DigitYearCheckBox->setChecked(pItem->Get4DigitYearFlag());
    m_pUI->m_pShowDayOfWeekCheckBox->setChecked(pItem->GetShowDayOfWeekFlag());
    m_pUI->m_pLongDayOfWeekFormatCheckBox->setChecked(
        pItem->GetLongDayOfWeekFormatFlag());
  }

  /* Select the proper units radio box. */
  if (EditMode==EDITMODE_UNITS)
  {
    int SelectedIndex;


    SelectedIndex=m_pItem->GetUnitFormatIndex();
    if ( (SelectedIndex<0) ||
        (SelectedIndex>=m_pUI->m_pUnitsVLayout->count()) )
    {
      MESSAGELOG_Error("Selected index out of bounds.");
      throw(std::runtime_error("Selected index out of bounds."));
    }

    /* Set the selected checkbox. */
    if (m_pUI->m_pUnitsVLayout->count()>0)
    {
      QRadioButton *pRadioButton;


      pRadioButton=dynamic_cast<QRadioButton*>(
          m_pUI->m_pUnitsVLayout->itemAt(SelectedIndex)->widget());
      if (pRadioButton==nullptr)
      {
        MESSAGELOG_Error("nullptr radio button pointer.");
        throw(std::runtime_error("nullptr radio button pointer."));
      }
      else
        pRadioButton->setChecked(true);
    }
  }

  /* Initialize the alignment comboboxes. */
  for(int Index=0;m_pAlignmentTable[Index].Text!=nullptr;Index++)
  {
    m_pUI->m_pLabelAlignmentComboBox->addItem(
        m_pAlignmentTable[Index].Text,m_pAlignmentTable[Index].Id);
    m_pUI->m_pDataAlignmentComboBox->addItem(
        m_pAlignmentTable[Index].Text,m_pAlignmentTable[Index].Id);
  }

  /* Initialize the color buttons to the system colors. */
  m_pUI->m_pBackgroundColorButton->setCurrentColor(
      ToQColor(GetDesktopWindowBackgroundColor()));
  m_pUI->m_pTextColorButton->setCurrentColor(
      ToQColor(GetDesktopWindowTextColor()));

  pPanelItem=dynamic_cast<PANELITEM_C const *>(pItem);
  pToolTipItem=dynamic_cast<TOOLTIPITEM_C const *>(pItem);
  if ( (pPanelItem==nullptr)==(pToolTipItem==nullptr) )   // XOR
  {
    MESSAGELOG_Error("(pPanelItem==nullptr)==(pToolTipItem==nullptr)");
    throw(std::runtime_error("(pPanelItem==nullptr)==(pToolTipItem==nullptr)"));
  }
  else if (pPanelItem!=nullptr)
  {
    /*
    ** Initialize the panel widgets.
    */

    /* Save the font. */
    m_Font=ToQFont(pPanelItem->GetFont());

    /* Set the alignment. */
    for(int Index=0;
        Index<m_pUI->m_pLabelAlignmentComboBox->count();Index++)
    {
      if (pPanelItem->GetLabelAlignment()==
          m_pUI->m_pLabelAlignmentComboBox->itemData(Index).toInt())
      {
        m_pUI->m_pLabelAlignmentComboBox->setCurrentIndex(Index);
        break;
      }
    }
    for(int Index=0;
        Index<m_pUI->m_pDataAlignmentComboBox->count();Index++)
    {
      if (pPanelItem->GetDataAlignment()==
          m_pUI->m_pDataAlignmentComboBox->itemData(Index).toInt())
      {
        m_pUI->m_pDataAlignmentComboBox->setCurrentIndex(Index);
        break;
      }
    }

    /* Set the padding. */
    m_pUI->m_pCenterPaddingSpinBox->setValue(
        pPanelItem->GetCenterPadding());
    m_pUI->m_pLeftRightPaddingSpinBox->setValue(
        pPanelItem->GetLeftRightPadding());
    m_pUI->m_pTopButtomPaddingSpinBox->setValue(
        pPanelItem->GetTopBottomPadding());

    /* Set the font. */
    m_pUI->m_pUseSystemFontCheckBox->setChecked(
        pPanelItem->GetUseSystemFontFlag());

    /* Set the colors in the color buttons. */
    m_pUI->m_pUseSystemTextColorCheckBox->setChecked(
        pPanelItem->GetUseSystemTextColorFlag());
    if ( (pPanelItem->GetUseSystemTextColorFlag()==false) &&
        (pPanelItem->GetTextColor()!=COLOR_C()) )
      m_pUI->m_pTextColorButton->setCurrentColor(
          ToQColor(pPanelItem->GetTextColor()));
    m_pUI->m_pUseSystemBackgroundColorCheckBox->setChecked(
        pPanelItem->GetUseSystemBackgroundColorFlag());
    if ( (pPanelItem->GetUseSystemBackgroundColorFlag()==false) &&
        (pPanelItem->GetBackgroundColor()!=COLOR_C()) )
    m_pUI->m_pBackgroundColorButton->setCurrentColor(
          ToQColor(pPanelItem->GetBackgroundColor()));
  }
  else if (pToolTipItem!=nullptr)
  {
    /*
    ** Initialize the tool tip widgets.
    */

    /* Hide options not used by tool tips. */
    m_pUI->m_pDisplayGroupBox->setVisible(false);
  }

  m_pPublic->OptionChangedSlot();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::
    ReadWidgets(PANELTOOLTIPITEM_C &Item)
{
  PANELITEM_C *pPanelItem;
  TOOLTIPITEM_C *pToolTipItem;


  DEBUGLOG_Printf1("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C::ReadWidgets(%p)",&Item);
  DEBUGLOG_LogIn();

  /*
  ** Read the moon data widgets.
  */

  /* The information index is the only thing that doesn't get set here, so
      just copy the it instead of the whole item. */
  Item.SetInformationIndex(m_pItem->GetInformationIndex());

  /* Read the selected unit (if enabled). */
  if (m_pUI->m_pUnitsVLayout->count()>0)
  {
    for(int Index=0;Index<m_pUI->m_pUnitsVLayout->count();Index++)
    {
      QRadioButton *pRadioButton;


      pRadioButton=dynamic_cast<QRadioButton*>(
          m_pUI->m_pUnitsVLayout->itemAt(Index)->widget());
      if (pRadioButton==nullptr)
      {
        MESSAGELOG_Error("nullptr");
        throw(std::runtime_error("nullptr"));
      }
      else if (pRadioButton->isChecked()==true)
      {
        Item.SetUnitFormatIndex(Index);
        break;
      }
    }
  }

  /* Get the time options. */
  Item.Set24HourFormatFlag(m_pUI->m_p24HourFormatCheckBox->isChecked());
  Item.SetShowSecondsFlag(m_pUI->m_pShowSecondsCheckBox->isChecked());

  /* Get the date options. */
  if (m_pUI->m_pStyleComboBox->count()>0)
    Item.SetUnitFormatIndex(m_pUI->m_pStyleComboBox->currentIndex());
  Item.SetUseGMTFlag(m_pUI->m_pTimeZoneComboBox->currentIndex()==0);
  Item.SetLongMonthFormatFlag(m_pUI->m_pLongMonthFormatCheckBox->isChecked());
  Item.Set4DigitYearFlag(m_pUI->m_p4DigitYearCheckBox->isChecked());
  Item.SetShowDayOfWeekFlag(m_pUI->m_pShowDayOfWeekCheckBox->isChecked());
  Item.SetLongDayOfWeekFormatFlag(
      m_pUI->m_pLongDayOfWeekFormatCheckBox->isChecked());
  Item.SetShowTimeZoneFlag(m_pUI->m_pShowTimeZoneCheckBox->isChecked());

  pPanelItem=dynamic_cast<PANELITEM_C *>(&Item);
  pToolTipItem=dynamic_cast<TOOLTIPITEM_C *>(&Item);
  if ( (pPanelItem==nullptr) && (pToolTipItem==nullptr) )
  {
    MESSAGELOG_Error("pPanelItem==nullptr && pToolTipItem==nullptr");
  }
  else if (pPanelItem!=nullptr)
  {
    /*
    ** Read the panel widgets.
    */

    pPanelItem=dynamic_cast<PANELITEM_C *>(&Item);

    /* Get the alignment. */
    pPanelItem->SetLabelAlignment(static_cast<PANELITEM_C::ALIGNMENT_E>(
        m_pUI->m_pLabelAlignmentComboBox->currentData().toInt()));
    pPanelItem->SetDataAlignment(static_cast<PANELITEM_C::ALIGNMENT_E>(
        m_pUI->m_pDataAlignmentComboBox->currentData().toInt()));

    /* Get the padding. */
    pPanelItem->SetCenterPadding(m_pUI->m_pCenterPaddingSpinBox->value());
    pPanelItem->SetLeftRightPadding(m_pUI->m_pLeftRightPaddingSpinBox->value());
    pPanelItem->SetTopBottomPadding(m_pUI->m_pTopButtomPaddingSpinBox->value());

    /* Get the system font flag and font. */
    pPanelItem->SetUseSystemFontFlag(m_pUI->m_pUseSystemFontCheckBox->isChecked());
    if (pPanelItem->GetUseSystemFontFlag()==false)
      pPanelItem->SetFont(ToFONT_C(m_Font));
    else
      pPanelItem->SetFont(GetDesktopWindowFont());

    /* Get the system text and background colors. */
    pPanelItem->SetUseSystemTextColorFlag(
        m_pUI->m_pUseSystemTextColorCheckBox->isChecked());
    if (m_pUI->m_pUseSystemTextColorCheckBox->isChecked()==false)
      pPanelItem->SetTextColor(ToCOLOR_C(m_pUI->m_pTextColorButton->currentColor()));
    else
      pPanelItem->SetTextColor(GetDesktopWindowTextColor());
    pPanelItem->SetUseSystemBackgroundColorFlag(
        m_pUI->m_pUseSystemBackgroundColorCheckBox->isChecked());
    if (m_pUI->m_pUseSystemBackgroundColorCheckBox->isChecked()==false)
      pPanelItem->SetBackgroundColor(
          ToCOLOR_C(m_pUI->m_pBackgroundColorButton->currentColor()));
    else
      pPanelItem->SetBackgroundColor(GetDesktopWindowBackgroundColor());
  }
  else if (pToolTipItem!=nullptr)
  {
    /*
    ** Read the tool tip widgets.
    */

    // Nothing. TOOLTIPITEM_C==PANELTOOLTIPITEM_C.
  }

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    PANELTOOLTIPITEMPREFERENCESDIALOG_C(QWidget *pParent) :  QDialog(pParent),
    m_pPrivate(new PANELTOOLTIPITEMPREFERENCESDIALOGPRIVATE_C(this))
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "PANELTOOLTIPITEMPREFERENCESDIALOG_C(%p)",pParent);
  DEBUGLOG_LogIn();

  m_pPrivate->m_pUI->setupUi(this);

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMPREFERENCESDIALOG_C::~PANELTOOLTIPITEMPREFERENCESDIALOG_C(void)
{
  DEBUGLOG_Printf0("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "~PANELTOOLTIPITEMPREFERENCESDIALOG_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMPREFERENCESDIALOG_C::
    ButtonBoxButtonClickedSlot(QAbstractButton *pButton)
{
  DEBUGLOG_Printf1("PANELTOOLTIPITEMPREFERENCESDIALOG_C::"
      "ButtonBoxButtonClickedSlot(%p)",pButton);
  DEBUGLOG_LogIn();

  try
  {
    switch(m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton))
    {
      default:
      case QDialogButtonBox::Cancel:
        reject();
        break;
      case QDialogButtonBox::Apply:
      case QDialogButtonBox::Ok:
        m_pPrivate->ReadWidgets(*m_pPrivate->m_pItem);
        if (m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton)==
            QDialogButtonBox::Ok)
          accept();
        OptionChangedSlot();
        break;
    }
  }
  catch(std::runtime_error const &RE)
  {
    Q_UNUSED(RE);
    reject(); // If any failure, "Cancel" out...
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELTOOLTIPITEMPREFERENCESDIALOG_C::ChangeFontButtonClickedSlot(void)
{
  QFont Font;
  bool OkFlag;


  DEBUGLOG_Printf0(
      "PANELTOOLTIPITEMPREFERENCESDIALOG_C::ChangeFontButtonClickedSlot()");
  DEBUGLOG_LogIn();

  Font=QFontDialog::getFont(&OkFlag,m_pPrivate->m_Font,this);
  if (OkFlag==true)
  {
    /* Only save if different. */
    if (Font!=QDialog::font())
    {
      m_pPrivate->m_Font=Font;
      OptionChangedSlot();
    }
  }

  DEBUGLOG_LogOut();
  return;
}

int PANELTOOLTIPITEMPREFERENCESDIALOG_C::Execute(PANELTOOLTIPITEM_C *pItem)
{
  QWidget *pParent;
  int Return;
  PANELITEM_C *pPanelItem;
  TOOLTIPITEM_C *pToolTipItem;


  DEBUGLOG_Printf1("PANELTOOLTIPITEMPREFERENCESDIALOG_C::Execute(%p)",pItem);
  DEBUGLOG_LogIn();

  try
  {
    /* Parameter checking. */
    if (pItem==nullptr)
    {
      MESSAGELOG_Error("pItem==nullptr.");
      throw(std::runtime_error("pItem==nullptr."));
    }

    pPanelItem=dynamic_cast<PANELITEM_C *>(m_pPrivate->m_pItem);
    pToolTipItem=dynamic_cast<TOOLTIPITEM_C *>(m_pPrivate->m_pItem);
    if ( (pPanelItem==nullptr)==(pToolTipItem==nullptr) )   // XOR
    {
      MESSAGELOG_Error("(pPanelItem==nullptr)==(pToolTipItem==nullptr)");
      throw(std::runtime_error("(pPanelItem==nullptr)==(pToolTipItem==nullptr)"));
    }

    /* Save the item pointer. */
    m_pPrivate->m_pItem=pItem;

    /* Modify the dialog box title,
        create a new work item, and copy the data of the function parameter. */
    if (pPanelItem!=nullptr)
    {
      setWindowTitle(tr("Panel ")+windowTitle());
      m_pPrivate->m_pWorkItem.reset(new PANELITEM_C(*pPanelItem));
    }
    else
    {
      setWindowTitle(tr("Tool Tip ")+windowTitle());
      m_pPrivate->m_pWorkItem.reset(new TOOLTIPITEM_C(*pToolTipItem));
    }

    /* Initialize the dialog box widgets. */
    m_pPrivate->InitializeWidgets(m_pPrivate->m_pItem);

    /* Some widgets may have been hidden, resize the dialog to minimum size. */
    resize(minimumSizeHint());

    /* Find the top most widget (the dialog box). */
    pParent=parentWidget();
    if (pParent!=nullptr)
    {
      while(pParent->parentWidget()!=nullptr)
        pParent=pParent->parentWidget();

      /* Re-center the dialog box on the parent. */
      move(pParent->pos().x()+
        (pParent->frameGeometry().width()-frameGeometry().width())/2,
        pParent->pos().y()+
        (pParent->frameGeometry().height()-frameGeometry().height())/2);
    }

    Return=exec();
  }
  catch(std::runtime_error const &RE)
  {
    Q_UNUSED(RE);
    Return=QDialog::Rejected;
  }

  delete m_pPrivate->m_pWorkItem.take();

  DEBUGLOG_LogOut();
  return(Return);
}

void PANELTOOLTIPITEMPREFERENCESDIALOG_C::OptionChangedSlot(void)
{
  QRadioButton *pRadioButton;
  MOONDATA_T MoonData;
  PANELTOOLTIPITEM_C Item;
  PANELITEM_C const *pPanelItem;
  bool EnableApplyFlag;


  DEBUGLOG_Printf0("PANELTOOLTIPITEMPREFERENCESDIALOG_C::OptionChangedSlot()");
  DEBUGLOG_LogIn();

  try
  {
    /* I consider this a fix for a Qt "bug". If there is only one radio button
        in a button group, then that radio button can actually be unselected.
        Why not just use a checkbox then? */
    if (m_pPrivate->m_pUI->m_pUnitsVLayout->count()==1)
    {
      pRadioButton=dynamic_cast<QRadioButton*>(
          m_pPrivate->m_pUI->m_pUnitsVLayout->itemAt(0)->widget());
      if (pRadioButton==nullptr)
      {
        MESSAGELOG_Error("nullptr radio button pointer.");
        throw(std::runtime_error("nullptr radio button pointer."));
      }
      else if (pRadioButton->isChecked()==false)
        pRadioButton->setChecked(true);
    }

    /* Enable/disable subordinate preferences. */
    m_pPrivate->m_pUI->m_pLongDayOfWeekFormatCheckBox->setEnabled(
        m_pPrivate->m_pUI->m_pShowDayOfWeekCheckBox->isChecked());
    m_pPrivate->m_pUI->m_pChangeFontButton->setEnabled(
        !m_pPrivate->m_pUI->m_pUseSystemFontCheckBox->isChecked());
    m_pPrivate->m_pUI->m_pTextColorButton->setEnabled(
        !m_pPrivate->m_pUI->m_pUseSystemTextColorCheckBox->isChecked());
    m_pPrivate->m_pUI->m_pBackgroundColorButton->setEnabled(
        !m_pPrivate->m_pUI->m_pUseSystemBackgroundColorCheckBox->isChecked());
    m_pPrivate->m_pUI->m_pLongMonthFormatCheckBox->setEnabled(
        m_pPrivate->m_pUI->m_pStyleComboBox->currentIndex()==0);
        // The first entry is the only one which _might_ use the long format and
        //  is always first in the array. See 'information.cpp'.

    /* Initialize the moon data with set values (just for display). */
    memset(&MoonData,0,sizeof(MoonData));
    MoonData.CTransData.MoonAge=12.3;
    MoonData.CTransData.h_moon=123.45;
    MoonData.CTransData.A_moon=123.45;
    MoonData.CTransData.DEC_moon=188.736583333;
    MoonData.CTransData.EarthMoonDistance=3118.382924981;
    MoonData.CTransData.MoonPhase=.33333;
    MoonData.CTransData.RA_moon=188.736583333;
    MoonData.TodaysRiseLT=13.415555556;
    MoonData.TodaysSetLT=13.415555556;
    MoonData.TomorrowsRiseLT=13.415555556;
    MoonData.TomorrowsSetLT=13.415555556;
    MoonData.CTransData.Visible=!0;
    MoonData.YesterdaysRiseLT=13.415555556;
    MoonData.YesterdaysSetLT=13.415555556;

    m_pPrivate->ReadWidgets(*m_pPrivate->m_pWorkItem);
    if (m_pPrivate->m_pWorkItem==nullptr)
    {
      MESSAGELOG_Error("m_pPrivate->m_pWorkItem==nullptr");
      throw(std::runtime_error("m_pPrivate->m_pWorkItem==nullptr"));
    }
    else
    {
      pPanelItem=dynamic_cast<PANELITEM_C const *>(&*(m_pPrivate->m_pWorkItem));
      if (pPanelItem==nullptr)
      {
        MESSAGELOG_Error("pPanelItem==nullptr");
        throw(std::runtime_error("pPanelItem==nullptr"));
      }
      else
        m_pPrivate->m_pUI->m_pMoonDataDisplayWidget->Update(pPanelItem,MoonData);

      if ( (dynamic_cast<PANELITEM_C*>(m_pPrivate->m_pItem)!=nullptr) &&
          (dynamic_cast<PANELITEM_C*>(&*m_pPrivate->m_pWorkItem)!=nullptr) )
        EnableApplyFlag=(*dynamic_cast<PANELITEM_C*>(m_pPrivate->m_pItem)!=
            *dynamic_cast<PANELITEM_C*>(&*m_pPrivate->m_pWorkItem));
      else if ( (dynamic_cast<TOOLTIPITEM_C*>(m_pPrivate->m_pItem)!=nullptr) &&
          (dynamic_cast<TOOLTIPITEM_C*>(&*m_pPrivate->m_pWorkItem)!=nullptr) )
        EnableApplyFlag=(*dynamic_cast<TOOLTIPITEM_C*>(m_pPrivate->m_pItem)!=
            *dynamic_cast<TOOLTIPITEM_C*>(&*m_pPrivate->m_pWorkItem));
      else
      {
        MESSAGELOG_Error("Unknown class.");
        EnableApplyFlag=false;
      }
      m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Apply)->
          setEnabled(EnableApplyFlag);
    }
  }
  catch(std::runtime_error const &RE)
  {
    Q_UNUSED(RE);
    // Just exit.
  }

  DEBUGLOG_LogOut();
  return;
}


#undef    PANELTOOLTIPITEMPREFERENCESDIALOG_CPP


/**
*** paneltooltipitempreferencesdialog.cpp
**/

/**
*** \file aboutwidget.h
*** \brief About widget.
*** \details Widget used to display information about a program.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(ABOUTWIDGET_H)
#define   ABOUTWIDGET_H


/****
*****
***** INCLUDES
*****
****/

#include  "ui_aboutwidget.h"

#include  "errorcode.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief About widget.
*** \details Widget used to display information about a program.
**/
class ABOUTWIDGET_C : public QWidget, private Ui::ABOUTWIDGETUI_C
{
  Q_OBJECT

  /**
  *** \brief Logo image.
  *** \details The image displayed as a program logo.
  **/
  Q_PROPERTY(QPixmap Pixmap READ GetImage WRITE SetImage)
  /**
  *** \brief Display name.
  *** \details The display name (dialog boxes, etc.) of the executable.
  **/
  Q_PROPERTY(QString DisplayName READ GetDisplayName WRITE SetDisplayName)
  /**
  *** \brief Executable name.
  *** \details The name of the executable.
  **/
  Q_PROPERTY(QString ExecutableName
      READ GetExecutableName WRITE SetExecutableName)
  /**
  *** \brief Program version.
  *** \details The version of the program.
  **/
  Q_PROPERTY(QString Version READ GetVersion WRITE SetVersion)
  /**
  *** \brief Information tab visibility.
  *** \details Visibility (shown/hidden) of the information tab.
  **/
  Q_PROPERTY(bool InformationTabVisible
      READ IsInformationTabVisible WRITE SetInformationTabVisible)
  /**
  *** \brief Information tab icon.
  *** \details The icon displayed in the information tab.
  **/
  Q_PROPERTY(QIcon InformationTabIcon
      READ GetInformationTabIcon WRITE SetInformationTabIcon)
  /**
  *** \brief Information text.
  *** \details The text displayed in the information display widget.
  **/
  Q_PROPERTY(QString InformationText
      READ GetInformationText WRITE SetInformationText)
  /**
  *** \brief "Check for update" widget visibility.
  *** \details Visibility (shown/hidden) of the "Check for update" widget.
  **/
  Q_PROPERTY(bool CheckForUpdateWidgetVisible
      READ IsCheckForUpdateWidgetVisible WRITE SetCheckForUpdateWidgetVisible)
  /**
  *** \brief License tab visibility.
  *** \details Visibility (shown/hidden) of the license tab.
  **/
  Q_PROPERTY(bool LicenseTabVisible
      READ IsLicenseTabVisible WRITE SetLicenseTabVisible)
  /**
  *** \brief License tab icon.
  *** \details The icon displayed in the license tab.
  **/
  Q_PROPERTY(QIcon LicenseTabIcon
      READ GetLicenseTabIcon WRITE SetLicenseTabIcon)
  /**
  *** \brief License text.
  *** \details The text displayed in the license display widget.
  **/
  Q_PROPERTY(QString LicenseText READ GetLicenseText WRITE SetLicenseText)
  /**
  *** \brief Change log tab visibility.
  *** \details Visibility (shown/hidden) of the change log tab.
  **/
  Q_PROPERTY(bool ChangeLogTabVisible
      READ IsChangeLogTabVisible WRITE SetChangeLogTabVisible)
  /**
  *** \brief Change log tab icon.
  *** \details The icon displayed in the change log tab.
  **/
  Q_PROPERTY(QIcon ChangeLogTabIcon
      READ GetChangeLogTabIcon WRITE SetChangeLogTabIcon)
  /**
  *** \brief Change log text.
  *** \details The text displayed in the change log display widget.
  **/
  Q_PROPERTY(QString ChangeLogText READ GetChangeLogText WRITE SetChangeLogText)

  public:
    /**
    *** \brief "About" widget tab list.
    *** \details The list of tab that are displayed in the "About" widget.
    **/
    /* Keep TAB_FIRST always 0, and keep enums sequential. */
    typedef enum enumTAB
    {
      ABOUTTAB_FIRST=0,
      ABOUTTAB_INFORMATION=ABOUTTAB_FIRST,
      ABOUTTAB_LICENSE=1,
      ABOUTTAB_CHANGELOG=2,
      ABOUTTAB_LAST=ABOUTTAB_CHANGELOG
    } ABOUTTAB_E;

    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent object.
    **/
    ABOUTWIDGET_C(QWidget *pParent=NULL);
    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    ABOUTWIDGET_C(ABOUTWIDGET_C const &Widget)=delete;
    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~ABOUTWIDGET_C(void);

    /**
    *** \brief Assignment operator.
    *** \details Assignment operator.
    *** \param Widget Reference to widget to copy.
    *** \returns Copy of widget.
    *** \note Function explicitly defined as deleted.
    **/
    ABOUTWIDGET_C const & operator=(ABOUTWIDGET_C const &Widget)=delete;

    /**
    *** \brief Returns the change log text.
    *** \details Returns the contents of the change log text display widget.
    *** \returns Change log text.
    **/
    QString GetChangeLogText(void);
    /**
    *** \brief Returns the change log tab icon.
    *** \details Returns the icon displayed in the change log tab.
    *** \returns Change log tab icon.
    **/
    QIcon GetChangeLogTabIcon(void);
    /**
    *** \brief Returns the display name text.
    *** \details Returns the contents of the display name text display widget.
    *** \returns Display name text.
    **/
    QString GetDisplayName(void);
    /**
    *** \brief Returns the executable name text.
    *** \details Returns the contents of the executable name text display widget.
    *** \returns Executable name text.
    **/
    QString GetExecutableName(void);
    /**
    *** \brief Returns the specified tab icon.
    *** \details Returns the specified tab icon.
    *** \param TabIndex Tab index of icon to return.
    *** \returns Specified tab icon.
    **/
    QIcon GetTabIcon(ABOUTTAB_E TabIndex);
    /**
    *** \brief Returns the logo image.
    *** \details Returns the image displayed as a program logo.
    *** \returns Logo image.
    **/
    QPixmap GetImage(void);
    /**
    *** \brief Returns the information text.
    *** \details Returns the contents of the information text display widget.
    *** \returns Information text.
    **/
    QString GetInformationText(void);
    /**
    *** \brief Returns the information tab icon.
    *** \details Returns the icon displayed in the information tab.
    *** \returns Information tab icon.
    **/
    QIcon GetInformationTabIcon(void);
    /**
    *** \brief Returns the license text.
    *** \details Returns the contents of the license text display widget.
    *** \returns License text.
    **/
    QString GetLicenseText(void);
    /**
    *** \brief Returns the license tab icon.
    *** \details Returns the icon displayed in the license tab.
    *** \returns License tab icon.
    **/
    QIcon GetLicenseTabIcon(void);
    /**
    *** \brief Returns the update text.
    *** \details Returns the contents of the update text display widget.
    *** \returns Update text.
    **/
    QString GetUpdateText(void);
    /**
    *** \brief Returns the version text.
    *** \details Returns the contents of the version text display widget.
    *** \returns Version text.
    **/
    QString GetVersion(void);
    /**
    *** \brief Returns the change log tab visibility.
    *** \details Returns the change log tab visibility.
    *** \retval 0 Change log tab is hidden.
    *** \retval !0 Change log tab is visible.
    **/
    bool IsChangeLogTabVisible(void);
    /**
    *** \brief Returns the "Check" button enabled state.
    *** \details Returns the enabled state of the "Check" button.
    *** \retval 0 "Check" button is disabled.
    *** \retval !0 "Check" button is enabled.
    **/
    bool IsCheckForUpdateButtonEnabled(void);
    /**
    *** \brief Returns the check for update widget visibility.
    *** \details Returns the check for update widget visibility.
    *** \retval 0 Check for update widget is hidden.
    *** \retval !0 Check for update widget is visible.
    **/
    bool IsCheckForUpdateWidgetVisible(void);
    /**
    *** \brief Returns the information tab visibility.
    *** \details Returns the information tab visibility.
    *** \retval 0 Information tab is hidden.
    *** \retval !0 Information tab is visible.
    **/
    bool IsInformationTabVisible(void);
    /**
    *** \brief Returns the license tab visibility.
    *** \details Returns the license tab visibility.
    *** \retval 0 License tab is hidden.
    *** \retval !0 License tab is visible.
    **/
    bool IsLicenseTabVisible(void);
    /**
    *** \brief Returns the visibility of the specified tab.
    *** \details Returns the visibility of the specified tab.
    *** \param TabIndex Tab index.
    *** \retval 0 Specified tab is hidden.
    *** \retval !0 Specified tab is visible.
    **/
    bool IsTabVisible(ABOUTTAB_E TabIndex);
    /**
    *** \brief Sets the change log text.
    *** \details Sets the contents of the change log text display widget.
    *** \param Text Change log text.
    **/
    void SetChangeLogText(QString Text);
    /**
    *** \brief Sets the change log tab icon.
    *** \details Sets the icon displayed in the change log tab.
    *** \param Icon Change log tab icon.
    **/
    void SetChangeLogTabIcon(QIcon Icon);
    /**
    *** \brief Shows/hides the change log tab.
    *** \brief Shows or hides the change log tab.
    *** \param VisibleFlag \n
    ***   0 - Hide tab.\n
    ***   !0 - Show tab.
    **/
    void SetChangeLogTabVisible(bool VisibleFlag);
    /**
    *** \brief Enables/disables the "Check" button.
    *** \details Enables/disables the "Check" button.
    *** \param EnabledFlag \n
    ***   0 - "Check" button is disabled.\n
    ***   !0 - "Check" button is enabled.
    **/
    void SetCheckForUpdateButtonEnabled(bool EnabledFlag);
    /**
    *** \brief Sets the check for update widget visibility.
    *** \details Sets the check for update widget visibility.
    *** \param VisibleFlag \n
    ***   0 - Check for update widget is hidden.
    ***   !0 - Check for update widget is visible.
    **/
    void SetCheckForUpdateWidgetVisible(bool VisibleFlag);
    /**
    *** \brief Sets the display name text.
    *** \details Sets the contents of the display name text display widget.
    *** \param Name Display name text.
    **/
    void SetDisplayName(QString Name);
    /**
    *** \brief Sets the executable name text.
    *** \details Sets the contents of the executable name text display widget.
    *** \param Name Executable name text.
    **/
    void SetExecutableName(QString Name);
    /**
    *** \brief Sets the logo image.
    *** \details Sets the image displayed as a program logo.
    *** \param Pixmap Logo image.
    **/
    void SetImage(QPixmap Pixmap);
    /**
    *** \brief Sets the information text.
    *** \details Sets the contents of the information text display widget.
    *** \param Text Information text.
    **/
    void SetInformationText(QString Text);
    /**
    *** \brief Sets the information tab icon.
    *** \details Sets the icon displayed in the information tab.
    *** \param Icon Information tab icon.
    **/
    void SetInformationTabIcon(QIcon Icon);
    /**
    *** \brief Shows/hides the information tab.
    *** \brief Shows or hides the information tab.
    *** \param VisibleFlag \n
    ***   0 - Hide tab.\n
    ***   !0 - Show tab.
    **/
    void SetInformationTabVisible(bool VisibleFlag);
    /**
    *** \brief Sets the license text.
    *** \details Sets the contents of the license text display widget.
    *** \param Text License text.
    **/
    void SetLicenseText(QString Text);
    /**
    *** \brief Sets the license tab icon.
    *** \details Sets the icon displayed in the license tab.
    *** \param Icon License tab icon.
    **/
    void SetLicenseTabIcon(QIcon Icon);
    /**
    *** \brief Shows/hides the license tab.
    *** \brief Shows or hides the license tab.
    *** \param VisibleFlag \n
    ***   0 - Hide tab.\n
    ***   !0 - Show tab.
    **/
    void SetLicenseTabVisible(bool VisibleFlag);
    /**
    *** \brief Sets the specified tab icon.
    *** \details Sets the specified tab icon.
    *** \param TabIndex Tab index of icon to set.
    *** \param Icon Tab icon.
    **/
    void SetTabIcon(ABOUTTAB_E TabIndex,QIcon Icon);
    /**
    *** \brief Sets the visibility of the specified tab.
    *** \details Sets the visibility of the specified tab.
    *** \param TabIndex Index of tab to show/hide.
    *** \param VisibleFlag \n
    ***   0 - Hide tab.\n
    ***   !0 - Show tab.
    **/
    void SetTabVisible(ABOUTTAB_E TabIndex,bool VisibleFlag);
    /**
    *** \brief Sets the update text.
    *** \details Sets the contents of the update text display widget.
    *** \param Text Update text.
    **/
    void SetUpdateText(QString Text);
    /**
    *** \brief Sets the version text.
    *** \details Sets the contents of the version text display widget.
    *** \param Version Version text.
    **/
    void SetVersion(QString Version);
    /**
    *** \brief Shows the specified tab.
    *** \details Shows the specified tab (makes it the current selection).
    *** \param TabIndex Index of tab to show/hide.
    **/
    void ShowTab(ABOUTTAB_E TabIndex);

  signals:
    /**
    *** \brief The "Check" button was clicked.
    *** \details Signal emitted when the "Check" button is clicked.
    **/
    void CheckForUpdateButtonClickedSignal(void);

  private:
    /**
    *** \brief Updates the executable name and version.
    *** \details Updates the executable name and version number in the dialog box.
    **/
    void SetExecutableNameAndVersionText(void);
    /**
    *** \brief Updates the tab widgets after changes have been made.
    *** \details Updates the tab widgets after changes have been made.
    **/
    void UpdateTabWidgets(void);

    /**
    *** \brief Program executable name.
    *** \details Executable name of the program.
    **/
    QString m_ExecutableName;
    /**
    *** \brief Program version.
    *** \details Version number of the program.
    **/
    QString m_Version;
    /**
    *** \brief Tab data.
    *** \details Data needed by each tab.
    **/
    struct structTABDATA
    {
      /**
      *** \brief Widget pointer.
      *** \details Pointer to widget held by the tab widget.
      **/
      QWidget *pWidget;
      /**
      *** \brief Tab icon.
      *** \details Icon displayed in the tab.
      **/
      QIcon Icon;
      /**
      *** \brief Tab text.
      *** \details Text displayed in the tab.
      **/
      QString Label;
      /**
      *** \brief Tab visibility.
      *** \details Tab visibility.\n
      *** 0 - Hidden.\n
      *** !0 - Visible.
      **/
      bool Visible;
    };
    /**
    *** \brief Tab list.
    *** \details The list of tabs contained in the tab widget.
    **/
    struct structTABDATA m_pTabWidgets[ABOUTTAB_LAST+1];
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if       defined(__cplusplus)
extern "C" {
#endif    /* defined(__cplusplus) */

#if       defined(__cplusplus)
}
#endif    /* defined(__cplusplus) */


#endif    /* !defined(ABOUTWIDGET_H) */


/**
*** aboutwidget.h
**/

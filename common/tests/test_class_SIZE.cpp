/**
*** \file test_class_SIZE.cpp
*** \brief SIZE_C unit tests.
*** \details Unit tests for SIZE_C.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Class__SIZE)

  static int const fc_Range=SIZE_C::mc_Maximum-SIZE_C::mc_Minimum;
  static int const fc_Normal=(SIZE_C::mc_Maximum+SIZE_C::mc_Minimum)/2;
  static int const fc_OverMinimum=SIZE_C::mc_Minimum+1;
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Woverflow"
  static int const fc_OverMaximum=SIZE_C::mc_Maximum+1; // Overflow okay.
  #pragma GCC diagnostic pop
  static int const fc_UnderMaximum=SIZE_C::mc_Maximum-1;
  static int const fc_UnderMinimum=SIZE_C::mc_Minimum-1;

  BOOST_AUTO_TEST_SUITE(Function__DefaultConstructor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size;


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Constructor_int_int)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(fc_UnderMinimum,0);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(SIZE_C::mc_Minimum,0);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(fc_OverMinimum,0);


      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(fc_Normal,0);


      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(fc_UnderMaximum,0);


      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(SIZE_C::mc_Maximum,0);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      SIZE_C Size(fc_OverMaximum,0);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==0);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      SIZE_C Size(0,fc_UnderMinimum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      SIZE_C Size(0,SIZE_C::mc_Minimum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      SIZE_C Size(0,fc_OverMinimum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      SIZE_C Size(0,fc_Normal);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      SIZE_C Size(0,fc_UnderMaximum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_0C)
    {
      SIZE_C Size(0,SIZE_C::mc_Maximum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_0D)
    {
      SIZE_C Size(0,fc_OverMaximum);


      BOOST_TEST(Size.GetWidth()==0);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__EqualityOperator_InequalityOperator)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_UnderMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_15)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_16)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_17)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_18)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_19)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_1B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_20)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_21)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_22)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_23)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_24)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_25)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_26)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_27)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_28)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_29)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_2A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_2B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_OverMinimum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_30)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_31)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_32)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_33)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_34)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_35)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_36)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_37)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_38)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_39)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_3A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_3B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_UnderMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_40)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_41)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_42)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_43)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_44)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_45)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_46)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_47)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_48)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_49)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_4A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_4B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_50)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_51)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_52)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_53)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_54)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_55)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_56)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_57)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_58)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_59)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_5A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_5B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_OverMaximum,fc_Normal);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_60)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_61)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_62)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_63)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_64)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_65)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_66)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_67)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_68)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_69)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_6A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_6B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,fc_UnderMinimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_70)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_71)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_72)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_73)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_74)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_75)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_76)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_77)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_78)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_79)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_7A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_7B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_80)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_81)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_82)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_83)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_84)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_85)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_86)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_87)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_88)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_89)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_8A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_8B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,fc_OverMinimum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_90)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_91)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_92)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_93)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_94)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_95)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_96)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_97)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_98)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_99)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_9A)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_9B)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,fc_UnderMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A0)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A1)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A2)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A3)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A4)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A5)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A6)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A7)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A8)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_A9)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_AA)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_AB)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B0)
    {
      SIZE_C Size0(fc_UnderMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B1)
    {
      SIZE_C Size0(SIZE_C::mc_Minimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B2)
    {
      SIZE_C Size0(fc_OverMinimum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B3)
    {
      SIZE_C Size0(fc_UnderMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B4)
    {
      SIZE_C Size0(SIZE_C::mc_Maximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B5)
    {
      SIZE_C Size0(fc_OverMaximum,fc_Normal);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B6)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMinimum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_B7)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Minimum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B8)
    {
      SIZE_C Size0(fc_Normal,fc_OverMinimum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_B9)
    {
      SIZE_C Size0(fc_Normal,fc_UnderMaximum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_BA)
    {
      SIZE_C Size0(fc_Normal,SIZE_C::mc_Maximum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==false);
      BOOST_TEST((Size0!=Size1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_BB)
    {
      SIZE_C Size0(fc_Normal,fc_OverMaximum);
      SIZE_C Size1(fc_Normal,fc_OverMaximum);


      BOOST_TEST((Size0==Size1)==true);
      BOOST_TEST((Size0!=Size1)==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetWidth)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(fc_UnderMinimum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(SIZE_C::mc_Minimum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(fc_OverMinimum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(fc_UnderMaximum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(SIZE_C::mc_Maximum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(fc_OverMaximum,fc_Normal);


      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__GetHeight)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(fc_Normal,fc_UnderMinimum);


      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(fc_Normal,SIZE_C::mc_Minimum);


      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(fc_Normal,fc_OverMinimum);


      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(fc_Normal,fc_UnderMaximum);


      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(fc_Normal,SIZE_C::mc_Maximum);


      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(fc_Normal,fc_OverMaximum);


      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Invalidate)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(0,0);


      BOOST_TEST(Size.IsValid()==true);

      Size.Invalidate();

      BOOST_TEST(Size.IsValid()==false);
      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__IsValid)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size;


      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size;


      Size.SetWidth(fc_Normal);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size;


      Size.SetHeight(fc_Normal);
      BOOST_TEST(Size.IsValid()==false);
    }

    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size;
      Size.SetWidth(fc_Normal);
      Size.SetHeight(fc_Normal);
      BOOST_TEST(Size.IsValid()==true);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetWidth)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetWidth(fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_Normal);
      BOOST_TEST(Size.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetWidthHeight)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_15)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_20)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_21)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_22)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_23)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_24)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_25)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_30)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_31)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_32)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_33)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_34)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_35)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_40)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_41)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_42)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_43)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_44)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_45)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_50)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMinimum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_51)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Minimum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_52)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMinimum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==fc_OverMinimum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_53)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_UnderMaximum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==fc_UnderMaximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_54)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(SIZE_C::mc_Maximum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_55)
    {
      SIZE_C Size(0,0);


      Size.SetWidthHeight(fc_OverMaximum,fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetHeight)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(fc_UnderMinimum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(SIZE_C::mc_Minimum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Minimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(fc_OverMinimum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==fc_OverMinimum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(fc_UnderMaximum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==fc_UnderMaximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(SIZE_C::mc_Maximum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Maximum);
      BOOST_TEST(Size.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      SIZE_C Size(fc_Normal,fc_Normal);


      Size.SetHeight(fc_OverMaximum);

      BOOST_TEST(Size.GetWidth()==fc_Normal);
      BOOST_TEST(Size.GetHeight()==SIZE_C::mc_Invalid);
      BOOST_TEST(Size.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()


/**
*** test_class_SIZE.cpp
**/

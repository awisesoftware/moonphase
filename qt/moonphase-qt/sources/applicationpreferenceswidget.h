/**
*** \file applicationpreferenceswidget.h
*** \brief Application preferences widget.
*** \details A widget which allows the user to change general preferences for
***   the application.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(APPLICATIONPREFERENCESWIDGET_H)
/**
*** \brief applicationpreferenceswidget.h identifier.
*** \details Identifier for applicationpreferenceswidget.h.
*** \internal
**/
#define   APPLICATIONPREFERENCESWIDGET_H


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"

#include  <QWidget>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief QAbstract forward declaration.
*** \details Forward declaration of QAbstractButton.
**/
class QAbstractButton;

/**
*** \brief Application preferences widget class.
*** \details A widget class which allows the user to change general preferences
***   for the application.
**/
class APPLICATIONPREFERENCESWIDGET_C : public QWidget
{
  Q_OBJECT

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    *** \param pParent Pointer to parent widget.
    **/
    APPLICATIONPREFERENCESWIDGET_C(QWidget *pParent=nullptr);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param Widget Object to copy.
    **/
    APPLICATIONPREFERENCESWIDGET_C(
        APPLICATIONPREFERENCESWIDGET_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~APPLICATIONPREFERENCESWIDGET_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    APPLICATIONPREFERENCESWIDGET_C
      operator=(APPLICATIONPREFERENCESWIDGET_C const &Widget)=delete;

    bool PreferencesChanged(void);

    void SavePreferences(void);

    /**
    *** \brief Set preferences pointer.
    *** \details Sets the preferences pointer of the preferences widget.
    *** \param pPreferences Pointer to preferences.
    **/
    void SetPreferencesPointer(PREFERENCES_C::APPLICATION_C *pPreferences);

  private:
    /**
    *** \brief Hide event.
    *** \details The widget is hidden.
    *** \param pEvent Event data.
    **/
    void hideEvent(QHideEvent *pEvent) override;

    /**
    *** \brief Set preferences.
    *** \details Sets the preferences of the preferences widget.
    *** \param Preferences Preferences data.
    **/
    void SetPreferences(PREFERENCES_C::APPLICATION_C Preferences);

    /**
    *** \brief Shows/hides the control panel.
    *** \details Shows/hides the control panel, may also start or stop the
    ***   animation timer.
    *** \param VisibleFlag 0=Hide control panel.\n!0=Show control panel.
    **/
    void SetVisible(bool VisibleFlag);

    /**
    *** \brief Show event.
    *** \details The widget is shown.
    *** \param pEvent Event data.
    **/
    void showEvent(QShowEvent *pEvent) override;

  private slots:
    /**
    *** \brief Animation timer triggered.
    *** \details The animation timer has been triggered, update the preview
    ***   image.
    **/
    void AnimationTimerTriggeredSlot(void);

    /**
    *** \brief Button box button clicked.
    *** \details A button in the button box was clicked.
    *** \param pButton Button data.
    **/
    void ButtonBoxButtonClickedSlot(QAbstractButton *pButton);

    /**
    *** \brief Preferences changed.
    *** \details The preferences have changed.
    **/
    void PreferencesChangedSlot(void);

    /**
    *** \brief Tray icon preferences changed.
    *** \details Preferences were changed which effect the tray icon.
    **/
    void TrayIconPreferencesChangedSlot(void);

  signals:
    /**
    *** \brief Tray icon preferences changed signal.
    *** \details Signal emitted when preferences are changed which effect the
    ***   tray icon.
    **/
    void TrayIconPreferencesChangedSignal(void);

  private:
    class APPLICATIONPREFERENCESWIDGETPRIVATE_C;
    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend APPLICATIONPREFERENCESWIDGETPRIVATE_C;
    /**
    *** \brief Implementation pointer (PIMPL).
    *** \details Pointer to implementation object (PIMPL).
    **/
    QScopedPointer<APPLICATIONPREFERENCESWIDGETPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(APPLICATIONPREFERENCESWIDGET_H) */


/**
*** applicationpreferenceswidget.h
**/

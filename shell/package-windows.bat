@echo off

:: %1 - Project directory (required).
:: %2 - Project (required).
:: %3 - Version (required).
:: %4 - Distribution (unused).
:: %5 - Architecture (required).

set ErrorFlag=0
if "%5"=="" (
  echo Error: missing parameters.
  set ErrorFlag=1
)
if not "%6"=="" (
  echo Error: extra parameters.
  set ErrorFlag=1
)
if not "%ErrorFlag%"=="0" (
  echo "Usage: $0 [projectdir] [projectname] [projectversion] [distribution] [x86 | x86_64]"
  exit /b 1
)

:: Unable to pass environment variables from child process to parent,
::  so the script is inlined.

:: windows-env.bat %1


:: Parameter checking.
if "%5"=="" (
  echo
  echo Error: missing architecture.
  exit /b 1
)

:: Set up architecture variable.
setlocal
set Arch=
set OneOfThree=
set QtArch=
if "%5"=="x86" (
  set Arch=x86
  set OneOfThree=x86
  set QtArch=msvc2015
)
if "%5"=="x86_64" (
  set Arch=amd64
  set OneOfThree=x64
  set QtArch=msvc2017_64
)

if "%Arch%"=="" (
  echo.
  echo Error: unsupported architecture ^(%5^). Use "x86" or "x86_64".
  exit /b 1
)

call "%VCVARS_ROOT_DIR%\vcvarsall.bat" %Arch%
if ERRORLEVEL 1 exit /b 1
set CMAKE_PREFIX_PATH=%QT5_ROOT_DIR%\%5;%PACKAGES_ROOT_DIR%\%5
set PATH=%PATH%;%CMAKE_ROOT_DIR%\bin
set PATH=%PATH%;%QT5_ROOT_DIR%\%QtArch%\bin
set PATH=%PATH%;%CYGWIN_ROOT_DIR%\bin
set VCPKG_TARGET_TRIPLET="%OneOfThree%-windows-static"


:: windows-env.bat

if ERRORLEVEL 1 exit /b 1
call shell\windows-build.bat
if ERRORLEVEL 1 exit /b 1
cd .build
if ERRORLEVEL 1 exit /b 1
cpack ..
if ERRORLEVEL 1 exit /b 1
mv %2-qt*.exe ..
if ERRORLEVEL 1 exit /b 1
cd ..

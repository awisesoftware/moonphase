/**
*** \file panelpreferencesdialog.h
*** \brief Information panel preferences dialog box.
*** \details A dialog box which is used to set the preferences for the
***   information panel.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELPREFERENCESDIALOG_H)
/**
*** \internal
*** \brief panelpreferencesdialog.h identifier.
*** \details Identifier for panelpreferencesdialog.h.
*** \endinternal
**/
#define   PANELPREFERENCESDIALOG_H


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"

#include  <QDialog>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class QAbstractButton;

/**
*** \brief Information panel preferences dialog box.
*** \details A dialog box which is used to set the preferences for the
***   information panel.
**/
class PANELPREFERENCESDIALOG_C : protected QDialog
{
  Q_OBJECT

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    *** \param pParent Pointer to parent widget.
    **/
    PANELPREFERENCESDIALOG_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELPREFERENCESDIALOG_C(PANELPREFERENCESDIALOG_C const &Dialog)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELPREFERENCESDIALOG_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELPREFERENCESDIALOG_C const & operator=(
        PANELPREFERENCESDIALOG_C const &Dialog)=delete;

    /**
    *** \brief Display panel preferences dialog box.
    *** \details Displays the panel preferences dialog box.
    *** \param pPreferences Pointer to panel display preferences.
    *** \param pDialogData Pointer to dialog data.
    *** \retval 0 Dialog "canceled" (rejected).
    *** \retval 1 Dialog "okayed" (accepted).
    **/
    int Execute(PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C *pPreferences,
        PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C const *pDialogData);

  protected slots:
    /**
    *** \brief Enable/disable the "Apply" button.
    *** \brief Enables/disables the "Apply" button as preferences are changed.
    **/
    void PreferenceChangedSlot(void);

  private slots:
    /**
    *** \brief Button box button clicked.
    *** \details A button in the button box was clicked.
    *** \param pButton Button data.
    **/
    void ButtonBoxButtonClickedSlot(QAbstractButton *pButton);

  private:
    /**
    *** \brief Panel preferences dialog box implementation class.
    *** \details The implementation class for the panel preferences dialog box.
    **/
    class PANELPREFERENCESDIALOGPRIVATE_C;
    friend class PANELPREFERENCESDIALOGPRIVATE_C;
    /**
    *** \brief Implementation pointer.
    *** \details Pointer to implementation.
    **/
    QScopedPointer<PANELPREFERENCESDIALOGPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELPREFERENCESDIALOG_H) */


/**
*** panelpreferencesdialog.h
**/

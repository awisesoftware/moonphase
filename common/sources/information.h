/**
*** \file information.h
*** \brief Information display data.
*** \details Data needed to display user selected information in various
***   formats and units.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(INFORMATION_H)
/**
*** \brief information.h identifier.
*** \details Identifier for information.h.
*** \internal
**/
#define   INFORMATION_H


/****
*****
***** INCLUDES
*****
****/

#include  "datetime.h"
#include  "moondata.h"

#include  <string>
#include  <vector>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Edit mode.
*** \details The edit mode of the dialog box. This determines which options
***   can be edited by the user.
**/
typedef enum enumEDITMODE
{
  EDITMODE_INVALID=-1,
      /**< Invalid edit mode. In this mode, no dialog box will be displayed. **/
  EDITMODE_DISPLAY=0,
      /**< Display edit mode. In this mode, the user can only edit display
          options (colors, font, etc.). **/
  EDITMODE_UNITS,
      /**< Units edit mode. In this mode, in addition to the display options,
          the user also can select the units of measurement. **/
  EDITMODE_TIME,
      /**< Time edit mode. In this mode, in addition to the display options, the
          user also can edit how the time is displayed. **/
  EDITMODE_DATETIME
      /**< Date/time edit mode. In this mode, in addition to the display options,
          the user also can edit how the date and time are displayed. **/
} EDITMODE_T;


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Get default unit format.
*** \details Returns the default unit format for a information index.
*** \param InfoItemIndex Information item index.
*** \param MetricFlag Use metric units flag.\n
***   false - Use standard units,\n true - Use metric units.
*** \return Default unit format index.
**/
int Information_GetDefaultUnitFormatIndex(
    int InfoItemIndex,bool MetricFlag);

/**
*** \brief Get information item edit mode.
*** \details Returns the mode used to determine what data is editable
***   (modifiable) and how to edit it for each information index.
*** \param InfoItemIndex Information item index.
*** \return Edit mode.
**/
EDITMODE_T Information_GetEditMode(int InfoItemIndex);

/**
*** \brief Get information index.
*** \details Returns the information index of an information label.
*** \param Label Information label.
*** \returns Information index.
**/
int Information_GetInformationIndex(std::string const &Label);

/**
*** \brief Get label for information item.
*** \details Returns a label for an information index.
*** \param InfoItemIndex Information item index.
*** \return Label.
**/
std::string Information_GetInformationLabel(int InfoItemIndex);

/**
*** \brief Get all information index labels.
*** \details Returns the labels for all information indices.
*** \return Label list.
**/
std::vector<std::string> Information_GetInformationLabels(void);

/**
*** \brief Get needs coordinates flag.
*** \details Returns a value indicating whether the information index needs the
***   location coordinates to be set in order for the results to be accurate.
*** \param InfoItemIndex Information item index.
*** \retval false Information index data is independent of location.
*** \retval true Information index data is dependent on location.
**/
bool Information_GetNeedsCoordinatesFlag(int InfoItemIndex);

/**
*** \brief Get unit format description list.
*** \details Returns descriptions of possible unit formats for an information
***   item.
*** \param InfoItemIndex Information item index.
*** \return Unit format description list.
**/
std::vector<std::string>
    Information_GetUnitFormatDescriptionList(int InfoItemIndex);

/**
*** \brief Get unit/format index.
*** \details Returns the unit/format index of a unit/format label.
*** \param InfoItemIndex Information item index.
*** \param Label Unit/format label.
*** \returns Unit/format index.
**/
int Information_GetUnitFormatIndex(int InfoItemIndex,std::string const &Label);

/**
*** \brief Get unit/format label.
*** \details Returns the unit/format label of a unit/format index.
*** \param InfoItemIndex Information item index.
*** \param UnitFormatIndex Unit/format index.
*** \returns Unit/format label.
**/
std::string Information_GetUnitFormatLabel(
    int InfoItemIndex,int UnitFormatIndex);

/**
*** \brief Prints item index information.
*** \details Formats and prints item information using the additional passed
***   parameters.
*** \param MoonData Moon data.
*** \param InfoItemIndex Information item index.
*** \param UnitFormatIndex Unit format index.
*** \param Options Additional formatting options.
*** \return
**/
std::string Information_Print(
    MOONDATA_T const &MoonData,int InfoItemIndex,int UnitFormatIndex,
    DATETIMEOPTIONS_C const &Options);


#endif    /* !defined(INFORMATION_H) */


/**
*** information.h
**/

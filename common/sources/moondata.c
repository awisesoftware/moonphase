/**
*** \file moondata.c
*** \brief moondata.h implementation.
*** \details Implementation file for moondata.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief moondata.cpp identifier.
*** \details Identifier for moondata.cpp.
*** \internal
**/
#define   MOONDATA_C


/****
*****
***** INCLUDES
*****
****/

#include  "moondata.h"
#if       defined(DEBUG_MOONDATA_C)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MOONDATA_C) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "sysdefs.h"

#include  <math.h>
#include  <time_msvc.h>


/****
*****
***** DEFINES
*****
****/

#define   HOURSINLUNARMONTH   (24*29.5)   // 24 hours/day * 29.5 days


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief 'calcephem' global variable.
*** \details Global variable used interface with 'calcephem'.
**/
extern double Glon;
/**
*** \brief 'calcephem' global variable.
*** \details Global variable used interface with 'calcephem'.
**/
extern double TimeZone;


/****
*****
***** PROTOTYPES
*****
****/

static STRUCTURE_PROTOTYPE_INITIALIZEMEMBERS(MoonData,MOONDATA_T);
static STRUCTURE_PROTOTYPE_UNINITIALIZEMEMBERS(MoonData,MOONDATA_T);


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief Find the date/time of a given moon phase.
*** \details Finds the given phase of the moon which is in the interval
***   containing the given date/time.
*** \param GMDate Date in GMT.
*** \param GMTime Time in GMT.
*** \param Phase Phase of the moon to find. The interval is [0,1), where
***   0 is a new moon, .5 is a full moon.
*** \returns The date/time of the given phase.
**/
static time_t FindPhase(long GMDate,double GMTime,double Phase)
{
  CTrans Data;
  double GMTime0;
  double Percent0;
  double GMTime1;
  double Percent1;
  double GMTempTime;
  struct tm GMTimetm;


  DEBUGLOG_Printf3("FindPhase(%f,%f,%f)",GMDate,GMTime,Phase);
  DEBUGLOG_LogIn();

  /* Calculate current phase. */
  memset(&Data,0,sizeof(Data));
  GMTime0=GMTime;
  CalcEphem(GMDate,GMTime0,&Data);
  Percent0=Data.MoonPhase;

  /* If already past target phase, select next one. */
  if (Percent0>=Phase)
    GMTime0+=HOURSINLUNARMONTH;

  /* Get GMT first estimate close to the desired phase. */
  GMTime0+=HOURSINLUNARMONTH*(Phase-Percent0);

  /* Calculate result of first estimate at target phase time. */
  CalcEphem(GMDate,GMTime0,&Data);
  Percent0=Data.MoonPhase-Phase;

  /* Second estimate at target phase time. */
  GMTime1=GMTime0-8;  // 8hour

  /* Keep estimating until "close enough". */
  while(fabs(Percent0)>0.000001)  // Necessary for the proper input resolution.
  {
    /* Calculate result of estimate. */
    CalcEphem(GMDate,GMTime1,&Data);
    Percent1=Data.MoonPhase-Phase;

    /* Estimates may drop down into the previous interval, but never up into
        the next interval. Because the values are not continuous between
        intervals (from .99 (99%) to 0 (0%)), if the result ends up in the
        previous interval, subtract 1 (100%) from the calculated result. */
    if (Percent1>.9)
      Percent1=Percent1-1;

    /* Calculate the next estimate (secant method). */
    GMTempTime=GMTime1-Percent1*(GMTime1-GMTime0)/(Percent1-Percent0);

    /* The current data becomes the old data, and the new estimate becomes
        the current estimate. */
    GMTime0=GMTime1;
    GMTime1=GMTempTime;
    Percent0=Percent1;
  }

  /* Build the time. */
  memset(&GMTimetm,0,sizeof(GMTimetm));
  GMTimetm.tm_year=(int)((GMDate/10000)-1900);
  GMDate-=(GMTimetm.tm_year+1900)*10000;
  GMTimetm.tm_mon=(int)(GMDate/100-1);
  GMDate-=(GMTimetm.tm_mon+1)*100;
  GMTimetm.tm_mday=(int)GMDate;
  GMTimetm.tm_hour=(int)GMTime0;
  GMTime0-=GMTimetm.tm_hour;
  GMTime0*=60.0;
  GMTimetm.tm_min=(int)GMTime0;
  GMTime0-=GMTimetm.tm_min;
  GMTime0*=60.0;
  GMTimetm.tm_sec=(int)GMTime0;

  DEBUGLOG_LogOut();
  return(timegm(&GMTimetm));
}

STRUCTURE_FUNCTION_INITIALIZE(MoonData,MOONDATA_T)

static STRUCTURE_PROTOTYPE_INITIALIZEMEMBERS(MoonData,MOONDATA_T)
{
  ERRORCODE_T ErrorCode;
  UNUSED(pStructure);


  DEBUGLOG_Printf1("MoonData_InitializeMembers(%p)",pStructure);
  DEBUGLOG_LogIn();

  ErrorCode=ERRORCODE_SUCCESS;

  DEBUGLOG_LogOut();
  return(ErrorCode);
}

STRUCTURE_FUNCTION_UNINITIALIZE(MoonData,MOONDATA_T)

static STRUCTURE_PROTOTYPE_UNINITIALIZEMEMBERS(MoonData,MOONDATA_T)
{
  ERRORCODE_T ErrorCode;
  UNUSED(pStructure);


  DEBUGLOG_Printf1("MoonData_UninitializeMembers(%p)",pStructure);
  DEBUGLOG_LogIn();

  ErrorCode=ERRORCODE_SUCCESS;

  DEBUGLOG_LogOut();
  return(ErrorCode);
}

float MoonData_GetMoonPhasePercent(MOONDATA_T const *pMoonData)
{
  float Percent;


  DEBUGLOG_Printf1("MoonData_GetMoonPhasePercent(%p)",pMoonData);
  DEBUGLOG_LogIn();

  Percent=(float)(100.0*pMoonData->CTransData.MoonPhase);

  DEBUGLOG_LogOut();
  return(Percent);
}

void MoonData_Recalculate(MOONDATA_T *pMoonData,time_t GMTt)
{
  struct tm GMTtm;
  struct tm LTtm;
  double GMTime;
  long GMDate;
  struct tm AdjustedGMtm;
  time_t NormalizedGMt;


  DEBUGLOG_Printf1("MoonData_Recalculate(%p)",pMoonData);
  DEBUGLOG_LogIn();

  /* Get the GM time. */
  GMTtm=*gmtime(&GMTt);

  /* Get the local time. */
  LTtm=*localtime(&GMTt);

  /* Convert time to hours. */
  GMTime=GMTtm.tm_hour+GMTtm.tm_min/60.0+GMTtm.tm_sec/3600.0;

  /* Covert date to CalcEphem format (YYYYMMDD).*/
  GMDate=10000*(GMTtm.tm_year+1900)+100*(GMTtm.tm_mon+1)+GMTtm.tm_mday;

  /* Update moon data. */
  Glon=pMoonData->CTransData.Glon;
  TimeZone=GMTtm.tm_hour-LTtm.tm_hour;
  CalcEphem(GMDate,GMTime,&pMoonData->CTransData);

  /* Update yesterdays rise/set times. */
  memcpy(&AdjustedGMtm,&GMTtm,sizeof(AdjustedGMtm));
  AdjustedGMtm.tm_mday--;                 /* Yesterday. */
  NormalizedGMt=timegm(&AdjustedGMtm);    /* Normalize the time data. */
  GMTtm=*gmtime(&NormalizedGMt);          /* Back to tm. */
  MoonRise(GMTtm.tm_year+1900,GMTtm.tm_mon+1,GMTtm.tm_mday,0.0,
    &pMoonData->YesterdaysRiseLT,&pMoonData->YesterdaysSetLT);

  /* Update todays rise/set times. */
  AdjustedGMtm.tm_mday++;                 /* Today. */
  NormalizedGMt=mktime(&AdjustedGMtm);    /* Normalize the time data. */
  GMTtm=*gmtime(&NormalizedGMt);          /* Back to tm. */
  MoonRise(GMTtm.tm_year+1900,GMTtm.tm_mon+1,GMTtm.tm_mday,0.0,
    &pMoonData->TodaysRiseLT,&pMoonData->TodaysSetLT);

  /* Update tomorrows rise/set times. */
  AdjustedGMtm.tm_mday++;                 /* Tomorrow. */
  NormalizedGMt=mktime(&AdjustedGMtm);    /* Normalize the time data. */
  GMTtm=*gmtime(&NormalizedGMt);          /* Back to tm. */
  MoonRise(GMTtm.tm_year+1900,GMTtm.tm_mon+1,GMTtm.tm_mday,0.0,
    &pMoonData->TomorrowsRiseLT,&pMoonData->TomorrowsSetLT);

  /* Calculate various next moon phases as necessary. */
  if ( (pMoonData->NextNewMoonGMt==0) ||
      (pMoonData->NextNewMoonGMt<GMTt) )
    pMoonData->NextNewMoonGMt=FindPhase(GMDate,GMTime,0);
  if ( (pMoonData->NextFirstQuarterMoonGMt==0) ||
      (pMoonData->NextFirstQuarterMoonGMt<GMTt) )
    pMoonData->NextFirstQuarterMoonGMt=FindPhase(GMDate,GMTime,.25);
  if ( (pMoonData->NextFullMoonGMt==0) ||
      (pMoonData->NextFullMoonGMt<GMTt) )
    pMoonData->NextFullMoonGMt=FindPhase(GMDate,GMTime,.5);
  if ( (pMoonData->NextLastQuarterMoonGMt==0) ||
      (pMoonData->NextLastQuarterMoonGMt<GMTt) )
    pMoonData->NextLastQuarterMoonGMt=FindPhase(GMDate,GMTime,.75);

  DEBUGLOG_LogOut();
  return;
}


#undef    MOONDATA_C


/**
*** moondata.c
**/

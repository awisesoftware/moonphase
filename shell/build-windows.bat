@echo off
setlocal enabledelayedexpansion

rem Parameters:
rem   %1 - Architecture (optional, defaults to "x86_64").
set Arg1=%1

rem Parameter checking.
if not [%2]==[] (
  echo Error: extra parameters.
) else (
  if [%Arg1%]==[] (
    set Arg1=x86_64
  )
rem MVS 2015,2017 TODO?
rem MVS 2019
  if [!Arg1!]==[x86_64] (
    set MVSArch=amd64
    set OneOfThree=x64
    set QtArch=x86_64
  )
  if [!MVSArch!]==[] (
    echo Error: unsupported architecture ^(%1^).
  )
)
if [%MVSArch%]==[] (
  echo.
  echo Usage: $0 ^[x86_64^]
  exit /b 1
)

rem Check environment variables.
if [%CMAKE_ROOT_DIR%]==[] (
  echo Error: CMAKE_ROOT_DIR is not set.
  exit /b 1
)
if [%CYGWIN_ROOT_DIR%]==[] (
  echo Error: CYGWIN_ROOT_DIR is not set.
  exit /b 1
)
if [%QT5_ROOT_DIR%]==[] (
  echo Error: QT5_ROOT_DIR is not set.
  exit /b 1
)
if [%VCPKG_ROOT_DIR%]==[] (
  echo Error: VCPKG_ROOT_DIR is not set.
  exit /b 1
)

rem Set up the environment.
call "%VCVARS_ROOT_DIR%\vcvarsall.bat" %MVSArch%
if ERRORLEVEL 1 exit /b 1
set CMAKE_PREFIX_PATH=%QT5_ROOT_DIR%\%QtArch%\
set PATH=%PATH%;%CMAKE_ROOT_DIR%\bin\
set PATH=%PATH%;%QT5_ROOT_DIR%\%QtArch%\bin\
set PATH=%PATH%;%CYGWIN_ROOT_DIR%\bin\
set VCPKG_TARGET_TRIPLET="%OneOfThree%-windows-static"

rem Build.
rmdir /s /q .build >nul 2>&1
mkdir .build
if not ERRORLEVEL 1 (
  cd .build
  if not ERRORLEVEL 1 cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT_DIR%\scripts\buildsystems\vcpkg.cmake" -DVCPKG_TARGET_TRIPLET=%VCPKG_TARGET_TRIPLET% -DMOONPHASE_OPTION_STATICLINK=true -G "NMake Makefiles" ..
  if not ERRORLEVEL 1 nmake /s /nologo
  cd ..
)

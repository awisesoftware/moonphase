/**
*** \file test_class_FONT.cpp
*** \brief FONT_C unit tests.
*** \details Unit tests for FONT_C.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Class__FONT)

//  static int const fc_Range=FONT_C::mc_Maximum-FONT_C::mc_Minimum;
//  static int const fc_Normal=(FONT_C::mc_Maximum+FONT_C::mc_Minimum)/2;
//  static int const fc_OverMinimum=FONT_C::mc_Minimum+1;
//  #pragma GCC diagnostic push
//  #pragma GCC diagnostic ignored "-Woverflow"
//  static int const fc_OverMaximum=FONT_C::mc_Maximum+1; // Overflow okay.
//  #pragma GCC diagnostic pop
//  static int const fc_UnderMaximum=FONT_C::mc_Maximum-1;
//  static int const fc_UnderMinimum=FONT_C::mc_Minimum-1;

  BOOST_AUTO_TEST_SUITE(Function__DefaultConstructor)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Constructor_stdstring_int_int_bool)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font("");


      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font("FontName");


      BOOST_TEST(Font.GetFamily()=="FontName");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      FONT_C Font("FontName",7);


      BOOST_TEST(Font.GetFamily()=="FontName");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==7);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      FONT_C Font("FontName",7,11);


      BOOST_TEST(Font.GetFamily()=="FontName");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==7);
      BOOST_TEST(Font.GetWeight()==11);
      BOOST_TEST(Font.IsValid()==true);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      FONT_C Font("FontName",7,11,true);


      BOOST_TEST(Font.GetFamily()=="FontName");
      BOOST_TEST(Font.GetItalicFlag()==true);
      BOOST_TEST(Font.GetPointSize()==7);
      BOOST_TEST(Font.GetWeight()==11);
      BOOST_TEST(Font.IsValid()==true);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__EqualityOperator_InequalityOperator)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font0;
      FONT_C Font1;


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font0("");
      FONT_C Font1;


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      FONT_C Font0("Name");
      FONT_C Font1;


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      FONT_C Font0("Name",5);
      FONT_C Font1;


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      FONT_C Font0("Name",5,7);
      FONT_C Font1;


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      FONT_C Font0("Name");
      FONT_C Font1("Name");


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      FONT_C Font0("Name",5);
      FONT_C Font1("Name");


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      FONT_C Font0("Name",5,7);
      FONT_C Font1("Name");


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      FONT_C Font0("Name");
      FONT_C Font1("Name",5);


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      FONT_C Font0("Name",5);
      FONT_C Font1("Name",5);


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      FONT_C Font0("Name",5,7);
      FONT_C Font1("Name",5);


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      FONT_C Font0("Name");
      FONT_C Font1("Name",5,7);


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0C)
    {
      FONT_C Font0("Name",5);
      FONT_C Font1("Name",5,7);


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0D)
    {
      FONT_C Font0("Name",5,7);
      FONT_C Font1("Name",5,7);


      BOOST_TEST((Font0==Font1)==true);
      BOOST_TEST((Font0!=Font1)==false);
    }
    BOOST_AUTO_TEST_CASE(Test_0E)
    {
      FONT_C Font0("Name",11,7);
      FONT_C Font1("Name",5,7);


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
    BOOST_AUTO_TEST_CASE(Test_0F)
    {
      FONT_C Font0("Name",5,13);
      FONT_C Font1("Name",5,7);


      BOOST_TEST((Font0==Font1)==false);
      BOOST_TEST((Font0!=Font1)==true);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Invalidate)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font("Name",3,5);


      BOOST_TEST(Font.IsValid()==true);

      Font.Invalidate();

      BOOST_TEST(Font.IsValid()==false);
      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__IsValid)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font("Name");


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      FONT_C Font("Name",3);


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      FONT_C Font("Name",-3,-5);


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      FONT_C Font("Name",3,-5);


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      FONT_C Font("Name",-3,5);


      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      FONT_C Font("Name",3,5);


      BOOST_TEST(Font.IsValid()==true);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetFamily)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      Font.SetFamily("");

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()=FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font;


      Font.SetFamily("Name");

      BOOST_TEST(Font.GetFamily()=="Name");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetItalicFlag)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      Font.SetItalicFlag(false);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()=FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font;


      Font.SetItalicFlag(true);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==true);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetPointSize)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      Font.SetPointSize(-1);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()=FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font;


      Font.SetPointSize(0);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==0);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      FONT_C Font;


      Font.SetPointSize(1);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==1);
      BOOST_TEST(Font.GetWeight()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__SetWeight)
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      FONT_C Font;


      Font.SetWeight(-1);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()=FONT_C::mc_Invalid);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      FONT_C Font;


      Font.SetWeight(0);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==0);
      BOOST_TEST(Font.IsValid()==false);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      FONT_C Font;


      Font.SetWeight(1);

      BOOST_TEST(Font.GetFamily()=="");
      BOOST_TEST(Font.GetItalicFlag()==false);
      BOOST_TEST(Font.GetPointSize()==FONT_C::mc_Invalid);
      BOOST_TEST(Font.GetWeight()==1);
      BOOST_TEST(Font.IsValid()==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()


/**
*** test_class_FONT.cpp
**/

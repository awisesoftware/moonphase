Troubleshooting
---------------

Users on certain versions of Windows have reported seeing a "System Error" dialog box when trying to start the program.

If the dialog box reads: "The program can't start because MSVCR100.dll is missing from your computer.  Try reinstalling the program to fix this problem.", you will need to download and install an update from Microsoft via the following links:
  32 bit Windows (x86)
    https://www.microsoft.com/en-us/download/details.aspx?id=8328
  64 bit Windows (x64)
    https://www.microsoft.com/en-us/download/details.aspx?id=13523

If the dialog box reads: "The program can't start because VCRUNTIME140.dll is missing from your computer.  Try reinstalling the program to fix this problem.", you will need to download and install an update from Microsoft via the following links:
  32 bit Windows (x86)
    https://www.microsoft.com/en-us/download/details.aspx?id=52685
  64 bit Windows (x64)
    https://www.microsoft.com/en-us/download/details.aspx?id=52685

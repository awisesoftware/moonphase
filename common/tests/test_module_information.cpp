/**
*** \file test_module_information.cpp
*** \brief 'information.cpp' unit tests.
*** \details Unit tests for the functions in 'information.cpp'.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/

#include  <boost/test/unit_test.hpp>

#include  "information.cpp"


/****
*****
***** DEFINES
*****
****/

#define   TEST_TIME_VALUE   (45296)   // 1/1/1970 12:34:56


/****
*****
***** DATA TYPES
*****
****/

class TZSETTER_C
{
  public:
    TZSETTER_C(void)
    {
      setenv("TZ", "PST7PDT",1);
      tzset();
    };
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/


/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Module__information)
  BOOST_AUTO_TEST_SUITE(Function__PrintArcHMS)
    BOOST_AUTO_TEST_CASE(DTime_00)
    {
      BOOST_TEST(PrintArcHMS(720,5)=="0h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_01)
    {
      BOOST_TEST(PrintArcHMS(540,5)=="12h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_02)
    {
      BOOST_TEST(PrintArcHMS(360,5)=="0h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_03)
    {
      BOOST_TEST(PrintArcHMS(180,5)=="12h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_04)
    {
      BOOST_TEST(PrintArcHMS(90,5)=="6h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_05)
    {
      BOOST_TEST(PrintArcHMS(15,5)=="1h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_06)
    {
      BOOST_TEST(PrintArcHMS(1,5)=="0h 4m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_07)
    {
      BOOST_TEST(PrintArcHMS(.5,5)=="0h 2m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_08)
    {
      BOOST_TEST(PrintArcHMS(.1,5)=="0h 0m 24.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_09)
    {
      BOOST_TEST(PrintArcHMS(.01,5)=="0h 0m 2.40000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_1)
    {
      BOOST_TEST(PrintArcHMS(31.4159265,5)=="2h 5m 39.82236s");
    }
    BOOST_AUTO_TEST_CASE(DTime_11)
    {
      BOOST_TEST(PrintArcHMS(-720,5)=="0h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_12)
    {
      BOOST_TEST(PrintArcHMS(-540,5)=="12h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_13)
    {
      BOOST_TEST(PrintArcHMS(-360,5)=="0h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_14)
    {
      BOOST_TEST(PrintArcHMS(-180,5)=="12h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_15)
    {
      BOOST_TEST(PrintArcHMS(-90,5)=="18h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_16)
    {
      BOOST_TEST(PrintArcHMS(-15,5)=="23h 0m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_17)
    {
      BOOST_TEST(PrintArcHMS(-1,5)=="23h 56m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_18)
    {
      BOOST_TEST(PrintArcHMS(-.5,5)=="23h 58m 0.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_19)
    {
      BOOST_TEST(PrintArcHMS(-.1,5)=="23h 59m 36.00000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_20)
    {
      BOOST_TEST(PrintArcHMS(-.01,5)=="23h 59m 57.60000s");
    }
    BOOST_AUTO_TEST_CASE(DTime_21)
    {
      BOOST_TEST(PrintArcHMS(-31.4159265,5)=="21h 54m 20.17764s");
    }
    BOOST_AUTO_TEST_CASE(Precision_00)
    {
      BOOST_TEST(PrintArcHMS(16.22222,0)=="1h 4m 53s");
    }
    BOOST_AUTO_TEST_CASE(Precision_01)
    {
      BOOST_TEST(PrintArcHMS(16.22222,1)=="1h 4m 53.3s");
    }
    BOOST_AUTO_TEST_CASE(Precision_02)
    {
      BOOST_TEST(PrintArcHMS(16.22222,2)=="1h 4m 53.33s");
    }
    BOOST_AUTO_TEST_CASE(Precision_03)
    {
      BOOST_TEST(PrintArcHMS(16.22222,3)=="1h 4m 53.333s");
    }
    BOOST_AUTO_TEST_CASE(Precision_04)
    {
      BOOST_TEST(PrintArcHMS(16.22222,4)=="1h 4m 53.3328s");
    }
    BOOST_AUTO_TEST_CASE(Precision_05)
    {
      BOOST_TEST(PrintArcHMS(16.22222,5)=="1h 4m 53.33280s");
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__WideStringToString)
    BOOST_AUTO_TEST_CASE(Value_00)
    {
      BOOST_TEST(WideStringToString(L"\u00B0")=="°");
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetDefaultUnitFormatIndex)
    BOOST_AUTO_TEST_CASE(BelowLowerBoundTrue)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(-1,true)==-1);
    }
    BOOST_AUTO_TEST_CASE(BelowLowerBoundFalse)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(-1,false)==-1);
    }
    BOOST_AUTO_TEST_CASE(Value_00T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(INFOTYPE_AGE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_01T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_ALTITUDE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_02T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_AZIMUTH,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_03T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_DATETIME,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_04T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_DECLINATION,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_05T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,true)==2);
    }
    BOOST_AUTO_TEST_CASE(Value_06T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_ILLUMINATION,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_07T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_NEXTNEWMOON,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_08T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_NEXTFIRSTQUARTER,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_09T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_NEXTFULLMOON,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_10T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_NEXTLASTQUARTER,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_11T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_PHASEPERCENT,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_12T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_PHASETEXT,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_13T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_RIGHTASCENTION,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_14T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_TODAYSRISE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_15T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_TODAYSSET,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_16T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_TOMORROWSRISE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_17T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_TOMORROWSSET,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_18T)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_VISIBLE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_19T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_YESTERDAYSRISE,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_20T)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_YESTERDAYSSET,true)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_00F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(INFOTYPE_AGE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_01F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_ALTITUDE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_02F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_AZIMUTH,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_03F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_DATETIME,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_04F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_DECLINATION,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_05F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,false)==3);
    }
    BOOST_AUTO_TEST_CASE(Value_06F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_ILLUMINATION,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_07F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_NEXTNEWMOON,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_08F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_NEXTFIRSTQUARTER,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_09F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_NEXTFULLMOON,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_10F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_NEXTLASTQUARTER,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_11F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_PHASEPERCENT,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_12F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_PHASETEXT,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_13F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_RIGHTASCENTION,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_14F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_TODAYSRISE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_15F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_TODAYSSET,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_16F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_TOMORROWSRISE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_17F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_TOMORROWSSET,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_18F)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_VISIBLE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_19F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_YESTERDAYSRISE,false)==0);
    }
    BOOST_AUTO_TEST_CASE(Value_20F)
    {
      BOOST_TEST(Information_GetDefaultUnitFormatIndex(
          INFOTYPE_YESTERDAYSSET,false)==0);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBoundFalse)
    {
      BOOST_TEST(
        Information_GetDefaultUnitFormatIndex(INFOTYPE_YESTERDAYSSET+1,false)==
        -1);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBoundTrue)
    {
      BOOST_TEST(
          Information_GetDefaultUnitFormatIndex(INFOTYPE_YESTERDAYSSET+1,true)==
          -1);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetEditMode)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      BOOST_TEST(Information_GetEditMode(-1)==EDITMODE_INVALID);
    }
    BOOST_AUTO_TEST_CASE(Value_00)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_AGE)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_01)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_ALTITUDE)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_02)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_AZIMUTH)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_03)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_DATETIME)==EDITMODE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Value_04)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_DECLINATION)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_05)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_DISTANCEFROMEARTH)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_06)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_ILLUMINATION)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_07)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_NEXTNEWMOON)==EDITMODE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Value_08)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_NEXTFIRSTQUARTER)==
          EDITMODE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Value_09)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_NEXTFULLMOON)==EDITMODE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Value_10)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_NEXTLASTQUARTER)==EDITMODE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Value_11)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_PHASEPERCENT)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_12)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_PHASETEXT)==EDITMODE_DISPLAY);
    }
    BOOST_AUTO_TEST_CASE(Value_13)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_RIGHTASCENTION)==EDITMODE_UNITS);
    }
    BOOST_AUTO_TEST_CASE(Value_14)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_TODAYSRISE)==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(Value_15)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_TODAYSSET)==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(Value_16)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_TOMORROWSRISE)==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(Value_17)
    {
      EDITMODE_T Mode=Information_GetEditMode(INFOTYPE_TOMORROWSSET);
      BOOST_TEST(Mode==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(Value_18)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_VISIBLE)==UNITTYPE_NONE);
    }
    BOOST_AUTO_TEST_CASE(Value_19)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_YESTERDAYSRISE)==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(Value_20)
    {
      BOOST_TEST(
          Information_GetEditMode(INFOTYPE_YESTERDAYSSET)==EDITMODE_TIME);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      BOOST_TEST(Information_GetEditMode(INFOTYPE_YESTERDAYSSET+1)==-1);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetInformationIndex)
    BOOST_AUTO_TEST_CASE(Test_Invalid)
    {
      BOOST_TEST(Information_GetInformationIndex("")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_00)
    {
      BOOST_TEST(Information_GetInformationIndex("Age")==INFOTYPE_AGE);
    }
    BOOST_AUTO_TEST_CASE(Test_01)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Altitude")==INFOTYPE_ALTITUDE);
    }
    BOOST_AUTO_TEST_CASE(Test_02)
    {
      BOOST_TEST(Information_GetInformationIndex("Azimuth")==INFOTYPE_AZIMUTH);
    }
    BOOST_AUTO_TEST_CASE(Test_03)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Date/time")==INFOTYPE_DATETIME);
    }
    BOOST_AUTO_TEST_CASE(Test_04)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Declination")==INFOTYPE_DECLINATION);
    }
    BOOST_AUTO_TEST_CASE(Test_05)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Distance from earth")==
          INFOTYPE_DISTANCEFROMEARTH);
    }
    BOOST_AUTO_TEST_CASE(Test_06)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Illumination (%)")==
          INFOTYPE_ILLUMINATION);
    }
    BOOST_AUTO_TEST_CASE(Test_07)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Next new moon")==
          INFOTYPE_NEXTNEWMOON);
    }
    BOOST_AUTO_TEST_CASE(Test_08)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Next first quarter")==
          INFOTYPE_NEXTFIRSTQUARTER);
    }
    BOOST_AUTO_TEST_CASE(Test_09)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Next full moon")==
          INFOTYPE_NEXTFULLMOON);
    }
    BOOST_AUTO_TEST_CASE(Test_0A)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Next last quarter")==
          INFOTYPE_NEXTLASTQUARTER);
    }
    BOOST_AUTO_TEST_CASE(Test_0B)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Phase (%)")==INFOTYPE_PHASEPERCENT);
    }
    BOOST_AUTO_TEST_CASE(Test_0C)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Phase (text)")==INFOTYPE_PHASETEXT);
    }
    BOOST_AUTO_TEST_CASE(Test_0D)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Right ascention")==
          INFOTYPE_RIGHTASCENTION);
    }
    BOOST_AUTO_TEST_CASE(Test_0E)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Todays rise")==INFOTYPE_TODAYSRISE);
    }
    BOOST_AUTO_TEST_CASE(Test_0F)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Todays set")==INFOTYPE_TODAYSSET);
    }
    BOOST_AUTO_TEST_CASE(Test_10)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Tomorrows rise")==
          INFOTYPE_TOMORROWSRISE);
    }
    BOOST_AUTO_TEST_CASE(Test_11)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Tomorrows set")==
          INFOTYPE_TOMORROWSSET);
    }
    BOOST_AUTO_TEST_CASE(Test_12)
    {
      BOOST_TEST(Information_GetInformationIndex("Visible")==INFOTYPE_VISIBLE);
    }
    BOOST_AUTO_TEST_CASE(Test_13)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Yesterdays rise")==
          INFOTYPE_YESTERDAYSRISE);
    }
    BOOST_AUTO_TEST_CASE(Test_14)
    {
      BOOST_TEST(
          Information_GetInformationIndex("Yesterdays set")==
          INFOTYPE_YESTERDAYSSET);
    }
  BOOST_AUTO_TEST_SUITE_END()
#if 0
  BOOST_AUTO_TEST_SUITE(Function__Information_GetFormatList)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      bool CaughtFlag=false;


      try
      {
        Information_GetFormatList(-1);
      }
      catch(std::runtime_error const &RE)
      {
        CaughtFlag=true;
      }
      BOOST_TEST(CaughtFlag);
    }
    BOOST_AUTO_TEST_CASE(List_00)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_AGE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_01)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_ALTITUDE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_02)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_AZIMUTH);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_03)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_DATETIME);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_04)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_DECLINATION);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_05)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_DISTANCEFROMEARTH);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_06)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_ILLUMINATION);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_07)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_NEXTNEWMOON);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_08)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_NEXTFIRSTQUARTER);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_09)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_NEXTFULLMOON);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_10)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_NEXTLASTQUARTER);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_11)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_PHASEPERCENT);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_12)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_PHASETEXT);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_13)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_RIGHTASCENTION);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_14)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_TODAYSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_15)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_TODAYSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_16)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_TOMORROWSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_17)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_TOMORROWSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_18)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_VISIBLE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_19)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_YESTERDAYSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_20)
    {
      std::vector<std::string> List=
          Information_GetFormatList(INFOTYPE_YESTERDAYSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      bool CaughtFlag=false;


      try
      {
        Information_GetFormatList(21);
      }
      catch(std::runtime_error const &RE)
      {
        CaughtFlag=true;
      }
      BOOST_TEST(CaughtFlag);
    }
  BOOST_AUTO_TEST_SUITE_END()
#endif
  BOOST_AUTO_TEST_SUITE(Function__Information_GetInformationLabel)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      BOOST_TEST(Information_GetInformationLabel(-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_00)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_AGE)=="Age");
    }
    BOOST_AUTO_TEST_CASE(Value_01)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_ALTITUDE)=="Altitude");
    }
    BOOST_AUTO_TEST_CASE(Value_02)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_AZIMUTH)=="Azimuth");
    }
    BOOST_AUTO_TEST_CASE(Value_03)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_DATETIME)=="Date/time");
    }
    BOOST_AUTO_TEST_CASE(Value_04)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_DECLINATION)=="Declination");
    }
    BOOST_AUTO_TEST_CASE(Value_05)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_DISTANCEFROMEARTH)==
          "Distance from earth");
    }
    BOOST_AUTO_TEST_CASE(Value_06)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_ILLUMINATION)==
          "Illumination (%)");
    }
    BOOST_AUTO_TEST_CASE(Value_07)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_NEXTNEWMOON)==
          "Next new moon");
    }
    BOOST_AUTO_TEST_CASE(Value_08)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_NEXTFIRSTQUARTER)==
          "Next first quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_09)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_NEXTFULLMOON)==
          "Next full moon");
    }
    BOOST_AUTO_TEST_CASE(Value_10)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_NEXTLASTQUARTER)==
          "Next last quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_11)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_PHASEPERCENT)=="Phase (%)");
    }
    BOOST_AUTO_TEST_CASE(Value_12)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_PHASETEXT)=="Phase (text)");
    }
    BOOST_AUTO_TEST_CASE(Value_13)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_RIGHTASCENTION)==
          "Right ascention");
    }
    BOOST_AUTO_TEST_CASE(Value_14)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_TODAYSRISE)=="Todays rise");
    }
    BOOST_AUTO_TEST_CASE(Value_15)
    {
      BOOST_TEST(
          Information_GetInformationLabel(INFOTYPE_TODAYSSET)=="Todays set");
    }
    BOOST_AUTO_TEST_CASE(Value_16)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_TOMORROWSRISE)==
          "Tomorrows rise");
    }
    BOOST_AUTO_TEST_CASE(Value_17)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_TOMORROWSSET)==
          "Tomorrows set");
    }
    BOOST_AUTO_TEST_CASE(Value_18)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_VISIBLE)=="Visible");
    }
    BOOST_AUTO_TEST_CASE(Value_19)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_YESTERDAYSRISE)==
          "Yesterdays rise");
    }
    BOOST_AUTO_TEST_CASE(Value_20)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_YESTERDAYSSET)==
          "Yesterdays set");
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      BOOST_TEST(Information_GetInformationLabel(INFOTYPE_YESTERDAYSSET+1)=="");
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetInformationLabels)
    BOOST_AUTO_TEST_CASE(List)
    {
      std::vector<std::string> List=Information_GetInformationLabels();
      BOOST_TEST(List.size()==21);
      BOOST_TEST(List[ 0]=="Age");
      BOOST_TEST(List[ 1]=="Altitude");
      BOOST_TEST(List[ 2]=="Azimuth");
      BOOST_TEST(List[ 3]=="Date/time");
      BOOST_TEST(List[ 4]=="Declination");
      BOOST_TEST(List[ 5]=="Distance from earth");
      BOOST_TEST(List[ 6]=="Illumination (%)");
      BOOST_TEST(List[ 7]=="Next new moon");
      BOOST_TEST(List[ 8]=="Next first quarter");
      BOOST_TEST(List[ 9]=="Next full moon");
      BOOST_TEST(List[10]=="Next last quarter");
      BOOST_TEST(List[11]=="Phase (%)");
      BOOST_TEST(List[12]=="Phase (text)");
      BOOST_TEST(List[13]=="Right ascention");
      BOOST_TEST(List[14]=="Todays rise");
      BOOST_TEST(List[15]=="Todays set");
      BOOST_TEST(List[16]=="Tomorrows rise");
      BOOST_TEST(List[17]=="Tomorrows set");
      BOOST_TEST(List[18]=="Visible");
      BOOST_TEST(List[19]=="Yesterdays rise");
      BOOST_TEST(List[20]=="Yesterdays set");
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetNeedsCoordinatesFlag)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(-1)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_00)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_AGE)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_01)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_ALTITUDE)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_02)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_AZIMUTH)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_03)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_DATETIME)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_04)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_DECLINATION)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_05)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_DISTANCEFROMEARTH)==
          false);
    }
    BOOST_AUTO_TEST_CASE(Value_06)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_ILLUMINATION)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_07)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_NEXTNEWMOON)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_08)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_NEXTFIRSTQUARTER)==
          false);
    }
    BOOST_AUTO_TEST_CASE(Value_09)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_NEXTFULLMOON)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_10)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_NEXTLASTQUARTER)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_11)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_PHASEPERCENT)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_12)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_PHASETEXT)==false);
    }
    BOOST_AUTO_TEST_CASE(Value_13)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_RIGHTASCENTION)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_14)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_TODAYSRISE)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_15)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_TODAYSSET)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_16)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_TOMORROWSRISE)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_17)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_TOMORROWSSET)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_18)
    {
      BOOST_TEST(Information_GetNeedsCoordinatesFlag(INFOTYPE_VISIBLE)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_19)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_YESTERDAYSRISE)==true);
    }
    BOOST_AUTO_TEST_CASE(Value_20)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_YESTERDAYSSET)==true);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      BOOST_TEST(
          Information_GetNeedsCoordinatesFlag(INFOTYPE_YESTERDAYSSET+1)==false);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetUnitFormatDescriptionList)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(-1);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_00)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_AGE);
      BOOST_TEST(List.size()==1);
      BOOST_TEST(List[0]=="days (Days)");
    }
    BOOST_AUTO_TEST_CASE(List_01)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_ALTITUDE);
      BOOST_TEST(List.size()==2);
      BOOST_TEST(List[0]=="° (Degrees)");
      BOOST_TEST(List[1]=="㎭ (Radians)");
    }
    BOOST_AUTO_TEST_CASE(List_02)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_AZIMUTH);
      BOOST_TEST(List.size()==2);
      BOOST_TEST(List[0]=="° (Degrees)");
      BOOST_TEST(List[1]=="㎭ (Radians)");
    }
    BOOST_AUTO_TEST_CASE(List_03)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_DATETIME);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_04)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_DECLINATION);
      BOOST_TEST(List.size()==1);
      BOOST_TEST(List[0]=="hms (Hours, minutes, seconds)");
    }
    BOOST_AUTO_TEST_CASE(List_05)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_DISTANCEFROMEARTH);
      BOOST_TEST(List.size()==5);
      BOOST_TEST(List[0]=="ft (Feet)");
      BOOST_TEST(List[1]=="m (Meters)");
      BOOST_TEST(List[2]=="km (Kilometers)");
      BOOST_TEST(List[3]=="mi (Miles)");
      BOOST_TEST(List[4]=="au (Astronomical units)");
    }
    BOOST_AUTO_TEST_CASE(List_06)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_ILLUMINATION);
      BOOST_TEST(List.size()==1);
      BOOST_TEST(List[0]=="% (Percent)");
    }
    BOOST_AUTO_TEST_CASE(List_07)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_NEXTNEWMOON);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_08)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_NEXTFIRSTQUARTER);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_09)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_NEXTFULLMOON);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_10)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_NEXTLASTQUARTER);
      BOOST_TEST(List.size()==3);
      BOOST_TEST(List[0]=="M. d, y");
      BOOST_TEST(List[1]=="m/d/y");
      BOOST_TEST(List[2]=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(List_11)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_PHASEPERCENT);
      BOOST_TEST(List.size()==1);
      BOOST_TEST(List[0]=="% (Percent)");
    }
    BOOST_AUTO_TEST_CASE(List_12)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_PHASETEXT);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_13)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_RIGHTASCENTION);
      BOOST_TEST(List.size()==1);
      BOOST_TEST(List[0]=="hms (Hours, minutes, seconds)");
    }
    BOOST_AUTO_TEST_CASE(List_14)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_TODAYSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_15)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_TODAYSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_16)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_TOMORROWSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_17)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_TOMORROWSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_18)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_VISIBLE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_19)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_YESTERDAYSRISE);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(List_20)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_YESTERDAYSSET);
      BOOST_TEST(List.size()==0);
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      std::vector<std::string> List=
          Information_GetUnitFormatDescriptionList(INFOTYPE_YESTERDAYSSET+1);
      BOOST_TEST(List.size()==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetUnitFormatIndex)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(-1,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0000)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_AGE,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0001)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_AGE,"Days")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0100)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_ALTITUDE,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0101)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_ALTITUDE,"Degrees")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0102)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_ALTITUDE,"Radians")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0200)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_AZIMUTH,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0201)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_AZIMUTH,"Degrees")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0202)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_AZIMUTH,"Radians")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0300)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_DATETIME,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0301)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_DATETIME,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0302)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_DATETIME,"m/d/y")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0303)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_DATETIME,"d/m/y")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_0400)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_DECLINATION,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0401)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_DECLINATION,"Hours, minutes, seconds")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0500)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_DISTANCEFROMEARTH,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0501)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_DISTANCEFROMEARTH,"Feet")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0502)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,"Meters")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0503)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,"Kilometers")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_0504)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,"Miles")==3);
    }
    BOOST_AUTO_TEST_CASE(Test_0505)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_DISTANCEFROMEARTH,"Astronomical units")==4);
    }
    BOOST_AUTO_TEST_CASE(Test_0600)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_ILLUMINATION,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0601)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_ILLUMINATION,"Percent")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0700)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFIRSTQUARTER,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0701)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_NEXTFIRSTQUARTER,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0702)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFIRSTQUARTER,"m/d/y")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0703)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFIRSTQUARTER,"d/m/y")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_0800)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFULLMOON,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0801)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFULLMOON,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0802)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFULLMOON,"m/d/y")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0803)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTFULLMOON,"d/m/y")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_0900)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTLASTQUARTER,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_0901)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_NEXTLASTQUARTER,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_0902)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTLASTQUARTER,"m/d/y")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_0903)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTLASTQUARTER,"d/m/y")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_1000)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_NEXTNEWMOON,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_1001)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTNEWMOON,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1002)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTNEWMOON,"m/d/y")==1);
    }
    BOOST_AUTO_TEST_CASE(Test_1003)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_NEXTNEWMOON,"d/m/y")==2);
    }
    BOOST_AUTO_TEST_CASE(Test_1100)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_PHASEPERCENT,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_1101)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_PHASEPERCENT,"Percent")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1200)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_PHASETEXT,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1300)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_RIGHTASCENTION,"")==-1);
    }
    BOOST_AUTO_TEST_CASE(Test_1301)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(
          INFOTYPE_RIGHTASCENTION,"Hours, minutes, seconds")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1400)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_TODAYSRISE,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1401)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TODAYSRISE,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1500)
    {
      BOOST_TEST(Information_GetUnitFormatIndex(INFOTYPE_TODAYSSET,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1501)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TODAYSSET,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1600)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TOMORROWSRISE,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1601)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TOMORROWSRISE,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1700)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TOMORROWSSET,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1701)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_TOMORROWSSET,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1800)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_VISIBLE,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1900)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_YESTERDAYSRISE,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_1901)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_YESTERDAYSRISE,"M. d, y")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_2000)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_YESTERDAYSSET,"")==0);
    }
    BOOST_AUTO_TEST_CASE(Test_2001)
    {
      BOOST_TEST(
          Information_GetUnitFormatIndex(INFOTYPE_YESTERDAYSSET,"M. d, y")==0);
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Function__Information_GetUnitFormatLabel)
    BOOST_AUTO_TEST_CASE(Test_0000)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(-1,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0001)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(0,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0002)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(-1,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0100)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_AGE,0)=="Days");
    }
    BOOST_AUTO_TEST_CASE(Test_0200)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_ALTITUDE,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0201)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_ALTITUDE,0)=="Degrees");
    }
    BOOST_AUTO_TEST_CASE(Test_0202)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_ALTITUDE,1)=="Radians");
    }
    BOOST_AUTO_TEST_CASE(Test_0300)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_AZIMUTH,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0301)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_AZIMUTH,0)=="Degrees");
    }
    BOOST_AUTO_TEST_CASE(Test_0302)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_AZIMUTH,1)=="Radians");
    }
    BOOST_AUTO_TEST_CASE(Test_0400)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_DATETIME,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0401)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_DATETIME,0)=="M. d, y");
    }
    BOOST_AUTO_TEST_CASE(Test_0402)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_DATETIME,1)=="m/d/y");
    }
    BOOST_AUTO_TEST_CASE(Test_0403)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_DATETIME,2)=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(Test_0500)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_DECLINATION,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0501)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_DECLINATION,0)=="Hours, minutes, seconds");
    }
    BOOST_AUTO_TEST_CASE(Test_0600)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_DISTANCEFROMEARTH,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0601)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_DISTANCEFROMEARTH,0)=="Feet");
    }
    BOOST_AUTO_TEST_CASE(Test_0602)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_DISTANCEFROMEARTH,1)=="Meters");
    }
    BOOST_AUTO_TEST_CASE(Test_0603)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_DISTANCEFROMEARTH,2)=="Kilometers");
    }
    BOOST_AUTO_TEST_CASE(Test_0604)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_DISTANCEFROMEARTH,3)=="Miles");
    }
    BOOST_AUTO_TEST_CASE(Test_0605)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_DISTANCEFROMEARTH,4)=="Astronomical units");
    }
    BOOST_AUTO_TEST_CASE(Test_0700)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_ILLUMINATION,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0701)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_ILLUMINATION,0)=="Percent");
    }
    BOOST_AUTO_TEST_CASE(Test_0800)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFIRSTQUARTER,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0801)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_NEXTFIRSTQUARTER,0)=="M. d, y");
    }
    BOOST_AUTO_TEST_CASE(Test_0802)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFIRSTQUARTER,1)=="m/d/y");
    }
    BOOST_AUTO_TEST_CASE(Test_0803)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFIRSTQUARTER,2)=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(Test_0900)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFULLMOON,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_0901)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_NEXTFIRSTQUARTER,0)=="M. d, y");
    }
    BOOST_AUTO_TEST_CASE(Test_0902)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFIRSTQUARTER,1)=="m/d/y");
    }
    BOOST_AUTO_TEST_CASE(Test_0903)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTFIRSTQUARTER,2)=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(Test_1000)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTLASTQUARTER,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1001)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_NEXTLASTQUARTER,0)=="M. d, y");
    }
    BOOST_AUTO_TEST_CASE(Test_1002)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTLASTQUARTER,1)=="m/d/y");
    }
    BOOST_AUTO_TEST_CASE(Test_1003)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTLASTQUARTER,2)=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(Test_1100)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_NEXTNEWMOON,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1101)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTNEWMOON,0)=="M. d, y");
    }
    BOOST_AUTO_TEST_CASE(Test_1102)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTNEWMOON,1)=="m/d/y");
    }
    BOOST_AUTO_TEST_CASE(Test_1103)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_NEXTNEWMOON,2)=="d/m/y");
    }
    BOOST_AUTO_TEST_CASE(Test_1200)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_PHASEPERCENT,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1201)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_PHASEPERCENT,0)=="Percent");
    }
    BOOST_AUTO_TEST_CASE(Test_1300)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_PHASETEXT,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1301)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_PHASETEXT,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1400)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_RIGHTASCENTION,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1401)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(
          INFOTYPE_RIGHTASCENTION,0)=="Hours, minutes, seconds");
    }
    BOOST_AUTO_TEST_CASE(Test_1500)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TODAYSRISE,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1501)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TODAYSRISE,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1600)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TODAYSSET,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1601)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TODAYSSET,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1700)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TOMORROWSRISE,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1701)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TOMORROWSRISE,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1800)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TOMORROWSSET,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1801)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_TOMORROWSSET,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1900)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_VISIBLE,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_1901)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_VISIBLE,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_2000)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_YESTERDAYSRISE,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_2001)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_YESTERDAYSRISE,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_2100)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_YESTERDAYSSET,-1)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_2101)
    {
      BOOST_TEST(Information_GetUnitFormatLabel(INFOTYPE_YESTERDAYSSET,0)=="");
    }
    BOOST_AUTO_TEST_CASE(Test_2200)
    {
      BOOST_TEST(
          Information_GetUnitFormatLabel(INFOTYPE_YESTERDAYSSET+1,-1)=="");
    }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_FIXTURE_TEST_SUITE(Function__Information_Print,TZSETTER_C)
    BOOST_AUTO_TEST_CASE(BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(Information_Print(MoonData,-1,0,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_00BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_AGE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0000)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_AGE,0,Options)=="24.1 days");
    }
    BOOST_AUTO_TEST_CASE(Value_00AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_AGE,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_01BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_ALTITUDE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0100)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.h_moon=33.333333;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ALTITUDE,0,Options)=="33.33°");
    }
    BOOST_AUTO_TEST_CASE(Value_0101)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.h_moon=33.333333;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ALTITUDE,1,Options)=="0.58178㎭");
    }
    BOOST_AUTO_TEST_CASE(Value_01AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.h_moon=33.333333;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ALTITUDE,2,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_02BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.A_moon=33.3333333333;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_AZIMUTH,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0200)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.A_moon=33.3333333333;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_AZIMUTH,0,Options)=="33.33°");
    }
    BOOST_AUTO_TEST_CASE(Value_0201)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.A_moon=33.3333333333;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_AZIMUTH,1,Options)=="0.58178㎭");
    }
    BOOST_AUTO_TEST_CASE(Value_02AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.h_moon=33.333333;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_AZIMUTH,2,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_03BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0300)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;
      char pBuffer[1024];
      time_t Time;


      memset(&MoonData,0,sizeof(MoonData));
      Time=time(nullptr);
      strftime(pBuffer,1024,"%b. %d, %y %I:%M %p",localtime(&Time));
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,0,Options)==
          std::string(pBuffer));
    }
    BOOST_AUTO_TEST_CASE(Value_0301)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;
      char pBuffer[1024];
      time_t Time;


      memset(&MoonData,0,sizeof(MoonData));
      Time=time(nullptr);
      strftime(pBuffer,1024,"%m/%d/%y %I:%M %p",localtime(&Time));
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,1,Options)==
          std::string(pBuffer));
    }
    BOOST_AUTO_TEST_CASE(Value_0302)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;
      char pBuffer[1024];
      time_t Time;


      memset(&MoonData,0,sizeof(MoonData));
      Time=time(nullptr);
      strftime(pBuffer,1024,"%d/%m/%y %I:%M %p",localtime(&Time));
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,2,Options)==
          std::string(pBuffer));
    }
    BOOST_AUTO_TEST_CASE(Value_0303)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;
      char pBuffer[1024];
      time_t Time;


      memset(&MoonData,0,sizeof(MoonData));
      Options.SetUseGMTFlag(true);
      Time=time(nullptr);
      strftime(pBuffer,1024,"%d/%m/%y %I:%M %p",gmtime(&Time));
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,2,Options)==
          std::string(pBuffer));
    }
    BOOST_AUTO_TEST_CASE(Value_03AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonAge=24.1357;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DATETIME,3,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_04BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.DEC_moon=66.66666666;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DECLINATION,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0400)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.DEC_moon=66.66666666;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_DECLINATION,0,Options)==
          "4h 26m 40.00s");
    }
    BOOST_AUTO_TEST_CASE(Value_04AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.DEC_moon=66.66666666;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DECLINATION,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_05BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance*EARTH_RADIUS;
      BOOST_TEST(Information_Print(MoonData,4,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0500)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,0,Options)==
          "20903520 ft");
    }
    BOOST_AUTO_TEST_CASE(Value_0501)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,1,Options)==
          "6371393 m");
    }
    BOOST_AUTO_TEST_CASE(Value_0502)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,2,Options)==
          "6371 km");
    }
    BOOST_AUTO_TEST_CASE(Value_0503)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,3,Options)==
          "3959 mi");
    }
    BOOST_AUTO_TEST_CASE(Value_0504)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1000;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,4,Options)==
          "0.04259 au");
    }
    BOOST_AUTO_TEST_CASE(Value_05AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.EarthMoonDistance=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_DISTANCEFROMEARTH,5,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_06BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.5*2*M_PI;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0600)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)=="0.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_0602)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.25;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)=="50.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_0603)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.5;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)==
          "100.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_0604)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.75;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)=="50.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_0605)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=1;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)=="0.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_0606)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=2;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,0,Options)=="0.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_06AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.5*2*M_PI;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_ILLUMINATION,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_07BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTNEWMOON,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0700)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextNewMoonGMt=1577923200;   /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTNEWMOON,0,Options)==
          "Jan. 02, 20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0701)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextNewMoonGMt=1578009600;   /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTNEWMOON,1,Options)==
          "01/03/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0702)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextNewMoonGMt=1578096000;   /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTNEWMOON,2,Options)==
          "04/01/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_07AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTNEWMOON,3,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_08BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFIRSTQUARTER,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0800)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFirstQuarterMoonGMt=1580515200;  /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFIRSTQUARTER,0,Options)==
          "Feb. 01, 20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0801)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFirstQuarterMoonGMt=1580688000;  /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFIRSTQUARTER,1,Options)==
          "02/03/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0802)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFirstQuarterMoonGMt=1580774400;  /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFIRSTQUARTER,2,Options)==
          "04/02/20 12:00 AM");
    }

    BOOST_AUTO_TEST_CASE(Value_08AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFIRSTQUARTER,3,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_09BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFULLMOON,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_0900)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFullMoonGMt=1583020800;  /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTFULLMOON,0,Options)==
          "Mar. 01, 20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0901)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFullMoonGMt=1583107200;  /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTFULLMOON,1,Options)==
          "03/02/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_0902)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextFullMoonGMt=1583280000;  /* Fake phase. */
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_NEXTFULLMOON,2,Options)==
          "04/03/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_09AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTFULLMOON,3,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_10BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTLASTQUARTER,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1000)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextLastQuarterMoonGMt=1585699200; /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTLASTQUARTER,0,Options)==
          "Apr. 01, 20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_1001)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextLastQuarterMoonGMt=1585785600; /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTLASTQUARTER,1,Options)==
          "04/02/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_1002)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      MoonData.NextLastQuarterMoonGMt=1585872000; /* Fake phase. */
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTLASTQUARTER,2,Options)==
          "03/04/20 12:00 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_10AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C
          Options(DATETIMEOPTIONS_C::DATETIMEOPTION_CLEARALL,true);


      memset(&MoonData,0,sizeof(MoonData));
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_NEXTLASTQUARTER,3,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_11BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1100)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,0,Options)=="0.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_1101)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.25;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,0,Options)=="25.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_1102)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.5;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,0,Options)=="50.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_1103)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.75;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,0,Options)=="75.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_1104)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=1;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASEPERCENT,0,Options)==
          "100.0%");
    }
    BOOST_AUTO_TEST_CASE(Value_11AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.5*2*M_PI;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASEPERCENT,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_12BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1200)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=0;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)=="New moon");
    }
    BOOST_AUTO_TEST_CASE(Value_1201)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.01;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)=="New moon");
    }
    BOOST_AUTO_TEST_CASE(Value_1202)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.02;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waxing crescent");
    }
    BOOST_AUTO_TEST_CASE(Value_1203)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.23;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waxing crescent");
    }
    BOOST_AUTO_TEST_CASE(Value_1204)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.24;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "First quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1205)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.25;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "First quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1206)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.26;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "First quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1207)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.27;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waxing gibbous");
    }
    BOOST_AUTO_TEST_CASE(Value_1208)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.48;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waxing gibbous");
    }
    BOOST_AUTO_TEST_CASE(Value_1209)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.49;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Full moon");
    }
    BOOST_AUTO_TEST_CASE(Value_1210)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.50;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Full moon");
    }
    BOOST_AUTO_TEST_CASE(Value_1211)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.51;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Full moon");
    }
    BOOST_AUTO_TEST_CASE(Value_1212)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.52;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waning gibbous");
    }
    BOOST_AUTO_TEST_CASE(Value_1213)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.73;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waning gibbous");
    }
    BOOST_AUTO_TEST_CASE(Value_1214)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.74;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Last quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1215)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.75;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Last quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1216)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.76;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Last quarter");
    }
    BOOST_AUTO_TEST_CASE(Value_1217)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.77;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waning crescent");
    }
    BOOST_AUTO_TEST_CASE(Value_1218)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.98;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "Waning crescent");
    }
    BOOST_AUTO_TEST_CASE(Value_1219)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=.99;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,0,Options)==
          "New moon");
    }
    BOOST_AUTO_TEST_CASE(Value_12AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.MoonPhase=100;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_PHASETEXT,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_13BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.RA_moon=66.66666666;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_RIGHTASCENTION,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1300)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.RA_moon=66.66666666;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_RIGHTASCENTION,0,Options)==
          "4h 26m 40.00s");
    }
    BOOST_AUTO_TEST_CASE(Value_13AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.RA_moon=66.66666666;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_RIGHTASCENTION,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_14BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TODAYSRISE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1400)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TODAYSRISE,0,Options)==
          "07:34 PM");
    }
    BOOST_AUTO_TEST_CASE(Value_1401)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysRiseLT=-999;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TODAYSRISE,0,Options)=="None");
    }
    BOOST_AUTO_TEST_CASE(Value_14AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TODAYSRISE,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_15BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TODAYSSET,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1500)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TODAYSSET,0,Options)==
          "04:43 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_1501)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysSetLT=-999;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TODAYSSET,0,Options)=="None");
    }
    BOOST_AUTO_TEST_CASE(Value_15AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TodaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TODAYSSET,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_16BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSRISE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1600)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TOMORROWSRISE,0,Options)==
          "07:34 PM");
    }
    BOOST_AUTO_TEST_CASE(Value_1601)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsRiseLT=-999;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSRISE,0,Options)=="None");
    }
    BOOST_AUTO_TEST_CASE(Value_16AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSRISE,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_17BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSSET,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1700)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_TOMORROWSSET,0,Options)==
          "04:43 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_1701)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsSetLT=-999;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSSET,0,Options)=="None");
    }
    BOOST_AUTO_TEST_CASE(Value_17AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.TomorrowsSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_TOMORROWSSET,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_18BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.Visible=0;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_VISIBLE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1800)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.Visible=0;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_VISIBLE,0,Options)=="No");
    }
    BOOST_AUTO_TEST_CASE(Value_1801)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.Visible=!0;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_VISIBLE,0,Options)=="Yes");
    }
    BOOST_AUTO_TEST_CASE(Value_18AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.CTransData.Visible=!0;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_VISIBLE,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_19BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSRISE,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_1900)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_YESTERDAYSRISE,0,Options)==
          "07:34 PM");
    }
    BOOST_AUTO_TEST_CASE(Value_1901)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysRiseLT=-999;
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_YESTERDAYSRISE,0,Options)==
          "None");
    }
    BOOST_AUTO_TEST_CASE(Value_19AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysRiseLT=12.566666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSRISE,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_20BelowLowerBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSSET,-1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(Value_2000)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(Information_Print(MoonData,INFOTYPE_YESTERDAYSSET,0,Options)==
          "04:43 AM");
    }
    BOOST_AUTO_TEST_CASE(Value_2001)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysSetLT=-999;
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSSET,0,Options)=="None");
    }
    BOOST_AUTO_TEST_CASE(Value_20AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSSET,1,Options)=="");
    }
    BOOST_AUTO_TEST_CASE(AboveUpperBound)
    {
      MOONDATA_T MoonData;
      DATETIMEOPTIONS_C Options;


      memset(&MoonData,0,sizeof(MoonData));
      MoonData.YesterdaysSetLT=21.716666667;
      Options.SetUseGMTFlag(true);
      BOOST_TEST(
          Information_Print(MoonData,INFOTYPE_YESTERDAYSSET+1,0,Options)=="");
    }
  BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()


/**
*** test_module_information.cpp
**/

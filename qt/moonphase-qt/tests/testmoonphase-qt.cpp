/**
*** \file testmoonphaseqt.cpp
*** \brief testmoonphaseqt.cpp implementation.
*** \details Implementation file for testmoonphaseqt.cpp.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief testmoonphaseqt.cpp identifier.
*** \details Identifier for testmoonphaseqt.cpp.
*** \internal
**/
#define   TESTMOONPHASEQT_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "config.h"

/**
*** \brief Master test suite name.
*** \details The name of the master test suite.
**/
#define   BOOST_TEST_MODULE   MOONPHASEQT_EXECUTABLENAME
#include  <boost/test/included/unit_test.hpp>


/****
*****
***** DEFINES
*****
****/



/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/


#undef    TESTMOONPHASEQT_CPP


/**
*** testmoonphaseqt.cpp
**/

#!/bin/bash

# Parameters:
#   $1 - Architecture (optional, defaults to "x86_64").
Arg1=$1

# Parameter checking.
if [ ! -z "$2" ]; then
  echo "Error: extra parameters."
else
  if [ -z "$1" ]; then
    Arg1=x86_64
  fi
  if [ "$Arg1" == "x86_64" ]; then
    QtArch=x86_64
  fi
  if [ -z "$QtArch" ]; then
    echo "Error: unsupported architecture ($1)."
  fi
fi
if [ -z "$QtArch" ]; then
  echo ""
  echo "Usage: $0 [x86_64]"
  exit 1
fi

# Check environment variables.
if [ -z "$HOMEBREW_ROOT_DIR" ]; then
  echo "Error: HOMEBREW_ROOT_DIR is not set."
  exit 1
fi

# Set up the environment.
export CMAKE_PREFIX_PATH=$HOMEBREW_ROOT_DIR/qt5:$HOMEBREW_ROOT_DIR/curl

# Build.
rm -rf .build >/dev/null
mkdir .build
cd .build &&
cmake -DCMAKE_BUILD_TYPE=Release -DMOONPHASE_OPTION_STATICLINK=true .. &&
make &&
cd ..

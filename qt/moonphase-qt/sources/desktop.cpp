/**
*** \file desktop.cpp
*** \brief desktop.h implementation.
*** \details Implementation file for desktop.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief desktop.cpp identifier.
*** \details Identifier for desktop.cpp.
*** \endinternal
**/
#define   DESKTOP_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "desktop.h"
#if       defined(DEBUG_DESKTOP_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_DESKTOP_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "conversions.h"

#include  <QApplication>
#include  <QPalette>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

COLOR_C GetDesktopWindowBackgroundColor(void)
{
  DEBUGLOG_Printf0("GetDesktopWindowBackgroundColor()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
#if     !(defined(APPLICATION_TEST) && APPLICATION_TEST)
  return(ToCOLOR_C(qApp->palette().color(QPalette::Background)));
#else   /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
  return(
      COLOR_C(0x10/(float)255,0x20/(float)255,0x40/(float)255,0x80/(float)255));
#endif  /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
}

FONT_C GetDesktopWindowFont(void)
{
  DEBUGLOG_Printf0("GetDesktopWindowFont()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
#if     !(defined(APPLICATION_TEST) && APPLICATION_TEST)
  return(ToFONT_C(qApp->font()));
#else   /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
  return(FONT_C("TestFont",10,100));
#endif  /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
}

COLOR_C GetDesktopWindowTextColor(void)
{
  DEBUGLOG_Printf0("GetDesktopWindowTextColor()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
#if     !(defined(APPLICATION_TEST) && APPLICATION_TEST)
  return(ToCOLOR_C(qApp->palette().color(QPalette::Text)));
#else   /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
  return(
      COLOR_C(0x08/(float)255,0x04/(float)255,0x02/(float)255,0x01/(float)255));
#endif  /* !(defined(APPLICATION_TEST) && APPLICATION_TEST) */
}


#undef    DESKTOP_CPP


/**
*** desktop.cpp
**/

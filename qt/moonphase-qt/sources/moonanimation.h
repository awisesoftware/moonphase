/**
*** \file moonanimation.h
*** \brief Reads an image and separates it into frames.
*** \details Reads an image which contains a series of frames, and separates
***   the each frame into an individual image.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MOONANIMATION_H)
/**
*** \brief moonanimation.h identifier.
*** \details Identifier for moonanimation.h.
*** \internal
**/
#define   MOONANIMATION_H


/****
*****
***** INCLUDES
*****
****/

#include  "structure.h"

#include  <QList>
#include  <QPixmap>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Moon animation structure.
*** \details Structure for moon animation.
**/
typedef struct structMOONANIMATION
{
  /**
  *** \brief Image frames.
  *** \details List of frames in the image.
  **/
  QList<QPixmap> *pImages;
} MOONANIMATION_T;


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

STRUCTURE_PROTOTYPE_INITIALIZE(MoonAnimation,MOONANIMATION_T);
STRUCTURE_PROTOTYPE_INITIALIZEMEMBERS(MoonAnimation,MOONANIMATION_T);
STRUCTURE_PROTOTYPE_UNINITIALIZE(MoonAnimation,MOONANIMATION_T);
STRUCTURE_PROTOTYPE_UNINITIALIZEMEMBERS(MoonAnimation,MOONANIMATION_T);
/**
*** \brief Returns the image pathname.
*** \details Returns the pathname of the image.
*** \param pMoonAnimation Pointer to moon animation structure.
*** \param pPathname Pointer to storage for the pathname.
*** \retval >0 Success.
*** \retval <0 Failure.
**/
ERRORCODE_T MoonAnimation_GetPathname(
    MOONANIMATION_T const *pMoonAnimation,QString *pPathname);
/**
*** \brief Returns a frame.
*** \details Returns a frame from the image.
*** \param pMoonAnimation Pointer to moon animation structure.
*** \param Index Frame index.
*** \param ppFrame Storage for frame data pointer.
*** \retval >0 Success.
*** \retval <0 Failure.
**/
ERRORCODE_T MoonAnimation_GetFrame(MOONANIMATION_T const *pMoonAnimation,
    unsigned Index,QPixmap const **ppFrame);
/**
*** \brief Returns the frame count.
*** \details Returns the number of frames in the image.
*** \param pMoonAnimation Pointer to moon animation structure.
*** \retval >0 Success.
*** \retval <0 Failure.
**/
ERRORCODE_T MoonAnimation_GetFrameCount(MOONANIMATION_T const *pMoonAnimation);
/**
*** \brief Reads image and separates it into frames.
*** \details Reads an image which contains a series of frames, and separates
***   the each frame into an individual image.
*** \param pMoonAnimation Pointer to moon animation structure.
*** \param pPathname Animation path name.
*** \retval >0 Success.
*** \retval =0 Unable to read file.
*** \retval <0 Failure.
**/
ERRORCODE_T MoonAnimation_ReadFile(
    MOONANIMATION_T *pMoonAnimation,char const *pPathname);

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(MOONANIMATION_H) */


/**
*** moonanimation.h
**/

/**
*** \file applicationpreferenceswidget.cpp
*** \brief applicationpreferenceswidget.h implementation.
*** \details Implementation file for applicationpreferenceswidget.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief preferenceswidget.cpp identifier.
*** \details Identifier for preferenceswidget.cpp.
*** \internal
**/
#define   APPLICATIONPREFERENCESWIDGET_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "applicationpreferenceswidget.h"
#include  "ui_applicationpreferenceswidget.h"
#if       defined(DEBUG_APPLICATIONPREFERENCESWIDGET_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_APPLICATIONPREFERENCESWIDGET_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "conversions.h"
#include  "drawframe.h"

#include  <QShowEvent>
#include  <QTimer>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Application preferences widget implementation class.
*** \details The implementation class for the application preferences widget.
**/
class APPLICATIONPREFERENCESWIDGET_C::APPLICATIONPREFERENCESWIDGETPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pPublic Pointer to the interface class
    ***   (APPLICATIONPREFERENCESWIDGET_C).
    **/
    APPLICATIONPREFERENCESWIDGETPRIVATE_C(
        APPLICATIONPREFERENCESWIDGET_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    APPLICATIONPREFERENCESWIDGETPRIVATE_C(
        APPLICATIONPREFERENCESWIDGETPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~APPLICATIONPREFERENCESWIDGETPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    APPLICATIONPREFERENCESWIDGETPRIVATE_C
      operator=(APPLICATIONPREFERENCESWIDGETPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Get preferences.
    *** \details Gets the preferences of the preferences widget.
    *** \returns Preferences.
    **/
    PREFERENCES_C::APPLICATION_C GetPreferences(void) const;

    /**
    *** \brief Animation timer.
    *** \details Timer used to update the preview widget.
    **/
    QTimer m_AnimationTimer;

    /**
    *** \brief Saved preferences.
    *** \details Preferences in configuration file.
    **/
    PREFERENCES_C::APPLICATION_C m_LastSavedPreferences;

    /**
    *** \brief Preview icon.
    *** \details The images displayed in the preview widget.
    **/
    MOONANIMATION_T m_MoonPreviewImages;

    /**
    *** \brief Preview percent counter.
    *** \details Counter used to animate the preview widget.
    **/
    unsigned int m_PreviewPercentCounter;

    /**
    *** \brief Application preferences pointer.
    *** \details Pointer to the application preferences.
    **/
    PREFERENCES_C::APPLICATION_C *m_pPreferences;

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    APPLICATIONPREFERENCESWIDGET_C *m_pPublic;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_APPLICATIONPREFERENCESWIDGETUI_C> m_pUI;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

/**
*** \brief Animation update rate.
*** \details Time between animation updates.
**/
int const fc_AnimationTimerRate=200;  /* in milliseconds */


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

APPLICATIONPREFERENCESWIDGET_C::APPLICATIONPREFERENCESWIDGETPRIVATE_C::
    APPLICATIONPREFERENCESWIDGETPRIVATE_C(
    APPLICATIONPREFERENCESWIDGET_C *pPublic)
    : m_pUI(new Ui_APPLICATIONPREFERENCESWIDGETUI_C)
{
  DEBUGLOG_Printf1("APPLICATIONPREFERENCESWIDGET_C::"
      "APPLICATIONPREFERENCESWIDGETPRIVATE_C::"
      "APPLICATIONPREFERENCESWIDGETPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_pPreferences=nullptr;
  m_pPublic=pPublic;

  if(MoonAnimation_Initialize(&m_MoonPreviewImages)<0)
  {
    MESSAGELOG_Error("MoonAnimation_Initialize()<0");
    throw(std::runtime_error("MoonAnimation_Initialize()<0."));
  }
  m_PreviewPercentCounter=0;

  /* Set up the user interface. */
  m_pUI->setupUi(pPublic);

  DEBUGLOG_LogOut();
  return;
}

APPLICATIONPREFERENCESWIDGET_C::APPLICATIONPREFERENCESWIDGETPRIVATE_C::
    ~APPLICATIONPREFERENCESWIDGETPRIVATE_C(void)
{
  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::"
      "APPLICATIONPREFERENCESWIDGETPRIVATE_C::"
      "~APPLICATIONPREFERENCESWIDGETPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C
    APPLICATIONPREFERENCESWIDGET_C::APPLICATIONPREFERENCESWIDGETPRIVATE_C::
    GetPreferences(void) const
{
  PREFERENCES_C::APPLICATION_C Preferences;


  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::GetPreferences()");
  DEBUGLOG_LogIn();

  /* Get the widget data. */
  Preferences.SetAnimationPathname(
      m_pUI->m_pAnimationFilenameChooser->text().toStdString());
  Preferences.SetBackgroundColor(
      ToCOLOR_C(m_pUI->m_pBackgroundColorButton->currentColor()));
  Preferences.SetConfirmDiscardFlag(
      m_pUI->m_pConfirmDiscardCheckBox->isChecked());
  Preferences.SetConfirmQuitFlag(
      m_pUI->m_pConfirmQuitCheckBox->isChecked());
  Preferences.SetRemindOncePerSessionFlag(
      m_pUI->m_pRemindOncePerSessionCheckBox->isChecked());
  Preferences.SetStillRunningReminderFlag(
      m_pUI->m_pStillRunningReminderCheckBox->isChecked());
  Preferences.SetUnsavedChangesReminderFlag(
      m_pUI->m_pUnsavedChangesReminderCheckBox->isChecked());
  Preferences.SetUseOpaqueBackgroundFlag(
      m_pUI->m_pUseOpaqueBackgroundGroupBox->isChecked());

  DEBUGLOG_LogOut();
  return(Preferences);
}

APPLICATIONPREFERENCESWIDGET_C::APPLICATIONPREFERENCESWIDGET_C(QWidget *pParent)
    : QWidget(pParent),
    m_pPrivate(new APPLICATIONPREFERENCESWIDGETPRIVATE_C(this))
{
  DEBUGLOG_Printf1("APPLICATIONPREFERENCESWIDGET_C::"
      "APPLICATIONPREFERENCESWIDGET_C(%p)",pParent);
  DEBUGLOG_LogIn();

  /* Create and set up the animation timer. */
  connect(&m_pPrivate->m_AnimationTimer,SIGNAL(timeout()),
      this,SLOT(AnimationTimerTriggeredSlot()));

  DEBUGLOG_LogOut();
  return;
}

APPLICATIONPREFERENCESWIDGET_C::~APPLICATIONPREFERENCESWIDGET_C(void)
{
  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::"
      "~APPLICATIONPREFERENCESWIDGET_C()");
  DEBUGLOG_LogIn();

  if (MoonAnimation_Uninitialize(&m_pPrivate->m_MoonPreviewImages)<0)
  {
    MESSAGELOG_Error("MoonAnimation_Uninitialize()<0");
  }

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::AnimationTimerTriggeredSlot(void)
{
  QString PercentString;
  QPixmap Pixmap;


  DEBUGLOG_Printf0(
      "APPLICATIONPREFERENCESWIDGET_C::AnimationTimerTriggeredSlot()");
  DEBUGLOG_LogIn();

  /* Update the percent label. */
  PercentString.sprintf("%d%%",m_pPrivate->m_PreviewPercentCounter);
  m_pPrivate->m_pUI->m_pPreviewPercentLabel->setText(PercentString);

  /* Update the preview image. */
  Pixmap=DrawFrame(&m_pPrivate->m_MoonPreviewImages,
      m_pPrivate->m_PreviewPercentCounter,
      m_pPrivate->m_pUI->m_pUseOpaqueBackgroundGroupBox->isChecked(),
      m_pPrivate->m_pUI->m_pBackgroundColorButton->currentColor());
  m_pPrivate->m_pUI->m_pPreviewViewLabel->setPixmap(
      Pixmap.scaled(
      m_pPrivate->m_pUI->m_pPreviewViewLabel->width(),
      m_pPrivate->m_pUI->m_pPreviewViewLabel->height(),
      Qt::KeepAspectRatio,Qt::SmoothTransformation));

  /* Next percent. */
  m_pPrivate->m_PreviewPercentCounter=
      (m_pPrivate->m_PreviewPercentCounter+1)%100;

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::
    ButtonBoxButtonClickedSlot(QAbstractButton *pButton)
{
  DEBUGLOG_Printf1(
      "APPLICATIONPREFERENCESWIDGET_C::ButtonBoxButtonClickedSlot(%p)",pButton);
  DEBUGLOG_LogIn();

  switch(m_pPrivate->m_pUI->m_pButtonBox->standardButton(pButton))
  {
    case QDialogButtonBox::Save:
      m_pPrivate->m_LastSavedPreferences=*m_pPrivate->m_pPreferences;
      m_pPrivate->m_pPreferences->Save();
      break;

    case QDialogButtonBox::Reset:
      SetPreferences(m_pPrivate->m_LastSavedPreferences);
      break;

    default:
      MESSAGELOG_Error("Invalid button.");
      break;
  }

  PreferencesChangedSlot();

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::hideEvent(QHideEvent *pEvent)
{
  DEBUGLOG_Printf1("APPLICATIONPREFERENCESWIDGET_C::hideEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  SetVisible(false);
  pEvent->accept();

  DEBUGLOG_LogOut();
  return;
}

bool APPLICATIONPREFERENCESWIDGET_C::PreferencesChanged(void)
{
  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::PreferencesChanged()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pPrivate->m_pUI->m_pButtonBox->
      button(QDialogButtonBox::Save)->isEnabled());
}

void APPLICATIONPREFERENCESWIDGET_C::PreferencesChangedSlot(void)
{
  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::PreferencesChangedSlot()");
  DEBUGLOG_LogIn();

  /* Update a sub-option. */
  m_pPrivate->m_pUI->m_pRemindOncePerSessionCheckBox->setEnabled(
      m_pPrivate->m_pUI->m_pStillRunningReminderCheckBox->isChecked()!=false);

  /* Update the preferences variable. */
  *m_pPrivate->m_pPreferences=m_pPrivate->GetPreferences();

  /* Enable/disable the "Save" and "Reset" buttons. */
  m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Save)->setEnabled(
      m_pPrivate->m_LastSavedPreferences!=*m_pPrivate->m_pPreferences);
  m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Reset)->setEnabled(
      m_pPrivate->m_LastSavedPreferences!=*m_pPrivate->m_pPreferences);

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::SavePreferences(void)
{
  DEBUGLOG_Printf0("APPLICATIONPREFERENCESWIDGET_C::SavePreferences()");
  DEBUGLOG_LogIn();

  ButtonBoxButtonClickedSlot(m_pPrivate->m_pUI->m_pButtonBox->button(QDialogButtonBox::Save));

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::
    SetPreferences(PREFERENCES_C::APPLICATION_C Preferences)
{
  DEBUGLOG_Printf1(
      "APPLICATIONPREFERENCESWIDGET_C::SetPreferences(%p)",&Preferences);
  DEBUGLOG_LogIn();

  m_pPrivate->m_LastSavedPreferences=Preferences;

  /* Set the widget data. */
  m_pPrivate->m_pUI->m_pAnimationFilenameChooser->setText(
      QString::fromStdString(Preferences.GetAnimationPathname()));
  m_pPrivate->m_pUI->m_pBackgroundColorButton->setCurrentColor(
      ToQColor(Preferences.GetBackgroundColor()));
  m_pPrivate->m_pUI->m_pConfirmDiscardCheckBox->setChecked(
      Preferences.GetConfirmDiscardFlag());
  m_pPrivate->m_pUI->m_pConfirmQuitCheckBox->setChecked(
      Preferences.GetConfirmQuitFlag());
  m_pPrivate->m_pUI->m_pRemindOncePerSessionCheckBox->setChecked(
      Preferences.GetRemindOncePerSessionFlag());
  m_pPrivate->m_pUI->m_pStillRunningReminderCheckBox->setChecked(
      Preferences.GetStillRunningReminderFlag());
  m_pPrivate->m_pUI->m_pUnsavedChangesReminderCheckBox->setChecked(
      Preferences.GetUnsavedChangesReminderFlag());
  m_pPrivate->m_pUI->m_pUseOpaqueBackgroundGroupBox->setChecked(
      Preferences.GetUseOpaqueBackgroundFlag());
  PreferencesChangedSlot();

  TrayIconPreferencesChangedSlot();
  AnimationTimerTriggeredSlot();

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::SetPreferencesPointer(
    PREFERENCES_C::APPLICATION_C *pPreferences)
{
  PREFERENCES_C::APPLICATION_C Preferences;


  DEBUGLOG_Printf1(
      "APPLICATIONPREFERENCESWIDGET_C::SetPreferencesPointer(%p)",pPreferences);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pPreferences==nullptr)
    throw(std::invalid_argument("nullptr"));

  m_pPrivate->m_pPreferences=pPreferences;

  /* Set the widget data. */
  SetPreferences(*pPreferences);

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::SetVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1(
      "APPLICATIONPREFERENCESWIDGET_C::SetVisible(%u)",VisibleFlag);
  DEBUGLOG_LogIn();

  if (VisibleFlag==true)
    m_pPrivate->m_AnimationTimer.start(fc_AnimationTimerRate);
  else
    m_pPrivate->m_AnimationTimer.stop();

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::showEvent(QShowEvent *pEvent)
{
  DEBUGLOG_Printf1("APPLICATIONPREFERENCESWIDGET_C::showEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  SetVisible(true);
  pEvent->accept();

  DEBUGLOG_LogOut();
  return;
}

void APPLICATIONPREFERENCESWIDGET_C::TrayIconPreferencesChangedSlot(void)
{
  DEBUGLOG_Printf0(
      "APPLICATIONPREFERENCESWIDGET_C::TrayIconPreferencesChangedSlot()");
  DEBUGLOG_LogIn();

  /* Try to load the file. Don't care if it fails. */
  MoonAnimation_ReadFile(&m_pPrivate->m_MoonPreviewImages,
      qPrintable(m_pPrivate->m_pUI->m_pAnimationFilenameChooser->text()));
  PreferencesChangedSlot();
  emit TrayIconPreferencesChangedSignal();

  DEBUGLOG_LogOut();
  return;
}


#undef    APPLICATIONPREFERENCESWIDGET_CPP


/**
*** applicationpreferenceswidget.cpp
**/

/**
*** \file aboutwidget.cpp
*** \brief aboutwidget.h implementation file.
*** \details Implementation file for aboutwidget.h.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief aboutwidget.cpp identifier.
*** \details Identifier for aboutwidget.cpp.
*** \internal
**/
#define   ABOUTWIDGET_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "aboutwidget.h"
#if       defined(DEBUG_ABOUTWIDGET_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_ABOUTWIDGET_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "errorcode.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

ABOUTWIDGET_C::ABOUTWIDGET_C(QWidget *pParent) : QWidget(pParent)
{
  int Index;


  DEBUGLOG_Printf1("ABOUTWIDGET_C::ABOUTWIDGET_C(%p)",pParent);
  DEBUGLOG_LogIn();

  Q_INIT_RESOURCE(aboutwidget);

  /* Setup the user interface. */
  setupUi(this);

  /* Copy the widget data in the tab widget. */
  for(Index=ABOUTTAB_FIRST;Index<=ABOUTTAB_LAST;Index++)
  {
    m_pTabWidgets[Index].pWidget=m_pAboutTabWidget->widget(Index);
    m_pTabWidgets[Index].Icon=m_pAboutTabWidget->tabIcon(Index);
    m_pTabWidgets[Index].Label=m_pAboutTabWidget->tabText(Index);
    m_pTabWidgets[Index].Visible=true;
  }

  connect(m_pCheckButton,SIGNAL(clicked()),
      this,SIGNAL(CheckForUpdateButtonClickedSignal()));

  DEBUGLOG_LogOut();
  return;
}

ABOUTWIDGET_C::~ABOUTWIDGET_C(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::~ABOUTWIDGET_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

QIcon ABOUTWIDGET_C::GetChangeLogTabIcon(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetChangeLogTabIcon()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(GetTabIcon(ABOUTTAB_CHANGELOG));
}

QString ABOUTWIDGET_C::GetChangeLogText(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetChangeLogText()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pChangeLogEdit->toPlainText());
}

QString ABOUTWIDGET_C::GetDisplayName(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetDisplayName()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pProgramNameLabel->text());
}

QString ABOUTWIDGET_C::GetExecutableName(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetExecutableName()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_ExecutableName);
}

QPixmap ABOUTWIDGET_C::GetImage(void)
{
  QPixmap Pixmap;


  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetImage()");
  DEBUGLOG_LogIn();

  Pixmap=*m_pProgramIcon->pixmap();

  DEBUGLOG_LogOut();
  return(Pixmap);
}

QIcon ABOUTWIDGET_C::GetInformationTabIcon(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetInformationTabIcon()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(GetTabIcon(ABOUTTAB_INFORMATION));
}

QString ABOUTWIDGET_C::GetInformationText(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetInformationText()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pInformationTextBrowser->toPlainText());
}

QIcon ABOUTWIDGET_C::GetLicenseTabIcon(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetLicenseTabIcon()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(GetTabIcon(ABOUTTAB_LICENSE));
}

QString ABOUTWIDGET_C::GetLicenseText(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetLicenseText()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pLicenseAgreementEdit->toPlainText());
}

QIcon ABOUTWIDGET_C::GetTabIcon(ABOUTTAB_E TabIndex)
{
  QIcon Icon;


  DEBUGLOG_Printf1("ABOUTWIDGET_C::GetTabIcon(%u)",TabIndex);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (TabIndex<ABOUTTAB_FIRST) || (TabIndex>ABOUTTAB_LAST) )
  {
    MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
  }
  else
    Icon=m_pTabWidgets[TabIndex].Icon;

  DEBUGLOG_LogOut();
  return(Icon);
}

QString ABOUTWIDGET_C::GetVersion(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::GetVersion()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Version);
}

bool ABOUTWIDGET_C::IsChangeLogTabVisible(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::IsChangeLogTabVisible()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(IsTabVisible(ABOUTTAB_CHANGELOG));
}

bool ABOUTWIDGET_C::IsCheckForUpdateButtonEnabled(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::IsCheckForUpdateButtonEnabled()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pCheckButton->isEnabled());
}

bool ABOUTWIDGET_C::IsCheckForUpdateWidgetVisible(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::IsCheckForUpdateWidgetVisible()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pCheckFrame->isVisible());
}

bool ABOUTWIDGET_C::IsInformationTabVisible(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::IsInformationTabVisible()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(IsTabVisible(ABOUTTAB_INFORMATION));
}

bool ABOUTWIDGET_C::IsLicenseTabVisible(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::IsLicenseTabVisible()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(IsTabVisible(ABOUTTAB_LICENSE));
}

bool ABOUTWIDGET_C::IsTabVisible(ABOUTTAB_E TabIndex)
{
  bool VisibleFlag;


  DEBUGLOG_Printf1("ABOUTWIDGET_C::IsTabVisible(%u)",TabIndex);
  DEBUGLOG_LogIn();

  if ( (TabIndex<ABOUTTAB_FIRST) || (TabIndex>ABOUTTAB_LAST) )
  {
    MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
    VisibleFlag=false;
  }
  else
    VisibleFlag=m_pTabWidgets[TabIndex].Visible;

  DEBUGLOG_LogOut();
  return(VisibleFlag);
}

void ABOUTWIDGET_C::SetChangeLogTabIcon(QIcon Icon)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetChangeLogTabIcon(%p)",&Icon);
  DEBUGLOG_LogIn();

  SetTabIcon(ABOUTTAB_CHANGELOG,Icon);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetChangeLogTabVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetChangeLogTabVisible(%u)",VisibleFlag);
  DEBUGLOG_LogIn();

  SetTabVisible(ABOUTTAB_CHANGELOG,VisibleFlag);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetChangeLogText(QString Text)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetChangeLogText(%p(%s))",&Text,qPrintable(Text));
  DEBUGLOG_LogIn();

  m_pChangeLogEdit->setPlainText(Text);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetCheckForUpdateButtonEnabled(bool EnabledFlag)
{
  DEBUGLOG_Printf1(
      "ABOUTWIDGET_C::SetCheckForUpdateButtonEnabled(%u)",EnabledFlag);
  DEBUGLOG_LogIn();

  m_pCheckButton->setEnabled(EnabledFlag);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetCheckForUpdateWidgetVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1(
      "ABOUTWIDGET_C::SetCheckForUpdateWidgetVisible(%u)",VisibleFlag);
  DEBUGLOG_LogIn();

  m_pCheckFrame->setVisible(VisibleFlag);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetDisplayName(QString Name)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetDisplayName(%p(%s))",&Name,qPrintable(Name));
  DEBUGLOG_LogIn();

  m_pProgramNameLabel->setText(Name);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetExecutableName(QString Name)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetExecutableName(%p(%s))",&Name,qPrintable(Name));
  DEBUGLOG_LogIn();

  m_ExecutableName=Name;
  SetExecutableNameAndVersionText();

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetImage(QPixmap Pixmap)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetImage(%p)",&Pixmap);
  DEBUGLOG_LogIn();

  m_pProgramIcon->setPixmap(Pixmap);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetInformationTabIcon(QIcon Icon)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetInformationTabIcon(%p)",&Icon);
  DEBUGLOG_LogIn();

  SetTabIcon(ABOUTTAB_INFORMATION,Icon);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetInformationTabVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetInformationTabVisible(%u)",VisibleFlag);
  DEBUGLOG_LogIn();

  SetTabVisible(ABOUTTAB_INFORMATION,VisibleFlag);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetInformationText(QString Text)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetInformationText(%p(%s))",&Text,qPrintable(Text));
  DEBUGLOG_LogIn();

  m_pInformationTextBrowser->setText(Text);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetLicenseTabIcon(QIcon Icon)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetLicenseTabIcon(%p)",&Icon);
  DEBUGLOG_LogIn();

  SetTabIcon(ABOUTTAB_LICENSE,Icon);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetLicenseTabVisible(bool VisibleFlag)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::SetLicenseTabVisible(%u)",VisibleFlag);
  DEBUGLOG_LogIn();

  SetTabVisible(ABOUTTAB_LICENSE,VisibleFlag);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetLicenseText(QString Text)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetLicenseText(%p(%s))",&Text,qPrintable(Text));
  DEBUGLOG_LogIn();

  m_pLicenseAgreementEdit->setPlainText(Text);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetTabIcon(ABOUTTAB_E TabIndex,QIcon Icon)
{
  DEBUGLOG_Printf2("ABOUTWIDGET_C::SetTabIcon(%u,%p)",TabIndex,&Icon);
  DEBUGLOG_LogIn();

  if ( (TabIndex<ABOUTTAB_FIRST) || (TabIndex>ABOUTTAB_LAST) )
  {
    MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
  }
  else
  {
    m_pTabWidgets[TabIndex].Icon=Icon;
    UpdateTabWidgets();
  }

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetTabVisible(ABOUTTAB_E TabIndex,bool VisibleFlag)
{
//  ERRORCODE_T ErrorCode;
  int WidgetIndex;
  int InsertIndex;
  int Index;


  DEBUGLOG_Printf2("ABOUTWIDGET_C::SetTabVisible(%d,%u)",TabIndex,VisibleFlag);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (TabIndex<ABOUTTAB_FIRST) || (TabIndex>ABOUTTAB_LAST) )
  {
    MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
  }
  else
  {
    WidgetIndex=m_pAboutTabWidget->indexOf(m_pTabWidgets[TabIndex].pWidget);
    if (VisibleFlag==0)
    {
      if (WidgetIndex!=-1)
        m_pAboutTabWidget->removeTab(WidgetIndex);
    }
    else
    {
      if (WidgetIndex==-1)
      {
        InsertIndex=0;
        for(Index=0;Index<TabIndex;Index++)
          if (m_pAboutTabWidget->indexOf(m_pTabWidgets[Index].pWidget)!=-1)
            InsertIndex++;
        m_pAboutTabWidget->insertTab(InsertIndex,m_pTabWidgets[Index].pWidget,
            m_pTabWidgets[Index].Icon,m_pTabWidgets[Index].Label);
      }
    }
  }

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetUpdateText(QString Text)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetUpdateText(%p(%s))",&Text,qPrintable(Text));
  DEBUGLOG_LogIn();

  m_pCheckLabel->setText(Text);

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetVersion(QString Version)
{
  DEBUGLOG_Printf2(
      "ABOUTWIDGET_C::SetVersion(%p(%s))",&Version,qPrintable(Version));
  DEBUGLOG_LogIn();

  m_Version=Version;
  SetExecutableNameAndVersionText();

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::SetExecutableNameAndVersionText(void)
{
  DEBUGLOG_Printf0("ABOUTWIDGET_C::SetExecutableNameAndVersionText()");
  DEBUGLOG_LogIn();

  m_pProgramVersionLabel->setText(m_ExecutableName+" (Version "+m_Version+")");

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::ShowTab(ABOUTTAB_E TabIndex)
{
  DEBUGLOG_Printf1("ABOUTWIDGET_C::ShowTab(%u)",TabIndex);
  DEBUGLOG_LogIn();

  if ( (TabIndex<ABOUTTAB_FIRST) || (TabIndex>ABOUTTAB_LAST) )
  {
    MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
  }
  else
  {
    if (m_pTabWidgets[TabIndex].Visible==false)
    {
      MESSAGELOG_LogError(ERRORCODE_INVALIDPARAMETER);
    }
    else
      m_pAboutTabWidget->setCurrentIndex(TabIndex);
  }

  DEBUGLOG_LogOut();
  return;
}

void ABOUTWIDGET_C::UpdateTabWidgets(void)
{
  int Index;
  int VisibleIndex;


  DEBUGLOG_Printf0("ABOUTWIDGET_C::UpdateTabWidgets()");
  DEBUGLOG_LogIn();

  for(Index=ABOUTTAB_FIRST;Index<=ABOUTTAB_LAST;Index++)
  {
    VisibleIndex=m_pAboutTabWidget->indexOf(m_pTabWidgets[Index].pWidget);
    if (VisibleIndex!=-1)
    {
      m_pAboutTabWidget->setTabIcon(VisibleIndex,m_pTabWidgets[Index].Icon);
      m_pAboutTabWidget->setTabText(VisibleIndex,m_pTabWidgets[Index].Label);
    }
  }

  DEBUGLOG_LogOut();
  return;
}


#undef    ABOUTWIDGET_CPP


/**
*** aboutwidget.cpp
**/

/**
*** \file commontypes.h
*** \brief Interface classes.
*** \details Classes to be implemented by each GUI framework.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef   COMMONTYPES_H
/**
*** \brief commontypes.h identifier.
*** \details Identifier for commontypes.h.
**/
#define   COMMONTYPES_H


/****
*****
***** INCLUDES
*****
****/

#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Color.
*** \details A color based on the RGB model.
*** \note All parameters are in the range from 0.0 to 1.0, where 0.0 is the
***   minimum, 1.0 is the maximum.
**/
class COLOR_C
{
  public:
    /**
    *** \brief Invalid color component.
    *** \details Indicates that a color component is invalid.
    **/
    static float const mc_Invalid;
    static float const mc_Minimum;
    static float const mc_Maximum;

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    COLOR_C(void);

    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param Red Red component.
    *** \param Green Green component.
    *** \param Blue Blue component.
    *** \param Alpha Alpha component.
    **/
    COLOR_C(float Red,float Green,float Blue,float Alpha=mc_Maximum);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~COLOR_C(void);

    /**
    *** \brief Equality operator.
    *** \details Compares two COLOR_C for equality.
    *** \param Color Value on right hand side of operator.
    *** \retval true Values are equivalent.
    *** \retval false Values are not equivalent.
    **/
    bool operator==(COLOR_C const &Color) const;

    /**
    *** \brief Inequality operator.
    *** \details Compares two COLOR_C for inequality.
    *** \param Color Value on right hand side of operator.
    *** \retval true Values are not equivalent.
    *** \retval false Values are equivalent.
    **/
    bool operator!=(COLOR_C const &Color) const;

    /**
    *** \brief Get alpha component.
    *** \details Returns the alpha component of the color.
    *** \returns Alpha component of color [0..1].
    **/
    float GetAlpha(void) const;

    /**
    *** \brief Get blue component.
    *** \details Returns the blue component of the color.
    *** \returns Blue component of color [0..1].
    **/
    float GetBlue(void) const;

    /**
    *** \brief Get green component.
    *** \details Returns the green component of the color.
    *** \returns Green component of color [0..1].
    **/
    float GetGreen(void) const;

    /**
    *** \brief Get red component.
    *** \details Returns the red component of the color.
    *** \returns Red component of color [0..1].
    **/
    float GetRed(void) const;

    /**
    *** \brief Invalidate color.
    *** \details Sets the color to an invalid state.
    **/
    void Invalidate(void);

    /**
    *** \brief Get color validity.
    *** \details Returns the validity of the color.
    *** \retval true Color is valid (all components are valid).
    *** \retval false Color is invalid (one or more components are invalid).
    **/
    bool IsValid(void) const;

    /**
    *** \brief Set alpha component.
    *** \details Sets the alpha component of the color.
    *** \param Alpha Alpha component of color.
    **/
    void SetAlpha(float Alpha);

    /**
    *** \brief Set blue component.
    *** \details Sets the blue component of the color.
    *** \param Blue Blue component of color.
    **/
    void SetBlue(float Blue);

    /**
    *** \brief Set green component.
    *** \details Sets the green component of the color.
    *** \param Green Green component of color.
    **/
    void SetGreen(float Green);

    /**
    *** \brief Set red component.
    *** \details Sets the red component of the color.
    *** \param Red Red component of color.
    **/
    void SetRed(float Red);

    /**
    *** \brief Set all color components.
    *** \details Sets all of the color components.
    *** \param Red Red component of color.
    *** \param Green Green component of color.
    *** \param Blue Blue component of color.
    *** \param Alpha Alpha component of color.
    **/
    void SetRGBA(float Red,float Green,float Blue,float Alpha=mc_Maximum);

  private:
    /**
    *** \brief Alpha component.
    *** \details Alpha component of the color.
    **/
    float m_Alpha;
    /**
    *** \brief Blue component.
    *** \details Blue component of the color.
    **/
    float m_Blue;
    /**
    *** \brief Green component.
    *** \details Green component of the color.
    **/
    float m_Green;
    /**
    *** \brief Red component.
    *** \details Red component of the color.
    **/
    float m_Red;
};

/**
*** \brief Text drawing font.
*** \details Font used for rendering text.
**/
class FONT_C
{
  public:
    /**
    *** \brief Invalid color component.
    *** \details Indicates that a color component is invalid.
    **/
    static int const mc_Invalid;

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    FONT_C(void);

    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param Family Font family name.
    *** \param Point Size of the font.
    *** \param Weight Thickness of the font.
    *** \param ItalicsFlag Draw as italics.
    **/
    FONT_C(std::string const &Family,
        int Point=mc_Invalid,int Weight=mc_Invalid,bool ItalicsFlag=false);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~FONT_C(void);

    /**
    *** \brief Equality operator.
    *** \details Compares two FONT_C for equality.
    *** \param Font Value on right hand side of operator.
    *** \retval true Values are equivalent.
    *** \retval false Values are not equivalent.
    **/
    bool operator==(FONT_C const &Font) const;

    /**
    *** \brief Inequality operator.
    *** \details Compares two FONT_C for inequality.
    *** \param Font Value on right hand side of operator.
    *** \retval true Values are not equivalent.
    *** \retval false Values are equivalent.
    **/
    bool operator!=(FONT_C const &Font) const;

    /**
    *** \brief Get font family name.
    *** \details Returns the family name of the font.
    *** \returns Family name.
    **/
    std::string GetFamily(void) const;

    /**
    *** \brief Get font italics flag.
    *** \details Gets the italics flag for the font.
    *** \retval true Render in italics.
    *** \retval false Render normal.
    **/
    bool GetItalicFlag(void) const;

    /**
    *** \brief Get font point size.
    *** \details Gets the point size for the font.
    *** \returns Point size.
    **/
    int GetPointSize(void) const;

    /**
    *** \brief Get font weight.
    *** \details Gets the weight for the font.
    *** \returns Weight.
    **/
    int GetWeight(void) const;

    /**
    *** \brief Invalidate font.
    *** \details Sets the font to an invalid state.
    **/
    void Invalidate(void);

    /**
    *** \brief Get font validity.
    *** \details Returns the validity of the font.
    *** \retval true Font is valid (all components are valid).
    *** \retval false Font is invalid (one or more components are invalid).
    **/
    bool IsValid(void) const;

    /**
    *** \brief Set font family name.
    *** \details Sets the family name of the font.
    *** \param Family Family name.
    **/
    void SetFamily(std::string const &Family);

    /**
    *** \brief Set font italics flag.
    *** \details Sets the italics flag for the font.
    *** \param ItalicFlag true - render in italics,\n false - render normal.
    **/
    void SetItalicFlag(bool ItalicFlag);

    /**
    *** \brief Set font point size.
    *** \details Sets the point size for the font.
    *** \param Size Point size (in pixels).
    **/
    void SetPointSize(int Size);

    /**
    *** \brief Set font weight.
    *** \details Sets the weight for the font.
    *** \param Weight Weight.
    **/
    void SetWeight(int Weight);

  private:
    /**
    *** \brief Family name.
    *** \details Name of the font family.
    **/
    std::string m_Family;

    /**
    *** \brief Italic flag.
    *** \details Render the font normally or in italics.
    **/
    bool m_ItalicFlag;

    /**
    *** \brief Point size.
    *** \details Point size (in pixels) of the font.
    **/
    int m_PointSize;

    /**
    *** \brief Weight.
    *** \details Weight of the font.
    **/
    int m_Weight;
};

/**
*** \brief Object position in a plane.
*** \details The position of an object in a plane.
**/
class POSITION_C
{
  public:
    static int const mc_Invalid;
    static int const mc_Minimum;
    static int const mc_Maximum;

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    POSITION_C(void);

    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param X X coordinate.
    *** \param Y Y coordinate.
    **/
    POSITION_C(int X,int Y);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~POSITION_C(void);

    /**
    *** \brief Equality operator.
    *** \details Compares two POSITION_C for equality.
    *** \param Position Value on right hand side of operator.
    *** \retval true Values are equivalent.
    *** \retval false Values are not equivalent.
    **/
    bool operator==(POSITION_C const &Position) const;

    /**
    *** \brief Inequality operator.
    *** \details Compares two POSITION_C for inequality.
    *** \param Position Value on right hand side of operator.
    *** \retval true Values are not equivalent.
    *** \retval false Values are equivalent.
    **/
    bool operator!=(POSITION_C const &Position) const;

    /**
    *** \brief Get X coordinate.
    *** \details Returns the X coordinate of the position.
    *** \returns X coordinate.
    **/
    int GetX(void) const;

    /**
    *** \brief Get Y coordinate.
    *** \details Returns the Y coordinate of the position.
    *** \returns Y coordinate.
    **/
    int GetY(void) const;

    /**
    *** \brief Invalidate color.
    *** \details Sets the color to an invalid state.
    **/
    void Invalidate(void);

    /**
    *** \brief Get color validity.
    *** \details Returns the validity of the color.
    *** \retval true Color is valid (all components are valid).
    *** \retval false Color is invalid (one or more components are invalid).
    **/
    bool IsValid(void) const;

    /**
    *** \brief Set X coordinate.
    *** \details Sets the X coordinate of the position.
    *** \param X X coordinate.
    **/
    void SetX(int X);

    /**
    *** \brief Set X and Y coordinates.
    *** \details Sets the X and Y coordinates of the position.
    *** \param X X coordinate.
    *** \param Y Y coordinate.
    **/
    void SetXY(int X,int Y);

    /**
    *** \brief Set Y coordinate.
    *** \details Sets the Y coordinate of the position.
    *** \param Y Y coordinate.
    **/
    void SetY(int Y);

  private:
    /**
    *** \brief X coordinate.
    *** \details X coordinate of the position.
    **/
    int m_X;

    /**
    *** \brief Y coordinate.
    *** \details Y coordinate of the position.
    **/
    int m_Y;
};

/**
*** \brief Two dimensional object size.
*** \details Size of a two dimensional object.
**/
class SIZE_C
{
  public:
    static int const mc_Invalid;
    static int const mc_Minimum;
    static int const mc_Maximum;

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    SIZE_C(void);

    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param Width Width.
    *** \param Height Height.
    **/
    SIZE_C(int Width,int Height);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~SIZE_C(void);

    /**
    *** \brief Equality operator.
    *** \details Compares two SIZE_C for equality.
    *** \param Size Value on right hand side of operator.
    *** \retval true Values are equivalent.
    *** \retval false Values are not equivalent.
    **/
    bool operator==(SIZE_C const &Size) const;

    /**
    *** \brief Inequality operator.
    *** \details Compares two SIZE_C for inequality.
    *** \param Size Value on right hand side of operator.
    *** \retval true Values are not equivalent.
    *** \retval false Values are equivalent.
    **/
    bool operator!=(SIZE_C const &Size) const;

    /**
    *** \brief Get height.
    *** \details Returns the height.
    *** \returns Height value.
    **/
    int GetHeight(void) const;

    /**
    *** \brief Get width.
    *** \details Returns the width.
    *** \returns Width value.
    **/
    int GetWidth(void) const;

    /**
    *** \brief Invalidate the color.
    *** \details Sets the color to an invalid state.
    **/
    void Invalidate(void);

    /**
    *** \brief Get validity flag.
    *** \details Returns the validity of the data.
    *** \retval true All values are valid.
    *** \retval false One or more values are invalid.
    **/
    bool IsValid(void) const;

    /**
    *** \brief Set height.
    *** \details Sets the height.
    *** \param Height Height value.
    **/
    void SetHeight(int Height);

    /**
    *** \brief Set width.
    *** \details Sets the width.
    *** \param Width Width value.
    **/
    void SetWidth(int Width);

    /**
    *** \brief Set width and height.
    *** \details Sets the width and height.
    *** \param Width Width value.
    *** \param Height Height value.
    **/
    void SetWidthHeight(int Width,int Height);

  private:
    /**
    *** \brief Height.
    *** \details Height value.
    **/
    int m_Height;

    /**
    *** \brief Width.
    *** \details Width value.
    **/
    int m_Width;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* COMMONTYPES_H */


/**
*** commontypes.h
**/

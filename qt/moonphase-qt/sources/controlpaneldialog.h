/**
*** \file controlpaneldialog.h
*** \brief Application main window.
*** \details The main window of the application.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(CONTROLPANELDIALOG_H)
/**
*** \internal
*** \brief controlpaneldialog.h identifier.
*** \details Identifier for controlpaneldialog.h.
*** \endinternal
**/
#define   CONTROLPANELDIALOG_H


/****
*****
***** INCLUDES
*****
****/

#include  <QDialog>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class QAbstractButton;

/**
*** \brief Application main window.
*** \details The main window of the application.
**/
class CONTROLPANELDIALOG_C : public QDialog
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    CONTROLPANELDIALOG_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    CONTROLPANELDIALOG_C(CONTROLPANELDIALOG_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~CONTROLPANELDIALOG_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    CONTROLPANELDIALOG_C const & operator=(
        CONTROLPANELDIALOG_C const &Dialog)=delete;

  protected:
    /**
    *** \brief Close event handler.
    *** \details Handler called when a window close event occurs.
    *** \param pEvent Close event data.
    **/
    void closeEvent(QCloseEvent *pEvent);

  private slots:
    /**
    *** \brief Button box button clicked.
    *** \details A button in the button box was clicked.
    *** \param pButton
    **/
    void ButtonBoxButtonClickedSlot(QAbstractButton *pButton);

    /**
    *** \brief Check button clicked.
    *** \details The Check button was clicked.
    **/
    void CheckButtonClickedSlot(void);

    /**
    *** \brief Update panel timer triggered.
    *** \details The timer to update the information panel triggered.
    **/
    void InformationPanelToolTipTimerTriggeredSlot(void);

    /**
    *** \brief Message from another instance was received.
    *** \details A messsage from another instance was received.
    *** \param Message
    **/
    void InstanceMessageSlot(QString const &Message);

    /**
    *** \brief Quit requested.
    *** \details The user wants to quit.
    **/
    void QuitSlot(void);

    /**
    *** \brief Show control panel.
    *** \details The control panel needs to be shown.
    **/
    void ShowControlPanelSlot(void);

    /**
    *** \brief Show the information panel.
    *** \details User request that the information panel be shown.
    **/
    void ShowInformationPanelSlot(void);

    /**
    *** \brief A tray icon preference changed.
    *** \details A preference related to the tray icon changed.
    **/
    void TrayIconPreferencesChangedSlot(void);

    /**
    *** \brief Update timer triggered.
    *** \details The update timer has been triggered, update the moon data and
    ***   (possibly) the icon.
    **/
    void UpdateTimerTriggeredSlot(void);

    /**
    *** \brief Newest version number was downloaded.
    *** \details The number of the newest program version was downloaded.
    *** \param Version Version string.
    **/
    void VersionSlot(QString const &Version);

  private:
    class CONTROLPANELDIALOGPRIVATE_C;

    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend CONTROLPANELDIALOGPRIVATE_C;

    /**
    *** \brief Implementation pointer (PIMPL).
    *** \details Pointer to implementation object (PIMPL).
    **/
    QScopedPointer<CONTROLPANELDIALOGPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(CONTROLPANELDIALOG_H) */


/**
*** controlpaneldialog.h
**/

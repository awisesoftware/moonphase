/**
*** \file msvc.c
*** \brief Microsoft specific function implementations.
*** \details Function implementations that are not native to Microsoft SDKs,
***   but are needed by awistoolbox.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       defined(_WIN32) || defined(_WIN64)

/**
*** \brief msvc.c identifier.
*** \details Identifier for msvc.c.
*** \internal
**/
#define   MSVC_C


/****
*****
***** INCLUDES
*****
****/

#include  "stdio_msvc.h"
#if       defined(DEBUG_MSVC_C)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MSVC_C) */
#include  "debuglog.h"

#include  <stdarg.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <time.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

int asprintf(char **ppString,char const *pFormat,...)
{
  int Count;
  va_list List;

  DEBUGLOG_Printf3("asprintf(%p,%p(%s))",ppString,pFormat,pFormat);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if ( (ppString==NULL) || (pFormat==NULL) )
    Count=-1;
  else
  {
    va_start(List,pFormat);
    Count=vasprintf(ppString,pFormat,List);
    va_end(List);
  }

  DEBUGLOG_LogOut();
  return(Count);
}

time_t timegm(struct tm *pTimeTM)
{
  DEBUGLOG_Printf1("timegm(%p)",pTimeTM);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(_mkgmtime(pTimeTM));
}

int vasprintf(char **ppString,char const *pFormat,va_list pList)
{
  int Count;
  int Length;


  DEBUGLOG_Printf4("asprintf(%p,%p(%s),%p)",ppString,pFormat,pFormat,pList);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  Count=-1;
  if ( (ppString!=NULL) && (pFormat!=NULL) && (pList!=NULL) )
  {
    Length=_vscprintf(pFormat,pList);
    if (Length!=-1)
    {
      Length++;   /* '\0' terminator. */
      *ppString=(char *)malloc(Length);
      if ((*ppString)!=NULL)
      {
        Count=vsnprintf(*ppString,Count,pFormat,pList);
        if (Count==-1)
        {
          free(*ppString);
          *ppString=NULL;
        }
      }
    }
  }
  else
    *ppString=NULL;

  DEBUGLOG_LogOut();
  return(Count);
}


#undef    MSVC_C

#endif    /* defined(_WIN32) || defined(_WIN64) */


/**
*** msvc.c
**/

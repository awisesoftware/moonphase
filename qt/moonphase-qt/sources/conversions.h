/**
*** \file conversions.h
*** \brief Conversion to/from common types.
*** \details Conversions between common types and native types.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef   CONVERSIONS_H
/**
*** \brief conversions.h identifier.
*** \details Identifier for conversions.h.
**/
#define   CONVERSIONS_H


/****
*****
***** INCLUDES
*****
****/

#include  "commontypes.h"

#include  <QColor>
#include  <QFont>
#include  <QPoint>
#include  <QSize>



/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

QColor ToQColor(COLOR_C const &Color);
COLOR_C ToCOLOR_C(QColor const &Color);

QFont ToQFont(FONT_C const &Font);
FONT_C ToFONT_C(QFont const &Font);

QPoint ToQPoint(POSITION_C const &Point);
POSITION_C ToPOSITION_C(QPoint const &Point);

QSize ToQSize(SIZE_C const &Size);
SIZE_C ToSIZE_C(QSize const &Size);


#endif    /* CONVERSIONS_H */


/**
*** conversions.h
**/

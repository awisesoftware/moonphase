/**
*** \file drawframe.cpp
*** \brief drawframe.h implementation.
*** \details Implementation file for drawframe.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief drawframe.cpp identifier.
*** \details Identifier for drawframe.cpp.
*** \internal
**/
#define   DRAWFRAME_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "drawframe.h"
#if       defined(DEBUG_DRAWFRAME_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_DRAWFRAME_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  <QPainter>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

QPixmap DrawFrame(MOONANIMATION_T const *pMoonAnimation,unsigned int Percent,
    bool UseBackgroundColorFlag,QColor BackgroundColor)
{
  ERRORCODE_T ErrorCode;
  unsigned int Index;
  unsigned int FrameCount;
  QPixmap const *pConstPixmap;
  QPixmap Pixmap;


  DEBUGLOG_Printf1("PREFERENCESWIDGET_C::GetFrame(%u)",Percent);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pMoonAnimation==NULL)
  {
    ErrorCode=ERRORCODE_NULLPARAMETER;
    MESSAGELOG_LogError(ErrorCode);
  }
  else if (Percent>100)
  {
    ErrorCode=ERRORCODE_INVALIDPARAMETER;
    MESSAGELOG_LogError(ErrorCode);
  }
  else
  {
    ErrorCode=MoonAnimation_GetFrameCount(pMoonAnimation);
    MESSAGELOG_LogError(ErrorCode);
    if (ErrorCode>0)
    {
      /* ErrorCode has a frame count > 0. */
      FrameCount=static_cast<unsigned>(ErrorCode);
      Index=static_cast<unsigned>(((Percent/100.001)*FrameCount)+.5);
      if (Index>=FrameCount)
        Index-=FrameCount;
      ErrorCode=MoonAnimation_GetFrame(pMoonAnimation,Index,&pConstPixmap);
      MESSAGELOG_LogError(ErrorCode);
      if (ErrorCode>0)
        Pixmap=*pConstPixmap;
    }
  }
  if (ErrorCode<=0)
  {
    Pixmap.load(":/ErrorIcon");
    ErrorCode=ERRORCODE_SUCCESS;
  }
  if (UseBackgroundColorFlag==true)
  {
    QImage Background(Pixmap.width(),Pixmap.height(),QImage::Format_ARGB32);
    Background.fill(BackgroundColor);
    QPainter Painter(&Background);
    Painter.drawPixmap(0,0,Pixmap);
    Painter.end();
    Pixmap=QPixmap::fromImage(Background);
  }

  DEBUGLOG_LogOut();
  return(Pixmap);
}


#undef    DRAWFRAME_CPP


/**
*** drawframe.cpp
**/

/**
*** \file paneldialog.cpp
*** \brief paneldialog.h implementation.
*** \details Implementation file for paneldialog.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \brief paneldialog.cpp identifier.
*** \details Identifier for paneldialog.cpp.
*** \internal
**/
#define   PANELDIALOG_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "paneldialog.h"
#include  "ui_paneldialog.h"
#if       defined(DEBUG_PANELDIALOG_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_PANELDIALOG_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "config.h"
#include  "conversions.h"

#include  <QCloseEvent>
#include  <QApplication>
#include  <QDesktopWidget>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Information panel dialog implementation class.
*** \details The implementation class for the information panel dialog.
**/
class PANELDIALOG_C::PANELDIALOGPRIVATE_C
{
  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pPublic Pointer to the interface class (MOONDATAWIDGET_C).
    **/
    PANELDIALOGPRIVATE_C(PANELDIALOG_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELDIALOGPRIVATE_C(PANELDIALOGPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELDIALOGPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELDIALOGPRIVATE_C const & operator=(
        PANELDIALOGPRIVATE_C const &Dialog)=delete;

    /**
    *** \brief Check window position and adjust if necessary.
    *** \details Checks if the all or part of the windows is off-screen and
    ***   if so, adjusts the position of the window to keep it entirely
    ***   on screen.
    *** \param Position Current window position (from window or event data).
    *** \param Size Current size position (from window or event data).
    *** \param WMAdjustFlag Calculate and use window frame size and title bar
    ***   size when calculating the current and new position. Some events
    ***   use the position of the "client area" (no title bar or frame) and
    ***   others use the entire area (title bar and frame).
    **/
    void AdjustPosition(
        QPoint const &Position,QSize const &Size,bool WMAdjustFlag) const;

    /**
    *** \brief "Keep On-screen" flag value.
    *** \details The value of the "Keep On-screen" flag.
    *** \note Used by event handlers.
    **/
    bool m_KeepOnScreenFlag;

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    PANELDIALOG_C *m_pPublic;

    /**
    *** \brief Previous "Always On Top" flag value.
    *** \details The previous value of the "Always On Top" flag.
    **/
    bool m_PreviousAlwaysOnTopFlag;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_PANELDIALOGUI_C> m_pUI;

    /**
    *** \brief Start up mode flag.
    *** \details Application is just starting.
    *** \note This flag is used to initialize certain settings that only need
    ***   to be initialized on start up.
    **/
    bool m_StartFlag;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

PANELDIALOG_C::
    PANELDIALOGPRIVATE_C::PANELDIALOGPRIVATE_C(PANELDIALOG_C *pPublic)
    : m_pPublic(pPublic), m_pUI(new Ui_PANELDIALOGUI_C)
{
  DEBUGLOG_Printf1(
      "PANELDIALOG_C::PANELDIALOGPRIVATE_C::PANELDIALOGPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_KeepOnScreenFlag=false;
  m_PreviousAlwaysOnTopFlag=false;
  m_StartFlag=true;

  /* Set up the user interface. */
  m_pUI->setupUi(pPublic);

  DEBUGLOG_LogOut();
  return;
}

PANELDIALOG_C::PANELDIALOGPRIVATE_C::~PANELDIALOGPRIVATE_C(void)
{
  DEBUGLOG_Printf0(
      "PANELDIALOG_C::PANELDIALOGPRIVATE_C::~PANELDIALOGPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELDIALOG_C::PANELDIALOGPRIVATE_C::AdjustPosition(
    QPoint const &Position,QSize const &Size,bool WMAdjustFlag) const
{
  static bool InMoveFlag=false;   // Used to prevent infinite recursion.


  DEBUGLOG_Printf3(
      "PANELDIALOG_C::PANELDIALOGPRIVATE_C::AdjustPosition(%p,%p,%d)",
      &Position,&Size,WMAdjustFlag);
  DEBUGLOG_LogIn();

  /* No recursion. */
  if (!InMoveFlag)
  {
    int NewX;
    int NewY;
    int FrameSize;
    int TitleSize;
    QDesktopWidget *pDesktop;
    QRect AvailableGeometry;


    /* Unadjusted position. */
    NewX=Position.x();
    NewY=Position.y();

    /* Calculate frame dimensions. */
    FrameSize=m_pPublic->geometry().x()-m_pPublic->pos().x();
    TitleSize=m_pPublic->geometry().y()-m_pPublic->pos().y();

    /* Get screen geometry. */
    pDesktop=QApplication::desktop();
    AvailableGeometry=pDesktop->availableGeometry(m_pPublic);

    /* Off left edge? */
    if (NewX<FrameSize)
    {
      NewX=0;
      InMoveFlag=true;
    }
    /* Off right edge? */
    else if ((NewX+Size.width()+FrameSize)>AvailableGeometry.right())
    {
      NewX=AvailableGeometry.right()-(2*FrameSize+Size.width()-1);
      InMoveFlag=true;
    }
    /* Off top edge? */
    if (NewY<0)
    {
      NewY=0;
      InMoveFlag=true;
    }
    /* Off bottom edge? */
    else if ((NewY+Size.height()+FrameSize)>AvailableGeometry.bottom())
    {
      NewY=AvailableGeometry.bottom()-(FrameSize+Size.height()+TitleSize-1);
      InMoveFlag=true;
    }

    /* Need to move window. */
    if (InMoveFlag)
    {
      if (WMAdjustFlag)
      {
        if ( (NewX!=Position.x()) && (NewY==Position.y()) )
          NewY-=TitleSize;
        else if ( (NewX==Position.x()) && (NewY!=Position.y()) )
          NewX-=FrameSize;
      }

      m_pPublic->move(NewX,NewY);
      InMoveFlag=false;
    }
  }

  DEBUGLOG_LogOut();
  return;
}

PANELDIALOG_C::PANELDIALOG_C(QWidget *pParent)
    : QDialog(pParent), m_pPrivate(new PANELDIALOGPRIVATE_C(this))
{
  DEBUGLOG_Printf1("PANELDIALOG_C::PANELDIALOG_C(%p)",pParent);
  DEBUGLOG_LogIn();

  /* Set window title. */
  setWindowTitle(windowTitle()+tr(" - ")+tr(MOONPHASEQT_DISPLAYNAME));

  /* Want a panel entry. */
  setWindowFlags(Qt::Window);

  DEBUGLOG_LogOut();
  return;
}

PANELDIALOG_C::~PANELDIALOG_C(void)
{
  DEBUGLOG_Printf0("PANELDIALOG_C::~PANELDIALOG_C()");
  DEBUGLOG_LogIn();

  delete m_pPrivate.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELDIALOG_C::closeEvent(QCloseEvent *pEvent)
{
  DEBUGLOG_Printf1("PANELDIALOG_C::closeEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  /* Ignore the close event and fake a "Close" button clicked signal. */
  pEvent->ignore();
  CloseButtonClickedSlot();

  DEBUGLOG_LogOut();
  return;
}

void PANELDIALOG_C::CloseButtonClickedSlot(void)
{
  DEBUGLOG_Printf0("PANELDIALOG_C::CloseButtonClickedSlot()");
  DEBUGLOG_LogIn();

  setVisible(false);

  DEBUGLOG_LogOut();
  return;
}

void PANELDIALOG_C::moveEvent(QMoveEvent *pEvent)
{
  DEBUGLOG_Printf1("PANELDIALOG_C::moveEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  if (m_pPrivate->m_KeepOnScreenFlag)
  {
    m_pPrivate->AdjustPosition(pEvent->pos(),size(),true);
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELDIALOG_C::resizeEvent(QResizeEvent *pEvent)
{
  DEBUGLOG_Printf1("PANELDIALOG_C::resizeEvent(%p)",pEvent);
  DEBUGLOG_LogIn();

  if (m_pPrivate->m_KeepOnScreenFlag)
  {
    m_pPrivate->AdjustPosition(
        pos()+QPoint(geometry().x()-pos().x(),geometry().y()-pos().y()),
        pEvent->size(),true);
  }

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C PANELDIALOG_C::Update(
    PREFERENCES_C::MOONDATA_C::PANEL_C const *pPreferences,
    MOONDATA_T const &MoonData)
{
  DEBUGLOG_Printf2("PANELDIALOG_C::Update(%p,%p)",pPreferences,&MoonData);
  DEBUGLOG_LogIn();

  /* Show/hide the close button. */
  m_pPrivate->m_pUI->m_pButtonBox->setVisible(
      !pPreferences-> GetDisplayPreferencesPointer()->GetHideCloseButtonFlag());

  /* Adjust position if "Keep on-screen" option is set. */
  m_pPrivate->m_KeepOnScreenFlag=
      pPreferences->GetDisplayPreferencesPointer()->GetKeepOnScreenFlag();
  if (m_pPrivate->m_KeepOnScreenFlag)
    m_pPrivate->AdjustPosition(pos(),size(),false);

  /* Initialize dialog box settings only on start up. */
  if (m_pPrivate->m_StartFlag==true)
  {
    if (pPreferences->GetDisplayPreferencesPointer()->GetDisplayOnStartUpFlag())
      setVisible(true);
    if (pPreferences->GetDisplayPreferencesPointer()->GetPosition()!=
        PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidPosition)
      move(ToQPoint(pPreferences->GetDisplayPreferencesPointer()->GetPosition()));
    if (pPreferences->GetDisplayPreferencesPointer()->GetSize()!=
        PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidSize)
      resize(ToQSize(pPreferences->GetDisplayPreferencesPointer()->GetSize()));
    resize(minimumSizeHint());
        // Necessary if close button gets hidden on start up.

    m_pPrivate->m_StartFlag=false;
  }

  /* Only change windows flags if there are changes in the preferences. */
  if (pPreferences->GetDisplayPreferencesPointer()->GetAlwaysOnTopFlag()!=
      m_pPrivate->m_PreviousAlwaysOnTopFlag)
  {
    bool VisibleFlag;


    /* Changing the "Always On Top" flag hides the window, so save the current
        visibility and then restore it after the flag is set/cleared. */
    VisibleFlag=isVisible();

    /* Set/clear the "On Tip" value. */
    if (pPreferences->GetDisplayPreferencesPointer()->GetAlwaysOnTopFlag())
      setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint);
    else
      setWindowFlags(windowFlags()&~Qt::WindowStaysOnTopHint);

    /* Restore the window visibility. */
    setVisible(VisibleFlag);

    /* Save "Keep on-screen" flag for next time. */
    m_pPrivate->m_PreviousAlwaysOnTopFlag=
      pPreferences->GetDisplayPreferencesPointer()->GetAlwaysOnTopFlag();
  }

  /* Update the display widget. */
  m_pPrivate->m_pUI->m_pDisplayWidget->Update(
      pPreferences->GetItemListPointer(),MoonData);

  DEBUGLOG_LogOut();
  return(PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C(
      isVisible(),ToPOSITION_C(pos()),ToSIZE_C(size())));
}


#undef    PANELDIALOG_CPP


/**
*** paneldialog.cpp
**/

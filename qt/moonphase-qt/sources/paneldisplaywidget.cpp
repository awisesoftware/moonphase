/**
*** \file paneldisplaywidget.cpp
*** \brief paneldisplaywidget.h implementation.
*** \details Implementation file for paneldisplaywidget.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief paneldisplaywidget.cpp identifier.
*** \details Identifier for paneldisplaywidget.cpp.
*** \endinternal
**/
#define   PANELDISPLAYWIDGET_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "paneldisplaywidget.h"
#include  "ui_paneldisplaywidget.h"
#ifdef    DEBUG_PANELDISPLAYWIDGET_CPP
#ifndef   USE_DEBUGLOG
#define   USE_DEBUGLOG
#endif    /* USE_DEBUGLOG */
#endif    /* DEBUG_PANELDISPLAYWIDGET_CPP */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "conversions.h"
#include  "information.h"
#include  "paneltooltipitem.h"

#include  <QLabel>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Moon data item display widget implementation class.
*** \details The implementation class for the moon data item display widget.
**/
class PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGETPRIVATE_C
{
  public:
    /**
    *** \brief Widget column enumerations.
    *** \details Enumerations of the columns in the widget.
    **/
    typedef enum enumCOLUMN
    {
      _COLUMN_FIRST=0,              /**< First column. **/
      COLUMN_LABEL=_COLUMN_FIRST,   /**< Column containing the item label. **/
      COLUMN_DATA=1,                /**< Column containing the item data. **/
      _COLUMN_LAST=COLUMN_DATA,     /**< Last column. **/
      _COLUMN_COUNT=_COLUMN_LAST+1, /**< Number of columns. */
    } COLUMN_E;

    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    *** \param pPublic Pointer to the interface class
    ***   (PANELDISPLAYWIDGET_C).
    **/
    PANELDISPLAYWIDGETPRIVATE_C(PANELDISPLAYWIDGET_C *pPublic);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELDISPLAYWIDGETPRIVATE_C(
        PANELDISPLAYWIDGETPRIVATE_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELDISPLAYWIDGETPRIVATE_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELDISPLAYWIDGETPRIVATE_C const & operator=(
        PANELDISPLAYWIDGETPRIVATE_C const &Dialog)=delete;

    /**
    *** \brief Render an item.
    *** \details Renders a single item to the widget.
    *** \param Position Line number (starting at 0) in the widget.
    *** \param pItem Pointer to item data.
    *** \param MoonData Moon data.
    **/
    void RenderItem(unsigned Position,PANELTOOLTIPITEM_C const *pItem,
        MOONDATA_T const &MoonData);

    /**
    *** \brief Resize widget.
    *** \details Adjusts the number of lines in the widget (if necessary).
    *** \param Size Required number of lines in widget.
    **/
    void Resize(long unsigned Size);

    /**
    *** \brief Interface class pointer.
    *** \details Pointer to the interface class.
    **/
    PANELDISPLAYWIDGET_C *m_pPublic;

    /**
    *** \brief User interface pointer.
    *** \details Pointer to the user interface.
    **/
    QScopedPointer<Ui_PANELDISPLAYWIDGETUI_C> m_pUI;
};


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGETPRIVATE_C::
    PANELDISPLAYWIDGETPRIVATE_C(PANELDISPLAYWIDGET_C *pPublic)
    : m_pPublic(pPublic), m_pUI(new Ui_PANELDISPLAYWIDGETUI_C)
{
  DEBUGLOG_Printf1("PANELDISPLAYWIDGET_C::"
      "PANELDISPLAYWIDGETPRIVATE_C::"
      "PANELDISPLAYWIDGETPRIVATE_C(%p)",pPublic);
  DEBUGLOG_LogIn();

  m_pPublic=pPublic;
  m_pUI->setupUi(pPublic);

  DEBUGLOG_LogOut();
  return;
}

PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGETPRIVATE_C::
    ~PANELDISPLAYWIDGETPRIVATE_C(void)
{
  DEBUGLOG_Printf0("PANELDISPLAYWIDGET_C::"
      "PANELDISPLAYWIDGETPRIVATE_C::"
      "~PANELDISPLAYWIDGETPRIVATE_C()");
  DEBUGLOG_LogIn();

  delete m_pUI.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGETPRIVATE_C::
    RenderItem(unsigned Position,PANELTOOLTIPITEM_C const *pItem,
    MOONDATA_T const &MoonData)
{
  QLayoutItem *pLayoutItem;
  QLabel *pLabel;
  QLabel *pData;
  PANELITEM_C const *pPanelItem;
  TOOLTIPITEM_C const *pToolTipItem;
  DATETIMEOPTIONS_C const *pDTOptions;


  DEBUGLOG_Printf3("PANELDISPLAYWIDGET_C::"
      "PANELDISPLAYWIDGETPRIVATE_C::RenderItem(%u,%p,%p)",
      Position,pItem,&MoonData);
  DEBUGLOG_LogIn();

  try
  {
    pPanelItem=dynamic_cast<PANELITEM_C const *>(pItem);
    pToolTipItem=dynamic_cast<TOOLTIPITEM_C const *>(pItem);
    if ( (pPanelItem==nullptr)==(pToolTipItem==nullptr) )   // XOR
    {
      MESSAGELOG_Error("(pPanelItem==nullptr)==(pToolTipItem==nullptr)).");
    }
    else
    {
      /* Get the label pointers. */
      pLayoutItem=m_pUI->m_pItemGLayout->
          itemAtPosition(static_cast<signed>(Position),0);
      if (pLayoutItem==nullptr)
      {
        MESSAGELOG_Error("pLayoutItem==nullptr.");
        throw(std::runtime_error("pLayoutItem==nullptr."));
      }
      pLabel=dynamic_cast<QLabel*>(pLayoutItem->widget());
      if (pLabel==nullptr)
      {
        MESSAGELOG_Error("pLabel==nullptr.");
        throw(std::runtime_error("pLabel==nullptr."));
      }
      pLayoutItem=m_pUI->m_pItemGLayout->
          itemAtPosition(static_cast<signed>(Position),1);
      if (pLayoutItem==nullptr)
      {
        MESSAGELOG_Error("pLayoutItem==nullptr.");
        throw(std::runtime_error("pLayoutItem==nullptr."));
      }
      pData=dynamic_cast<QLabel*>(pLayoutItem->widget());
      if (pData==nullptr)
      {
        MESSAGELOG_Error("pData==nullptr.");
        throw(std::runtime_error("pData==nullptr."));
      }

      if (pPanelItem!=nullptr)
      {
        QString StyleSheet;
        QString LabelStyleSheet;
        QString DataStyleSheet;


        /* Set the alignment. */
        switch(pPanelItem->GetLabelAlignment())
        {
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_CENTER:
            pLabel->setAlignment(Qt::AlignHCenter);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_JUSTIFY:
            pLabel->setAlignment(Qt::AlignJustify);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_LEFT:
            pLabel->setAlignment(Qt::AlignLeft);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_RIGHT:
            pLabel->setAlignment(Qt::AlignRight);
            break;
          default:
            MESSAGELOG_Error("Unknown alignment type.");
            throw(std::runtime_error("Unknown alignment type."));
            break;
        }
        switch(pPanelItem->GetDataAlignment())
        {
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_CENTER:
            pData->setAlignment(Qt::AlignHCenter);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_JUSTIFY:
            pData->setAlignment(Qt::AlignJustify);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_LEFT:
            pData->setAlignment(Qt::AlignLeft);
            break;
          case PANELITEM_C::ALIGNMENT_E::ALIGNMENT_RIGHT:
            pData->setAlignment(Qt::AlignRight);
            break;
          default:
            MESSAGELOG_Error("Unknown alignment type.");
            throw(std::runtime_error("Unknown alignment type."));
            break;
        }

        /* Prepare the paddings string. */
        StyleSheet+=QString("padding-top:%1px;padding-bottom:%1px;").
            arg(pPanelItem->GetTopBottomPadding());

        LabelStyleSheet+=QString("padding-left:%1px;padding-right:%2px;").
            arg(pPanelItem->GetLeftRightPadding()).
            arg(pPanelItem->GetCenterPadding());
        DataStyleSheet+=QString("padding-left:%1px;padding-right:%2px;").
            arg(pPanelItem->GetCenterPadding()).
            arg(pPanelItem->GetLeftRightPadding());

        /* Set the font. */
        pLabel->setFont(ToQFont(pPanelItem->GetFont()));
        pData->setFont(ToQFont(pPanelItem->GetFont()));

        /* Prepare the background color string. */
        StyleSheet+=QString("background-color: rgb(%1,%2,%3);").
            arg(pPanelItem->GetBackgroundColor().GetRed()*255).
            arg(pPanelItem->GetBackgroundColor().GetGreen()*255).
            arg(pPanelItem->GetBackgroundColor().GetBlue()*255);

        /* Prepare the text color string. */
        StyleSheet+=QString("color: rgb(%1,%2,%3);").
            arg(pPanelItem->GetTextColor().GetRed()*255).
            arg(pPanelItem->GetTextColor().GetGreen()*255).
            arg(pPanelItem->GetTextColor().GetBlue()*255);

        /* Set the style sheet. */
        pLabel->setStyleSheet(StyleSheet+LabelStyleSheet);
        pData->setStyleSheet(StyleSheet+DataStyleSheet);
      }
      else if (pToolTipItem!=nullptr)
      {
        /*
        ** Read the tool tip widgets.
        */

        // Nothing. TOOLTIPITEM_C==PANELTOOLTIPITEM_C.
      }

      /* Calculate the data and display both the label and data. */
      pLabel->setText(QString::fromStdString(
          Information_GetInformationLabel(pItem->GetInformationIndex())));
      pDTOptions=dynamic_cast<DATETIMEOPTIONS_C const *>(pItem);
      if (pDTOptions==nullptr)
      {
        MESSAGELOG_Error("pDTOptions==nullptr");
      }
      else
        pData->setText(QString::fromStdString(
            Information_Print(MoonData,pItem->GetInformationIndex(),
            pItem->GetUnitFormatIndex(),
            *pDTOptions)));
    }
  }
  catch(std::runtime_error const &RE)
  {
    Q_UNUSED(RE);
    // Just exit.
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELDISPLAYWIDGET_C::
    PANELDISPLAYWIDGETPRIVATE_C::Resize(long unsigned Size)
{
  unsigned RowCount;


  DEBUGLOG_Printf1("PANELDISPLAYWIDGET_C::"
      "PANELDISPLAYWIDGETPRIVATE_C::Resize(%u)",Size);
  DEBUGLOG_LogIn();

  /* QGridLayout.rowCount() returns 1 when the layout is empty, so use
      QGridLayout.count() divided by the number of columns instead. */
  RowCount=static_cast<unsigned>(m_pUI->m_pItemGLayout->count()/_COLUMN_COUNT);

  /* Need to add rows? */
  if (Size>RowCount)
  {
    for(unsigned Index=RowCount;Index<Size;Index++)
    {
      m_pUI->m_pItemGLayout->addWidget(
          new QLabel,static_cast<signed>(Index),COLUMN_LABEL);
      m_pUI->m_pItemGLayout->addWidget(
          new QLabel,static_cast<signed>(Index),COLUMN_DATA);
    }
  }
  /* Need to remove rows? */
  else if (Size<(unsigned)RowCount)
  {
    for(signed Index=static_cast<int>(RowCount)-1;
        Index>=static_cast<signed>(Size);Index--)
    {
      QLayoutItem *pItem;


      /* Remove 2nd to last item. */
      pItem=m_pUI->m_pItemGLayout->takeAt(2*Index);
      m_pUI->m_pItemGLayout->removeWidget(pItem->widget());
      delete pItem->widget();
      delete pItem;

      /* Remove last item. */
      pItem=m_pUI->m_pItemGLayout->takeAt(2*Index);
      m_pUI->m_pItemGLayout->removeWidget(pItem->widget());
      delete pItem->widget();
      delete pItem;
    }

    /* Allows the widget to be resized smaller (depending on contents). */
    m_pUI->m_pItemGLayout->invalidate();
  }


  /* Sanity check. */
  if (Size!=
      (unsigned)(m_pUI->m_pItemGLayout->count()/_COLUMN_COUNT))
  {
    MESSAGELOG_Error("Sanity check failed");
  }

  DEBUGLOG_LogOut();
  return;
}

PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGET_C(QWidget *pParent)
    : QWidget(pParent), m_pPrivate(new PANELDISPLAYWIDGETPRIVATE_C(this))
{
  DEBUGLOG_Printf1(
      "PANELDISPLAYWIDGET_C::PANELDISPLAYWIDGET_C(%p)",pParent);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PANELDISPLAYWIDGET_C::~PANELDISPLAYWIDGET_C(void)
{
  DEBUGLOG_Printf0(
      "PANELDISPLAYWIDGET_C::~PANELDISPLAYWIDGET_C()");
  DEBUGLOG_LogIn();

  delete m_pPrivate.take();

  DEBUGLOG_LogOut();
  return;
}

void PANELDISPLAYWIDGET_C::
    Update(PANELTOOLTIPITEM_C const *pItem,MOONDATA_T const &MoonData)
{
  DEBUGLOG_Printf2(
      "PANELDISPLAYWIDGET_C::Update(%p,%p)",pItem,&MoonData);
  DEBUGLOG_LogIn();

  if (pItem==nullptr)
  {
    MESSAGELOG_Error("pItem==nullptr");
  }
  else
  {
    m_pPrivate->Resize(1);
    m_pPrivate->RenderItem(0,pItem,MoonData);
  }

  DEBUGLOG_LogOut();
  return;
}

void PANELDISPLAYWIDGET_C::
    Update(PANELITEMLIST_C const *pItemList,MOONDATA_T const &MoonData)
{
  DEBUGLOG_Printf2(
      "PANELDISPLAYWIDGET_C::Update(%p,%p)",pItemList,&MoonData);
  DEBUGLOG_LogIn();

  if (pItemList==nullptr)
  {
    MESSAGELOG_Error("pItemList==nullptr");
  }
  else
  {
    m_pPrivate->Resize(static_cast<unsigned long>(pItemList->Size()));
    for(unsigned Index=0;Index<pItemList->Size();Index++)
      m_pPrivate->RenderItem(
          Index,(*pItemList)[Index],MoonData);
  }

  DEBUGLOG_LogOut();
  return;
}


#undef    PANELDISPLAYWIDGET_CPP


/**
*** paneldisplaywidget.cpp
**/

/**
*** \file updatecheck.h
*** \brief TODO
*** \details TODO
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(UPDATECHECK_H)
/**
*** \internal
*** \brief updatecheck.h identifier.
*** \details Identifier for updatecheck.h.
*** \endinternal
**/
#define   UPDATECHECK_H


/****
*****
***** INCLUDES
*****
****/

#include  <QThread>

#include  <QObject>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Update checking thread.
*** \details Thread used to check for updates. Implemented as the thread because
***   the update checking function is blocking.
**/
class CHECKTHREAD_C : public QThread
{
  Q_OBJECT

  friend void UpdateCheck(QObject *pReceiver,char const *pFunction);

  private:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent object.
    **/
    CHECKTHREAD_C(QObject *pParent=nullptr);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    CHECKTHREAD_C(CHECKTHREAD_C const &Thread)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~CHECKTHREAD_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    CHECKTHREAD_C const & operator=(CHECKTHREAD_C const &Thread)=delete;

  private:
    /**
    *** \brief Thread starting point.
    *** \details The starting point for the thread.
    **/
    void run(void) override;

  signals:
    /**
    *** \brief Version signal.
    *** \details Signal sent when the thread has completed.
    *** \param Version The version number string or an empty string on error.
    **/
    void VersionSignal(QString const &Version);
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Check for an update.
*** \details Checks for a program update and sends the version number string to
***   the given object/function.
*** \param pReceiver Object to receive the version number string.
*** \param pFunction Function to receive the version number string. It must
***   accept only QString as a parameter.
*** \note The QString will be the version number string or an empty string on
***   error.
**/
void UpdateCheck(QObject *pReceiver,char const *pFunction);


#endif    /* !defined(UPDATECHECK_H) */


/**
*** updatecheck.h
**/

/**
*** \file datetime.h
*** \brief Print date/time.
*** \details Converts a date/time to a string in accordance with various
***   options.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(DATETIME_H)
/**
*** \internal
*** \brief datetime.h identifier.
*** \details Identifier for datetime.h.
*** \endinternal
**/
#define   DATETIME_H


/****
*****
***** INCLUDES
*****
****/

#include  <string>
#include  <time.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief PREFERENCESMANAGER_C forward declaration.
*** \details Forward declaration of PREFERENCESMANAGER_C.
**/
class PREFERENCESMANAGER_C;

/**
*** \brief Date/time options.
*** \details What and how to print various options.
**/
class DATETIMEOPTIONS_C
{
  public:
    /**
    *** \brief Date/time flags.
    *** \details What and how to print various options.
    *** \todo Strip leading 0 - month, day and/or hour?
    **/
    typedef enum enumDATETIMEOPTIONS
    {
      DATETIMEOPTION_CLEARALL=0,
          /**< No options set. **/
      DATETIMEOPTION_24HOURFORMAT=1,
          /**< Print the hour in 24 hour format. **/
      DATETIMEOPTION_4DIGITYEAR=2,
          /**< Print the year using 4 digits instead of 2. **/
      DATETIMEOPTION_LONGDAYOFWEEKFORMAT=4,
          /**< Print the day of the week in a long format (not abbreviated). **/
      DATETIMEOPTION_LONGMONTHFORMAT=8,
          /**< Print the month in a long format (not abbreviated). **/
      DATETIMEOPTION_SHOWDAYOFWEEK=16,
          /**< Print the day of the week (abbreviated). **/
      DATETIMEOPTION_SHOWSECONDS=32,
          /**< Print the number of seconds. **/
      DATETIMEOPTION_SHOWTIMEZONE=64
          /**< Print the time zone. **/
    } DATETIMEOPTIONS_F;

    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    DATETIMEOPTIONS_C(void);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param Options Initial options.
    *** \param UseGMTFlag true - Use GMT for date/time,\n
    ***   false - Use local time zone for date/time.
    **/
    DATETIMEOPTIONS_C(DATETIMEOPTIONS_F const &Options,bool UseGMTFlag=false);

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~DATETIMEOPTIONS_C(void);

    /**
    *** \brief Equality operator.
    *** \details Compares two DATETIMEOPTIONS_C for equality.
    *** \param Options Value on right hand side of operator.
    *** \retval true Values are equivalent.
    *** \retval false Values are not equivalent.
    **/
    bool operator==(DATETIMEOPTIONS_C const &Options) const;

    /**
    *** \brief Inequality operator.
    *** \details Compares two DATETIMEOPTIONS_C for inequality.
    *** \param Options Value on right hand side of operator.
    *** \retval true Values are not equivalent.
    *** \retval false Values are equivalent.
    **/
    bool operator!=(DATETIMEOPTIONS_C const &Options) const;

    /**
    *** \brief Return 24 hour format flag.
    *** \details Returns the 24 hour format flag.
    *** \retval true Use 24 hour format.
    *** \retval false Use 12 hour (am/pm) format.
    **/
    bool Get24HourFormatFlag(void) const;

    /**
    *** \brief Return 4 digit year format flag.
    *** \details Returns the 4 digit year format flag.
    *** \retval true Use 4 digit year format.
    *** \retval false Use 2 digit year format.
    **/
    bool Get4DigitYearFlag(void) const;

    /**
    *** \brief Get date/time options.
    *** \details Return the date/time options as a DATETIMEOPTIONS_F type.
    *** \return Date/time options.
    **/
    DATETIMEOPTIONS_F GetDateTimeOptions(void) const;

    /**
    *** \brief Return long day of the week format flag.
    *** \details Returns the long day of the week format flag.
    *** \retval true Use long day of the week format (Sunday).
    *** \retval false Use short day of the week format (Sun.).
    **/
    bool GetLongDayOfWeekFormatFlag(void) const;

    /**
    *** \brief Return month format flag.
    *** \details Returns the month format flag.
    *** \retval true Use long month format (January).
    *** \retval false Use short month format (Jan.).
    **/
    bool GetLongMonthFormatFlag(void) const;

    /**
    *** \brief Return show the day of the week flag.
    *** \details Returns the show the day of the week flag.
    *** \retval true Show the day of the week.
    *** \retval false Don't show the day of the week.
    **/
    bool GetShowDayOfWeekFlag(void) const;

    /**
    *** \brief Return show the seconds flag.
    *** \details Returns the show the seconds flag.
    *** \retval true Show the seconds.
    *** \retval false Don't show the seconds.
    **/
    bool GetShowSecondsFlag(void) const;

    /**
    *** \brief Return show the time zone flag.
    *** \details Returns the show the time zone flag.
    *** \retval true Show the time zone.
    *** \retval false Don't show the time zone.
    **/
    bool GetShowTimeZoneFlag(void) const;

    /**
    *** \brief Return show the use GMT flag.
    *** \details Returns the show the use GMT (Greenwich Mean Time) flag.
    *** \retval true Use GMT for date/time.
    *** \retval false Use local time zone for date/time.
    **/
    bool GetUseGMTFlag(void) const;

    /**
    *** \brief Load preferences.
    *** \details Reads date/time preferences from the configuration file.
    *** \param pManager Pointer to preferences manager.
    **/
    void Load(PREFERENCESMANAGER_C *pManager);

    /**
    *** \brief Save preferences.
    *** \details Writes the date/time preferences to the configuration file.
    *** \param pManager Pointer to preferences manager.
    **/
    void Save(PREFERENCESMANAGER_C *pManager) const;

    /**
    *** \brief Set 24 hour format flag.
    *** \details Sets the 24 hour format flag.
    *** \param Flag true - Use 24 hour format,\n
    ***   false - Use 12 hour (am/pm) format.
    **/
    void Set24HourFormatFlag(bool Flag);

    /**
    *** \brief Set 4 digit year format flag.
    *** \details Sets the 4 digit year format flag.
    *** \param Flag true - Use 4 digit year format,\n
    ***   false - Use 2 digit year format.
    **/
    void Set4DigitYearFlag(bool Flag);

    /**
    *** \brief Set date/time options.
    *** \details Sets the date/time options using a DATETIMEOPTIONS_F type.
    *** \param Options Date/time options.
    **/
    void SetDateTimeOptions(DATETIMEOPTIONS_F const &Options);

    /**
    *** \brief Set long day of the week format flag.
    *** \details Sets the long day of the week format flag.
    *** \param Flag true - Use long day of the week format (Sunday),\n
    ***   false - Use short day of the week format (Sun.).
    **/
    void SetLongDayOfWeekFormatFlag(bool Flag);

    /**
    *** \brief Set long month format flag.
    *** \details Sets the long month format flag.
    *** \param Flag true - Use long month format (January),\n
    ***   false - Use short month format (Jan.).
    **/
    void SetLongMonthFormatFlag(bool Flag);

    /**
    *** \brief Set show the day of the week flag.
    *** \details Sets the show the day of the week flag.
    *** \param Flag true - Show the day of the week,\n
    ***   false - Don't show the day of the week.
    **/
    void SetShowDayOfWeekFlag(bool Flag);

    /**
    *** \brief Set show the seconds flag.
    *** \details Sets the show the seconds flag.
    *** \param Flag true - Show the seconds,\n
    ***   false - Don't show the seconds.
    **/
    void SetShowSecondsFlag(bool Flag);

    /**
    *** \brief Set show the time zone flag.
    *** \details Sets the show the time zone flag.
    *** \param Flag true - Show the time zone,\n
    ***   false - Don't show the time zone.
    **/
    void SetShowTimeZoneFlag(bool Flag);

    /**
    *** \brief Set show the use GMT flag.
    *** \details Sets the show the use GMT (Greenwich Mean Time) flag.
    *** \param Flag true - Use GMT for date/time,\n
    ***   false - Use local time zone for date/time.
    **/
    void SetUseGMTFlag(bool Flag);

  private:
    /**
    *** \brief Date/time options.
    *** \details Options for the date/time print functions.
    **/
    DATETIMEOPTIONS_F m_DateTimeOptions;

    /**
    *** \brief GMT or local time zone for date/time.
    *** \details Display date/time in GMT or local time zone.
    **/
    bool m_UseGMTFlag;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

/**
*** \brief Print date/time.
*** \details Converts a date/time to a string in accordance to various options.
*** \param DateTimeModeFlag \n
***   0 - Modify time format only\n
***   !0 - Modify date and time formats.
*** \param pTime Time/date to use (GMT time).
*** \param DTFormat Basic date format (day, month, year).
*** \param Options Options use to modify the format.
*** \returns Resulting text.
**/
std::string DateTime_Print(
    bool DateTimeModeFlag,struct tm const *pTime,
    std::string const &DTFormat,DATETIMEOPTIONS_C const &Options);

/**
*** \brief Date/time options bitwise NOT operator.
*** \details Performs a bitwise NOT on date/time options.
*** \param RHS Date/time options.
*** \return Bitwise NOT of input.
**/
DATETIMEOPTIONS_C::DATETIMEOPTIONS_F
    operator~(DATETIMEOPTIONS_C::DATETIMEOPTIONS_F RHS);

/**
*** \brief Date/time options bitwise AND operator.
*** \details Performs a bitwise AND on two date/time options.
*** \param LHS First set of date/time options.
*** \param RHS Second set of date/time options.
*** \return Bitwise AND of inputs.
**/
DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator&(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS);

/**
*** \brief Date/time options bitwise AND assignment operator.
*** \details Performs a bitwise AND assignment on two date/time options.
*** \param LHS First set of date/time options.
*** \param RHS Second set of date/time options.
*** \return Bitwise AND of inputs.
*** \return LHS contains bitwise AND of inputs.
**/
DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator&=(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS);

/**
*** \brief Date/time options bitwise OR operator.
*** \details Performs a bitwise OR on two date/time options.
*** \param LHS First set of date/time options.
*** \param RHS Second set of date/time options.
*** \return Bitwise OR of inputs.
**/
DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator|(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS);

/**
*** \brief Date/time options bitwise OR assignment operator.
*** \details Performs a bitwise OR assignment on two date/time options.
*** \param LHS First set of date/time options.
*** \param RHS Second set of date/time options.
*** \return Bitwise OR of inputs.
*** \return LHS contains bitwise OR of inputs.
**/
DATETIMEOPTIONS_C::DATETIMEOPTIONS_F operator|=(
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F &LHS,
    DATETIMEOPTIONS_C::DATETIMEOPTIONS_F const &RHS);


#endif    /* !defined(DATETIME_H) */


/**
*** datetime.h
**/

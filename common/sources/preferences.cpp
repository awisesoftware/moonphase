/**
*** \file preferences.cpp
*** \brief preferences.cpp implementation.
*** \details Implementation file for preferences.cpp.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief preferences.cpp identifier.
*** \details Identifier for preferences.cpp.
*** \endinternal
**/
#define   PREFERENCES_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"
#if       defined(DEBUG_PREFERENCES_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_PREFERENCES_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "paneltooltipitem.h"

#include  <limits>
#include  <math.h>
#include  <stdexcept>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/

POSITION_C const PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidPosition=
    POSITION_C(-32767,-32767);
SIZE_C const PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidSize=
    SIZE_C(-32767,-32767);

#if       defined(APPLICATION_TEST)
char const *PREFERENCESIO_C::mc_pTestINIFilename="unittest.ini";
#endif    /* defined(APPLICATION_TEST) */

static char const *fc_pDefaultAnimationPathname="";
static COLOR_C const fc_DefaultTrayIconBackgroundColor=COLOR_C(0,0,0,255/255.0);
static bool const fc_DefaultConfirmDiscardFlag=true;
static bool const fc_DefaultConfirmQuitFlag=true;
static bool const fc_DefaultRemindOncePerSessionFlag=false;
static bool const fc_DefaultStillRunningReminderFlag=true;
static bool const fc_DefaultUnsavedChangesReminderFlag=true;
static bool const fc_DefaultUseOpaqueBackgroundFlag=false;

static bool const fc_DefaultDefaultToGMTFlag=false;
static bool const fc_DefaultDefaultToMetricUnitsFlag=true;
static double const fc_DefaultLatitude=0.0;
static double const fc_DefaultLongitude=0.0;

static bool const fc_DefaultAlwaysOnTopFlag=false;
static bool const fc_DefaultDisplayOnStartUpFlag=false;
static bool const fc_DefaultHideCloseButtonFlag=false;
static bool const fc_DefaultKeepOnScreenFlag=false;
static POSITION_C const fc_DefaultPosition=
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidPosition;
static SIZE_C const fc_DefaultSize=
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::mc_InvalidSize;

static char const *fc_pApplicationGroupKey="ApplicationPreferences";
/**
*** \brief Animation pathname key.
*** \details Key to access animation pathname in configuration file.
**/
static char const *fc_pAGAnimationPathnameKey="AnimationPathname";
/**
*** \brief Background color key.
*** \details Key to access background color in configuration file.
**/
static char const *fc_pAGBackgroundColor="BackgroundColor";
/**
*** \brief Confirm discard changes flag key.
*** \details Key to access confirm discard changes flag in configuration
***   file.
**/
static char const *fc_pAGConfirmDiscardChangesKey="ConfirmDiscardChanges";
/**
*** \brief Confirm quit flag key.
*** \details Key to access confirm quit flag in configuration file.
**/
static char const *fc_pAGConfirmQuitFlagKey="ConfirmQuit";
/**
*** \brief Remind once per session flag key.
*** \details Key to access remind once per session flag in configuration
***   file.
**/
static char const *fc_pAGRemindOncePerSessionKey="RemindOncePerSession";
/**
*** \brief Still running reminder flag key.
*** \details Key to access still running reminder flag in configuration
***   file.
**/
static char const *fc_pAGStillRunningReminderKey="StillRunningReminder";
/**
*** \brief Update interval key.
*** \details Key to access update interval in configuration file.
**/
static char const *fc_pAGUnsavedChangesReminderKey="UnsavedChangesReminder";
/**
*** \brief Use opaque background flag key.
*** \details Key to access use opaque background flag in configuration
***   file.
**/
static char const *fc_pAGUseOpaqueBackgroundKey="UseOpaqueBackground";

static char const *fc_pMoonDataOptionsGroupKey="MoonDataOptions";

/** \todo
*** \brief Default to metric units key.
*** \details Key to access default to metric flag in configuration
***   file.
**/
static char const *fc_pMDODefaultToGMTKey="DefaultToGMT";

/**
*** \brief Default to metric units key.
*** \details Key to access default to metric flag in configuration
***   file.
**/
static char const *fc_pMDODefaultToMetricUnitsKey="DefaultToMetricUnits";

/**
*** \brief Latitude key.
*** \details Key to access latitude in configuration file.
**/
static char const *fc_pMDOLatitudeKey="Latitude";

/**
*** \brief Longitude key.
*** \details Key to access Longitude in configuration file.
**/
static char const *fc_pMDOLongitude="Longitude";

static char const *fc_pPanelDisplayGroupKey="PanelDisplayPreferences";
static char const *fc_pPDGAlwaysOnTopFlagKey="AlwaysOnTop";
static char const *fc_pPDGDisplayOnStartUpFlagKey="DisplayOnStartUp";
static char const *fc_pPDGKeepOnScreenFlag="KeepOnScreen";
static char const *fc_pPDGHideCloseButtonFlagKey="HideCloseButton";
static char const *fc_pPDGPositionKey="Position";
static char const *fc_pPDGSizeKey="Size";



/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/* TODO Move? */
bool CloseEnough(double D0,double D1)
{
  bool Return;
  double Abs0=fabs(D0);
  double Abs1=fabs(D1);
  double Diff=fabs(D0-D1);

  // Shortcut, handles infinities.
  if (D0==D1)
    Return=true;
  // D0 or D1 is zero or both are extremely close to it.
  // Relative error is less meaningful here.
  else if ((D0==0)||(D1==0)||(Diff<std::numeric_limits<double>::min()))  /* D0,D1 needed? */
    Return=(Diff<(std::numeric_limits<double>::epsilon()*std::numeric_limits<double>::min()));
  // Use relative error.
  else
    Return=Diff/fmin((Abs0+Abs1),std::numeric_limits<double>::max())<std::numeric_limits<double>::epsilon();

  return(Return);
}

PREFERENCES_C::PREFERENCES_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::PREFERENCES_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::~PREFERENCES_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::~PREFERENCES_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C *
    PREFERENCES_C::GetApplicationPreferencesPointer(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::GetApplicationPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_ApplicationPreferences);
}

PREFERENCES_C::MOONDATA_C * PREFERENCES_C::GetMoonDataPreferencesPointer()
{
  DEBUGLOG_Printf0("PREFERENCES_C::GetMoonDataPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_MoonDataPreferences);
}

void PREFERENCES_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::Load()");
  DEBUGLOG_LogIn();

  m_ApplicationPreferences.Load();
  m_MoonDataPreferences.Load();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::Save()");
  DEBUGLOG_LogIn();

  throw(std::runtime_error("PREFERENCES_C::Save() should never be called."));

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C::APPLICATION_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::APPLICATION_C::APPLICATION_C()");
  DEBUGLOG_LogIn();

  m_AnimationPathname=fc_pDefaultAnimationPathname;
  m_BackgroundColor=fc_DefaultTrayIconBackgroundColor;
  m_ConfirmDiscardFlag=fc_DefaultConfirmDiscardFlag;
  m_ConfirmQuitFlag=fc_DefaultConfirmQuitFlag;
  m_RemindOncePerSessionFlag=fc_DefaultRemindOncePerSessionFlag;
  m_StillRunningReminderFlag=fc_DefaultStillRunningReminderFlag;
  m_UnsavedChangesReminderFlag=fc_DefaultUnsavedChangesReminderFlag;
  m_UseOpaqueBackgroundFlag=fc_DefaultUseOpaqueBackgroundFlag;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C::APPLICATION_C(
    PREFERENCES_C::APPLICATION_C const &Preferences)
    : PREFERENCES_C::APPLICATION_C()
{
  DEBUGLOG_Printf1("PREFERENCES_C::"
      "APPLICATION_C::APPLICATION_C(PREFERENCES_C::APPLICATION_C const &:%p)",
      &Preferences);
  DEBUGLOG_LogIn();

  m_AnimationPathname=Preferences.m_AnimationPathname;
  m_BackgroundColor=Preferences.m_BackgroundColor;
  m_ConfirmDiscardFlag=Preferences.m_ConfirmDiscardFlag;
  m_ConfirmQuitFlag=Preferences.m_ConfirmQuitFlag;
  m_RemindOncePerSessionFlag=Preferences.m_RemindOncePerSessionFlag;
  m_StillRunningReminderFlag=Preferences.m_StillRunningReminderFlag;
  m_UnsavedChangesReminderFlag=Preferences.m_UnsavedChangesReminderFlag;
  m_UseOpaqueBackgroundFlag=Preferences.m_UseOpaqueBackgroundFlag;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C::~APPLICATION_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::~APPLICATION_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::APPLICATION_C const & PREFERENCES_C::
    APPLICATION_C::operator=(PREFERENCES_C::APPLICATION_C const &Preferences)
{
  DEBUGLOG_Printf1("PREFERENCES_C::APPLICATION_C::"
      "operator=(PREFERENCES_C::APPLICATION_C const &:%p)",&Preferences);
  DEBUGLOG_LogIn();

  if (this!=&Preferences)
  {
    m_AnimationPathname=Preferences.m_AnimationPathname;
    m_BackgroundColor=Preferences.m_BackgroundColor;
    m_ConfirmDiscardFlag=Preferences.m_ConfirmDiscardFlag;
    m_ConfirmQuitFlag=Preferences.m_ConfirmQuitFlag;
    m_RemindOncePerSessionFlag=Preferences.m_RemindOncePerSessionFlag;
    m_StillRunningReminderFlag=Preferences.m_StillRunningReminderFlag;
    m_UnsavedChangesReminderFlag=Preferences.m_UnsavedChangesReminderFlag;
    m_UseOpaqueBackgroundFlag=Preferences.m_UseOpaqueBackgroundFlag;
  }

  DEBUGLOG_LogOut();
  return(*this);
}

bool PREFERENCES_C::APPLICATION_C::operator==(
    PREFERENCES_C::APPLICATION_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::APPLICATION_C::"
      "operator==(PREFERENCES_C::APPLICATION_C const &:%p) const",&Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=
      (m_AnimationPathname==Preferences.m_AnimationPathname) &&
      (m_ConfirmDiscardFlag==Preferences.m_ConfirmDiscardFlag) &&
      (m_ConfirmQuitFlag==Preferences.m_ConfirmQuitFlag) &&
      (m_StillRunningReminderFlag==Preferences.m_StillRunningReminderFlag) &&
      (m_UnsavedChangesReminderFlag==Preferences.m_UnsavedChangesReminderFlag) &&
      (m_UseOpaqueBackgroundFlag==Preferences.m_UseOpaqueBackgroundFlag);
  /* If both StillRunningReminderFlag variables are set, compare the
      compare the RemindOncePerSessionFlag. */
  if ( (m_StillRunningReminderFlag==Preferences.m_StillRunningReminderFlag) &&
      m_StillRunningReminderFlag)
    EqualFlag=EqualFlag &&
        (m_RemindOncePerSessionFlag==Preferences.m_RemindOncePerSessionFlag);
  /* If both UseOpaqueBackgroundFlag variables are set, compare the
      compare the BackgroundColor. */
  if ( (m_UseOpaqueBackgroundFlag==Preferences.m_UseOpaqueBackgroundFlag) &&
      m_UseOpaqueBackgroundFlag)
    EqualFlag=EqualFlag && (m_BackgroundColor==Preferences.m_BackgroundColor);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::APPLICATION_C::operator!=(
    PREFERENCES_C::APPLICATION_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::APPLICATION_C::"
      "operator!=(PREFERENCES_C::APPLICATION_C const:%p) const",&Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*this)==Preferences;

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

std::string PREFERENCES_C::APPLICATION_C::GetAnimationPathname(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetAnimationPathname() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_AnimationPathname);
}

COLOR_C PREFERENCES_C::APPLICATION_C::GetBackgroundColor(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::APPLICATION_C::GetBackgroundColor() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_BackgroundColor);
}

bool PREFERENCES_C::APPLICATION_C::GetConfirmDiscardFlag(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetConfirmDiscardFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_ConfirmDiscardFlag);
}

bool PREFERENCES_C::APPLICATION_C::GetConfirmQuitFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::APPLICATION_C::GetConfirmQuitFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_ConfirmQuitFlag);
}

bool PREFERENCES_C::APPLICATION_C::GetRemindOncePerSessionFlag(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetRemindOncePerSessionFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_RemindOncePerSessionFlag);
}

bool PREFERENCES_C::APPLICATION_C::GetStillRunningReminderFlag(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetStillRunningReminderFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_StillRunningReminderFlag);
}

bool PREFERENCES_C::APPLICATION_C::GetUnsavedChangesReminderFlag(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetUnsavedChangesReminderFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UnsavedChangesReminderFlag);
}

bool PREFERENCES_C::APPLICATION_C::GetUseOpaqueBackgroundFlag(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::APPLICATION_C::GetUseOpaqueBackgroundFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_UseOpaqueBackgroundFlag);
}

void PREFERENCES_C::APPLICATION_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::APPLICATION_C::Load()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pApplicationGroupKey);
  m_AnimationPathname=
      GetPreference(fc_pAGAnimationPathnameKey,m_AnimationPathname);
  m_BackgroundColor=
      GetPreference(fc_pAGBackgroundColor,m_BackgroundColor);
  m_ConfirmDiscardFlag=
      GetPreference(fc_pAGConfirmDiscardChangesKey,m_ConfirmDiscardFlag);
  m_ConfirmQuitFlag=
      GetPreference(fc_pAGConfirmQuitFlagKey,m_ConfirmQuitFlag);
  m_RemindOncePerSessionFlag=
      GetPreference(fc_pAGRemindOncePerSessionKey,m_RemindOncePerSessionFlag);
  m_StillRunningReminderFlag=
      GetPreference(fc_pAGStillRunningReminderKey,m_StillRunningReminderFlag);
  m_UnsavedChangesReminderFlag=GetPreference(
      fc_pAGUnsavedChangesReminderKey,m_UnsavedChangesReminderFlag);
  m_UseOpaqueBackgroundFlag=
      GetPreference(fc_pAGUseOpaqueBackgroundKey,m_UseOpaqueBackgroundFlag);
  EndGroup();

  DEBUGLOG_LogOut();
  return;
}
void PREFERENCES_C::APPLICATION_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::APPLICATION_C::Save()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pApplicationGroupKey);
  SetPreference(fc_pAGAnimationPathnameKey,m_AnimationPathname);
  SetPreference(fc_pAGBackgroundColor,m_BackgroundColor);
  SetPreference(fc_pAGConfirmDiscardChangesKey,m_ConfirmDiscardFlag);
  SetPreference(fc_pAGConfirmQuitFlagKey,m_ConfirmQuitFlag);
  SetPreference(fc_pAGRemindOncePerSessionKey,m_RemindOncePerSessionFlag);
  SetPreference(fc_pAGStillRunningReminderKey,m_StillRunningReminderFlag);
  SetPreference(fc_pAGUnsavedChangesReminderKey,m_UnsavedChangesReminderFlag);
  SetPreference(fc_pAGUseOpaqueBackgroundKey,m_UseOpaqueBackgroundFlag);
  EndGroup();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::
    APPLICATION_C::SetAnimationPathname(std::string const &Pathname)
{
  DEBUGLOG_Printf2(
      "PREFERENCES_C::APPLICATION_C::SetAnimationPathname(std::string &:%p(%s))",
      &Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  m_AnimationPathname=Pathname;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::
    APPLICATION_C::SetBackgroundColor(COLOR_C const &BackgroundColor)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetBackgroundColor(COLOR_C const &:%p)",
      &BackgroundColor);
  DEBUGLOG_LogIn();

  m_BackgroundColor=BackgroundColor;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::
    APPLICATION_C::SetConfirmDiscardFlag(bool ConfirmDiscardFlag)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetConfirmDiscardFlag(bool:%u)",
      ConfirmDiscardFlag);
  DEBUGLOG_LogIn();

  m_ConfirmDiscardFlag=ConfirmDiscardFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::APPLICATION_C::SetConfirmQuitFlag(bool ConfirmQuitFlag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::APPLICATION_C::SetConfirmQuitFlag(bool:%u)",
      ConfirmQuitFlag);
  DEBUGLOG_LogIn();

  m_ConfirmQuitFlag=ConfirmQuitFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::APPLICATION_C::SetRemindOncePerSessionFlag(bool OnceFlag)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetRemindOncePerSessionFlag(bool:%u)",
      OnceFlag);
  DEBUGLOG_LogIn();

  m_RemindOncePerSessionFlag=OnceFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::
    APPLICATION_C::SetStillRunningReminderFlag(bool ReminderFlag)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetStillRunningReminderFlag(bool:%u)",
      ReminderFlag);
  DEBUGLOG_LogIn();

  m_StillRunningReminderFlag=ReminderFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::
    APPLICATION_C::SetUnsavedChangesReminderFlag(bool ReminderFlag)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetUnsavedChangesReminderFlag(bool:%u)",
      ReminderFlag);
  DEBUGLOG_LogIn();

  m_UnsavedChangesReminderFlag=ReminderFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::APPLICATION_C::SetUseOpaqueBackgroundFlag(bool UseFlag)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::APPLICATION_C::SetUseOpaqueBackgroundFlag(bool:%u)",
      UseFlag);
  DEBUGLOG_LogIn();

  m_UseOpaqueBackgroundFlag=UseFlag;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::MOONDATA_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::MOONDATA_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::MOONDATA_C(MOONDATA_C const &Preferences) : PREFERENCES_C::MOONDATA_C()
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::"
      "MOONDATA_C(PREFERENCES_C::MOONDATA_C const &:%p)",&Preferences);
  DEBUGLOG_LogIn();

  m_Options=Preferences.m_Options;
  m_PanelPreferences=Preferences.m_PanelPreferences;
  m_ToolTipPreferences=Preferences.m_ToolTipPreferences;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::~MOONDATA_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::~MOONDATA_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C const & PREFERENCES_C::
    MOONDATA_C::operator=(PREFERENCES_C::MOONDATA_C const &Preferences)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::"
      "operator=(PREFERENCES_C::MOONDATA_C const &:%p)",&Preferences);
  DEBUGLOG_LogIn();

  if (this!=&Preferences)
  {
    m_Options=Preferences.m_Options;
    m_PanelPreferences=Preferences.m_PanelPreferences;
    m_ToolTipPreferences=Preferences.m_ToolTipPreferences;
  }

  DEBUGLOG_LogOut();
  return(*this);
}

bool PREFERENCES_C::MOONDATA_C::operator==(MOONDATA_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::operator==(MOONDATA_C:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(m_Options==Preferences.m_Options) &&
      (m_PanelPreferences==Preferences.m_PanelPreferences) &&
      (m_ToolTipPreferences==Preferences.m_ToolTipPreferences);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::operator!=(MOONDATA_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::operator!=(MOONDATA_C:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Preferences);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C *
    PREFERENCES_C::MOONDATA_C::GetOptionsPointer(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::GetOptionsPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_Options);
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C const *
    PREFERENCES_C::MOONDATA_C::GetOptionsPointer(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::GetOptionsPointer() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_Options);
}

PREFERENCES_C::MOONDATA_C::PANEL_C *
    PREFERENCES_C::MOONDATA_C::GetPanelPreferencesPointer(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::GetPanelPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_PanelPreferences);
}

PREFERENCES_C::MOONDATA_C::PANEL_C const *
    PREFERENCES_C::MOONDATA_C::GetPanelPreferencesPointer(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::GetPanelPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_PanelPreferences);
}

PREFERENCES_C::MOONDATA_C::TOOLTIP_C *
    PREFERENCES_C::MOONDATA_C::GetToolTipPreferencesPointer(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::GetToolTipPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_ToolTipPreferences);
}

PREFERENCES_C::MOONDATA_C::TOOLTIP_C const *
    PREFERENCES_C::MOONDATA_C::GetToolTipPreferencesPointer(void) const
{
  DEBUGLOG_Printf0("GetToolTipPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_ToolTipPreferences);
}


void PREFERENCES_C::MOONDATA_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::Load()");
  DEBUGLOG_LogIn();

  m_Options.Load();
  m_PanelPreferences.Load();
  m_ToolTipPreferences.Load();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::Save()");
  DEBUGLOG_LogIn();

  m_Options.Save();
  m_PanelPreferences.Save();
  m_ToolTipPreferences.Save();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C::OPTIONS_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::OPTIONS_C()");
  DEBUGLOG_LogIn();

  m_DefaultToGMTFlag=fc_DefaultDefaultToGMTFlag;
  m_DefaultToMetricUnitsFlag=fc_DefaultDefaultToMetricUnitsFlag;
  m_Latitude=fc_DefaultLatitude;
  m_Longitude=fc_DefaultLongitude;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C::OPTIONS_C(OPTIONS_C const &Options)
    : PREFERENCES_C::MOONDATA_C::OPTIONS_C()
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "OPTIONS_C(PREFERENCES_C::MOONDATA_C::OPTIONS_C const &:%p)",&Options);
  DEBUGLOG_LogIn();

  m_DefaultToGMTFlag=Options.m_DefaultToGMTFlag;
  m_DefaultToMetricUnitsFlag=Options.m_DefaultToMetricUnitsFlag;
  m_Latitude=Options.m_Latitude;
  m_Longitude=Options.m_Longitude;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C::~OPTIONS_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::~OPTIONS_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::OPTIONS_C const & PREFERENCES_C::MOONDATA_C::OPTIONS_C::
    operator=(PREFERENCES_C::MOONDATA_C::OPTIONS_C const &Options)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "operator=(PREFERENCES_C::MOONDATA_C::OPTIONS_C const &:%p)",&Options);
  DEBUGLOG_LogIn();

  if (this!=&Options)
  {
    m_DefaultToGMTFlag=Options.m_DefaultToGMTFlag;
    m_DefaultToMetricUnitsFlag=Options.m_DefaultToMetricUnitsFlag;
    m_Latitude=Options.m_Latitude;
    m_Longitude=Options.m_Longitude;
  }

  DEBUGLOG_LogOut();
  return(*this);
}

bool PREFERENCES_C::MOONDATA_C::OPTIONS_C::
    operator==(PREFERENCES_C::MOONDATA_C::OPTIONS_C const &Options) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "operator==(PREFERENCES_C::MOONDATA_C::OPTIONS_C const:%p) const",
      &Options);
  DEBUGLOG_LogIn();

  EqualFlag=
      (m_DefaultToGMTFlag==Options.m_DefaultToGMTFlag) &&
      (m_DefaultToMetricUnitsFlag==Options.m_DefaultToMetricUnitsFlag) &&
      CloseEnough(m_Latitude,Options.m_Latitude) &&
      CloseEnough(m_Longitude,Options.m_Longitude);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::
    OPTIONS_C::operator!=(OPTIONS_C const &Options) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "operator!=(PREFERENCES_C::MOONDATA_C::OPTIONS_C const:%p) const",
      &Options);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Options);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetDefaultToGMTFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "GetDefaultToGMTFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_DefaultToGMTFlag);
}

bool PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetDefaultToMetricUnitsFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "GetDefaultToMetricUnitsFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_DefaultToMetricUnitsFlag);
}

double PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetLatitude(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetLatitude() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Latitude);
}

double PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetLongitude(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::GetLongitude() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Longitude);
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::Load()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pMoonDataOptionsGroupKey);
  m_DefaultToGMTFlag=
      GetPreference(fc_pMDODefaultToGMTKey,m_DefaultToGMTFlag);
  m_DefaultToMetricUnitsFlag=
      GetPreference(fc_pMDODefaultToMetricUnitsKey,m_DefaultToMetricUnitsFlag);
  m_Latitude=GetPreference(fc_pMDOLatitudeKey,m_Latitude);
  m_Longitude=GetPreference(fc_pMDOLongitude,m_Longitude);
  EndGroup();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::OPTIONS_C::Save()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pMoonDataOptionsGroupKey);
  SetPreference(fc_pMDODefaultToGMTKey,m_DefaultToGMTFlag);
  SetPreference(fc_pMDODefaultToMetricUnitsKey,m_DefaultToMetricUnitsFlag);
  SetPreference(fc_pMDOLatitudeKey,m_Latitude);
  SetPreference(fc_pMDOLongitude,m_Longitude);
  EndGroup();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::
    SetDefaultToGMTFlag(bool DefaultToGMTFlag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "SetDefaultToGMTFlag(bool:%u)",DefaultToGMTFlag);
  DEBUGLOG_LogIn();

  m_DefaultToGMTFlag=DefaultToGMTFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::
    SetDefaultToMetricUnitsFlag(bool DefaultToMetricUnitsFlag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::OPTIONS_C::"
      "SetDefaultToMetricUnitsFlag(bool:%u)",DefaultToMetricUnitsFlag);
  DEBUGLOG_LogIn();

  m_DefaultToMetricUnitsFlag=DefaultToMetricUnitsFlag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::SetLatitude(double Latitude)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::OPTIONS_C::SetLatitude(double:%u)",Latitude);
  DEBUGLOG_LogIn();

  m_Latitude=Latitude;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::OPTIONS_C::SetLongitude(double Longitude)
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::OPTIONS_C::SetLongitude(double:%u)",Longitude);
  DEBUGLOG_LogIn();

  m_Longitude=Longitude;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::PANELTOOLTIP_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::PANELTOOLTIP_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::~PANELTOOLTIP_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::~PANELTOOLTIP_C()");
  DEBUGLOG_LogIn();

  delete m_pItemList.release();

  DEBUGLOG_LogOut();
  return;
}

PANELTOOLTIPITEMLIST_C *
    PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::GetItemListPointer(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::GetItemListPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pItemList.get());
}

PANELTOOLTIPITEMLIST_C const *
    PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::GetItemListPointer(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C::GetItemListPointer() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_pItemList.get());
}

PREFERENCES_C::MOONDATA_C::PANEL_C::PANEL_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::PANEL_C()");
  DEBUGLOG_LogIn();

  m_pItemList.reset(new PANELITEMLIST_C);

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::~PANEL_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::~PANEL_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C const &
    PREFERENCES_C::MOONDATA_C::PANEL_C::operator=(
    PREFERENCES_C::MOONDATA_C::PANEL_C const &Preferences)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::operator=("
      "PREFERENCES_C::MOONDATA_C::PANEL_C const &)");
  DEBUGLOG_LogIn();

  if (this!=&Preferences)
  {
    PANELITEMLIST_C *pNewList;


    pNewList=new PANELITEMLIST_C(*Preferences.GetItemListPointer());
    delete m_pItemList.release();
    m_pItemList.reset(pNewList);

    m_DialogData=Preferences.m_DialogData;
    m_DisplayPreferences=Preferences.m_DisplayPreferences;
  }

  DEBUGLOG_LogOut();
  return(*this);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    operator==(PREFERENCES_C::MOONDATA_C::PANEL_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "operator==(PREFERENCES_C::MOONDATA_C::PANEL_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  /* Ignore m_DialogData. */

  EqualFlag=( (m_DisplayPreferences==Preferences.m_DisplayPreferences) &&
      (*m_pItemList==*Preferences.m_pItemList) );

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    operator!=(PREFERENCES_C::MOONDATA_C::PANEL_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "operator!=(PREFERENCES_C::MOONDATA_C::PANEL_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Preferences);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C *
    PREFERENCES_C::MOONDATA_C::PANEL_C::GetDialogDataPointer(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::GetDialogDataPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_DialogData);
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C *
    PREFERENCES_C::MOONDATA_C::PANEL_C::GetDisplayPreferencesPointer(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::GetDisplayPreferencesPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_DisplayPreferences);
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const *
    PREFERENCES_C::MOONDATA_C::PANEL_C::GetDisplayPreferencesPointer(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::"
      "PANEL_C::GetDisplayPreferencesPointer() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(&m_DisplayPreferences);
}

PANELITEMLIST_C *
    PREFERENCES_C::MOONDATA_C::PANEL_C::GetItemListPointer(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::GetItemListPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(dynamic_cast<PANELITEMLIST_C *>(m_pItemList.get()));
}

PANELITEMLIST_C const *
    PREFERENCES_C::MOONDATA_C::PANEL_C::GetItemListPointer(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::"
      "PANEL_C::GetItemListPointer() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(dynamic_cast<PANELITEMLIST_C const *>(m_pItemList.get()));
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::Load()");
  DEBUGLOG_LogIn();

  m_pItemList->Load();
  m_DisplayPreferences.Load();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::Save()");
  DEBUGLOG_LogIn();

  m_pItemList->Save();
  m_DisplayPreferences.Save();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::DIALOGDATA_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::DIALOGDATA_C()");
  DEBUGLOG_LogIn();

  m_VisibleFlag=false;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::DIALOGDATA_C(
    bool VisibleFlag,POSITION_C const &Position,SIZE_C const &Size)
{
  DEBUGLOG_Printf3("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DIALOGDATA_C::DIALOGDATA_C(bool:%u,POSITION_C const &:%p,SIZE_C const &:%p)",
      VisibleFlag,&Position,&Size);
  DEBUGLOG_LogIn();

  m_VisibleFlag=VisibleFlag;
  m_Position=Position;
  m_Size=Size;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::~DIALOGDATA_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::~DIALOGDATA_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

POSITION_C
  PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::GetPosition(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::GetPosition() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Position);
}

SIZE_C PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::GetSize(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DIALOGDATA_C::GetSize() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Size);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    DIALOGDATA_C::GetVisibleFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DIALOGDATA_C::GetVisibleFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_VisibleFlag);
}



PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::DISPLAY_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::DISPLAY_C()");
  DEBUGLOG_LogIn();

  m_AlwaysOnTopFlag=false;
  m_DisplayOnStartUpFlag=false;
  m_HideCloseButtonFlag=false;
  m_KeepOnScreenFlag=false;
  m_Position=mc_InvalidPosition;
  m_Size=mc_InvalidSize;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::DISPLAY_C(
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &Preferences)
    : PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C()
{
  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::DISPLAY_C("
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &:%p)",
      &Preferences);
  DEBUGLOG_LogIn();

  m_AlwaysOnTopFlag=Preferences.m_AlwaysOnTopFlag;
  m_DisplayOnStartUpFlag=Preferences.m_DisplayOnStartUpFlag;
  m_HideCloseButtonFlag=Preferences.m_HideCloseButtonFlag;
  m_KeepOnScreenFlag=Preferences.m_KeepOnScreenFlag;
  m_Position=Preferences.m_Position;
  m_Size=Preferences.m_Size;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::~DISPLAY_C(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::~DISPLAY_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::operator=(
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &Preferences)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::operator=("
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &:%p)",&Preferences);
  DEBUGLOG_LogIn();

  if (this!=&Preferences)
  {
    m_AlwaysOnTopFlag=Preferences.m_AlwaysOnTopFlag;
    m_DisplayOnStartUpFlag=Preferences.m_DisplayOnStartUpFlag;
    m_HideCloseButtonFlag=Preferences.m_HideCloseButtonFlag;
    m_KeepOnScreenFlag=
        Preferences.m_KeepOnScreenFlag;
    m_Position=Preferences.m_Position;
    m_Size=Preferences.m_Size;
  }

  DEBUGLOG_LogOut();
  return(*this);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::operator==(
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::operator==("
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(m_AlwaysOnTopFlag==Preferences.m_AlwaysOnTopFlag) &&
      (m_DisplayOnStartUpFlag==Preferences.m_DisplayOnStartUpFlag) &&
      (m_HideCloseButtonFlag==Preferences.m_HideCloseButtonFlag) &&
      (m_KeepOnScreenFlag==Preferences.m_KeepOnScreenFlag) &&
      (m_Position==Preferences.m_Position) &&
      (m_Size==Preferences.m_Size);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::operator!=(
    PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::DISPLAY_C("
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Preferences);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::GetAlwaysOnTopFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::GetAlwaysOnTopFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_AlwaysOnTopFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::GetDisplayOnStartUpFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::GetDisplayOnStartUpFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_DisplayOnStartUpFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::GetKeepOnScreenFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::GetKeepOnScreenFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_KeepOnScreenFlag);
}

bool PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::GetHideCloseButtonFlag(void) const
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::GetHideCloseButtonFlag() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_HideCloseButtonFlag);
}

POSITION_C PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::GetPosition(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::GetPosition() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Position);
}

SIZE_C PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::GetSize(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::GetSize() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(m_Size);
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::Load()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pPanelDisplayGroupKey);
  m_AlwaysOnTopFlag=GetPreference(fc_pPDGAlwaysOnTopFlagKey,m_AlwaysOnTopFlag);
  m_DisplayOnStartUpFlag=
      GetPreference(fc_pPDGDisplayOnStartUpFlagKey,m_DisplayOnStartUpFlag);
  m_HideCloseButtonFlag=
      GetPreference(fc_pPDGHideCloseButtonFlagKey,m_HideCloseButtonFlag);
  m_KeepOnScreenFlag=GetPreference(fc_pPDGKeepOnScreenFlag,
      m_KeepOnScreenFlag);
  m_Position=GetPreference(fc_pPDGPositionKey,m_Position);
  m_Size=GetPreference(fc_pPDGSizeKey,m_Size);

  EndGroup();
  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::Save()");
  DEBUGLOG_LogIn();

  BeginGroup(fc_pPanelDisplayGroupKey);
  SetPreference(fc_pPDGAlwaysOnTopFlagKey,m_AlwaysOnTopFlag);
  SetPreference(fc_pPDGDisplayOnStartUpFlagKey,m_DisplayOnStartUpFlag);
  SetPreference(fc_pPDGHideCloseButtonFlagKey,m_HideCloseButtonFlag);
  SetPreference(fc_pPDGKeepOnScreenFlag,m_KeepOnScreenFlag);
  SetPreference(fc_pPDGPositionKey,m_Position);
  SetPreference(fc_pPDGSizeKey,m_Size);
  EndGroup();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::SetAlwaysOnTopFlag(bool Flag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetAlwaysOnTopFlag(bool:%u)",Flag);
  DEBUGLOG_LogIn();

  m_AlwaysOnTopFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::SetDisplayOnStartUpFlag(bool Flag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetDisplayOnStartUpFlag(bool:%u)",Flag);
  DEBUGLOG_LogIn();

  m_DisplayOnStartUpFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::SetKeepOnScreenFlag(bool Flag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetKeepOnScreenFlag(bool:%u)",Flag);
  DEBUGLOG_LogIn();

  m_KeepOnScreenFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::SetHideCloseButtonFlag(bool Flag)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetHideCloseButtonFlag(bool:%u)",Flag);
  DEBUGLOG_LogIn();

  m_HideCloseButtonFlag=Flag;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::
    DISPLAY_C::SetPosition(POSITION_C const &Position)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetPosition(POSITION_C const &:%p)",&Position);
  DEBUGLOG_LogIn();

  m_Position=Position;

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::PANEL_C::DISPLAY_C::SetSize(SIZE_C const &Size)
{
  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::PANEL_C::"
      "DISPLAY_C::SetSize(SIZE_C const &:%p)",&Size);
  DEBUGLOG_LogIn();

  m_Size=Size;

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::TOOLTIP_C::TOOLTIP_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::TOOLTIP_C()");
  DEBUGLOG_LogIn();

  m_pItemList.reset(new TOOLTIPITEMLIST_C);

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::TOOLTIP_C::~TOOLTIP_C(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::~TOOLTIP_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

bool PREFERENCES_C::MOONDATA_C::TOOLTIP_C::
    operator==(PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::"
      "operator==(PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*m_pItemList==*Preferences.m_pItemList);

  DEBUGLOG_LogOut();
  return(EqualFlag);
}

bool PREFERENCES_C::MOONDATA_C::TOOLTIP_C::
    operator!=(PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &Preferences) const
{
  bool EqualFlag;


  DEBUGLOG_Printf1("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::"
      "operator!=(PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &:%p) const",
      &Preferences);
  DEBUGLOG_LogIn();

  EqualFlag=(*this==Preferences);

  DEBUGLOG_LogOut();
  return(!EqualFlag);
}

void PREFERENCES_C::MOONDATA_C::TOOLTIP_C::Load(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::Load()");
  DEBUGLOG_LogIn();

  m_pItemList->Load();

  DEBUGLOG_LogOut();
  return;
}

void PREFERENCES_C::MOONDATA_C::TOOLTIP_C::Save(void)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::Save()");
  DEBUGLOG_LogIn();

  m_pItemList->Save();

  DEBUGLOG_LogOut();
  return;
}

PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &
    PREFERENCES_C::MOONDATA_C::TOOLTIP_C::operator=(
    PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &Preferences)
{
  DEBUGLOG_Printf0("PREFERENCES_C::MOONDATA_C::TOOLTIP_C::operator=("
      "PREFERENCES_C::MOONDATA_C::TOOLTIP_C const &)");
  DEBUGLOG_LogIn();

  if (this!=&Preferences)
  {
    TOOLTIPITEMLIST_C *pNewList;


    pNewList=new TOOLTIPITEMLIST_C(*Preferences.GetItemListPointer());
    delete m_pItemList.release();
    m_pItemList.reset(pNewList);
  }

  DEBUGLOG_LogOut();
  return(*this);
}

TOOLTIPITEMLIST_C *
  PREFERENCES_C::MOONDATA_C::TOOLTIP_C::GetItemListPointer(void)
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::TOOLTIP_C::GetItemListPointer()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(dynamic_cast<TOOLTIPITEMLIST_C*>(m_pItemList.get()));
}

TOOLTIPITEMLIST_C const *
  PREFERENCES_C::MOONDATA_C::TOOLTIP_C::GetItemListPointer(void) const
{
  DEBUGLOG_Printf0(
      "PREFERENCES_C::MOONDATA_C::TOOLTIP_C::GetItemListPointer() const");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(dynamic_cast<TOOLTIPITEMLIST_C const *>(m_pItemList.get()));
}


#undef    PREFERENCES_CPP


/**
*** preferences.cpp
**/

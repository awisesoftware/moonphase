/**
*** \file updatecheck.cpp
*** \brief updatecheck.h implementation.
*** \details Implementation file for updatecheck.h.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief updatecheck.cpp identifier.
*** \details Identifier for updatecheck.cpp.
*** \endinternal
**/
#define   UPDATECHECK_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "updatecheck.h"
#if       defined(DEBUG_UPDATECHECK_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_UPDATECHECK_CPP) */
#include  "debuglog.h"
#include  "messagelog.h"

#include  "update.h"

#include  <QThread>
#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

CHECKTHREAD_C::CHECKTHREAD_C(QObject *pParent) : QThread(pParent)
{
  DEBUGLOG_Printf1("CHECKTHREAD_C::CHECKTHREAD_C(%p)",pParent);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

CHECKTHREAD_C::~CHECKTHREAD_C(void)
{
  DEBUGLOG_Printf0("CHECKTHREAD_C::~CHECKTHREAD_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void CHECKTHREAD_C::run(void)
{
#if       defined(MOONPHASEQT_UPDATECHECKFLAG)
  std::string Version;


  DEBUGLOG_Printf0("CHECKTHREAD_C::run()");
  DEBUGLOG_LogIn();

  CheckForUpdate("https://gitlab.com/awisesoftware/binaries/raw/master/"
      "moonphase/current_release",Version);

  emit VersionSignal(QString::fromStdString(Version));

  DEBUGLOG_LogOut();
#endif    /* defined(MOONPHASEQT_UPDATECHECKFLAG) */
  return;
}

void UpdateCheck(QObject *pReceiver,char const *pFunction)
{
#if       !defined(MOONPHASEQT_UPDATECHECKFLAG)
  Q_UNUSED(pReceiver);
  Q_UNUSED(pFunction);
#else     /* !defined(MOONPHASEQT_UPDATECHECKFLAG) */
  CHECKTHREAD_C *pCheckThread;


  DEBUGLOG_Printf3("UpdateCheck(%p,%p(%s))",pReceiver,pFunction,pFunction);
  DEBUGLOG_LogIn();

  /* Create the thread. */
  pCheckThread=new CHECKTHREAD_C(pReceiver);

  /* Set up the callbacks. */
  QObject::connect(pCheckThread,SIGNAL(VersionSignal(QString)),
      pReceiver,pFunction);
  QObject::connect(pCheckThread,&CHECKTHREAD_C::finished,
      pCheckThread,&QObject::deleteLater);

  /* Start the thread. */
  pCheckThread->start();

  DEBUGLOG_LogOut();
#endif    /* !defined(MOONPHASEQT_UPDATECHECKFLAG) */
  return;
}


#undef    UPDATECHECK_CPP


/**
*** updatecheck.cpp
**/

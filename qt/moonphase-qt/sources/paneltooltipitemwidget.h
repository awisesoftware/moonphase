/**
*** \file paneltooltipitemwidget.h
*** \brief Information panel/tool tip item widget.
*** \details Allows the user to add/edit/remove items in the information panel
***   and/or tool tip.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELTOOLTIPITEMWIDGET_H)
/**
*** \internal
*** \brief paneltooltipitemwidget.h identifier.
*** \details Identifier for paneltooltipitemwidget.h.
*** \endinternal
**/
#define   PANELTOOLTIPITEMWIDGET_H


/****
*****
***** INCLUDES
*****
****/

#include  "preferences.h"

#include  <QWidget>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Information panel/tool tip item widget class.
*** \details Class which allows the user to add/edit/remove items in the
***   information panel and/or tool tip.
**/
class PANELTOOLTIPITEMWIDGET_C : public QWidget
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    PANELTOOLTIPITEMWIDGET_C(QWidget *pParent=nullptr);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELTOOLTIPITEMWIDGET_C(PANELTOOLTIPITEMWIDGET_C const &Widget)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELTOOLTIPITEMWIDGET_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELTOOLTIPITEMWIDGET_C const & operator=(
        PANELTOOLTIPITEMWIDGET_C const &Thread)=delete;

    /**
    *** \brief TODO
    *** \details TODO
    *** \param GMTFlag
    **/
    void SetDefaultToGMTFlag(bool GMTFlag);

    /**
    *** \brief TODO
    *** \details TODO
    *** \param MetricFlag
    **/
    void SetDefaultToMetricUnitsFlag(bool MetricFlag);

    /**
    *** \brief Set panel/tool tip preferences pointer.
    *** \details Sets the pointer to the panel/tool tip preferences.
    *** \param pPreferences Panel/tool tip preferences pointer.
    **/
    void SetPreferencesPointer(
        PREFERENCES_C::MOONDATA_C::PANELTOOLTIP_C *pPreferences);

  private slots:
    /**
    *** \brief "Add (+)" button clicked.
    *** \details The "Add (+)" button was clicked. Adds the selected item(s)
    ***   from the data list to the display list.
    **/
    void AddDataItemButtonClickedSlot(void);

    /**
    *** \brief Data item selected.
    *** \details An item in the data item list was selected.
    **/
    void DataItemSelectionChangedSlot(void);

    /**
    *** \brief Display item selected.
    *** \details An item in the display item list was selected.
    **/
    void DisplayItemSelectionChangedSlot(void);

    /**
    *** \brief Display "Preferences" button clicked.
    *** \details The display "Preferences" button was clicked. Displays the
    ***   information panel or tool tip preferences dialog box.
    **/
    void DisplayPreferencesButtonClickedSlot(void);

    /**
    *** \brief "Down (down arrow)" button clicked.
    *** \details The "Down (down arrow)" button was clicked. Moves a display
    ***   item down in the list.
    **/
    void MoveDownDisplayItemButtonClickedSlot(void);

    /**
    *** \brief "Up (up arrow)" button clicked.
    *** \details The "Up (up arrow)" button was clicked. Moves a display item
    ***   down in the list.
    **/
    void MoveUpDisplayItemButtonClickedSlot(void);

    /**
    *** \brief Options button was clicked.
    *** \details The options button was clicked.
    **/
    void ItemPreferencesButtonClickedSlot(void);

    /**
    *** \brief Display item removed from display.
    *** \details Removes an item from the display list.
    **/
    void RemoveDisplayItemButtonClickedSlot(void);

  signals:
    void PreferencesChangedSignal(void);

    void GetPanelDataSignal(QPoint &Position,QSize &Size,bool &VisibleFlag);

  protected:
    /**
    *** \brief Panel/tooltip item widget implementation class.
    *** \details The implementation class for the panel/tooltip item widget.
    **/
    class PANELTOOLTIPITEMWIDGETPRIVATE_C;

    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend class PANELTOOLTIPITEMWIDGETPRIVATE_C;

    /**
    *** \brief Implementation pointer (PIMPL).
    *** \details Pointer to implementation object (PIMPL).
    **/
    QScopedPointer<PANELTOOLTIPITEMWIDGETPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELTOOLTIPITEMWIDGET_H) */


/**
*** paneltooltipitemwidget.h
**/

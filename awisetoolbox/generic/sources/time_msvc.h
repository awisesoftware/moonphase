/**
*** \file time_msvc.h
*** \brief Microsoft omissions from "time.h".
*** \details Function prototypes that are not native to Microsoft SDKs,
***   but are needed by awisetoolbox or other programs.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       defined(_WIN32) || defined(_WIN64)

#if       !defined(TIME_MSVC_H)
/**
*** \brief time_msvc.h identifier.
*** \details Identifier for time_msvc.h.
*** \internal
**/
#define   TIME_MSVC_H


/****
*****
***** INCLUDES
*****
****/

#include  <stdarg.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if       defined(__cplusplus)
extern "C" {
#endif    /* defined(__cplusplus) */

time_t timegm(struct tm *pTimeTM);

#if       defined(__cplusplus)
}
#endif    /* defined(__cplusplus) */


#endif    /* !defined(TIME_MSVC_H) */

#endif    /* defined(_WIN32) || defined(_WIN64) */


/**
*** time_msvc.h
**/

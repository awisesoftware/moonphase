/**
*** \file test_module_commontypes.cpp
*** \brief 'commontypes.cpp' unit tests.
*** \details Unit tests for the functions in 'commontypes.cpp'.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/****
*****
***** INCLUDES
*****
****/

#include  <boost/test/unit_test.hpp>

#include  "commontypes.cpp"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/****
*****
***** TESTS
*****
****/

BOOST_AUTO_TEST_SUITE(Module__commontypes)
#include  "test_class_COLOR.cpp"
#include  "test_class_FONT.cpp"
#include  "test_class_POSITION.cpp"
#include  "test_class_SIZE.cpp"
BOOST_AUTO_TEST_SUITE_END()


/**
*** test_module_commontypes.cpp
**/

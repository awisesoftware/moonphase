/**
*** \file paneldisplaywidget.h
*** \brief Panel display widget.
*** \details A widget which displays the panel items.
**/

/*
** This file is part of moonphase.
** Copyright (C) 2014-2020 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PANELDISPLAYWIDGET_H)
/**
*** \internal
*** \brief paneldisplaywidget.h identifier.
*** \details Identifier for paneldisplaywidget.h.
*** \endinternal
**/
#define   PANELDISPLAYWIDGET_H


/****
*****
***** INCLUDES
*****
****/

#include  "moondata.h"

#include  <QWidget>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class PANELTOOLTIPITEM_C;
class PANELITEMLIST_C;

/**
*** \brief Moon data item display widget interface class.
*** \details Interface class for the moon data item display widget.
**/
class PANELDISPLAYWIDGET_C : public QWidget
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent widget.
    **/
    PANELDISPLAYWIDGET_C(QWidget *pParent=NULL);

    /**
    *** \brief Copy constructor.
    *** \details Copy constructor.
    *** \param pWidget Pointer to widget to copy.
    *** \note Function explicitly defined as deleted.
    **/
    PANELDISPLAYWIDGET_C(PANELDISPLAYWIDGET_C const &Dialog)=delete;

    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~PANELDISPLAYWIDGET_C(void);

    /**
    *** \brief = operator.
    *** \details Assignment operator.
    *** \param Widget Object to copy.
    *** \returns Copy of original widget.
    **/
    PANELDISPLAYWIDGET_C const & operator=(
        PANELDISPLAYWIDGET_C const &Thread)=delete;

    /**
    *** \brief Display an item.
    *** \details Displays a single item in the widget.
    *** \param pItem Pointer to item data.
    *** \param MoonData Moon data.
    **/
    void Update(PANELTOOLTIPITEM_C const *pItem,MOONDATA_T const &MoonData);

    /**
    *** \brief Display a list of items.
    *** \details Displays multiple items in the widget.
    *** \param pItemList Pointer to the list of item data.
    *** \param MoonData Moon data.
    **/
    void Update(PANELITEMLIST_C const *pItemList,MOONDATA_T const &MoonData);

  private:
    /**
    *** \brief Moon data item display widget implementation class.
    *** \details The implementation class for the moon data item display widget.
    **/
    class PANELDISPLAYWIDGETPRIVATE_C;
    /**
    *** \brief Private class access to public class.
    *** \details Allows the private class to access public class.
    **/
    friend class PANELDISPLAYWIDGETPRIVATE_C;
    /**
    *** \brief Implementation pointer.
    *** \details Pointer to implementation.
    **/
    QScopedPointer<PANELDISPLAYWIDGETPRIVATE_C> m_pPrivate;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* !defined(PANELDISPLAYWIDGET_H) */


/**
*** paneldisplaywidget.h
**/
